package com.kavsdkexample.ui.device_protection;

import java.io.File;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.kavsdk.antivirus.Antivirus;
import com.kavsdk.antivirus.AntivirusInstance;
import com.kavsdk.antivirus.QuarantineException;
import com.kavsdk.antivirus.QuarantineItemInfo;
import com.kavsdkexample.R;
import com.kavsdkexample.shared.AppStatus;
import com.kavsdkexample.shared.dialogs.BaseDialogFragment;
import com.kavsdkexample.shared.dialogs.DialogsSupplier;
import com.kavsdkexample.shared.dialogs.OnDialogFragmentResultListener;
import com.kavsdkexample.shared.dialogs.UiUpdater;
import com.kavsdkexample.shared.activities.DialogFragmentActivity;

/**
 * This is a sample activity for showing the quarantine items
 */
public class QuarantineActivity extends DialogFragmentActivity
                                implements UiUpdater, OnDialogFragmentResultListener {
    private static final String TAG = QuarantineActivity.class.getSimpleName();
    
    private static final int REQUEST_SELECT_FOLDER = 0;

    /* GUI controls */
    private TextView mQuantityTextView;
    private ListView mQuarantineItemsListView;
    private TextView mCustomPathTextView;
    
    /* This method always returns the same object */
    private Antivirus mAntivirusComponent;
    
    private String mCustomRestorePath;
    private Button mRestoreAllButton;
    private Button mRemoveAllButton;

    public static void showPromptSelectCustomPath(Context context) {
        Toast.makeText(context, R.string.quarantine_msg_no_access_to_recovery, Toast.LENGTH_LONG)
                .show();
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    
    /**
     *  This method updates GUI to reflect the changes in the quarantine 
     */
    @Override
    public void updateUi() {
        // Setting up an info message about the quarantine
        long count = 0;
        String text;
        
        try {
            count = mAntivirusComponent.getQuarantineObjectsCount();
            text = getString(R.string.str_quarantine_files_number) + count;
        } catch (QuarantineException e) {
            e.printStackTrace();
            text = getString(R.string.str_quarantine_error);
            Log.d(TAG, "Quarantine objects counting failed!");
        }
        
        mQuantityTextView.setText(text);
        // Updating the quarantine listView
        ((QuarantineItemsAdapter) mQuarantineItemsListView.getAdapter()).notifyDataSetChanged();
    }

    @Override
    public DialogFragment onCreateDialogFragment(int dialogId, Bundle bundle) {
        if (dialogId == DialogsSupplier.DIALOG_PROCESS_QUARANTUNE_ITEM) {
            return ProcessQuarantineItemDialog.newInstance(bundle)
                    .setOnListUpdatedListener(this);
        }
        return null;
    }
    
    protected void checkPromptSelectCustomDir() {
        if (TextUtils.isEmpty(mCustomRestorePath)) {
            showPromptSelectCustomPath(this);
        }
    }

    protected void setCustomPath() {
        File initDir = getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);
        if (initDir == null) {
            initDir = getFilesDir();
        }
        DirectoryChooserFragment.show(this, initDir, DirectoryChooserFragment.DIRECTORY_MODE, REQUEST_SELECT_FOLDER);
    }
    
    private void updateCustomPathView() {
        mCustomPathTextView.setText(mCustomRestorePath);
        mCustomPathTextView.setVisibility(TextUtils.isEmpty(mCustomRestorePath) ? View.GONE
                : View.VISIBLE);
    }
    
    @Override
    public void onDialogFragmentResult(BaseDialogFragment dialog, Object data) {
        if (dialog instanceof DirectoryChooserFragment) {
            mCustomRestorePath = ((File) data).getAbsolutePath();
            updateCustomPathView();
        }
    }

    @Override
    public void initUi(Bundle savedInstanceState) {
        setContentView(R.layout.quarantine_activity);
        mQuantityTextView = (TextView) findViewById(R.id.filesNumber);
        mCustomPathTextView =  (TextView) findViewById(R.id.customPath);
        mRemoveAllButton = (Button) findViewById(R.id.removeAll);
        mRestoreAllButton = (Button) findViewById(R.id.restoreAll);
        Button btn = (Button) findViewById(R.id.setCustomPath);
        btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                setCustomPath();
            }
        });
        mQuarantineItemsListView = (ListView) findViewById(R.id.quarantineItems);
    }

    @Override
    public void initComponent(AppStatus status, String additionalInfo) {
        mAntivirusComponent = AntivirusInstance.getInstance();

        mRemoveAllButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Log.d(TAG, "Quarantine clearing");
                mAntivirusComponent.clearQuarantine();
                Log.d(TAG, "Quarantine cleared!");
                updateUi();
            }
        });

        mRestoreAllButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Log.d(TAG, "Restoring all items from quarantine");
                    int count = (int)mAntivirusComponent.getQuarantineObjectsCount();
                    int err = 0;
                    if (count > 0) {
                        for (QuarantineItemInfo item : mAntivirusComponent.getQuarantineItems(0, count - 1)) {
                            try {
                                Log.d(TAG, String.format("Restoring '%s' from quarantine", item.mQuarantineName));
                                mAntivirusComponent.restoreFromQuarantine(item.mQuarantineName, mCustomRestorePath);
                                Log.d(TAG, String.format("Item '%s' restored from quarantine", item.mQuarantineName));
                            } catch(QuarantineException e) {
                                Log.d(TAG, String.format("Can't restore item '%s' from quarantine", item.mQuarantineName));
                                err++;
                            }
                        }
                        updateUi();
                        if (err > 0) {
                            checkPromptSelectCustomDir();
                        }
                    }
                    Log.d(TAG, "All items restored from quarantine");
                } catch (QuarantineException e) {
                    Log.e(TAG, "There is problem during restoring items from quarantine");
                    e.printStackTrace();
                }
            }
        });

        mQuarantineItemsListView.setAdapter(new QuarantineItemsAdapter(this, mAntivirusComponent));
        mQuarantineItemsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int count;
                try {
                    count = (int) mAntivirusComponent.getQuarantineObjectsCount();
                    if (count > 0) {
                        // Acquire all quarantine items and get a name of the item with a required position
                        String itemName = mAntivirusComponent.getQuarantineItems(0, count - 1).get(position).mQuarantineName;
                        Bundle bundle = new Bundle();
                        bundle.putString(ProcessQuarantineItemDialog.QUARANTINE_ITEM_NAME_ID, itemName);
                        bundle.putString(ProcessQuarantineItemDialog.QUARANTINE_CUSTOM_PATH_ID, mCustomRestorePath);
                        showFragmentDialog(DialogsSupplier.DIALOG_PROCESS_QUARANTUNE_ITEM, bundle);
                    }
                } catch (QuarantineException e) {
                    Log.e(TAG, "Quarantine error");
                    e.printStackTrace();
                }
            }
        });

        updateUi();
    }
}