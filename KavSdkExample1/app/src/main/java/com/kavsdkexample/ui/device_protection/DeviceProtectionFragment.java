package com.kavsdkexample.ui.device_protection;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.kavsdk.license.SdkLicenseViolationException;
import com.kavsdkexample.R;
import com.kavsdkexample.app.SdkSampleApplication;
import com.kavsdkexample.Utils;
import com.kavsdkexample.shared.AppStatus;
import com.kavsdkexample.shared.BaseFragment;
import com.kavsdkexample.shared.activities.DialogFragmentActivity;
import com.kavsdkexample.shared.dialogs.DialogsSupplier;
import com.kavsdkexample.storage.Settings;
import com.kavsdkexample.ui.SdkMainActivity;


/**
 * This is a fragment for managing antivirus parameters and handling antivirus events
 */
public class DeviceProtectionFragment extends BaseFragment implements OnClickListener {
    private boolean mIsSimWatchEnable;
    private TextView mSimWatchCommandCaptionView;

    @Override
    public View initUi(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.device_protection_fragment, container, false);

        Utils.initCommandView(view, R.id.rtp_monitor_button, R.string.rtp_monitor_caption, this);
        Utils.initCommandView(view, R.id.application_monitor_button, R.string.application_monitor_caption, this);
        Utils.initCommandView(view, R.id.folder_monitor_button, R.string.folder_monitor_caption, this);
        Utils.initCommandView(view, R.id.scanner_button, R.string.scanner_caption, this);
        Utils.initCommandView(view, R.id.app_cheker_button, R.string.str_open_app_checker, this);
        Utils.initCommandView(view, R.id.sim_watch_button, R.string.str_antivirus_enable_sim_watch, this);
        Utils.initCommandView(view, R.id.openQuarantine, R.string.str_antivirus_open_quarantine, this);
        Utils.initCommandView(view, R.id.threatDescriptionDialog, R.string.str_antivirus_threat_description_dialog, this);
        Utils.initCommandView(view, R.id.CloudCheckAvailable, R.string.str_antivirus_cloud_check_available, this);
        Utils.initCommandView(view, R.id.app_control_button, R.string.str_open_app_control, this);

        mSimWatchCommandCaptionView = (TextView) view.findViewById(R.id.sim_watch_button).findViewById(R.id.commandCaption);
        mIsSimWatchEnable = Settings.isSimWatchEnabled(getActivity(), false);
        updateSimWatchCommandCaption();

        return view;
    }

    @Override
    public void initComponent(AppStatus appStatus, String additionalInfo) {

    }

    private void performCloudAvailabilityCheck() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                final SdkMainActivity act = (SdkMainActivity) getActivity();
                final SdkSampleApplication app = SdkSampleApplication.from(act);
                final boolean result = app.getService().getAntivirusSample().isCloudAvailable();

                act.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String message = getString(R.string.str_antivirus_cloud_check_availability_result, result);
                        Toast.makeText(act, message, Toast.LENGTH_LONG).show();
                    }
                });
            }
        }).start();
    }

    @Override
    public void onClick(View v) {
        final Activity act = getActivity();
        switch (v.getId()) {
        case R.id.folder_monitor_button:
            startActivity(FolderMonitorActivity.getStartIntent(act));
            break;
        case R.id.scanner_button:
            startActivity(ScannerActivity.getStartIntent(act));
            break;
        case R.id.application_monitor_button:
            startActivity(AppMonitorActivity.getStartIntent(act));
            break;
        case R.id.rtp_monitor_button:
            startActivity(RtpMonitorActivity.getStartIntent(act));
            break;
        case R.id.app_cheker_button:
            startActivity(new Intent(act, CheckAppCategoryActivity.class));
            break;
        case R.id.openQuarantine:
            startActivity(new Intent(act, QuarantineActivity.class));
            break;
        case R.id.CloudCheckAvailable:
            performCloudAvailabilityCheck();
            break;
        case R.id.threatDescriptionDialog:
            ((DialogFragmentActivity) act).showFragmentDialog(DialogsSupplier.DIALOG_ANTIVIRUS_THREAT_DESCRIPTION);
            break;
        case R.id.sim_watch_button:
            mIsSimWatchEnable = !mIsSimWatchEnable;
            try {
                SdkSampleApplication.from(act).getService().setSimWatchEnabled(mIsSimWatchEnable);
                updateSimWatchCommandCaption();
            } catch (SdkLicenseViolationException e) {
                e.printStackTrace();
                Toast.makeText(getActivity(), getString(R.string.str_license_expired_error), Toast.LENGTH_LONG).show();
            }
            break;
        case R.id.app_control_button:
            startActivity(new Intent(act, AppControlActivity.class));
            break;
        default:
            break;
        }
    }

    void updateSimWatchCommandCaption() {
        mSimWatchCommandCaptionView
                .setText(mIsSimWatchEnable ? R.string.str_antivirus_disable_sim_watch
                        : R.string.str_antivirus_enable_sim_watch);
    }
    
}
