package com.kavsdkexample.ui.device_protection;

import java.util.concurrent.TimeUnit;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.kavsdkexample.R;
import com.kavsdkexample.samples.AntivirusSample.ScanResults;

public class ScanResultsDialog extends DialogFragment {
    
    private ScanResults mScanResults;
    private long mScanDuration;
    
    public void setResults(ScanResults results, long startTime) {
        mScanDuration = results.mFinishTime - startTime;
        mScanResults = results;
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        
        getDialog().setTitle(R.string.str_antivirus_scan_results_title);
        
        View view = inflater.inflate(R.layout.scan_result_dialog, container, false);
        
        TextView timeText = (TextView) view.findViewById(R.id.timeResultCaption);
        TextView filesFoundText = (TextView) view.findViewById(R.id.totalFilesCaption);
        TextView filesCheckedText = (TextView) view.findViewById(R.id.filesCheckedCaption);
        TextView filesSkippedText = (TextView) view.findViewById(R.id.filesSkippedCaption);
        TextView virusesDetectedText = (TextView) view.findViewById(R.id.virusCaption);
        TextView filesCuredText = (TextView) view.findViewById(R.id.filesCuredCaption);
        TextView filesDeletedText = (TextView) view.findViewById(R.id.filesDeletedCaption);
        TextView filesQuarantineedText = (TextView) view.findViewById(R.id.filesQuarantineedCaption);
        TextView filesSuspiciousText = (TextView) view.findViewById(R.id.filesSuspiciousCaption);
        
        timeText.setText(DateUtils.formatElapsedTime(TimeUnit.MILLISECONDS.toSeconds(mScanDuration)));
        if (mScanResults != null) {
        filesFoundText.setText(String.valueOf(mScanResults.mFilesCount));
        filesCheckedText.setText(String.valueOf(mScanResults.mCheckedObjects));
        filesSkippedText.setText(String.valueOf(mScanResults.mSkippedObjects));
        virusesDetectedText.setText(String.valueOf(mScanResults.mVirusesDetected));
        filesCuredText.setText(String.valueOf(mScanResults.mCuredObjects));
        filesDeletedText.setText(String.valueOf(mScanResults.mFilesDeleted));
        filesQuarantineedText.setText(String.valueOf(mScanResults.mFilesQuarantined));
        filesSuspiciousText.setText(String.valueOf(mScanResults.mSuspiciousCount));
        }
        
        Button okButton = (Button) view.findViewById(R.id.checkButton);
        
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();              
            }
        });
        setRetainInstance(true);
        return view;
    }
    
    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance()) {
            getDialog().setOnDismissListener(null);
        }
        super.onDestroyView();
    }
}
