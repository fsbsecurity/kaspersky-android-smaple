package com.kavsdkexample.ui.device_protection;

import java.util.List;

import android.app.Dialog;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.kavsdkexample.R;
import com.kavsdkexample.app.SdkSampleApplication;
import com.kavsdkexample.shared.dialogs.BaseDialogFragment;

public class ApplicationChooserFragment extends BaseDialogFragment {

    public static final int APPLICATION_MODE = 0;   

    private ListView mListView;
    private TextView mHintEmptySubfolders;

    public static void show(FragmentActivity activity) {
        Bundle args = new Bundle();
        show(activity, ApplicationChooserFragment.class, args, APPLICATION_MODE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View root = inflater.inflate(R.layout.fragment_application_chooser_dialog, container, false);
        initControls(root);
        return root;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    private void initControls(View root) {
        mListView = (ListView) root.findViewById(android.R.id.list);
        mHintEmptySubfolders = (TextView) root.findViewById(android.R.id.hint);

        mListView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ApplicationInfo app = (ApplicationInfo)mListView.getAdapter().getItem(position);
                setDialogResult(app.packageName);
                dismissAllowingStateLoss();
            }
        });

        refreshAdapter();

        View v = root.findViewById(android.R.id.button1);
        v.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                onCancelClick();
            }
        });
        v = root.findViewById(android.R.id.button2);
        v.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                onOkClick();
            }
        });
    }

    protected void onOkClick() {
        ApplicationInfo app = (ApplicationInfo)mListView.getSelectedItem();
        if (app != null) {
            setDialogResult(app.packageName);
        }
        dismissAllowingStateLoss();
    }

    protected void onCancelClick() {
        dismissAllowingStateLoss();
    }

    private void refreshAdapter() {
        final SdkSampleApplication app = SdkSampleApplication.from(getActivity());
        List<ApplicationInfo> apps = app.getService().getAntivirusSample().getApplicationsForScan();
        ApplicationChooserAdapter adapter = new ApplicationChooserAdapter(getActivity(), apps);
        mListView.setAdapter(adapter);
        mHintEmptySubfolders.setVisibility(adapter.getCount() > 0 ? View.GONE : View.VISIBLE);
    }

    private static class ApplicationChooserAdapter extends BaseAdapter {

        private final Context mContext;
        private final List<ApplicationInfo> mApps;

        ApplicationChooserAdapter(Context context, List<ApplicationInfo> apps) {
            mContext = context;
            mApps = apps;
        }

        @Override
        public int getCount() {
            return mApps.size();
        }

        @Override
        public Object getItem(int position) {
            return mApps.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ApplicationInfo app = mApps.get(position);

            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(android.R.layout.simple_list_item_2, parent, false);
            }

            PackageManager pm = mContext.getPackageManager();
            CharSequence name = pm.getApplicationLabel(app);

            ((TextView) convertView.findViewById(android.R.id.text1)).setText(name);
            ((TextView) convertView.findViewById(android.R.id.text2)).setText(app.packageName);

            return convertView;     }

    }
}
