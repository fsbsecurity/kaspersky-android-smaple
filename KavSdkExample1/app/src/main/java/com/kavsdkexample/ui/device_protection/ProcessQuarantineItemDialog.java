package com.kavsdkexample.ui.device_protection;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.kavsdk.antivirus.Antivirus;
import com.kavsdk.antivirus.AntivirusInstance;
import com.kavsdkexample.shared.dialogs.DialogsSupplier;
import com.kavsdkexample.shared.dialogs.UiUpdater;

/**
 * This is a dialog for removing quarantine items
 */
public class ProcessQuarantineItemDialog extends DialogFragment {
    public static final String QUARANTINE_ITEM_NAME_ID = "item_name";
    public static final String QUARANTINE_CUSTOM_PATH_ID = "custom_path";
    
    private UiUpdater mUiUpdater;
    
    public static ProcessQuarantineItemDialog newInstance(Bundle bundle) {
        ProcessQuarantineItemDialog dialog = new ProcessQuarantineItemDialog();
        
        // Supply the information about a quarantine item as an argument.
        dialog.setArguments(bundle);
        
        return dialog;
    }
    
    public ProcessQuarantineItemDialog setOnListUpdatedListener(UiUpdater updater) {
        // A notifier used for GUI updated after the exclusions list changes
        mUiUpdater = updater;
        return this;
    }
    
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Activity act = getActivity();
        final String itemName = getArguments().getString(QUARANTINE_ITEM_NAME_ID);
        final String customPath = getArguments().getString(QUARANTINE_CUSTOM_PATH_ID);
        final Antivirus antivirus = AntivirusInstance.getInstance();
        return DialogsSupplier.getProcessQuarantineItemDialog(act, antivirus, itemName, customPath, mUiUpdater);
    }
}
