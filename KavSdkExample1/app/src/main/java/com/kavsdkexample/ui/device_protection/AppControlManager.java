package com.kavsdkexample.ui.device_protection;

import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;

import com.kavsdk.appcontrol.AppControl;
import com.kavsdk.appcontrol.AppControlAction;
import com.kavsdk.appcontrol.AppControlFactory;
import com.kavsdk.appcontrol.AppControlList;
import com.kavsdk.appcontrol.AppControlListener;
import com.kavsdk.appcontrol.AppControlMode;
import com.kavsdk.license.SdkLicenseViolationException;
import com.kavsdk.shared.iface.ServiceStateStorage;
import com.kavsdkexample.R;
import com.kavsdkexample.Utils;

import java.io.IOException;

public class AppControlManager implements AppControlListener {

    private static final String TAG = AppControlManager.class.getSimpleName();
    private AppControl mAppControl;
    private final Context mContext;
    private final Handler mHandler = new Handler(Looper.getMainLooper());
    
    public AppControlManager(ServiceStateStorage storage, Context context) {
        mContext = context;
        try {
            mAppControl = AppControlFactory.getInstance(storage, context);
            mAppControl.setBlockingIntent(getBlockingIntent());
        } catch (SdkLicenseViolationException e) {
            e.printStackTrace();
        }
    }
    
    public void enableAppControl(boolean enable) {
        if (enable) {
            mAppControl.start();
        } else {
            mAppControl.stop();
        }
    }
    
    public void enableWindowManager(boolean enable) {
        if (enable) {
            mAppControl.setListener(this);
        } else {
            mAppControl.setListener(null);
        }
    }
    
    public AppControlList getBlackList() {
        return mAppControl.getBlackList();
    }
    
    public AppControlList getWhiteList() {
        return mAppControl.getWhiteList();
    }
    
    public void setMode(AppControlMode mode) {
        mAppControl.setMode(mode);
    }
    
    public AppControlMode getMode() {
        return mAppControl.getMode();
    }
    
    public void saveChanges() {
        try {
            mAppControl.saveChanges();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private Intent getBlockingIntent() {
        Intent blockingIntent = new Intent(mContext, AppBlockActivity.class);
        blockingIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        blockingIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        blockingIntent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        blockingIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        return blockingIntent;
    }

    @Override
    public AppControlAction onAppOpened(String prevAppPkg, String currAppPkg) {     
        return AppControlAction.Default;
    }

    @Override
    public AppControlAction onBlock(String prevAppPkg, String currAppPkg) {
        
        mHandler.post(new Runnable() {
            
            @Override
            public void run() {

                final WindowManager windowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
                final LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View infoView = inflater.inflate(R.layout.block_app_screen, null);
                final LayoutParams layoutParams = new WindowManager.LayoutParams(
                        WindowManager.LayoutParams.MATCH_PARENT, 
                        WindowManager.LayoutParams.MATCH_PARENT, 
                        WindowManager.LayoutParams.TYPE_SYSTEM_ALERT, 
                        WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                            | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH 
                            | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, PixelFormat.TRANSLUCENT);
                
                final Button button = (Button) infoView.findViewById(R.id.block_app_button);
                
                button.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Utils.launchHomeScreen(mContext);
                        windowManager.removeView(infoView);                     
                    }
                });
                
                windowManager.addView(infoView, layoutParams);              
            }
        });

        Log.i(TAG, String.format("Top package was blocked (at the Example side)"));
        //This is because we have already performed the blocking and there is no need to do it through an Intent
        return AppControlAction.Allow;
    }

    @Override
    public AppControlAction onAbsentInBothLists(String prevAppPkg, String currAppPkg) {
        return AppControlAction.Default;
    }       
}
