package com.kavsdkexample.ui.device_protection;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.kavsdk.license.SdkLicenseViolationException;
import com.kavsdk.rootdetector.RootDetector;
import com.kavsdkexample.R;
import com.kavsdkexample.shared.dialogs.BaseSimpleDialog;

public class RootCheckDialog extends BaseSimpleDialog {

    @Override
    protected String check() {
        try {
            boolean result = RootDetector.getInstance().checkRoot();
            int resultStrId = result ? R.string.str_root_check_dialog_rooted : R.string.str_root_check_dialog_not_rooted;
            return mContext.getString(resultStrId);
            
        } catch (SdkLicenseViolationException e) {
            return mContext.getString(R.string.str_lic_expired);
        }
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View result = super.onCreateView(inflater, container, savedInstanceState);
        mTextView.setVisibility(View.GONE);
        return result;
    }

    @Override
    protected int getHintTextId() {
        return View.NO_ID;
    }

    @Override
    protected int getTitleId() {
        return R.string.str_root_check_dialog_title;
    }

    @Override
    protected ArrayAdapter<?> getAdapter() {
        return null;
    }
}
