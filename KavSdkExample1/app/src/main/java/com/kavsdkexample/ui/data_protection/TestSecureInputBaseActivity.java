package com.kavsdkexample.ui.data_protection;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.kavsdk.license.SdkLicenseViolationException;
import com.kavsdk.secureinput.widget.KeyboardLayout;
import com.kavsdk.secureinput.widget.SafeEditText;
import com.kavsdk.secureinput.widget.SecureInputMethodSettings;
import com.kavsdkexample.R;
import com.kavsdkexample.shared.AppStatus;
import com.kavsdkexample.shared.activities.BaseActivity;

public abstract class TestSecureInputBaseActivity extends BaseActivity implements OnCheckedChangeListener {
    
    private static final int KEYBOARD_KEY_TEXT_SP = 20;
    private static boolean sErrorInit;
    
    private SafeEditText mSafeEditText1;
    private SafeEditText mSafeEditText2;
    private TextView mEditText;
    private TextView mPasswordEditText;
    
    private Spinner mSpinnerHorizontalGravityOption;
    private Spinner mSpinnerVerticalGravityOption;
    private GravityAdapter mHorizontalGravityAdapter;
    private GravityAdapter mVerticalGravityAdapter;
    
    static {
        try {
            SecureInputMethodSettings.addKeyboardLayout(new KeyboardLayout(R.xml.qwerty, R.xml.qwerty_shift));
            SecureInputMethodSettings.addKeyboardLayout(new KeyboardLayout(R.xml.symbols, R.xml.symbols_shift));
        } catch (SdkLicenseViolationException e) {
            e.printStackTrace();
            sErrorInit = true;
        }
    }

    @Override
    public void initUi(Bundle savedInstanceState) {
        if (!sErrorInit) {
            setContentView(R.layout.test_secure_input_activity);

            TextView label = (TextView) findViewById(R.id.label);
            label.setText(getLabelStrId());

            mSafeEditText1 = (SafeEditText) findViewById(R.id.safeEditText1);
            mSafeEditText2 = (SafeEditText) findViewById(R.id.safeEditText2);
            mEditText = (TextView) findViewById(R.id.editText);
            mPasswordEditText = (TextView) findViewById(R.id.passwordEditText);

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
                mSafeEditText2.setVisibility(View.GONE);
            }

            CheckBox showText = (CheckBox) findViewById(R.id.showText);
            showText.setChecked(mSafeEditText1.isShowText());
            showText.setOnCheckedChangeListener(this);

            CheckBox showCursor = (CheckBox) findViewById(R.id.showCursor);
            showCursor.setChecked(mSafeEditText1.isCursorVisible());
            showCursor.setOnCheckedChangeListener(this);

            CheckBox shuffleKeys = (CheckBox) findViewById(R.id.checkBoxShuffleKeys);
            shuffleKeys.setChecked(SecureInputMethodSettings.isShuffleKeysEnabled());
            shuffleKeys.setOnCheckedChangeListener(this);

            OnItemSelectedListener listener = new OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    int gravity = mHorizontalGravityAdapter.getItem(mSpinnerHorizontalGravityOption.getSelectedItemPosition()).getValue() | mVerticalGravityAdapter.getItem(mSpinnerVerticalGravityOption.getSelectedItemPosition()).getValue();
                    mSafeEditText1.setGravity(gravity);
                    mSafeEditText2.setGravity(gravity);
                    mEditText.setGravity(gravity);
                    mPasswordEditText.setGravity(gravity);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            };

            mHorizontalGravityAdapter = new GravityAdapter(this, false);
            mSpinnerHorizontalGravityOption = (Spinner) findViewById(R.id.spinHorizontalGravity);
            mSpinnerHorizontalGravityOption.setAdapter(mHorizontalGravityAdapter);
            mSpinnerHorizontalGravityOption.setOnItemSelectedListener(listener);

            mVerticalGravityAdapter = new GravityAdapter(this, true);
            mSpinnerVerticalGravityOption = (Spinner) findViewById(R.id.spinVerticalGravity);
            mSpinnerVerticalGravityOption.setAdapter(mVerticalGravityAdapter);
            mSpinnerVerticalGravityOption.setOnItemSelectedListener(listener);
        } else {
            Toast.makeText(this, R.string.str_error_license_absent_or_expired, Toast.LENGTH_SHORT)
                    .show();
            finish();
        }
    }

    @Override
    public void initComponent(AppStatus appStatus, String additionalInfo) {
        if (!sErrorInit) {
            int textSizePx = (int) (getResources().getDisplayMetrics().scaledDensity * KEYBOARD_KEY_TEXT_SP);
            SecureInputMethodSettings.setKeyTextSize(textSizePx);
            SecureInputMethodSettings.setLabelSize(textSizePx);
        } else {
            Toast.makeText(this, R.string.str_error_license_absent_or_expired, Toast.LENGTH_SHORT)
                    .show();
            finish();
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        final int id = buttonView.getId();
        switch (id) {
        case R.id.showText:
            mSafeEditText1.setShowText(isChecked);
            mSafeEditText2.setShowText(isChecked);
            break;
        case R.id.showCursor:
            mSafeEditText1.setCursorVisible(isChecked);
            mSafeEditText2.setCursorVisible(isChecked);
            mEditText.setCursorVisible(isChecked);
            mPasswordEditText.setCursorVisible(isChecked);
            break;
        case R.id.checkBoxShuffleKeys:
            SecureInputMethodSettings.setShuffleKeys(isChecked);
            mSafeEditText1.dismissSecureKeyboard();
            mSafeEditText2.dismissSecureKeyboard();
            break;
        default:
            throw new IllegalArgumentException("Unknown identifier.");
        }

    }
    
    protected abstract int getLabelStrId();
    
    /************************************* Inner classes *****************************************/
    public static class ConfigChangesActivity extends TestSecureInputBaseActivity {
        private static final String TAG = ConfigChangesActivity.class.getSimpleName();

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            Log.d(TAG, "ConfigChangesActivity::onCreate()");
            super.onCreate(savedInstanceState);
        }
        
        @Override
        protected int getLabelStrId() {
            return R.string.str_secure_input_config_changes_activity_label;
        }
    }
    
    public static class RegularActivity extends TestSecureInputBaseActivity {
        private static final String TAG = ConfigChangesActivity.class.getSimpleName();

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            Log.d(TAG, "RegularActivity::onCreate()");
            super.onCreate(savedInstanceState);
        }
        
        @Override
        protected int getLabelStrId() {
            return R.string.str_secure_input_regular_activity_label;
        }
    }

    private enum GravityOption {
        Left(Gravity.LEFT, R.string.str_secure_input_gravity_left), 
        CenterHorizontal(Gravity.CENTER_HORIZONTAL, R.string.str_secure_input_gravity_center), 
        Right(Gravity.RIGHT, R.string.str_secure_input_gravity_right),
        Top(Gravity.TOP, R.string.str_secure_input_gravity_top), 
        CenterVertical(Gravity.CENTER_VERTICAL, R.string.str_secure_input_gravity_center), 
        Bottom(Gravity.BOTTOM, R.string.str_secure_input_gravity_bottom);
        
        public static final GravityOption[] HORIZONTAL_OPTIONS = {Left, CenterHorizontal, Right};
        public static final GravityOption[] VERTICAL_OPTIONS = {Top, CenterVertical, Bottom};
        
        private final int mValue;
        private final int mResIdTitle;
        
        GravityOption(int value, int resIdTitle) {
            mValue = value;
            mResIdTitle = resIdTitle;
        }

        public int getValue() {
            return mValue;
        }

        public int getTitleResId() {
            return mResIdTitle;
        }
    }
    
    private static class GravityAdapter extends BaseAdapter {
        private final boolean mVerticalGravity;
        private final LayoutInflater mLi;

        GravityAdapter(Context context, boolean verticalGravity) {
            mLi = LayoutInflater.from(context);
            mVerticalGravity = verticalGravity;
        }

        private GravityOption[] values() {
            return mVerticalGravity ? 
                    GravityOption.VERTICAL_OPTIONS : 
                    GravityOption.HORIZONTAL_OPTIONS;
        }
        
        @Override
        public int getCount() {
            return values().length;
        }

        @Override
        public GravityOption getItem(int position) {
            return values()[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getView(android.R.layout.simple_spinner_item, position, convertView, parent);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getView(android.R.layout.simple_spinner_dropdown_item, position, convertView,
                    parent);
        }

        private View getView(int resLayout, int position, View convertView, ViewGroup parent) {

            View v = convertView;
            ViewHolder holder = null;
            if (v == null) {
                v = mLi.inflate(resLayout, parent, false);
                holder = new ViewHolder(v);
                v.setTag(holder);
            } else {
                holder = (ViewHolder) v.getTag();
            }

            GravityOption item = getItem(position);
            holder.mText.setText(item.getTitleResId());
            return v;
        }

        private static class ViewHolder {
            final TextView mText;

            ViewHolder(View root) {
                mText = (TextView) root.findViewById(android.R.id.text1);
            }
        }

    }
}
