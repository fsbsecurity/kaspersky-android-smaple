package com.kavsdkexample.ui.device_protection;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.os.FileObserver;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.kavsdkexample.R;
import com.kavsdkexample.shared.dialogs.BaseDialogFragment;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

/**
 * Fragment provides functionality for selecting the directory
 */
public class DirectoryChooserFragment extends BaseDialogFragment {
    private static final String TAG = DirectoryChooserFragment.class.getSimpleName();
    private static final boolean DEBUG = true;
    private static final String EXTRA_INIT_DIR = "init_dir";
    private static final String EXTRA_MODE = "mode";
    
    public static final int FILE_MODE = 0;
    public static final int DIRECTORY_MODE = 1; 

    private File mCurrentDir;
    private TextView mSelectedDir;
    private ListView mListView;
    private TextView mHintEmptySubfolders;
    private int mMode;

    public static void show(FragmentActivity activity, File initDir, int mode, int requestId) {
        Bundle args = new Bundle();
        args.putString(EXTRA_INIT_DIR, initDir.getAbsolutePath());
        args.putInt(EXTRA_MODE, mode);
        show(activity, DirectoryChooserFragment.class, args, requestId);
    }

    private static File extractInitDir(Bundle args) {
        String path = Environment.getExternalStorageDirectory().getAbsolutePath();
        if (args != null) {
            path = args.getString(EXTRA_INIT_DIR);
        }
        return new File(path);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCurrentDir = extractInitDir(getArguments());
        mMode = getArguments() == null ? DIRECTORY_MODE : getArguments().getInt(EXTRA_MODE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View root = inflater.inflate(R.layout.fragment_directory_chooser_dialog, container, false);
        initControls(root);
        return root;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    private void initControls(View root) {

        mListView = (ListView) root.findViewById(android.R.id.list);
        mListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DirectoryChooserAdapter adapter = (DirectoryChooserAdapter) mListView.getAdapter();
                File file = adapter.getItem(position);
                if (mMode == DIRECTORY_MODE || mMode == FILE_MODE && file.isDirectory()) {
                    refreshAdapter(file);
                } else {
                    if(DEBUG){
                        Log.d(TAG, "Selected direcrory: " + mCurrentDir.getAbsolutePath());
                    }
                    setDialogResult(file);
                    dismissAllowingStateLoss();
                }
            }
        });

        mSelectedDir = (TextView) root.findViewById(android.R.id.text1);
        mHintEmptySubfolders = (TextView) root.findViewById(android.R.id.hint);

        View v = root.findViewById(android.R.id.icon);
        v.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                onUpDirectoryClick();
            }
        });
        v = root.findViewById(android.R.id.button1);
        v.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                onCancelClick();
            }
        });
        v = root.findViewById(android.R.id.button2);
        v.setEnabled(mMode == DIRECTORY_MODE);
        v.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                onOkClick();
            }
        });

        refreshAdapter(mCurrentDir);
    }

    protected void onUpDirectoryClick() {
        DirectoryChooserAdapter adapter = (DirectoryChooserAdapter) mListView.getAdapter();
        File fDir = adapter.getSelectedDir();
        if (fDir.getParentFile() != null) {
            refreshAdapter(fDir.getParentFile());
        }
    }

    protected void onOkClick() {
        if(DEBUG){
            Log.d(TAG, "Selected direcrory: " + mCurrentDir.getAbsolutePath());
        }
        setDialogResult(mCurrentDir);
        dismissAllowingStateLoss();
    }

    protected void onCancelClick() {
        dismissAllowingStateLoss();
    }

    private void refreshAdapter(File dir) {
        DirectoryChooserAdapter adapter = new DirectoryChooserAdapter(getActivity(), dir, mMode);
        mListView.setAdapter(adapter);
        mSelectedDir.setText(dir.getAbsolutePath());
        mHintEmptySubfolders.setVisibility(adapter.getCount() > 0 ? View.GONE : View.VISIBLE);
        mCurrentDir = dir;
    }

    /************************************* Inner classes *****************************************/
    private static class DirectoryChooserAdapter extends BaseAdapter {
        private static final String TAG = DirectoryChooserFragment.DirectoryChooserAdapter.class
                .getSimpleName();

        private final LayoutInflater mLi;
        private final Handler mHandler;
        private final int mMode;

        private List<File> mFiles;
        private File mSelectedDir;
        private FileObserver mFileObserver;

        public DirectoryChooserAdapter(Context context, File parent, int mode) {
            mLi = LayoutInflater.from(context);
            mHandler = new Handler();
            mMode = mode;
            changeDirectory(parent);
        }

        @Override
        protected void finalize() throws Throwable {
            try {
                if (mFileObserver != null) {
                    mFileObserver.stopWatching();
                }
            } finally {
                super.finalize();
            }
        }

        public void changeDirectory(File parent) {
            mFiles = new ArrayList<File>();
            File[] contents = parent.listFiles();
            if (contents != null) {
                for (File f : contents) {
                    if (mMode == FILE_MODE || mMode == DIRECTORY_MODE && f.isDirectory()) {
                        mFiles.add(f);
                    }
                }
            }
            Collections.sort(mFiles, new Comparator<File>() {
                //@Override
                public int compare(File lhs, File rhs) {
                    return !lhs.isDirectory() && rhs.isDirectory() ? 1 : 
                        (lhs.isDirectory() && !rhs.isDirectory() ? -1 :
                                lhs.getName().toLowerCase(Locale.getDefault()).
                                        compareTo(rhs.getName().toLowerCase(Locale.getDefault())));
                }
            });
            
            mSelectedDir = parent;
            
            //stop previous observer
            if (mFileObserver != null) {
                mFileObserver.stopWatching();
            }
            
            mFileObserver = createFileObserver(parent.getAbsolutePath());
            mFileObserver.startWatching();
        }

        public File getSelectedDir() {
            return mSelectedDir;
        }

        /**
         * Sets up a FileObserver to watch the current directory.
         */
        private FileObserver createFileObserver(String path) {
            return new FileObserver(path, FileObserver.CREATE | FileObserver.DELETE
                    | FileObserver.MOVED_FROM | FileObserver.MOVED_TO) {

                @Override
                public void onEvent(int event, String path) {
                    if (!TextUtils.isEmpty(path)) {
                        if(DEBUG){
                            Log.d(TAG, "FileObserver received event " + event + " , path = "+ path);
                        }
                        mHandler.post(new Runnable() {

                            @Override
                            public void run() {
                                changeDirectory(mSelectedDir);
                                notifyDataSetChanged();
                            }
                        });
                    }
                }
            };
        }

        @Override
        public int getCount() {
            return mFiles.size();
        }

        //@Override
        public File getItem(int position) {
            return mFiles.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            ViewHolder holder = null;
            if (v == null) {
                v = mLi.inflate(android.R.layout.simple_list_item_1, parent, false);
                holder = new ViewHolder(v);
                v.setTag(holder);
            } else {
                holder = (ViewHolder) v.getTag();
            }

            File file = getItem(position);
            holder.mTextView.setText(file.getName());
            holder.mTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                    file.isDirectory() ? holder.initTextSize : holder.initTextSize*3/4);
            return v;
        }

        private static class ViewHolder {
            TextView mTextView;
            public final float initTextSize;

            ViewHolder(View root) {
                mTextView = (TextView) root.findViewById(android.R.id.text1);
                initTextSize = mTextView.getTextSize();
            }
        }
    }
}
