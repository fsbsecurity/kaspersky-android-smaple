package com.kavsdkexample.ui.network_protection;

import android.app.Activity;
import android.widget.ArrayAdapter;

import com.kavsdk.certificatechecker.CertificateCheckResult;
import com.kavsdk.certificatechecker.CertificateCheckService;
import com.kavsdkexample.R;
import com.kavsdkexample.shared.dialogs.BaseSimpleDialog;

/**
 * This dialog checks whether a certificate of an entered website is valid
 * and shows the result in a toast.
 */
public class CheckCertificateDialog extends BaseSimpleDialog {
    
    private String mResultStr;
    private String mNullResultStr;
    private String mErrorStr;
    
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        
        mResultStr     = getString(R.string.str_check_certificate_dialog_result);
        mNullResultStr = getString(R.string.str_check_certificate_dialog_result_null);
        mErrorStr      = getString(R.string.str_check_certificate_dialog_result_error);
    }
    
    /* 
     * This method checks the URL entered into the edit box.
     * In order to verify a certificate, SDK asks a KSN server via a network. 
     * Therefore, this method has to be invoked from a non-GUI thread
     */
    @Override
    protected String check() {
        try {
            final CertificateCheckService service = new CertificateCheckService();
            final String certificateUrl = mTextView.getText().toString();
            
            CertificateCheckResult result = service.checkCertificate(certificateUrl);
            
            if (result != null) {
                return mResultStr + result.toString();
            } else {
                return mNullResultStr;
            }
            
        } catch (Exception e) {
            return mErrorStr + e.getClass().getSimpleName() + " " + e.getMessage();
        }
    }

    @Override
    protected int getHintTextId() {
        return R.string.str_check_certificate_dialog_enter_url;
    }

    @Override
    protected ArrayAdapter<?> getAdapter() {
        return null;
    }

    @Override
    protected int getTitleId() {
        return R.string.str_dsc_check_certificate_url;
    }

    @Override
    protected int getTextId() {
        return R.string.str_check_certificate_default_url;
    }
}
