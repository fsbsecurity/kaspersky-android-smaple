package com.kavsdkexample.ui;


import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import com.kavsdk.taskreputation.OverlapActivityListener;
import com.kavsdk.taskreputation.TaskReputationManager;
import com.kavsdk.taskreputation.Verdict;
import com.kavsdk.webfilter.WebFilterControl;
import com.kavsdkexample.R;
import com.kavsdkexample.app.SdkSampleApplication;
import com.kavsdkexample.shared.AppStatus;
import com.kavsdkexample.shared.activities.DialogFragmentActivity;
import com.kavsdkexample.shared.dialogs.AlertDialogFragment;
import com.kavsdkexample.shared.dialogs.BaseDialogFragment;
import com.kavsdkexample.shared.dialogs.DialogsSupplier;
import com.kavsdkexample.shared.dialogs.OnDialogFragmentResultListener;
import com.kavsdkexample.ui.data_protection.DataProtectionFragment;
import com.kavsdkexample.ui.data_protection.SecureSmsAddPhoneDialog;
import com.kavsdkexample.ui.data_protection.SecureSmsAddPhoneDialog.AddItemObserver;
import com.kavsdkexample.ui.device_protection.AntivirusCheckUrlDialog;
import com.kavsdkexample.ui.device_protection.AntivirusThreatInfoDialog;
import com.kavsdkexample.ui.device_protection.DeviceProtectionFragment;
import com.kavsdkexample.ui.license_info.LicenseInfoFragment;
import com.kavsdkexample.ui.network_protection.WebAndNetworkProtectionFragment;
import com.kavsdkexample.ui.network_protection.WebFilterActivity;
import com.kavsdkexample.ui.plugins.PluginFragment;
import com.kavsdkexample.ui.risk_detection.RiskDetectionFragment;
import com.kavsdkexample.ui.self_defense.EnterClientUserIdDialog;
import com.kavsdkexample.ui.self_defense.SelfDefenseFragment;
import com.kavsdkexample.ui.update.UpdateFragment;

import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * This is the main activity of KAV SDK sample application. The activity contains a tab host with
 * fragments. Each fragment shows the usage of the functionality of KAV SDK associated with its area
 */
public class SdkMainActivity extends DialogFragmentActivity implements OverlapActivityListener, OnDialogFragmentResultListener {
    private static final String TAG = SdkMainActivity.class.getSimpleName();
    
    /*
     * Main tabs of SDK functional options
     */
    private static final List<TabInfo> TABS;
    private static final List<TabInfo> BAD_LICENSE_TABS;

    /**
     * A name of extra data used to define a URL
     */
    public static final String EXTRA_URL = "url";

    static {
        TabInfo licenseTab = new TabInfo(LicenseInfoFragment.class, null, R.string.license_info_title);
        List<TabInfo> lst = new ArrayList<SdkMainActivity.TabInfo>();
        lst.add(new TabInfo(RiskDetectionFragment.class, null, R.string.str_main_activity_risk_detection_tab));
        lst.add(new TabInfo(DataProtectionFragment.class, null, R.string.str_main_activity_data_protection_tab));
        lst.add(new TabInfo(DeviceProtectionFragment.class, null, R.string.str_main_activity_antivirus_tab));
        lst.add(new TabInfo(SelfDefenseFragment.class, null, R.string.str_main_activity_self_defense_tab));
        lst.add(new TabInfo(WebAndNetworkProtectionFragment.class, null, R.string.str_main_activity_web_protection_tab));
        lst.add(new TabInfo(UpdateFragment.class, null, R.string.str_main_activity_updates));
        lst.add(new TabInfo(PluginFragment.class, null, R.string.str_main_activity_plugins_tab));
        lst.add(licenseTab);
        TABS = Collections.unmodifiableList(lst);
        List<TabInfo> lst2 = new ArrayList<SdkMainActivity.TabInfo>();
        lst2.add(licenseTab);
        BAD_LICENSE_TABS = Collections.unmodifiableList(lst2);
    }
    
    /*
     * Pager for main tabs
     */
    private ViewPager mViewPager;

    private String mLastError = "";
    
    public String getLastError() {
        return mLastError;
    }

    @Override
    public void initUi(Bundle savedInstanceState) {
        setContentView(R.layout.sdk_main_activity);
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setPageMargin(getResources().getDimensionPixelSize(
                R.dimen.options_pager_separator_margin));
    }

    @Override
    public void initComponent(AppStatus status, String error) {
        if (status == AppStatus.InitFailed) {
            initTabs(BAD_LICENSE_TABS);
        } else {
            TaskReputationManager.getInstance(this).addOverlapActivityListener(this, this);
            initTabs(TABS);
            mViewPager.setCurrentItem(0);
            if (status == AppStatus.InitNotRequired) {
                Intent intent = getIntent();
                if (intent != null) {
                    parseIntent(intent);
                }
            } else {
                Log.d(TAG, "App initialized");
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
    }

    private void initTabs(List<TabInfo> tabs) {
        OptionsPagerAdapter adapter = new OptionsPagerAdapter(this, getSupportFragmentManager(),
                tabs);
        mViewPager.setAdapter(adapter);
        mViewPager.setOffscreenPageLimit(0);
    }
    
    /*
     * Return current fragment of pager or null if initialization has not yet passed or failed
     */
    private Fragment getCurrentFragment() {
        if (mViewPager.getAdapter() != null) {
            return ((OptionsPagerAdapter) mViewPager.getAdapter()).getCurrentPage();
        }
        return null;
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        TaskReputationManager.getInstance(this).removeOverlapActivityListener(this);
    }
    
    /**
     * This method intercepts URLs from {@link com.kavsdkexample.ui.network_protection.AddExclusionActivity}
     */
    @Override
    protected void onNewIntent(Intent intent) {
        parseIntent(intent);
        super.onNewIntent(intent);
    }
    
    private void parseIntent(Intent intent) {
        WebFilterControl webFilterControl = ((SdkSampleApplication)getApplication()).getWebFilterControl();
        String urlString = intent.getStringExtra(SdkMainActivity.EXTRA_URL);
        if (urlString != null && webFilterControl != null) {
            try {
                URL url = new URL(urlString);
                // Let's add a whole domain into the exceptions list
                String host = url.getHost();
                webFilterControl.addExclusion(host);
                webFilterControl.saveExclusions();
                Intent newIntent = new Intent(this, WebFilterActivity.class);
                startActivity(newIntent);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public DialogFragment onCreateDialogFragment(int dialogId, Bundle bundle) {
        DialogFragment dialog = null;
        switch (dialogId) {
        case DialogsSupplier.DIALOG_ROOT_CHECK:
            dialog = DialogsSupplier.getRootCheckDialog();
            break;
        case DialogsSupplier.DIALOG_WIFI_CHECK:
            dialog = AlertDialogFragment.newInstance(DialogsSupplier.DIALOG_WIFI_CHECK);
            break;
        case DialogsSupplier.DIALOG_CERTIFICATE_CHECK:
            dialog = DialogsSupplier.getCertificateCheckDialog();
            break;
        case DialogsSupplier.DIALOG_REPUTATION_CHECK:
            dialog = DialogsSupplier.getReputationCheckDialog();
            break;
        case DialogsSupplier.DIALOG_REPUTATION_EXT_CHECK:
            dialog = DialogsSupplier.getReputationExtCheckDialog();
            break;
        case DialogsSupplier.DIALOG_FINANCIAL_CHECK:
            dialog = DialogsSupplier.getFinancialCheckDialog();
            break;
        case DialogsSupplier.DIALOG_USB_DEBUG_CHECK:
            dialog = AlertDialogFragment.newInstance(DialogsSupplier.DIALOG_USB_DEBUG_CHECK);
            break;
        case DialogsSupplier.DIALOG_NON_MARKET_APPS_CHECK:
            dialog = AlertDialogFragment.newInstance(DialogsSupplier.DIALOG_NON_MARKET_APPS_CHECK);
            break;
        case DialogsSupplier.DIALOG_DEVICE_PASSWORD_QUALITY_CHECK:
            dialog = AlertDialogFragment.newInstance(DialogsSupplier.DIALOG_DEVICE_PASSWORD_QUALITY_CHECK);
            break;
        case DialogsSupplier.DIALOG_ADD_PHONE_NUMBER:
            dialog = new SecureSmsAddPhoneDialog()
                .setObserver((AddItemObserver) ((SdkSampleApplication) getApplication()).getSecureSmsComponent());
            break;
        case DialogsSupplier.DIALOG_ANTIVIRUS_CHECK_URL:
            dialog = new AntivirusCheckUrlDialog();
            break;
        case DialogsSupplier.DIALOG_ANTIVIRUS_THREAT_DESCRIPTION:
            dialog = new AntivirusThreatInfoDialog();
            break;
        case DialogsSupplier.DIALOG_CHECK_FIRMWARE:
            dialog = AlertDialogFragment.newInstance(DialogsSupplier.DIALOG_CHECK_FIRMWARE);
            break;
        case DialogsSupplier.DIALOG_ENTER_CLIENT_USER_ID:
            dialog = new EnterClientUserIdDialog();
            break;
        default:
            break;
        }
        return dialog;
    }

    @Override
    public void onOverlapActivity(String packageName, String activityClassName) {
        new CheckReputationTask(this).execute(new String[] {packageName});
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // force call onActivityResult for current fragment
        Fragment flagment = getCurrentFragment();
        if (flagment != null) {
            flagment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onDialogFragmentResult(BaseDialogFragment dialog, Object data) {
        Fragment flagment = getCurrentFragment();
        if (flagment instanceof OnDialogFragmentResultListener) {
            ((OnDialogFragmentResultListener) flagment).onDialogFragmentResult(dialog, data);
        }
    }
    
    /************************************* Inner classes *****************************************/

    private static class TabInfo {
        private final Class<?> mClass;
        private final Bundle   mArgs;
        private final int      mTitleResId;
        
        TabInfo(Class<?> clazz, Bundle params, int titleId) {
            mClass      = clazz;
            mArgs       = params;
            mTitleResId = titleId;
        }
    }

    /*
     * Adapter for swipe Views with Tabs
     */
    private static class OptionsPagerAdapter extends FragmentStatePagerAdapter {

        private final Context mContext;
        private final List<TabInfo> mTabs;
        private final Map<TabInfo, Fragment> mCache;
        
        private Fragment mCurrentPage;
            
        public OptionsPagerAdapter(Context context, FragmentManager fm, List<TabInfo> lst) {
            super(fm);
            mContext = context;
            mTabs = lst;
            mCache = new WeakHashMap<TabInfo, Fragment>(mTabs.size());
        }
            
        public Fragment getCurrentPage() {
            return mCurrentPage;
        }
        
        private TabInfo getTab(int position) {
            return mTabs.get(position);
        }

        @Override
        public Fragment getItem(int position) {
            TabInfo tab = getTab(position);
            Fragment fragment = mCache.get(tab);
            if (fragment == null) {
                fragment = Fragment.instantiate(mContext, tab.mClass.getName(), tab.mArgs);
                mCache.put(tab, fragment);
            }
            return fragment;
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            super.setPrimaryItem(container, position, object);
            mCurrentPage = (Fragment) object;
        }
        
        @Override
        public int getCount() {
            return mTabs.size();
        }
        
         @Override
         public CharSequence getPageTitle(int position) {
             TabInfo tab = getTab(position);
             
             return mContext.getString(tab.mTitleResId);
         }
    }
    
    private static class CheckReputationTask extends AsyncTask<String, Void, Verdict> {

        private WeakReference<Context> mContext;
        private String mPackageName;
        
        public CheckReputationTask(Context context) {
            mContext = new WeakReference<Context>(context);
        }
        
        @Override
        protected Verdict doInBackground(String... params) {
            mPackageName = params[0];
            return TaskReputationManager.getInstance(mContext.get()).getPackageReputation(mPackageName);
        }
        
        @Override
        protected void onPostExecute(Verdict result) {
            
            final Context context = mContext.get();
            
            if (context == null) {
                return;
            }
            
            Toast.makeText(
                context,
                "onOverlapActivity, package = " + mPackageName + ", verdict = " + result.toString(),
                Toast.LENGTH_LONG).show();
        }
    }
}
