package com.kavsdkexample.ui.network_protection;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.kavsdk.antispam.BothListsResult;
import com.kavsdk.antispam.CallFilterControl;
import com.kavsdk.antispam.CallFilterErrors;
import com.kavsdk.antispam.CallFilterEvent;
import com.kavsdk.antispam.CallFilterEventOrigin;
import com.kavsdk.antispam.CallFilterItem;
import com.kavsdk.antispam.CallFilterItemFactory;
import com.kavsdk.antispam.CallFilterItemFactoryImpl;
import com.kavsdk.antispam.CustomFilter;
import com.kavsdk.antispam.FilterModes;
import com.kavsdk.antispam.ItemEvents;
import com.kavsdkexample.R;
import com.kavsdkexample.app.BlockedNumbersController;
import com.kavsdkexample.app.SdkSampleApplication;
import com.kavsdkexample.shared.AppStatus;
import com.kavsdkexample.shared.OnAddItemToAntispamListener;
import com.kavsdkexample.shared.activities.BaseActivity;
import com.kavsdkexample.storage.Settings;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

@SuppressLint("InflateParams")
public class AntispamActivity extends BaseActivity implements CustomFilter, OnAddItemToAntispamListener {
    private static final int INDEX_BLACK_LIST = 0;
    private static final int INDEX_WHITE_LIST = 1;
    private static final int INDEX_BOTH_LISTS = 2;
        
    private static final String DELETE_DIALOG_ID = "DELETE_DIALOG_TAG";
    private static final String ADD_DIALOG_ID = "ADD_DIALOG_TAG";
        
    private static final String LOG_TAG = AntispamActivity.class.getSimpleName();
    
    private CallFilterControl mAntiSpamComponent;

    private Handler mHandler;

    private ListView mAsItemsList;
    private Spinner mAntispamModeSpinner;
        
    private CallFilterItem mItemToDelete;
    private boolean mDeleteItemFromWhiteList;
    private CheckBox mAntispamEnableCheckBox;
    private CheckBox mAntispamNonNumericCheckBox;
    private CheckBox mAddContactsToWhiteCheckBox;
    private BlockedNumbersController mBlockedNumbersController; 
        
    @Override
    public void onDestroy() {
        super.onDestroy();
        SdkSampleApplication.from(this).setAntiSpamFilter(null);
    }
    
    @Override
    public void initComponent(AppStatus status, String additionalInfo) {
        mHandler = new Handler();
        SdkSampleApplication app = SdkSampleApplication.from(this);
        mAntiSpamComponent = app.getCallFilterControl();
        app.setAntiSpamFilter(this);
        mAntispamEnableCheckBox.setChecked(mAntiSpamComponent.isFiltering());
        setAllViewsEnabled(mAntiSpamComponent.isFiltering());
        mAsItemsList.setAdapter(new AsItemsAdapter(this, mAntiSpamComponent, mBlockedNumbersController.getElements()));
        mAntispamNonNumericCheckBox.setChecked(mAntiSpamComponent.getNonNumericNumbersState());
        mAddContactsToWhiteCheckBox.setChecked(mAntiSpamComponent.getContactsToWhiteListState());

        mAsItemsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (!view.isEnabled()) {
                    return;
                }
                
                final int untrustedNumbersCount = mAntiSpamComponent.getBlackListItemsCount() + mAntiSpamComponent.getWhiteListItemsCount();
                
                if (position > 0 && position < (untrustedNumbersCount + 1)) {
                    position--;
                    if (position < mAntiSpamComponent.getBlackListItemsCount()) {
                        mItemToDelete = mAntiSpamComponent.getBlackListItem(position);
                        mDeleteItemFromWhiteList = false;
                    } else {
                        mItemToDelete = mAntiSpamComponent.getWhiteListItem(position
                                - mAntiSpamComponent.getBlackListItemsCount());
                        mDeleteItemFromWhiteList = true;
                    }

                    showDeleteDialog();
                }
            }
        });

        mAntispamNonNumericCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mAntiSpamComponent.blockNonNumericNumbers(isChecked);
                saveAntispamSettings();
            }
        });

        mAddContactsToWhiteCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {            
                mAntiSpamComponent.addContactsToWhiteList(isChecked);
                saveAntispamSettings();
            }
        });
        
        setSpinnerViewToAntispamMode();
        
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this, 
                R.array.as_modes, 
                android.R.layout.simple_spinner_item);
        
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mAntispamModeSpinner.setAdapter(adapter);
    }

    @Override
    public void initUi(Bundle savedInstanceState) {     
        setContentView(R.layout.antispam_fragment); 
        
        SdkSampleApplication app = SdkSampleApplication.from(this);
        mBlockedNumbersController = app.getBlockedNumbersController();
        
        mAntispamEnableCheckBox = (CheckBox) findViewById(R.id.antispamEnabledCheckbox);
        mAntispamEnableCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {            
                enableAntispam(isChecked);
                setAllViewsEnabled(isChecked);
            }
        });

        mAntispamModeSpinner = (Spinner) findViewById(R.id.antispamModeSpinner);

        mAntispamModeSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
    
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {   
                setAntispamFilterMode();
            }
    
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {            
            }
        });
        
        mAsItemsList = (ListView) findViewById(R.id.antispamItemsList);
        
        mAsItemsList.setEmptyView(findViewById(R.id.antispamItemsListEmptyView));
        
        mAntispamNonNumericCheckBox = (CheckBox) findViewById(R.id.antispamNonNumericCheckBox);
        
        mAddContactsToWhiteCheckBox = (CheckBox) findViewById(R.id.antispamAddContactsToWhiteCheckBox);
        
        Button addItemButton = (Button) findViewById(R.id.antispamAddItemButton);
        addItemButton.setOnClickListener(new OnClickListener() {            
            @Override
            public void onClick(View v) {
                showAddItemDialog();
            }
        });
    }
    
    private void setAllViewsEnabled(boolean enabled) {
        ViewGroup controls = (ViewGroup) findViewById(R.id.antispamSettingsLayout);
        
        for (int i = 0; i < controls.getChildCount(); ++i) {
            
            View currentControl = controls.getChildAt(i);
            currentControl.setEnabled(enabled);
            
            if (currentControl instanceof ListView) {
                ListView list = (ListView) currentControl;
                BaseAdapter adapter = (BaseAdapter)list.getAdapter();               
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
            }
        }
    }

    private void enableAntispam(boolean enable) {       
        if (enable) {
            mAntiSpamComponent.startFiltering();
        } else{
            mAntiSpamComponent.stopFiltering(); 
        }
        Settings.setAntiSpamEnabled(this, enable);
    }
    
    private void saveAntispamSettings() {
        try {
            mAntiSpamComponent.saveChanges();
            
        } catch (IOException e) {
            Toast.makeText(this, R.string.str_antispam_saving_error, Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    private void setSpinnerViewToAntispamMode() {
        switch (mAntiSpamComponent.getFilteringMode()) {
            case FilterModes.BLACK_LIST_FILTER:
                mAntispamModeSpinner.setSelection(INDEX_BLACK_LIST);
                break;
            case FilterModes.WHITE_LIST_FILTER:
                mAntispamModeSpinner.setSelection(INDEX_WHITE_LIST);
                break;
            case FilterModes.BOTH_LISTS_FILTER:
                mAntispamModeSpinner.setSelection(INDEX_BOTH_LISTS);
                break;
            default:
                break;
        }
    }

    private int getAntispamModeFromSpinnerView() {
        int uiPosition = mAntispamModeSpinner.getSelectedItemPosition();        
        int mode;
        
        switch (uiPosition) {
            case INDEX_BLACK_LIST: 
                mode = FilterModes.BLACK_LIST_FILTER;
                break;
            case INDEX_WHITE_LIST:
                mode = FilterModes.WHITE_LIST_FILTER;
                break;
            case INDEX_BOTH_LISTS:
                mode = FilterModes.BOTH_LISTS_FILTER;
                break;
            default: 
                throw new RuntimeException("Incorrect antispam mode in UI selected");
            }
        return mode;
    }

    private void setAntispamFilterMode() {
        int mode = getAntispamModeFromSpinnerView();
        mAntiSpamComponent.setFilteringMode(mode);
        Settings.setAntiSpamMode(this, mode);
        saveAntispamSettings();
    }
    
    private void showAddItemDialog() {
        AddItemDialogFragment dialog = new AddItemDialogFragment();
        dialog.show(getSupportFragmentManager(), ADD_DIALOG_ID);
    }
    
    private void showDeleteDialog() {
        new DeleteDialogFragment().show(getSupportFragmentManager(), DELETE_DIALOG_ID);
    }
    
    private void deleteSelectedItem() {
        if (mItemToDelete != null) {
            Iterator<CallFilterItem> iterator; 
            
            if (mDeleteItemFromWhiteList) {
                iterator = mAntiSpamComponent.getWhiteList();
            } else {
                iterator = mAntiSpamComponent.getBlackList();
            }
            
            while (iterator.hasNext()) {
                CallFilterItem item = iterator.next();
                
                if (mItemToDelete.getEventType() != item.getEventType() ||
                    !TextUtils.equals(mItemToDelete.getPhoneNumberMask(), item.getPhoneNumberMask()) ||
                    !TextUtils.equals(mItemToDelete.getBodyTextMask(), item.getBodyTextMask())) {
                    
                    continue;
                }
            
                iterator.remove();
                saveAntispamSettings();  // Call this on good result to save antispam changes to file.
                ((AsItemsAdapter)mAsItemsList.getAdapter()).notifyDataSetChanged(); // refresh list view
                break;
            }   
        }
    }

    @Override
    public void onAddItem(String phoneMask, String textMask, boolean toWhiteList) {
        
        CallFilterItemFactory itemFactory = new CallFilterItemFactoryImpl();
        CallFilterItem item;
        
        if (textMask == null) {
            item = itemFactory.createPhoneFilterItem(phoneMask);
        } else {
            item = itemFactory.createSmsFilterItem(phoneMask, textMask);
        }
        
        int result;         
        if (toWhiteList) {
            result = mAntiSpamComponent.addToWhiteList(item);
        } else {
            result = mAntiSpamComponent.addToBlackList(item);
        }
        
        int msg;
        switch (result) {
            case CallFilterErrors.OK:
                msg = R.string.str_antispam_message_item_added;
                saveAntispamSettings();  // Call this on good result to save antispam changes to file.
                ((AsItemsAdapter) mAsItemsList.getAdapter()).notifyDataSetChanged(); // refresh list view
                break;
            case CallFilterErrors.INVALID_ITEM:
                msg = R.string.str_antispam_message_invalid_item;
                break;
            case CallFilterErrors.ITEM_ALREADY_EXISTS:
                msg = R.string.str_antispam_message_item_exists;
                break;
            default:
                msg = R.string.str_antispam_message_error_occured;
        }
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    
    @Override
    public void onBlackList(final CallFilterItem item, final CallFilterEvent event, final CallFilterEventOrigin origin) {
        // You should launch a separate thread here for long running operations
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                String msg;
                if (event.getBody() == null) { // this is phone call
                    msg = getString(R.string.str_antispam_call_blocked, event.getNumber(), origin);
                } else {
                    msg = getString(R.string.str_antispam_sms_blocked, event.getBody(), event.getNumber(), origin);
                }
    
                Toast.makeText(AntispamActivity.this, msg, Toast.LENGTH_LONG).show();
                Log.d(LOG_TAG, msg);
            }       
        });             
    }

    @Override
    public void onWhiteList(final CallFilterItem item, final CallFilterEvent event, final CallFilterEventOrigin origin) {
        // You should launch a separate thread here for long running operations
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                String msg;
                if (event.getBody() == null) { // this is phone call
                    msg = getString(R.string.str_antispam_call_not_blocked, event.getNumber(), origin);
                } else {
                    msg = getString(R.string.str_antispam_sms_not_blocked, event.getBody(), event.getNumber(), origin);
                }

                Toast.makeText(AntispamActivity.this, msg, Toast.LENGTH_LONG).show();   
                Log.d(LOG_TAG, msg);
            }       
        });         
    }

    @Override
    public int onBothLists(final CallFilterEvent event, boolean initialEvent, final CallFilterEventOrigin origin) {
        // You should launch a separate thread here for long running operations
        if (initialEvent) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    // You can ask user if he wants to add this number to white or black list.
                    String msg = getString(R.string.str_antispam_both_lists_match, event.getNumber());
                    Toast.makeText(AntispamActivity.this, msg, Toast.LENGTH_LONG).show();       
                    Log.d(LOG_TAG, msg + " " + origin.toString());
                }       
            });
        }
        return BothListsResult.REJECT;
    }
    
    @Override
    public void onEventBlocked(final CallFilterEvent event, final CallFilterEventOrigin origin) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                String msg = getString(R.string.str_antispam_on_blocked_event, event.getNumber(), origin);
                Toast.makeText(AntispamActivity.this, msg, Toast.LENGTH_LONG).show();   
                Log.d(LOG_TAG, msg);
                
                if (origin == CallFilterEventOrigin.CallLogChanged || origin == CallFilterEventOrigin.InboxSMS)
                {
                    mBlockedNumbersController.add(event.getNumber());
                    BaseAdapter adapter = (BaseAdapter)mAsItemsList.getAdapter();               
                    if (adapter != null) {
                        adapter.notifyDataSetChanged();
                    }
                }
            }       
        });
    }
    
    public static class DeleteDialogFragment extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final AntispamActivity activity = (AntispamActivity) getActivity();
            setRetainInstance(true);

            return new AlertDialog.Builder(activity)
                .setTitle(R.string.str_antispam_delete_dialog_title)
                .setMessage(R.string.str_antispam_delete_dialog_confirm)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {                        
                    
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        activity.deleteSelectedItem();
                    }
                    
                })
                .setNegativeButton(android.R.string.no, null)
                .create();
        }
        
         @Override
         public void onDestroyView() {

             if (getDialog() != null && getRetainInstance()) {
                 getDialog().setOnDismissListener(null);
             }
             super.onDestroyView();
         } 

    }
    
    public static class AddItemDialogFragment extends DialogFragment {
        
        //for using with R.array.as_item_list
        private static final int INDEX_BLACK_LIST = 0;
        //for using with R.array.as_event_type
        private static final int INDEX_TYPE_CALL = 0;

        private Spinner mListTypeSpinner;
        private Spinner mEventTypeSpinner;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Context context = getActivity();
            final Dialog dialog = new Dialog(context);

            dialog.setContentView(R.layout.antispam_add_item_dialog);
            dialog.setTitle(R.string.str_antispam_add_item_button);
            setRetainInstance(true);

            mListTypeSpinner = (Spinner) dialog.findViewById(R.id.listTypeSpinner);
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                    context,
                    R.array.as_item_list, 
                    android.R.layout.simple_spinner_item);
            
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mListTypeSpinner.setAdapter(adapter);
            
            mEventTypeSpinner = (Spinner) dialog.findViewById(R.id.eventTypeSpinner);
            adapter = ArrayAdapter.createFromResource(
                    context,
                    R.array.as_event_type, 
                    android.R.layout.simple_spinner_item);
            
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mEventTypeSpinner.setAdapter(adapter);
            
            Button addButton = (Button) dialog.findViewById(R.id.addButton);
            addButton.setOnClickListener(new View.OnClickListener() {           
                
                @Override
                public void onClick(View v) {
                    boolean toWhiteList = false;
                    if (mListTypeSpinner.getSelectedItemPosition() != INDEX_BLACK_LIST) {
                        toWhiteList = true;
                    }
                        
                    String phoneMask = ((EditText) dialog.findViewById(R.id.phoneText)).getText().toString();
                    String textMask;
                        
                    if (mEventTypeSpinner.getSelectedItemPosition() == INDEX_TYPE_CALL) {
                        textMask = null;
                    } else {
                        textMask = ((EditText) dialog.findViewById(R.id.smsText)).getText().toString();
                    }

                    if (getActivity() instanceof OnAddItemToAntispamListener) {
                        ((OnAddItemToAntispamListener) getActivity()).onAddItem(phoneMask, textMask, toWhiteList);
                    }
                    dismiss();
                }
            });
        
            Button cancelButton = (Button) dialog.findViewById(R.id.cancelButton);
            cancelButton.setOnClickListener(new View.OnClickListener() {            
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                }
            });
            
            return dialog;
        }
        
         @Override
         public void onDestroyView() {

             if (getDialog() != null && getRetainInstance()) {
                 getDialog().setOnDismissListener(null);
             }
             super.onDestroyView();
         } 
    }
    
    private static class AsItemsAdapter extends BaseAdapter {
        
        private static final int TYPE_ITEM = 0;
        private static final int TYPE_SEPARATOR = 1;        
        private static final int TYPE_COUNT = TYPE_SEPARATOR + 1;
        
        private LayoutInflater mInflater;
        private CallFilterControl mAntiSpamComponent;
        private final List<String> mBlockedNumbers;
        private Resources mResources;
       
        public AsItemsAdapter(Context context, CallFilterControl antiSpamComponent, final List<String> blockedNumbers) {
            mInflater = LayoutInflater.from(context);
            mAntiSpamComponent = antiSpamComponent;
            mBlockedNumbers = blockedNumbers;
            mResources = context.getResources();
        }
        
        @Override
        public int getViewTypeCount() {
            return TYPE_COUNT;
        }
        
        @Override
        public int getItemViewType(int position) {
            if (position == 0 ||
                position == mAntiSpamComponent.getBlackListItemsCount() + mAntiSpamComponent.getWhiteListItemsCount() + 1) {
                return TYPE_SEPARATOR;
            }
            
            return TYPE_ITEM;
        }
        
        @Override
        public int getCount() {
            return mAntiSpamComponent.getBlackListItemsCount() + mAntiSpamComponent.getWhiteListItemsCount() + mBlockedNumbers.size() + 2;           
        }
    
        @Override
        public Object getItem(int position) {
            return position;
        }
    
        @Override
        public long getItemId(int position) {
            return position;
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            int type = getItemViewType(position);
            if (convertView == null) {
                holder = new ViewHolder();
                switch(type) {
                case TYPE_ITEM:
                    convertView = mInflater.inflate(android.R.layout.simple_list_item_1, null);              
                    holder.textView = (TextView)convertView.findViewById(android.R.id.text1); 
                    break;
                case TYPE_SEPARATOR:
                    convertView = mInflater.inflate(R.layout.antispam_items_separator, null);
                    holder.textView = (TextView)convertView.findViewById(R.id.antispam_separator_text);
                    break;
                default:
                    throw new RuntimeException("Wrong item type");
                }
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder)convertView.getTag();
            }
            
            CallFilterItem item;
            int itemIndex;
            String listTypeStr;
            
            final int untrustedNumbersCount = mAntiSpamComponent.getBlackListItemsCount() + mAntiSpamComponent.getWhiteListItemsCount();
            
            if (position == 0) { //untrusted numbers header         
                holder.textView.setText(mResources.getString(R.string.str_antispam_untrusted_numbers_header));
            } else if (position < untrustedNumbersCount + 1) {//untrusted numbers items
                
                position--;
            
                if (position < mAntiSpamComponent.getBlackListItemsCount()) {
                    itemIndex = position;
                    listTypeStr = mResources.getString(R.string.str_antispam_adapter_black_list);
                    item = mAntiSpamComponent.getBlackListItem(itemIndex);          
                } else {
                    itemIndex = position - mAntiSpamComponent.getBlackListItemsCount();
                    listTypeStr = mResources.getString(R.string.str_antispam_adapter_white_list);
                    item = mAntiSpamComponent.getWhiteListItem(itemIndex);
                }
                
                StringBuilder resultStr = new StringBuilder();
                resultStr.append(itemIndex);
                resultStr.append(". ");
                resultStr.append(listTypeStr);
                resultStr.append(" ");
                
                final String phoneMaskStr = item.getPhoneNumberMask();
                final String textMask = item.getBodyTextMask();
                
                if (item.getEventType() == ItemEvents.SMS_EVENT) {
                    resultStr.append(mResources.getString(R.string.str_antispam_adapter_sms_item));
                } else {
                    resultStr.append(mResources.getString(R.string.str_antispam_adapter_voice_item));
                }
                
                if (phoneMaskStr != null && phoneMaskStr.length() > 0) {
                    resultStr.append(mResources.getString(R.string.str_antispam_adapter_phone_number, phoneMaskStr));
                }
                
                if (item.getEventType() == ItemEvents.SMS_EVENT && textMask != null && textMask.length() > 0) {
                    resultStr.append(mResources.getString(R.string.str_antispam_adapter_text_mask, textMask));
                }
                
                holder.textView.setText(resultStr.toString());
                
            } else if (position == untrustedNumbersCount + 1) { //blocked numbers header
                holder.textView.setText(mResources.getString(R.string.str_antispam_blocked_numbers_header));
            } else { //blocked numbers items
                holder.textView.setText(mBlockedNumbers.get(position - untrustedNumbersCount - 2));
            }
                                
            convertView.setEnabled(parent.isEnabled());
            return convertView;
        }     
    }

     public static class ViewHolder {
            public TextView textView;
     }
}
