package com.kavsdkexample.ui.data_protection;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kavsdk.securestorage.file.CryptoFileInputStream;
import com.kavsdk.securestorage.file.CryptoFileOutputStream;
import com.kavsdk.securestorage.fingerprint.FingerprintOperationException;
import com.kavsdk.securestorage.fingerprint.FingerprintOperationHelper;
import com.kavsdk.securestorage.fingerprint.FingerprintOperationObserver;
import com.kavsdk.securestorage.fingerprint.OperationController;
import com.kavsdkexample.BuildConfig;
import com.kavsdkexample.R;
import com.kavsdkexample.Utils;
import com.kavsdkexample.shared.AppStatus;
import com.kavsdkexample.shared.activities.BaseActivity;
import com.kavsdkexample.ui.NoticeDialogFragment;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.nio.charset.Charset;


/**
 * Demonstration of using of Secure File SDK component.
 * This activity allows the user to write to and read from a secure file.
 *
 * @see {@link TestDatabaseActivity}
 */
public class TestSecureFileActivity extends BaseActivity implements View.OnClickListener, FingerprintOperationObserver, NoticeDialogFragment.NoticeDialogListener {


    private static final String FINGERPRINT_DIALOG = "fingerprint_dialog";

    /*
     * GUI controls
     */
    private Button mButtonReadFile;
    private Button mButtonWriteFile;
    private Button mButtonAppendFile;
    private EditText mEditTextFileName;
    private EditText mEditTextFilePassword;
    private EditText mEditTextUserText;

    /*Fingerprint GUI*/
    private Button mAddFingerprintButton;
    private Button mRemoveFingerprintButton;
    private Button mUpdateSecretKeyButton;
    private CheckBox mUseFingerprintCheckBox;
    private FrameLayout mFingerprintLayout;

    private Dialog mOverwriteDialog;

    private volatile OperationController mOperationController;


    @Override
    public void initUi(Bundle savedInstanceState) {
        setContentView(R.layout.test_secure_file_activity);
        // GUI initialization
        mButtonReadFile = (Button) findViewById(R.id.buttonReadFile);
        mButtonWriteFile = (Button) findViewById(R.id.buttonWriteFile);
        mButtonAppendFile = (Button) findViewById(R.id.buttonAppendFile);

        mEditTextFileName = (EditText) findViewById(R.id.editTextFileName);
        mEditTextUserText = (EditText) findViewById(R.id.editTextUserText);
        mEditTextFilePassword = (EditText) findViewById(R.id.editTextFilePassword);

        //Fingerprint UI
        mAddFingerprintButton = (Button) findViewById(R.id.add_fingerprint_button);
        mRemoveFingerprintButton = (Button) findViewById(R.id.remove_fingerprint_button);
        mUpdateSecretKeyButton = (Button) findViewById(R.id.update_secret_key);
        mUseFingerprintCheckBox = (CheckBox) findViewById(R.id.use_fingerprint_checkbox);
        mFingerprintLayout = (FrameLayout) findViewById(R.id.fingerprint_container);

        mButtonReadFile.setOnClickListener(this);
        mButtonWriteFile.setOnClickListener(this);
        mButtonAppendFile.setOnClickListener(this);

        //Fingerprint UI listeners
        mAddFingerprintButton.setOnClickListener(this);
        mRemoveFingerprintButton.setOnClickListener(this);
        mUpdateSecretKeyButton.setOnClickListener(this);
    }


    @Override
    public void initComponent(AppStatus appStatus, String additionalInfo) {

    }

    @Override
    public void onClick(View v) {
        final String path = mEditTextFileName.getText().toString();
        final String password = mEditTextFilePassword.getText().toString();

        if (TextUtils.isEmpty(path)) {
            onError("Path can't be empty");
            return;
        }
        if (TextUtils.isEmpty(password) && (
                v.getId() == R.id.buttonReadFile && !mUseFingerprintCheckBox.isChecked() ||
                        v.getId() == R.id.buttonWriteFile && !mUseFingerprintCheckBox.isChecked() ||
                        v.getId() == R.id.buttonAppendFile && !mUseFingerprintCheckBox.isChecked() ||
                        v.getId() == R.id.add_fingerprint_button)) {
            onError("Password can't be empty");
            return;
        }

        setButtonsEnabled(false);
        switch (v.getId()) {
            case R.id.buttonReadFile:
                new ReadFileTask(this, mUseFingerprintCheckBox.isChecked()).execute(path, password);
                break;
            case R.id.buttonAppendFile:
                startWriteFileTask(path, password, true, mUseFingerprintCheckBox.isChecked());
                 break;
            case R.id.buttonWriteFile:
                File file = new File(path);
                if (file.exists()) {
                    showOverwriteDialog();
                } else {
                    startWriteFileTask(path, password, false, mUseFingerprintCheckBox.isChecked());
                }
                break;
            case R.id.add_fingerprint_button:
                new AddFingerprintTask().execute(path, password);
                break;
            case R.id.update_secret_key:
                try {
                    FingerprintOperationHelper.updateSecretKey(TestSecureFileActivity.this);
                    Toast.makeText(this, "Secret key has been updated", Toast.LENGTH_SHORT).show();
                    setButtonsEnabled(true);
                } catch (FingerprintOperationException e) {
                    if (BuildConfig.DEBUG) {
                        e.printStackTrace();
                    }
                    onError(DataProtectionUtility.getFingerprintOperationErrorMessage(e, this));
                }
                break;
            case R.id.remove_fingerprint_button:
                try {
                    FingerprintOperationHelper.removeFingerprint(path);
                    Toast.makeText(this,
                            "Fingerprint password for file :" + path + "has been removed",
                            Toast.LENGTH_SHORT)
                            .show();
                    setButtonsEnabled(true);
                } catch (FingerprintOperationException e) {
                    if (BuildConfig.DEBUG) {
                        e.printStackTrace();
                    }
                    onError(DataProtectionUtility.getFingerprintOperationErrorMessage(e, this));
                }
                break;
            default:
                break;
        }
    }

    private void onError(String errorStr) {
        String message = "Error occured: " + errorStr;
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        setButtonsEnabled(true);
    }

    private void onFileRead(String result) {
        mEditTextUserText.setText(result);
        setButtonsEnabled(true);
    }

    private void onFileWritten() {
        Toast.makeText(this, "Operation successfully completed", Toast.LENGTH_LONG).show();
        setButtonsEnabled(true);
    }

    private void setButtonsEnabled(boolean enabled) {
        mButtonReadFile.setEnabled(enabled);
        mButtonWriteFile.setEnabled(enabled);
        mButtonAppendFile.setEnabled(enabled);
        mAddFingerprintButton.setEnabled(enabled);
        mRemoveFingerprintButton.setEnabled(enabled);
        mUpdateSecretKeyButton.setEnabled(enabled);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mOverwriteDialog != null) {
            mOverwriteDialog.cancel();
            mOverwriteDialog = null;
        }
    }


    @Override
    protected void onResume() {
        final String messagePrefix = "Fingeprint operations are not allowed: \n";
        //Check message about fingerprint operation error
        String fingerprintErrorMessage = DataProtectionUtility.getFingerprintErrorMessage(this);
        TextView errorView =  (TextView) mFingerprintLayout.findViewById(R.id.fingerprint_error_view);
        LinearLayout fingerprintControlsLayout = (LinearLayout) mFingerprintLayout.findViewById(R.id.fingerprint_controls_layout);
        if (fingerprintErrorMessage != null) {
            errorView.setText(messagePrefix + fingerprintErrorMessage);
            fingerprintControlsLayout.setVisibility(View.GONE);
            errorView.setVisibility(View.VISIBLE);
            //Toggle checkbox, if it was previously checked.
            if (mUseFingerprintCheckBox.isChecked()) {
                mUseFingerprintCheckBox.toggle();
            }
        } else {
            fingerprintControlsLayout.setVisibility(View.VISIBLE);
            errorView.setVisibility(View.GONE);
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        dismissFingerprintDialog();
        cancelCurrentOperation();
    }

    private void startWriteFileTask(final String path, final String password, boolean append, boolean withFingerprint) {
        final String content = mEditTextUserText.getText().toString();
        new WriteFileTask(this, append, withFingerprint).execute(path, password, content);
    }

    private void showOverwriteDialog() {
        mOverwriteDialog = new AlertDialog.Builder(this).
                setTitle(R.string.str_secure_storage_test_file_overwrite_dialog_title).
                setMessage(R.string.str_secure_storage_test_file_overwrite_dialog_message).
                setPositiveButton(R.string.str_secure_storage_test_file_overwrite_dialog_positive, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        String path = mEditTextFileName.getText().toString();
                        String password = mEditTextFilePassword.getText().toString();
                        startWriteFileTask(path, password, false, mUseFingerprintCheckBox.isChecked());
                        mOverwriteDialog = null;
                    }
                }).
                setNegativeButton(R.string.str_secure_storage_test_file_overwrite_dialog_negative, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        setButtonsEnabled(true);
                        mOverwriteDialog = null;
                    }
                }).
                setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        setButtonsEnabled(true);
                        mOverwriteDialog = null;
                    }
                }).
                create();
        mOverwriteDialog.show();
    }

    @Override
    public void onFingerprintInputStart(final OperationController operationController) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mOperationController = operationController;
                showFingerprintDialog();
            }
        });
    }

    @Override
    public void onFingerprintInputStop() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dismissFingerprintDialog();
            }
        });
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {

    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        if (dialog instanceof FingerprintDialogFragment) {
            cancelCurrentOperation();
        }
    }

    private void cancelCurrentOperation() {
        if (mOperationController != null) {
            mOperationController.cancelOperation();
            mOperationController = null;
        }
    }

    private void dismissFingerprintDialog() {
        DialogFragment fragment = (DialogFragment) getSupportFragmentManager().findFragmentByTag(FINGERPRINT_DIALOG);
        if (fragment != null) {
            fragment.dismiss();
        }
    }

    private void showFingerprintDialog() {
        final FingerprintDialogFragment fragment = new FingerprintDialogFragment();
        fragment.show(getSupportFragmentManager(), FINGERPRINT_DIALOG);
    }

    private static class ReadFileTask extends AsyncTask<String, Void, String> {

        private final WeakReference<TestSecureFileActivity> mRef;
        private String mErrorStr;
        private boolean mWithFingerprint;

        public ReadFileTask(TestSecureFileActivity a, boolean withFingerprint) {
            mRef = new WeakReference<TestSecureFileActivity>(a);
            mWithFingerprint = withFingerprint;
        }

        @Override
        protected String doInBackground(String... params) {

            String filename = params[0];
            String password = params[1];
            String result = null;

            CryptoFileInputStream in = null;

            try {
                if (!mWithFingerprint) {
                    in = new CryptoFileInputStream(filename, password);
                } else {
                    TestSecureFileActivity activity = mRef.get();
                    if (activity != null) {
                        in = new CryptoFileInputStream(activity.getApplicationContext(), filename, activity);
                    } else {
                        mErrorStr = "Wrong state";
                        return null;
                    }
                }
                byte[] buffer = new byte[in.available()];

                in.read(buffer);
                result = new String(buffer, Charset.defaultCharset());


            } catch (IOException e) {
                mErrorStr = e.getMessage();

            } catch (FingerprintOperationException e) {
                if (BuildConfig.DEBUG) {
                    e.printStackTrace();
                }
                TestSecureFileActivity activity = mRef.get();
                if (activity != null) {
                    mErrorStr = DataProtectionUtility.getFingerprintOperationErrorMessage(e, activity);
                } else {
                    mErrorStr = e.getLocalizedMessage();
                }
            } finally {
                Utils.closeQuietly(in);
            }
            return result;
        }

        protected void onPostExecute(String result) {

            TestSecureFileActivity activity = mRef.get();
            if (activity == null) {
                return;
            }

            if (mErrorStr != null) {
                activity.onError(mErrorStr);
            } else {
                activity.onFileRead(result);
            }
        }
    }

    private static class WriteFileTask extends AsyncTask<String, Void, Void> {

        private final WeakReference<TestSecureFileActivity> mRef;
        private String mErrorStr;
        private final boolean mAppend;
        private boolean mWithFingerprint;

        public WriteFileTask(TestSecureFileActivity a, boolean append, boolean withFingerprint) {
            mRef = new WeakReference<TestSecureFileActivity>(a);
            mAppend = append;
            mWithFingerprint = withFingerprint;
        }

        @Override
        protected Void doInBackground(String... params) {

            String filename = params[0];
            String password = params[1];
            String content = params[2];

            CryptoFileOutputStream out = null;

            try {
                if (!mWithFingerprint) {
                    out = new CryptoFileOutputStream(filename, mAppend, password);
                } else {
                    TestSecureFileActivity activity = mRef.get();
                    if (activity != null) {
                        out = new CryptoFileOutputStream(activity.getApplicationContext(), filename, mAppend, activity);
                    } else {
                        mErrorStr = "Wrong state";
                        return null;
                    }
                }
                out.write(content.getBytes(Charset.defaultCharset()));

            } catch (IOException e) {
                mErrorStr = e.getMessage();

            } catch (FingerprintOperationException e) {
                if (BuildConfig.DEBUG) {
                    e.printStackTrace();
                }
                TestSecureFileActivity activity = mRef.get();
                if (activity != null) {
                    mErrorStr = DataProtectionUtility.getFingerprintOperationErrorMessage(e, activity);
                } else {
                    mErrorStr = e.getLocalizedMessage();
                }
            } finally {
                Utils.closeQuietly(out);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            TestSecureFileActivity activity = mRef.get();
            if (activity == null) {
                return;
            }

            if (mErrorStr != null) {
                activity.onError(mErrorStr);
            } else {
                activity.onFileWritten();
            }
        }
    }

    private class AddFingerprintTask extends AsyncTask<String, Void, Void> {

        private String mErrorStr;

        @Override
        protected Void doInBackground(String... params) {
            String path = params[0];
            String password = params[1];
            try {
                FingerprintOperationHelper.addFingerprintToCryptoFile(TestSecureFileActivity.this,
                        path, password, TestSecureFileActivity.this);
            } catch (FingerprintOperationException | SecurityException | IOException e) {
                if (BuildConfig.DEBUG) {
                    e.printStackTrace();
                }
                mErrorStr = DataProtectionUtility.getFingerprintOperationErrorMessage(e, TestSecureFileActivity.this);
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void arg) {
            mOperationController = null;
            if (mErrorStr != null) {
                onError(mErrorStr);
            } else {
                Toast.makeText(TestSecureFileActivity.this, "Fingerprint password has been added", Toast.LENGTH_SHORT).show();
                setButtonsEnabled(true);
            }
        }
    }

}