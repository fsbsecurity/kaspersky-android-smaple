package com.kavsdkexample.ui.device_protection;

import com.kavsdk.antivirus.appmonitor.AppInstallationMonitor;
import com.kavsdkexample.R;
import com.kavsdkexample.app.SdkSampleApplication;
import com.kavsdkexample.app.SdkService;
import com.kavsdkexample.shared.AppStatus;
import com.kavsdkexample.shared.activities.BaseActivity;
import com.kavsdkexample.storage.Settings;
import com.kavsdkexample.Utils;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;

public class AppMonitorActivity extends BaseActivity implements OnClickListener {
    private static final String TAG = AppMonitorActivity.class.getSimpleName();
    
    public static Intent getStartIntent(Context context) {
        return new Intent(context, AppMonitorActivity.class);
    }

    @Override
    public void initUi(Bundle savedInstanceState) {
        setContentView(R.layout.app_monitor_activity);
        final View view = getWindow().getDecorView();
        
        Utils.initCheckBox(view, R.id.EnableApkInstallation, true, 
                Settings.isApkInstallationEnabled(this, false), this);
        Utils.initCheckBox(view, R.id.ApkInstallationUds, true, 
                Settings.isAppMonitorUdsEnabled(this, true), this);
        Utils.initCheckBox(view, R.id.ApkInstallationDeepScan, true, 
                Settings.isAppMonitorDeepScanEnabled(this, false), this);
        Utils.initCheckBox(view, R.id.ApkInstallationScanMissedApps, true, 
                Settings.isAppMonitorScanMissedEnabled(this, false), this);
    }

    @Override
    public void initComponent(AppStatus appStatus, String additionalInfo) {

    }

    @Override
    public void onClick(View v) {
        String scanCheckboxLogString = null;
        
        switch (v.getId()) {
        case R.id.ApkInstallationUds:
            updateAppInstallationMonitor(v, new AppMonitorOptionsApplier() {
                @Override
                public void onUpdateScanOption(AppInstallationMonitor appInstallationMonitor, boolean enable) {
                    Settings.setAppMonitorUdsEnabled(AppMonitorActivity.this, enable);
                    appInstallationMonitor.setScanUdsAllow(enable);
                }
            });
            break;
        case R.id.ApkInstallationDeepScan:
            updateAppInstallationMonitor(v, new AppMonitorOptionsApplier() {
                @Override
                public void onUpdateScanOption(AppInstallationMonitor appInstallationMonitor, boolean enable) {
                    Settings.setAppMonitorDeepScanEnabled(AppMonitorActivity.this, enable);
                    appInstallationMonitor.setDeepScan(enable);
                }
            });
            break;
        case R.id.ApkInstallationScanMissedApps:
            updateAppInstallationMonitor(v, new AppMonitorOptionsApplier() {
                @Override
                public void onUpdateScanOption(AppInstallationMonitor appInstallationMonitor, boolean enable) {
                    Settings.setAppMonitorScanMissedEnabled(AppMonitorActivity.this, enable);
                    if (!enable) {
                        appInstallationMonitor.resetMissedPackagesList();
                    }
                }
            });
            break;
        case R.id.EnableApkInstallation:
            boolean apkMonitorEnabled = ((CheckBox) v).isChecked();
            Settings.setApkInstallationEnabled(AppMonitorActivity.this, apkMonitorEnabled);
            ((SdkSampleApplication)getApplication()).getService().updateApkMonitorState(apkMonitorEnabled);
            break;
        default:
            break;
        }
        
        if (scanCheckboxLogString != null) {
            Log.d(TAG, scanCheckboxLogString
                    + (((CheckBox) v).isChecked() ? "enabled" : "disabled"));
        }
    }
    
    private void updateAppInstallationMonitor(View v, AppMonitorOptionsApplier applier) {
        SdkService serv = ((SdkSampleApplication)getApplication()).getService();
        
        if (serv.isReady()) {
            applier.onUpdateScanOption(serv.getAppInstallationMonitor(), ((CheckBox) v).isChecked());
        }
    }
    
    /************************************* Inner classes *****************************************/
    private interface AppMonitorOptionsApplier {
        void onUpdateScanOption(AppInstallationMonitor appInstallationMonitor, boolean enable);
    }
}
