package com.kavsdkexample.ui.plugins;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.kavsdkexample.R;
import com.kavsdkexample.ui.NoticeDialogFragment;



public class MobAppActionFragmentDialog extends NoticeDialogFragment {

    private EditText mActionTypeView;
    private EditText mActionResultView;
    private EditText mDescriptionView;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View rootView = inflater.inflate(R.layout.dialog_kfp_mob_app_action, null);
        mActionTypeView = (EditText) rootView.findViewById(R.id.kfp_dialog_action_type);
        mActionResultView = (EditText) rootView.findViewById(R.id.kfp_dialog_action_result);
        mDescriptionView = (EditText) rootView.findViewById(R.id.kfp_dialog_description);
        builder.setView(rootView);
        builder.setPositiveButton(R.string.ok_button_text, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mListener.onDialogPositiveClick(MobAppActionFragmentDialog.this);
            }
        });
        builder.setNegativeButton(R.string.cancel_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mListener.onDialogNegativeClick(MobAppActionFragmentDialog.this);
            }
        });
        return builder.create();
    }

    public String getActionType() {
        return mActionTypeView.getText().toString();
    }

    public String getActionResult() {
        return mActionResultView.getText().toString();
    }
    public String getDescription() {
        return mDescriptionView.getText().toString();
    }

}
