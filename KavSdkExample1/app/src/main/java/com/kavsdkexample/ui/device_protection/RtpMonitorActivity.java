package com.kavsdkexample.ui.device_protection;

import com.kavsdk.antivirus.Antivirus;
import com.kavsdk.antivirus.MonitorConstants;
import com.kavsdkexample.R;
import com.kavsdkexample.app.SdkSampleApplication;
import com.kavsdkexample.app.SdkService;
import com.kavsdkexample.shared.AppStatus;
import com.kavsdkexample.shared.activities.BaseActivity;
import com.kavsdkexample.storage.Settings;
import com.kavsdkexample.Utils;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;


public class RtpMonitorActivity extends BaseActivity implements OnClickListener {
    private static final String TAG = RtpMonitorActivity.class.getSimpleName();
    
    private Antivirus mAntivirusComponent;
    private SdkService mSdkService;
    
    private View mOpenRtpMonitorDirectoryManagerCommandView;
    private TextView mOpenRtpMonitorDirectoryManagerCommandCaptionView;
    
    public static Intent getStartIntent(Context context) {
        return new Intent(context, RtpMonitorActivity.class);
    }

    @Override
    public void initUi(Bundle savedInstanceState) {
        setContentView(R.layout.rtp_monitor_activity);
        final View view = getWindow().getDecorView();

        OnClickListener actionListener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.rtpDelete:
                        Settings.setMonitorAction(RtpMonitorActivity.this, Settings.DELETE_THREAT_ACTION);
                        break;
                    case R.id.rtpQuarantine:
                        Settings.setMonitorAction(RtpMonitorActivity.this, Settings.QUARANTINE_THREAT_ACTION);
                        break;
                    case R.id.rtpSkip:
                        Settings.setMonitorAction(RtpMonitorActivity.this, Settings.SKIP_THREAT_ACTION);
                        break;
                    default:
                        break;
                }
            }
            
        };

        RadioButton rtpDeleteRadioButton = Utils.initRadioButton(view, R.id.rtpDelete, actionListener);
        RadioButton rtpQuarantineRadioButton = Utils.initRadioButton(view, R.id.rtpQuarantine, actionListener);
        RadioButton rtpSkipRadioButton = Utils.initRadioButton(view, R.id.rtpSkip, actionListener);
        RadioButton checkedRadioButton = null;  
        switch (Settings.getMonitorAction(this)) {
        case Settings.DELETE_THREAT_ACTION:
            checkedRadioButton = rtpDeleteRadioButton;
            break;
        case Settings.QUARANTINE_THREAT_ACTION:
            checkedRadioButton = rtpQuarantineRadioButton;
            break;
        case Settings.SKIP_THREAT_ACTION:
            checkedRadioButton = rtpSkipRadioButton;
            break;
        default:
            break;
        }
        if (checkedRadioButton != null) {
            checkedRadioButton.setChecked(true);
        }
        
        mOpenRtpMonitorDirectoryManagerCommandView = view.findViewById(R.id.openRtpMonitorDirectoryManager);
        mOpenRtpMonitorDirectoryManagerCommandView.setOnClickListener(this);
        mOpenRtpMonitorDirectoryManagerCommandView.setEnabled(true);
        mOpenRtpMonitorDirectoryManagerCommandCaptionView = 
                (TextView) mOpenRtpMonitorDirectoryManagerCommandView.findViewById(R.id.commandCaption);
        mOpenRtpMonitorDirectoryManagerCommandCaptionView.setText(R.string.str_av_open_rtp_monitor_directory_manager);
    }

    @Override
    public void initComponent(AppStatus appStatus, String additionalInfo) {
        final SdkSampleApplication app = ((SdkSampleApplication)getApplication());
        mAntivirusComponent = app.getAntivirus();
        mSdkService = app.getService();

        final View view = getWindow().getDecorView();
        int scanMode = Settings.getMonitorScanMode(this, mAntivirusComponent.getMonitorScanMode());

        //Scan and clean modes check boxes
        Utils.initCheckBox(view, R.id.RtpEnable, true, Settings.isMonitorEnabled(this, mAntivirusComponent.isMonitorActive()), this);
        Utils.initCheckBox(view, R.id.RtpCheckExe, scanMode >= 0,
                (scanMode & MonitorConstants.EXECUTABLES_ONLY) != 0, this);
        Utils.initCheckBox(view, R.id.RtpCheckArchives, scanMode >= 0,
                (scanMode & MonitorConstants.ARCHIVED) != 0, this);
        Utils.initCheckBox(view, R.id.RtpScanUds, scanMode >= 0,
                (scanMode & MonitorConstants.ALLOW_UDS) != 0, this);
        Utils.initCheckBox(view, R.id.RtpSkipRiskwareAdware, scanMode >= 0,
                (scanMode & MonitorConstants.SKIP_RISKWARE_ADWARE) != 0, this);
        Utils.initCheckBox(view, R.id.RtpScanSuspicious, true,
                (scanMode & MonitorConstants.DETECT_SUSPICIOUS) != 0, this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.openRtpMonitorDirectoryManager:
            Intent intent = RtpMonitorDirectoryManagerActivity.getStartIntent(RtpMonitorActivity.this);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            break;
        case R.id.RtpEnable:
            // Updating the real time protection monitor check box and enable/disable the monitor
            boolean monitorEnabled = ((CheckBox) v).isChecked();
            mSdkService.enableRtpMonitor(v, monitorEnabled);
            Settings.setMonitorEnabled(RtpMonitorActivity.this, monitorEnabled);
            break;
        case R.id.RtpCheckExe:
            int oldScanMode = mAntivirusComponent.getMonitorScanMode();
            int scanMode = oldScanMode;
            if (((CheckBox) v).isChecked()) {
                scanMode |= MonitorConstants.EXECUTABLES_ONLY;
                if ((oldScanMode & MonitorConstants.ARCHIVED) != 0) {
                    scanMode |= MonitorConstants.ARCHIVED_ONLYEXECUTABLE;
                }
            } else {
                scanMode &= ~(MonitorConstants.EXECUTABLES_ONLY | MonitorConstants.ARCHIVED_ONLYEXECUTABLE);
            }
            int resScanMode = mAntivirusComponent.setMonitorScanMode(scanMode);
            String msgScanMode = resScanMode < 0 ? "Change RTP monitor scan mode failed with error "
                    + Utils.convertErrorToString(RtpMonitorActivity.this, resScanMode)
                    : "Change RTP monitor scan mode OK";
            Log.d(TAG, msgScanMode);
            Settings.setMonitorScanMode(RtpMonitorActivity.this, scanMode);
            break;
        case R.id.RtpCheckArchives:
            oldScanMode = mAntivirusComponent.getMonitorScanMode();
            scanMode = oldScanMode;
            if (((CheckBox) v).isChecked()) {
                scanMode |= MonitorConstants.ARCHIVED;
                if ((oldScanMode & MonitorConstants.EXECUTABLES_ONLY) != 0) {
                    scanMode |= MonitorConstants.ARCHIVED_ONLYEXECUTABLE;
                }
            } else {
                scanMode &= ~(MonitorConstants.ARCHIVED | MonitorConstants.ARCHIVED_ONLYEXECUTABLE);
            }
            resScanMode = mAntivirusComponent.setMonitorScanMode(scanMode);
            msgScanMode = resScanMode < 0 ? "Change RTP monitor scan mode failed with error "
                    + Utils.convertErrorToString(RtpMonitorActivity.this, resScanMode)
                    : "Change RTP monitor scan mode OK";
            Log.d(TAG, msgScanMode);
            Settings.setMonitorScanMode(RtpMonitorActivity.this, scanMode);
            break;
        case R.id.RtpScanUds:
            oldScanMode = mAntivirusComponent.getMonitorScanMode();
            scanMode = oldScanMode;
            if (((CheckBox) v).isChecked()) {
                scanMode |= MonitorConstants.ALLOW_UDS;
            } else {
                scanMode &= ~(MonitorConstants.ALLOW_UDS);
            }
            mAntivirusComponent.setMonitorScanMode(scanMode);
            Settings.setMonitorScanMode(RtpMonitorActivity.this, scanMode);
            break;
        case R.id.RtpSkipRiskwareAdware:
            oldScanMode = mAntivirusComponent.getMonitorScanMode();
            scanMode = oldScanMode;
            if (((CheckBox) v).isChecked()) {
                scanMode |= MonitorConstants.SKIP_RISKWARE_ADWARE;
            } else {
                scanMode &= ~(MonitorConstants.SKIP_RISKWARE_ADWARE);
            }
            mAntivirusComponent.setMonitorScanMode(scanMode);
            Settings.setMonitorScanMode(RtpMonitorActivity.this, scanMode);
            break;
        case R.id.RtpScanSuspicious:
            oldScanMode = mAntivirusComponent.getMonitorScanMode();
            scanMode = oldScanMode;
            if (((CheckBox) v).isChecked()) {
                scanMode |= MonitorConstants.DETECT_SUSPICIOUS;
            } else {
                scanMode &= ~(MonitorConstants.DETECT_SUSPICIOUS);
            }
            mAntivirusComponent.setMonitorScanMode(scanMode);
            Settings.setMonitorScanMode(RtpMonitorActivity.this, scanMode);
            break;
        default:
            break;
        }
    }
}
