package com.kavsdkexample.ui.data_protection;

import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kavsdk.license.SdkLicenseViolationException;
import com.kavsdk.securestorage.database.AbstractCursor;
import com.kavsdk.securestorage.database.SQLiteDatabase;
import com.kavsdk.securestorage.database.SQLiteException;
import com.kavsdk.securestorage.database.util.DatabaseUtils;
import com.kavsdk.securestorage.fingerprint.FingerprintOperationException;
import com.kavsdk.securestorage.fingerprint.FingerprintOperationHelper;
import com.kavsdk.securestorage.fingerprint.FingerprintOperationObserver;
import com.kavsdk.securestorage.fingerprint.OperationController;
import com.kavsdkexample.R;
import com.kavsdkexample.shared.AppStatus;
import com.kavsdkexample.shared.activities.BaseActivity;
import com.kavsdkexample.ui.NoticeDialogFragment;

import java.io.File;

/**
 * Demonstration of using of Secure Database SDK component.
 * This sample activity allows to work with a secure database,
 * view its tables and execute SQL queries
 *
 * @see {@link TestSecureFileActivity}
 */
public class TestDatabaseActivity extends BaseActivity implements View.OnClickListener, FingerprintOperationObserver, NoticeDialogFragment.NoticeDialogListener {

    private static final String TAG = TestDatabaseActivity.class.getSimpleName();

    private static final String FINGERPRINT_DIALOG = "fingerprint_dialog";

    /* A maximum number of rows in table to show */
    private static final int MAX_ROWS = 100;

    /*
     * GUI controls
     */
    private Button mButtonOpenDatabase;
    private Button mButtonCloseDatabase;
    private Button mButtonDeleteDatabase;
    private Button mButtonShowTables;
    private Button mButtonSql;
    private EditText mEditTextDatabaseName;
    private EditText mEditTextDatabasePassword;
    private EditText mEditTextSql;

    /*Fingerprint GUI*/
    private Button mAddFingerprintButton;
    private Button mRemoveFingerprintButton;
    private Button mUpdateSecretKeyButton;
    private CheckBox mUseFingerprintCheckBox;
    private FrameLayout mFingerprintLayout;
    private volatile SQLiteDatabase mDatabase;

    private OperationController mOperationController;

    @Override
    public void initUi(Bundle savedInstanceState) {
        setContentView(R.layout.test_database_activity);
        mButtonOpenDatabase = (Button) findViewById(R.id.buttonOpenDatabase);
        mButtonCloseDatabase = (Button) findViewById(R.id.buttonCloseDatabase);
        mButtonDeleteDatabase = (Button) findViewById(R.id.buttonDeleteDatabase);
        mButtonShowTables = (Button) findViewById(R.id.buttonShowTables);
        mButtonSql = (Button) findViewById(R.id.buttonSql);
        mEditTextDatabaseName = (EditText) findViewById(R.id.editTextDatabaseName);
        mEditTextDatabasePassword = (EditText) findViewById(R.id.editTextDatabasePassword);
        mEditTextSql = (EditText) findViewById(R.id.editTextSql);

        //Fingerprint UI
        mAddFingerprintButton = (Button) findViewById(R.id.add_fingerprint_button);
        mRemoveFingerprintButton = (Button) findViewById(R.id.remove_fingerprint_button);
        mUpdateSecretKeyButton = (Button) findViewById(R.id.update_secret_key);
        mUseFingerprintCheckBox = (CheckBox) findViewById(R.id.use_fingerprint_checkbox);
        mFingerprintLayout = (FrameLayout) findViewById(R.id.fingerprint_container);

        //Fingerprint UI listeners
        mAddFingerprintButton.setOnClickListener(this);
        mRemoveFingerprintButton.setOnClickListener(this);
        mUpdateSecretKeyButton.setOnClickListener(this);

        mButtonOpenDatabase.setOnClickListener(this);
        mButtonCloseDatabase.setOnClickListener(this);
        mButtonDeleteDatabase.setOnClickListener(this);
        mButtonShowTables.setOnClickListener(this);
        mButtonSql.setOnClickListener(this);
    }

    @Override
    public void initComponent(AppStatus appStatus, String additionalInfo) {

    }

    @Override
    protected void onResume() {
        final String messagePrefix = "Fingeprint operations are not allowed: \n";
        //Check message about fingerprint operation error
        String fingerprintErrorMessage = DataProtectionUtility.getFingerprintErrorMessage(this);
        TextView errorView =  (TextView) mFingerprintLayout.findViewById(R.id.fingerprint_error_view);
        LinearLayout fingerprintControlsLayout = (LinearLayout) mFingerprintLayout.findViewById(R.id.fingerprint_controls_layout);
        if (fingerprintErrorMessage != null) {
            errorView.setText(messagePrefix + fingerprintErrorMessage);
            fingerprintControlsLayout.setVisibility(View.GONE);
            errorView.setVisibility(View.VISIBLE);
            //Toggle checkbox, if it was previously checked.
            if (mUseFingerprintCheckBox.isChecked()) {
                mUseFingerprintCheckBox.toggle();
            }
        } else {
            fingerprintControlsLayout.setVisibility(View.VISIBLE);
            errorView.setVisibility(View.GONE);
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        dismissFingerprintDialog();
        cancelCurrentOperation();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.buttonOpenDatabase:
                openDatabase();
                break;

            case R.id.buttonCloseDatabase:
                closeDatabase();
                break;

            case R.id.buttonDeleteDatabase:
                deleteDatabase();
                break;

            case R.id.buttonShowTables:
                showTables();
                break;

            case R.id.buttonSql:
                executeSql();
                break;
            case R.id.add_fingerprint_button:
                addFingerprint();
                break;
            case R.id.update_secret_key:
                updateSecretKey();
                break;
            case R.id.remove_fingerprint_button:
                removeFingerprint();
                break;
            default:
                break;
        }
    }


    @Override
    public void onFingerprintInputStart(final OperationController operationController) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mOperationController = operationController;
                showFingerprintDialog();
            }
        });
    }

    @Override
    public void onFingerprintInputStop() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dismissFingerprintDialog();
            }
        });
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {

    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        if (dialog instanceof FingerprintDialogFragment) {
            cancelCurrentOperation();
        }
    }

    private final void openDatabase() {
        final String path = mEditTextDatabaseName.getText().toString();
        final String password = mEditTextDatabasePassword.getText().toString();
        boolean useFingerprint = mUseFingerprintCheckBox.isChecked();
        new OpenDatabaseTask(useFingerprint).execute(path, password);
        if (TextUtils.isEmpty(path)) {
            onError("Path must not be empty");
            return;
        }
        if (TextUtils.isEmpty(password) && !useFingerprint) {
            onError("Passwords must not be empty");
            return;
        }
    }

    private final void closeDatabase() {
        if (mDatabase != null) {
            try {
                mDatabase.close();
                mDatabase = null;
                Log.v(TAG, "Database is closed.");
            } catch (Exception e) {
                Log.e(TAG, "closeDatabase error:", e);
            }
        }
    }

    private final void deleteDatabase() {
        try {
            if (mDatabase != null) {
                mDatabase.close();
                mDatabase = null;
            }
            final String path = mEditTextDatabaseName.getText().toString();
            deleteFileWithException(path);
            deleteFileWithException(path + "-crypto");
            Log.v(TAG, "Database is deleted.");
        } catch (Exception e) {
            Log.e(TAG, "deleteDatabase error:", e);
        }
    }

    private static final void deleteFileWithException(final String path) throws Exception {
        File f = new File(path);
        if (f.exists()) {
            boolean result = f.delete();
            if (!result) {
                throw new Exception("Cannot delete " + path);
            }
        }
    }

    private final void showTables() {
        try {
            if (mDatabase == null) {
                throw new IllegalStateException("Database is not opened.");
            }
            Cursor c = mDatabase.rawQuery("SELECT name FROM sqlite_master WHERE type='table' ORDER BY name", null);
            Log.v(TAG, "--- TABLES ---");
            while (c.moveToNext()) {
                String name = c.getString(0);
                Log.v(TAG, name);
            }
            Log.v(TAG, "--- TABLES END ---");
        } catch (Exception e) {
            Log.e(TAG, "executeSql error:", e);
        }
    }

    private final void executeSql() {
        try {
            if (mDatabase == null) {
                throw new IllegalStateException("Database is not opened.");
            }
            Log.v(TAG, "--- EXECUTE ---");

            final String sql = mEditTextSql.getText().toString();
            final String trimmedSql = sql.trim();
            int n = DatabaseUtils.getSqlStatementType(trimmedSql);
            if (n == DatabaseUtils.STATEMENT_SELECT) {
                AbstractCursor cursor = (AbstractCursor) mDatabase.rawQuery(trimmedSql, null);
                final int columnCount = cursor.getColumnCount();
                StringBuilder c = new StringBuilder();
                c.append("|");
                for (int columnIndex = 0; columnIndex < columnCount; ++columnIndex) {
                    c.append(cursor.getColumnName(columnIndex));
                    c.append("|");
                }
                Log.v(TAG, c.toString());
                Log.v(TAG, "----------------");

                int rowIndex = 0;
                while (cursor.moveToNext()) {
                    if (rowIndex >= MAX_ROWS) {
                        Log.v(TAG, "...");
                        Log.v(TAG, "TOP " + MAX_ROWS + " PRINTED");
                        break;
                    }
                    c = new StringBuilder();
                    c.append("|");
                    for (int columnIndex = 0; columnIndex < columnCount; ++columnIndex) {
                        int type = cursor.getType(columnIndex);
                        switch (type) {
                            case AbstractCursor.FIELD_TYPE_NULL:
                                c.append("<null>");
                                break;
                            case AbstractCursor.FIELD_TYPE_INTEGER:
                                c.append(cursor.getLong(columnIndex));
                                break;
                            case AbstractCursor.FIELD_TYPE_FLOAT:
                                c.append(cursor.getDouble(columnIndex));
                                break;
                            case AbstractCursor.FIELD_TYPE_STRING:
                                c.append(cursor.getString(columnIndex));
                                break;
                            case AbstractCursor.FIELD_TYPE_BLOB:
                                c.append("<blob>");
                                break;
                            default:
                                break;
                        }
                        c.append("|");
                        ++rowIndex;
                    }
                    Log.v(TAG, c.toString());
                }
            } else {
                mDatabase.execSQL(trimmedSql);
            }
            Log.v(TAG, "--- EXECUTE END ---");
        } catch (Exception e) {
            Log.e(TAG, "executeSql error:", e);
        }
    }

    private void addFingerprint() {
        final String path = mEditTextDatabaseName.getText().toString();
        final String password = mEditTextDatabasePassword.getText().toString();
        if (TextUtils.isEmpty(path) || TextUtils.isEmpty(password)) {
            onError("Password and path must not be empty or null");
            return;
        }
        new AddFingerprintTask().execute(path, password);
    }

    private void removeFingerprint() {
        try {
            final String path = mEditTextDatabaseName.getText().toString();
            if (TextUtils.isEmpty(path)) {
                onError("Path must not be empty or null");
                return;
            }
            FingerprintOperationHelper.removeFingerprint(path);
            Toast.makeText(this,
                    "Fingerprint password for file :" + path + "has been removed",
                    Toast.LENGTH_SHORT)
                    .show();
        } catch (FingerprintOperationException e) {
            onError(DataProtectionUtility.getFingerprintOperationErrorMessage(e, this));
        }
    }

    private void updateSecretKey() {
        try {
            FingerprintOperationHelper.updateSecretKey(this);
            Toast.makeText(this, "Secret key has been updated", Toast.LENGTH_SHORT).show();
        } catch (FingerprintOperationException e) {
            onError(DataProtectionUtility.getFingerprintOperationErrorMessage(e, this));
        }
    }

    private void onError(String errorStr) {
        String message = "Error occured: " + errorStr;
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }


    private void dismissFingerprintDialog() {
        DialogFragment fragment = (DialogFragment) getSupportFragmentManager().findFragmentByTag(FINGERPRINT_DIALOG);
        if (fragment != null) {
            fragment.dismiss();
        }
    }

    private void showFingerprintDialog() {
        final FingerprintDialogFragment fragment = new FingerprintDialogFragment();
        fragment.show(getSupportFragmentManager(), FINGERPRINT_DIALOG);
    }

    private void cancelCurrentOperation() {
        if (mOperationController != null) {
            mOperationController.cancelOperation();
            mOperationController = null;
        }
    }

    private class AddFingerprintTask extends AsyncTask<String, Void, Void> {

        private volatile Exception mException;

        @Override
        protected Void doInBackground(String... params) {
            String path = params[0];
            String password = params[1];
            try {
                FingerprintOperationHelper.addFingeprintToDb(TestDatabaseActivity.this,
                        path, password, TestDatabaseActivity.this);
            } catch (FingerprintOperationException | SdkLicenseViolationException | SQLiteException e) {
               mException = e;
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void arg) {
            mOperationController = null;
            if (mException != null) {
                onError(DataProtectionUtility.getFingerprintOperationErrorMessage(mException, TestDatabaseActivity.this));
            } else {
                Toast.makeText(TestDatabaseActivity.this, "Fingerprint password has been added", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class OpenDatabaseTask extends AsyncTask<String, Void, Void> {

        private boolean mUseFingerprint;
        private Exception mException;

        OpenDatabaseTask(boolean useFingerprint) {
            mUseFingerprint = useFingerprint;
        }

        @Override
        protected Void doInBackground(String... params) {
            String path = params[0];
            String password = params[1];
            if (mUseFingerprint) {
                try {
                    mDatabase = SQLiteDatabase.openDatabase(TestDatabaseActivity.this,
                            TestDatabaseActivity.this,
                            path,
                            null,
                            SQLiteDatabase.OPEN_READWRITE,
                            null);
                    return null;
                } catch (SdkLicenseViolationException | FingerprintOperationException | SQLiteException e) {
                    mException = e;
                    return null;
                }
            } else {
                try {
                    mDatabase = SQLiteDatabase.openOrCreateDatabase(path, password, null);
                    Log.v(TAG, "Database is opened.");
                    int version = mDatabase.getVersion();
                    Log.v(TAG, " version = " + version);
                } catch (Exception e) {
                    Log.e(TAG, "openDatabase error:", e);
                    return null;
                }
                return null;
            }
        }


        @Override
        protected void onPostExecute(Void arg) {
            mOperationController = null;
            if (mException != null) {
                onError(DataProtectionUtility.getFingerprintOperationErrorMessage(mException, TestDatabaseActivity.this));
            }
        }
    }
}
