package com.kavsdkexample.ui.device_protection;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import com.kavsdk.appcategorizer.AppCategorizer;
import com.kavsdk.appcategorizer.AppCategory;
import com.kavsdkexample.R;
import com.kavsdkexample.shared.AppStatus;
import com.kavsdkexample.shared.activities.BaseActivity;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class CheckAppCategoryActivity extends BaseActivity {
    private final Handler mHandler = new Handler();
    private final Executor mExecutor = Executors.newCachedThreadPool();

    @Override
    public void initUi(Bundle savedInstanceState) {
        setContentView(R.layout.check_app_category_activity);
        final PackageManager pm = getPackageManager();
        mExecutor.execute(new  Runnable() {
            @Override
            public void run() {
                final List<ApplicationInfo> apps = pm.getInstalledApplications(PackageManager.GET_META_DATA);
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        final AppAdapter adapter = new AppAdapter(CheckAppCategoryActivity.this, apps);
                        final TextView appCategoryTextView = (TextView) findViewById(R.id.appCategoryTextView);
                        ListView appListView = (ListView) findViewById(R.id.appList);
                        appListView.setAdapter(adapter);
                        appListView.setOnItemClickListener(
                                new AppListItemClickListener(
                                        CheckAppCategoryActivity.this,
                                        mHandler,
                                        mExecutor,
                                        adapter,
                                        appCategoryTextView
                                )
                        );
                    }
                });
            }
        });
    }

    @Override
    public void initComponent(AppStatus appStatus, String additionalInfo) {

    }
    
    private static class AppListItemClickListener implements OnItemClickListener {
        private final Context mContext;
        private final Executor mExecutor;
        private final Handler mHandler;
        private final AppAdapter mAdapter;
        private final TextView mAppCategoryTextView;
        
        public AppListItemClickListener(Context context, Handler handler, Executor executor, AppAdapter adapter, TextView appCategoryTextView) {
            mContext = context;
            mHandler = handler;
            mExecutor = executor;
            mAdapter = adapter;
            mAppCategoryTextView = appCategoryTextView;
        }
        
        @Override
        public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
            mAppCategoryTextView.setText("Checking...");
            mExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        ApplicationInfo app = (ApplicationInfo) mAdapter.getItem(position);
                        AppCategorizer appCategorizer = AppCategorizer.getInstance();
                        final AppCategory category = appCategorizer.getCategory(app.packageName);
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (category != null) {
                                    mAppCategoryTextView.setText(category.name());
                                } else {
                                    mAppCategoryTextView.setText("UNKNOWN");
                                }
                            }
                        });
                        
                    } catch (final IOException e) {
                        e.printStackTrace();
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                }
            });
        }
    }
    
    private static class AppAdapter extends BaseAdapter {
        private List<ApplicationInfo> mApplications;
        private Context mContext;
        
        public AppAdapter(Context context, List<ApplicationInfo> applications) {
            mApplications = applications;
            mContext = context;
        }
        
        @Override
        public int getCount() {
            return mApplications.size();
        }

        @Override
        public Object getItem(int position) {
            return mApplications.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View rowView = convertView;
            ViewHolder holder;
            if (rowView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                rowView = inflater.inflate(R.layout.app_item, parent, false);
                holder = new ViewHolder();
                holder.textView = (TextView) rowView.findViewById(R.id.label);
                holder.imageView = (ImageView) rowView.findViewById(R.id.logo);
                rowView.setTag(holder);
            } else {
                holder = (ViewHolder) rowView.getTag();
            }
            PackageManager manager = mContext.getPackageManager();
            ApplicationInfo appInfo = (ApplicationInfo) getItem(position);
            holder.imageView.setImageDrawable(manager.getApplicationIcon(appInfo));
            holder.textView.setText(manager.getApplicationLabel(appInfo));
            return rowView;
        }

        private static class ViewHolder {
            public ImageView imageView;
            public TextView textView;
        }
        
    }
}
