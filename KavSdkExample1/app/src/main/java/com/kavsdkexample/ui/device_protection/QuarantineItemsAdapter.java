package com.kavsdkexample.ui.device_protection;

import com.kavsdk.antivirus.Antivirus;
import com.kavsdk.antivirus.QuarantineException;
import com.kavsdk.antivirus.QuarantineItemInfo;
import com.kavsdkexample.R;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * This is an adapter for showing the quarantine items
 */
public class QuarantineItemsAdapter extends BaseAdapter {   
    private static final String TAG = QuarantineItemsAdapter.class.getSimpleName();
    
    private final LayoutInflater mInflater;
    private final Antivirus mAntivirusComponent;

    public QuarantineItemsAdapter(Context context, Antivirus kavComponent) {
        mInflater = LayoutInflater.from(context);
        mAntivirusComponent = kavComponent;
    }
 
    @Override
    public int getCount() {
        int count = 0;
        try {
            count = (int) mAntivirusComponent.getQuarantineObjectsCount();
        } catch (QuarantineException e) {
            e.printStackTrace();
            Log.d(TAG, "Quarantine objects counting failed!");
        }
        return count;           
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mInflater.inflate(android.R.layout.simple_list_item_1, null);              
        }
        final TextView textView = (TextView) convertView.findViewById(android.R.id.text1);
        String str = "";
        try {
            int count = (int) mAntivirusComponent.getQuarantineObjectsCount();
            if (count > 0) {
                    final QuarantineItemInfo info = mAntivirusComponent.getQuarantineItems(0, count - 1).get(position);
                    StringBuilder sb = new StringBuilder();
                    sb.append("File name: ");
                    sb.append(info.mFileName);
                    sb.append("; Path: ");
                    sb.append(info.mFilePath);
                    sb.append("; ");
                    sb.append(info.mVirusName);
                    sb.append("; Date: ");
                    sb.append(info.mCatchDateStr);
                    str = sb.toString();
                }
        } catch (QuarantineException e) {
            str = null;
            Log.d(TAG, "Quarantine objects counting failed!");
        }
        textView.setText(str != null ? str : textView.getContext().getString(R.string.str_quarantine_error));

        return convertView;
    }     
}
