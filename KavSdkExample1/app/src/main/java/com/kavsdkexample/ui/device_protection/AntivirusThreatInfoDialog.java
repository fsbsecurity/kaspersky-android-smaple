package com.kavsdkexample.ui.device_protection;

import android.widget.ArrayAdapter;

import com.kavsdk.antivirus.info.ThreatDescription;
import com.kavsdkexample.R;
import com.kavsdkexample.shared.dialogs.BaseSimpleDialog;

import java.util.ArrayList;
import java.util.List;

public class AntivirusThreatInfoDialog extends BaseSimpleDialog {

    @Override
    protected String check() {
        final String virusName = mTextView.getText().toString();
        String result = ThreatDescription.getThreatDescription(virusName);
        if (result == null) {
            result = mContext.getString(R.string.str_antivitus_threat_info_dialog_enter_virus_name_result_no_description);
        }
        return result;
    }

    @Override
    protected int getHintTextId() {
        return R.string.str_antivitus_threat_info_dialog_enter_virus_name;
    }

    @Override
    protected ArrayAdapter<String> getAdapter() {
        List<String> virusNames = new ArrayList<String>();
        virusNames.add("Trojan-SMS.AndroidOS.FakeInst.a");
        virusNames.add("Backdoor.AndroidOS.Glodream.e");
        virusNames.add("Trojan-PSW.AndroidOS.FakeNefix.c");
        virusNames.add("Worm.AndroidOS.Smspacem.b");
        return new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line, virusNames);
    }

    @Override
    protected int getTitleId() {
        return R.string.str_antivirus_threat_description_dialog;
    }
}
