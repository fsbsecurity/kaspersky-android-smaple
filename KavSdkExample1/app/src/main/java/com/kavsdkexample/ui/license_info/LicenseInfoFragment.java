package com.kavsdkexample.ui.license_info;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.kavsdk.KavSdk;
import com.kavsdk.license.SdkLicense;
import com.kavsdk.license.SdkLicenseException;
import com.kavsdkexample.R;
import com.kavsdkexample.shared.AppStatus;
import com.kavsdkexample.ui.SdkMainActivity;
import com.kavsdkexample.app.SdkSampleApplication;
import com.kavsdkexample.shared.BaseFragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Demonstration of using of activation.
 * 
 */
public class LicenseInfoFragment extends BaseFragment {
    private static final String TAG = LicenseInfoFragment.class.getSimpleName();
    private Button mButton;
    private TextView mCodeTextView;
    private EditText mCodeEditText;
    private TextView mErrorMessageTextView;
    private TextView mExpiredDateTextView;
    private TextView mHashOfHardwareIdTextView;
    private TextView mInstallationIdTextView;

    @Override
    public View initUi(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.license_info_fragment, container, false);
        mButton = (Button) view.findViewById(R.id.button);
        mCodeTextView = (TextView) view.findViewById(R.id.textCode);
        mCodeEditText = (EditText) view.findViewById(R.id.editCode);
        mErrorMessageTextView = (TextView) view.findViewById(R.id.errorMessage);
        mExpiredDateTextView = (TextView) view.findViewById(R.id.expired);
        mHashOfHardwareIdTextView = (TextView) view.findViewById(R.id.hashOfHardwareId);
        mInstallationIdTextView = (TextView) view.findViewById(R.id.installationId);
        return view;
    }

    @Override
    public void initComponent(AppStatus appStatus, String additionalInfo) {
        final SdkSampleApplication app = ((SdkSampleApplication) getActivity().getApplication());
        final SdkMainActivity activity = (SdkMainActivity) getActivity();
        final boolean needNewCode = app.isNeedNewCode();

        mButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                case R.id.button:
                    if (needNewCode) {
                        executeActivate();
                    } else {
                        mInitializer.init(app, activity);
                    }
                    break;
                default:
                    break;
                }
            }
        });

        SdkLicense license = KavSdk.getLicense();
        if (license.isValid()) {
            long expiredTimestamp = license.getLicenseKeyExpireDate() * 1000;
            if (expiredTimestamp == 0) {
                mExpiredDateTextView.setText(R.string.license_not_available);
            } else {
                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
                mExpiredDateTextView.setText(sdf.format(new Date(expiredTimestamp)));
            }
            mCodeTextView.setVisibility(View.GONE);
            mCodeEditText.setVisibility(View.GONE);
            mButton.setVisibility(View.GONE);
            updateResultInfoLabel(getString(R.string.license_successful_activation), true);
        } else {
            mExpiredDateTextView.setText(R.string.license_not_available);
            updateResultInfoLabel(activity.getLastError(), false);
            boolean clientUserIDRequired = license.isClientUserIDRequired();
            if (!needNewCode) {
                mCodeTextView.setVisibility(View.GONE);
                mCodeEditText.setVisibility(View.GONE);
                mButton.setText(R.string.license_retry);
            } else if (clientUserIDRequired) {
                mCodeTextView.setText(R.string.str_enter_code_user_id_label);
                mCodeEditText.setText(R.string.str_enter_code_user_id_name);
                mCodeEditText.setHint(R.string.str_enter_code_user_id_hint);
                mButton.setText(R.string.str_enter_code_send);
            } else {
                mCodeTextView.setText(R.string.str_enter_code_activation_code_label);
                mCodeEditText.setText(R.string.str_enter_code_activation_code_name);
                mCodeEditText.setHint(R.string.str_enter_code_activation_code_hint);
                mButton.setText(R.string.str_enter_code_activate);
            }
        }

        mHashOfHardwareIdTextView.setText(KavSdk.getHashOfHardwareId());
        mInstallationIdTextView.setText(KavSdk.getInstallationId());
    }

    private void executeActivate() {
        try {
            Log.d(TAG, "Start");
            activate();
        } catch (Exception e) {
            updateResultInfoLabel(e.getMessage(), false);
            Log.e(TAG, "InitFailed", e);
        }
    }

    private void activate() {
        final String code = mCodeEditText.getText().toString();
        ((SdkMainActivity)getActivity()).showProgressDialog();
        new Thread() {
            @Override
            public void run() {
                activate(code);
            }
        }.start();
    }

    private void activate(String code) {
        try {
            SdkLicense license = KavSdk.getLicense();
            if (license.isClientUserIDRequired()) {
                license.sendClientUserID(code);
            } else {
                license.activate(code);
            }
            onActivationResult(null);
        } catch (SdkLicenseException e) {
            onActivationResult(e);
        }
    }
    
    private void onActivationResult(final SdkLicenseException exception) {
        if (exception == null) {
            Log.d(TAG, "Inited");
        } else {
            Log.e(TAG, "InitFailed: " + exception.getClass().getSimpleName() + " " + exception.getMessage());
        }
        final SdkMainActivity activity = (SdkMainActivity)getActivity();
        final SdkSampleApplication app = (SdkSampleApplication)getActivity().getApplication();
        activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    activity.hideProgressDialog();
                    if (exception != null) {
                        updateResultInfoLabel(exception.getMessage(), false);
                    } else {
                        mInitializer.init(app, activity);
                    }
                }
         });

    }
    
    private void updateResultInfoLabel(final CharSequence text, boolean success) {
        mErrorMessageTextView.setText(text);
        mErrorMessageTextView.setTextColor(getResources().getColor(success? R.color.success: R.color.error));
    }
}
