package com.kavsdkexample.ui.plugins;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.kavsdkexample.R;
import com.kavsdkexample.ui.NoticeDialogFragment;

public class AddStasticTargetFragmentDialog extends NoticeDialogFragment {

    private  EditText mUrlView;
    private EditText mCertView;
    private EditText mConsumerView;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View rootView = inflater.inflate(R.layout.dialog_kfp_add_statistic_target, null);
        mUrlView = (EditText) rootView.findViewById(R.id.kfp_dialog_url);
        mCertView = (EditText) rootView.findViewById(R.id.kfp_dialog_cert);
        mConsumerView = (EditText) rootView.findViewById(R.id.kfp_dialog_consumer);
        builder.setView(rootView);
        builder.setPositiveButton(R.string.ok_button_text, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mListener.onDialogPositiveClick(AddStasticTargetFragmentDialog.this);
                    }
                });
        builder.setNegativeButton(R.string.cancel_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mListener.onDialogNegativeClick(AddStasticTargetFragmentDialog.this);
            }
        });
        return builder.create();
    }

    public String getUrl() {
        return mUrlView.getText().toString();
    }

    public String getBase64Cert() {
        return mCertView.getText().toString();
    }
    public String getConsumerId() {
        return mConsumerView.getText().toString();
    }

}
