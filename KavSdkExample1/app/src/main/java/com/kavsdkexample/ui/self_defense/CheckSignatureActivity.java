package com.kavsdkexample.ui.self_defense;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import com.kavsdk.license.SdkLicenseViolationException;
import com.kavsdk.protection.Protection;
import com.kavsdkexample.R;
import com.kavsdkexample.Utils;
import com.kavsdkexample.shared.AppStatus;
import com.kavsdkexample.shared.activities.BaseActivity;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

/**
 * This is a sample activity for demonstrating the signature checker functionality.
 * It allows to check the application signature and shows the result
 */
public class CheckSignatureActivity extends BaseActivity implements OnClickListener {

    /**
     * A file name of a debug certificate, which is located in assets.
     * Note: For a 'real' application the certificate should be located in a safe place.
     * It will be a good choice to retrieve the certificate from a server at runtime.  
     */
    private static final String CERTIFICATE_FILE_NAME = "android_debug.crt";
    
    /* GUI controls */
    private TextView mResultTextView;
    private Button mCheckButton;

    @Override
    public void initUi(Bundle savedInstanceState) {
        setContentView(R.layout.check_signature_activity);

        mResultTextView = (TextView) findViewById(R.id.resultTextView);
        mCheckButton =    (Button)   findViewById(R.id.performCheckButton);

        mCheckButton.setOnClickListener(this);
    }

    @Override
    public void initComponent(AppStatus appStatus, String additionalInfo) {

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == mCheckButton.getId()) {
            mCheckButton.setEnabled(false);
            SignatureCheckTask task = new SignatureCheckTask(this);
            task.execute(CERTIFICATE_FILE_NAME);
        }
    }
    
    private void showResults(String result) {
        mResultTextView.setText(result);
        mCheckButton.setEnabled(true);
    }

    /**
     * A class for processing signature checking in background
     */
    private static class SignatureCheckTask extends AsyncTask<String, Void, String> {

        // A weak reference to not force the activity to live
        private final WeakReference<CheckSignatureActivity> mActivityRef;
        
        public SignatureCheckTask(CheckSignatureActivity activity) {
            mActivityRef = new WeakReference<CheckSignatureActivity>(activity);
        }
        
        @Override
        protected String doInBackground(String... filenames) {
            
            try {
                CheckSignatureActivity activity = mActivityRef.get();
                if (activity == null) {
                    cancel(true);
                    return null;
                }
                
                // Getting the certificate from assets
                X509Certificate supposedCert = readCertificateFromAssets(activity, filenames[0]);
                
                Protection service = new Protection(activity);
                
                // Comparing the application signature to the certificate
                return Utils.convertErrorToString(activity, service.verifyAppSignature(supposedCert));
                
            } catch (SdkLicenseViolationException e) {
                return"KAV_LICENSE_VIOLATION";
                
            } catch (CertificateException e) {
                return "Wrong certificate";
                
            } catch (IOException e) {
                return "Error reading certificate from assets";
            }
        }
        
        /* Shows the result */
        @Override
        protected void onPostExecute(String result) {
            // Checking the liveness of an owner activity. 
            // Perhaps there is no need to show the result
            CheckSignatureActivity activity = mActivityRef.get();
            if (activity != null) {
                activity.showResults(result);
            }
        }

        /*
         * Reads the certificate from assets
         */
        private X509Certificate readCertificateFromAssets(Context context, String filename) 
            throws IOException, CertificateException {

            BufferedInputStream in = new BufferedInputStream(context.getAssets().open(filename));
            try {
                CertificateFactory factory = CertificateFactory.getInstance("X509");
                X509Certificate cert = (X509Certificate) factory.generateCertificate(in);
                return cert;
            } finally {
                in.close();
            }
        }
    }
}
