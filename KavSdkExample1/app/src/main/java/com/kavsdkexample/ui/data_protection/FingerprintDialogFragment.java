package com.kavsdkexample.ui.data_protection;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import com.kavsdkexample.R;
import com.kavsdkexample.ui.NoticeDialogFragment;

public class FingerprintDialogFragment extends NoticeDialogFragment {


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View rootView = inflater.inflate(R.layout.dialog_fingerprint_input, null);
        builder.
                setView(rootView).
                setNegativeButton(R.string.cancel_button, this)
                .setTitle(R.string.fingerprint_dialog_title);
        return builder.create();
    }


    @Override
    public void onCancel(DialogInterface dialog) {
        mListener.onDialogNegativeClick(this);
        super.onCancel(dialog);

    }
}
