package com.kavsdkexample.ui.device_protection;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.kavsdk.antivirus.ScannerConstants;
import com.kavsdkexample.R;
import com.kavsdkexample.Utils;
import com.kavsdkexample.app.SdkSampleApplication;
import com.kavsdkexample.samples.AntivirusSample;
import com.kavsdkexample.shared.AppStatus;
import com.kavsdkexample.shared.activities.DialogFragmentActivity;
import com.kavsdkexample.shared.dialogs.BaseDialogFragment;
import com.kavsdkexample.shared.dialogs.DialogsSupplier;
import com.kavsdkexample.shared.dialogs.OnDialogFragmentResultListener;
import com.kavsdkexample.storage.Settings;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ScannerActivity extends DialogFragmentActivity implements OnClickListener,
                                                           OnDialogFragmentResultListener, AntivirusSample.Listener, NumberPicker.OnValueChangeListener {
    private static final String TAG = ScannerActivity.class.getSimpleName();
    public static final int MIN_THREADS_COUNT = 1;
    public static final int MAX_THREADS_COUNT = 10;

    private final AntivirusSample.Settings mSampleAntivirusSettings = new AntivirusSample.Settings();
    private AntivirusSample mAntivirusSample;
    
    private String mPrevDialogTag;
    
    private View mStartScanCommandView;
    private View mPauseScanCommandView;
    
    private TextView mStartScanCommandCaptionView;
    private TextView mPauseScanCommandCaptionView;

    private RadioButton mScanMemory;
    private RadioButton mScanAll;
    private RadioButton mScanInstalledApps;
    private RadioButton mScanInstalledApp;
    private RadioButton mDeleteIfNotCuredRadioButton;
    private RadioButton mQuarantineIfNotCuredRadioButton;
    private RadioButton mScanSelectedFolder;
    private RadioButton mScanFile;

    private CheckBox mMultithreadScanCheckbox;
    private CheckBox mMultithreadScanAddAppsCheckbox;
    private NumberPicker mExploringThreadsCountPicker;
    private NumberPicker mScanningThreadsCountPicker;
    
    private ScanResultsDialog mResultDialog;
    private long mStartTime;
    private boolean mNeedShowResultDialog;
    private boolean mIsActivityResumed;
    
    private File mSelectedFile;
    private File mSelectedFolder;

    private List<String> mExternalStoragePaths;
    
    public static Intent getStartIntent(Context context) {
        return new Intent(context, ScannerActivity.class);
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mAntivirusSample != null) {
            mAntivirusSample.removeListener(this);
        }
    }

    @Override
    public void initUi(Bundle savedInstanceState) {
        setContentView(R.layout.scanner_activity);

        final View view = getWindow().getDecorView();
        mResultDialog = new ScanResultsDialog();
        
        // Selector of an action for non-cured files
        mDeleteIfNotCuredRadioButton = Utils.initRadioButton(view, R.id.deleteIfNotCured, this);
        mQuarantineIfNotCuredRadioButton = Utils.initRadioButton(view, R.id.quarantineIfNotCured, this);
        RadioButton skipIfNotCuredRadioButton = Utils.initRadioButton(view, R.id.skipIfNotCured, this);
        RadioButton checkedRadioButton = null;    
        switch (Settings.getActionIfNotCured(this, Settings.QUARANTINE_THREAT_ACTION)) {
        case Settings.DELETE_THREAT_ACTION:
            checkedRadioButton = mDeleteIfNotCuredRadioButton;
            break;
        case Settings.QUARANTINE_THREAT_ACTION:
            checkedRadioButton = mQuarantineIfNotCuredRadioButton;
            break;
        case Settings.SKIP_THREAT_ACTION:
            checkedRadioButton = skipIfNotCuredRadioButton;
            break;
        default:
            break;
        }
        if (checkedRadioButton != null) {
            checkedRadioButton.setChecked(true);
        }

        // Selector of a memory partition to scan
        mScanMemory = Utils.initRadioButton(view, R.id.ScanMemory, this);
        mScanSelectedFolder = Utils.initRadioButton(view, R.id.ScanSelectedFolder, this);
        mScanFile = Utils.initRadioButton(view, R.id.ScanFile, this);
        RadioGroup rg = (RadioGroup)view.findViewById(R.id.ObjectsForScan);
        mExternalStoragePaths = com.kavsdk.utils.Utils.getStoragePaths(Integer.MAX_VALUE);
        int i = 1;
        Map<String, RadioButton> cardButtons = new HashMap<>();
        for (final String path : mExternalStoragePaths) {
            RadioButton rb = (RadioButton) getLayoutInflater().inflate(R.layout.radiobutton_template, null);
            cardButtons.put(path, rb);
            rb.setText(String.format(getString(R.string.str_antivirus_sd_card_name), i, path));
            rb.setSaveEnabled(false);
            
            rg.addView(rb);
            rb.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSampleAntivirusSettings.mMemoryPartition = AntivirusSample.SCAN_SD_CARD;
                    mSampleAntivirusSettings.mPathToScan = path;
                    Settings.setScanAction(ScannerActivity.this, AntivirusSample.SCAN_SD_CARD);
                    Settings.setSdCardPathForScan(ScannerActivity.this, path);
                }
            });
            i++;
        }
        
        mScanAll = Utils.initRadioButton(view, R.id.ScanAll, this);
        mScanInstalledApps = Utils.initRadioButton(view, R.id.ScanInstalledAppsRadio, this);
        mScanInstalledApp = Utils.initRadioButton(view, R.id.ScanInstalledAppRadio, this);

        checkedRadioButton = null;
        mSampleAntivirusSettings.mMemoryPartition = Settings.getScanAction(ScannerActivity.this, AntivirusSample.SCAN_PHONE_MEMORY);
        mSampleAntivirusSettings.mPathToScan = Settings.getSdCardPathForScan(ScannerActivity.this, "/");
        switch (mSampleAntivirusSettings.mMemoryPartition) {
        case AntivirusSample.SCAN_PHONE_MEMORY:
            checkedRadioButton = mScanMemory;
            break;
        case AntivirusSample.SCAN_SD_CARD:
            checkedRadioButton = cardButtons.get(mSampleAntivirusSettings.mPathToScan);
            if (checkedRadioButton == null) {
                mSampleAntivirusSettings.mMemoryPartition = AntivirusSample.SCAN_PHONE_MEMORY;
                checkedRadioButton = mScanMemory;
            }
            break;
        case AntivirusSample.SCAN_SELECTED_FOLDER:
            checkedRadioButton = mScanSelectedFolder;
            mScanSelectedFolder.setText(getString(R.string.str_antivirus_scan_selected_folder)
                    + mSampleAntivirusSettings.mPathToScan);
            break;
        case AntivirusSample.SCAN_ALL_FILES:
            checkedRadioButton = mScanAll;
            break;
        case AntivirusSample.SCAN_INSTALLED_APPS:
            checkedRadioButton = mScanInstalledApps;
            break;
        case AntivirusSample.SCAN_INSTALLED_APP:
            checkedRadioButton = mScanInstalledApp;
            mScanInstalledApp.setText(getString(R.string.str_antivirus_scan_installed_application)
                    + mSampleAntivirusSettings.mPathToScan);
            break;
        case AntivirusSample.SCAN_FILE:
            checkedRadioButton = mScanFile;
            mScanFile.setText(getString(R.string.str_antivirus_scan_file)
                    + mSampleAntivirusSettings.mPathToScan);
            break;
        default:
            break;
        }
        if (checkedRadioButton != null) {
            checkedRadioButton.setChecked(true);
        }
        mStartScanCommandView = view.findViewById(R.id.StartScan);
        mStartScanCommandView.setEnabled(true);
        mStartScanCommandView.setOnClickListener(this);
        mStartScanCommandCaptionView = (TextView) mStartScanCommandView.findViewById(R.id.commandCaption);
        mStartScanCommandCaptionView.setText(R.string.str_antivirus_start_scan_button);
        
        mPauseScanCommandView = view.findViewById(R.id.PauseScan);
        mPauseScanCommandView.setEnabled(false);
        mPauseScanCommandView.setOnClickListener(this);
        mPauseScanCommandCaptionView = (TextView) mPauseScanCommandView.findViewById(R.id.commandCaption);
        mPauseScanCommandCaptionView.setText(R.string.str_antivirus_pause_scan_button);

        initMultiScanUi();
    }

    @Override
    public void initComponent(AppStatus status, String additionalInfo) {
        final SdkSampleApplication app = ((SdkSampleApplication)getApplication());

        mAntivirusSample = app.getService().getAntivirusSample();
        mSampleAntivirusSettings.mScanMode = Settings.getOdsScanMode(this, mSampleAntivirusSettings.mScanMode);
        mSampleAntivirusSettings.mCleanMode = Settings.getOdsCleanMode(this, mSampleAntivirusSettings.mCleanMode);

        mAntivirusSample.addListener(this);

        if (!mAntivirusSample.isStopped()) {
            mStartScanCommandCaptionView.setText(R.string.str_antivirus_cancel_scan_button);
        }
        
        final View view = getWindow().getDecorView();


        Utils.initCheckBox(view, R.id.ScanCheckExe, true,
                (mSampleAntivirusSettings.mScanMode & ScannerConstants.SCAN_MODE_ONLYEXECUTABLE) != 0, this);
        Utils.initCheckBox(view, R.id.ScanCheckArchives, true,
                (mSampleAntivirusSettings.mScanMode & ScannerConstants.SCAN_MODE_ARCHIVED) != 0, this);
        Utils.initCheckBox(view, R.id.ScanUds, true,
                (mSampleAntivirusSettings.mScanMode & ScannerConstants.SCAN_MODE_ALLOW_UDS) != 0, this);
        Utils.initCheckBox(view, R.id.ScanSkipRiskwareAdware, true,
                (mSampleAntivirusSettings.mScanMode & ScannerConstants.SCAN_MODE_SKIP_RISKWARE_ADWARE) != 0, this);
        Utils.initCheckBox(view, R.id.ScanDeepScan, true,
                (mSampleAntivirusSettings.mScanMode & ScannerConstants.SCAN_MODE_DEEP_SCAN_ENABLED) != 0, this);
        Utils.initCheckBox(view, R.id.ScanTryCure, true,
                mSampleAntivirusSettings.mCleanMode == ScannerConstants.CLEAN_MODE_CLEAN, this);
        Utils.initCheckBox(view, R.id.ScanSuspicious, true,
                (mSampleAntivirusSettings.mScanMode & ScannerConstants.SCAN_MODE_DETECT_SUSPICIOUS) != 0, this);
    }

    /*
     * Starts the scanning process. 
     * Invoked by pressing "Scan" button
     */
    private void processScanButtonClick() {

        if (mAntivirusSample.isStopped()) {

            if (!processScanSettings()) {
                return;
            }

            mPauseScanCommandView.setEnabled(true);
            mStartScanCommandView.setEnabled(false);

            mStartTime = System.currentTimeMillis();
            mAntivirusSample.startScan(mSampleAntivirusSettings);

        } else {
            mAntivirusSample.stopScan();
            mStartScanCommandView.setEnabled(false);
            mPauseScanCommandView.setEnabled(false);
            mPauseScanCommandCaptionView.setText(R.string.str_antivirus_pause_scan_button);
        }
    }

    private boolean processScanSettings() {

        mSampleAntivirusSettings.mDeleteIfNotCured = mDeleteIfNotCuredRadioButton.isChecked();
        mSampleAntivirusSettings.mQuarantineIfNotCured = mQuarantineIfNotCuredRadioButton.isChecked();

        if (mSampleAntivirusSettings.mMemoryPartition == AntivirusSample.SCAN_INSTALLED_APPS) {
            //This setting is ignored for scanning installed applications
            mSampleAntivirusSettings.mCleanMode = ScannerConstants.CLEAN_MODE_DONOTCLEAN;
        }

        boolean modeScanFile = (mSampleAntivirusSettings.mMemoryPartition == AntivirusSample.SCAN_FILE);
        boolean modeScanFolder = (mSampleAntivirusSettings.mMemoryPartition == AntivirusSample.SCAN_SELECTED_FOLDER);

        if (modeScanFile || modeScanFolder) {
            File pathToScan = new File(mSampleAntivirusSettings.mPathToScan);
            boolean settingsValid = false;

            if (!pathToScan.exists()) {
                showError(getString(R.string.str_antivirus_path_not_exist, pathToScan.getAbsolutePath()));
            } else if (modeScanFile && !pathToScan.isFile()) {
                showError(getString(R.string.str_antivirus_path_not_file, pathToScan.getAbsolutePath()));
            } else if (modeScanFolder && !pathToScan.isDirectory()) {
                showError(getString(R.string.str_antivirus_path_not_folder, pathToScan.getAbsolutePath()));
            } else {
                settingsValid = true;
            }

            return settingsValid;
        }

        return true;
    }

    private void showError(String errorMsg) {
        Toast.makeText(this, errorMsg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        mIsActivityResumed = true;

        if (mNeedShowResultDialog) {
            mResultDialog.show(getSupportFragmentManager(), null);
            mNeedShowResultDialog = false;
        }
    }
    
    @Override
    protected void onPause() {
        super.onPause();
        mIsActivityResumed = false;
    }

    @Override
    public void onStartScan() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mStartScanCommandCaptionView.setText(R.string.str_antivirus_cancel_scan_button);
                mStartScanCommandView.setEnabled(true);
            }
        });
    }

    @Override
    public void onStopScan() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mPauseScanCommandView.setEnabled(false);

                mStartScanCommandCaptionView.setText(R.string.str_antivirus_start_scan_button);

                mResultDialog.setResults(mAntivirusSample.getScanResults(), mStartTime);

                if (mIsActivityResumed) {
                    mResultDialog.show(getSupportFragmentManager(), null);
                } else {
                    mNeedShowResultDialog = true;
                }

                boolean scanApps = (mSampleAntivirusSettings.mMultithreaded && mSampleAntivirusSettings.mMultithreadedAddApps) ||
                        mSampleAntivirusSettings.mMemoryPartition == AntivirusSample.SCAN_INSTALLED_APPS ||
                        mSampleAntivirusSettings.mMemoryPartition == AntivirusSample.SCAN_INSTALLED_APP;

                if (!mAntivirusSample.getDetectedApplicationList().isEmpty() && scanApps) {
                    startActivity(DetectedApplicationsActivity.newStartIntent(ScannerActivity.this));
                }

                mStartScanCommandView.setEnabled(true);
            }
        });
    }

    @Override
    public void onQuarantine() {
        //empty
    }
    
    private File getDefaultSelectedDir() {
        if (mExternalStoragePaths.isEmpty()) {
            return getFilesDir();
        }

        for (String path : mExternalStoragePaths) {
            if (path.startsWith("/sdcard")) {
                return new File(path);
            }
        }

        return new File(mExternalStoragePaths.get(0));
    }

    protected void doSelectFolder() {
        File initDir = mSelectedFolder != null ? mSelectedFolder : getDefaultSelectedDir();
        DirectoryChooserFragment.show(this, initDir, DirectoryChooserFragment.DIRECTORY_MODE, DirectoryChooserFragment.DIRECTORY_MODE);
    }
    
    protected void doSelectFile() {
        File initDir = mSelectedFile != null ? mSelectedFile.getParentFile() : getDefaultSelectedDir();

        DirectoryChooserFragment.show(this, initDir, DirectoryChooserFragment.FILE_MODE, DirectoryChooserFragment.FILE_MODE);
    }
    
    protected void doSelectApplication() {
        ApplicationChooserFragment.show(this);
    }

    @Override
    public void onDialogFragmentResult(BaseDialogFragment dialog, Object data) {
        if (dialog instanceof DirectoryChooserFragment) {
            String path = ((File) data).getAbsolutePath();
            switch (dialog.getRequestId()) {
                case DirectoryChooserFragment.DIRECTORY_MODE:
                    mSelectedFolder = (File)data;
                    mScanSelectedFolder.setText(getString(R.string.str_antivirus_scan_selected_folder) + path);
                    mSampleAntivirusSettings.mMemoryPartition = AntivirusSample.SCAN_SELECTED_FOLDER;
                    mSampleAntivirusSettings.mPathToScan = path;
                    Settings.setScanAction(this, AntivirusSample.SCAN_SELECTED_FOLDER);
                    Settings.setSdCardPathForScan(this, path);
                    break;
                case DirectoryChooserFragment.FILE_MODE:
                    mSelectedFile = (File)data;
                    mScanFile.setText(getString(R.string.str_antivirus_scan_file) + path);
                    mSampleAntivirusSettings.mMemoryPartition = AntivirusSample.SCAN_FILE;
                    mSampleAntivirusSettings.mPathToScan = path;
                    Settings.setScanAction(this, AntivirusSample.SCAN_FILE);
                    Settings.setSdCardPathForScan(this, path);
                    break;
                default:
                    break;
            }
        } else if (dialog instanceof ApplicationChooserFragment) {
            String appPackage = data.toString();
            mScanInstalledApp.setText(getString(R.string.str_antivirus_scan_installed_application) + appPackage);
            mSampleAntivirusSettings.mMemoryPartition = AntivirusSample.SCAN_INSTALLED_APP;
            mSampleAntivirusSettings.mPathToScan = appPackage;
            Settings.setScanAction(this, AntivirusSample.SCAN_INSTALLED_APP);
            Settings.setSdCardPathForScan(this, appPackage);
        }

    }
    
    @Override
    public void onClick(View v) {
        String scanCheckboxLogString = null;
        
        switch (v.getId()) {
            case R.id.MultithreadedScan:
                updateMultiScanUi(((CheckBox) v).isChecked());
                break;
            case R.id.MultithreadedScanAddApplications:
                mSampleAntivirusSettings.mMultithreadedAddApps = ((CheckBox) v).isChecked();
                Settings.setMultithreadedModeAddApps(this, mSampleAntivirusSettings.mMultithreadedAddApps);
                break;
            case R.id.ScanCheckExe:
                // Updating of "check only executables while scanning" check box
                // Note: flags ScannerConstants.SCAN_MODE_ONLYEXECUTABLE
                // and ScannerConstants.SCAN_MODE_ARCHIVED_ONLYEXECUTABLE
                // are dependable
                if (((CheckBox) v).isChecked()) {
                    mSampleAntivirusSettings.mScanMode |= ScannerConstants.SCAN_MODE_ONLYEXECUTABLE;
                    if ((mSampleAntivirusSettings.mScanMode & ScannerConstants.SCAN_MODE_ARCHIVED) != 0) {
                        mSampleAntivirusSettings.mScanMode |= ScannerConstants.SCAN_MODE_ARCHIVED_ONLYEXECUTABLE;
                    }
                } else {
                    mSampleAntivirusSettings.mScanMode &= ~(ScannerConstants.SCAN_MODE_ONLYEXECUTABLE | ScannerConstants.SCAN_MODE_ARCHIVED_ONLYEXECUTABLE);
                }
                scanCheckboxLogString = "'Scan: check only executables' is ";
                Settings.setOdsScanMode(this, mSampleAntivirusSettings.mScanMode);
                break;
            case R.id.ScanCheckArchives:
                // Updating of "check archives while scanning" check box
                // Note: flags ScannerConstants.SCAN_MODE_ONLYEXECUTABLE
                // and ScannerConstants.SCAN_MODE_ARCHIVED_ONLYEXECUTABLE
                // are dependable
                if (((CheckBox) v).isChecked()) {
                    mSampleAntivirusSettings.mScanMode |= ScannerConstants.SCAN_MODE_ARCHIVED;
                    if ((mSampleAntivirusSettings.mScanMode & ScannerConstants.SCAN_MODE_ONLYEXECUTABLE) != 0) {
                        mSampleAntivirusSettings.mScanMode |= ScannerConstants.SCAN_MODE_ARCHIVED_ONLYEXECUTABLE;
                    }
                } else {
                    mSampleAntivirusSettings.mScanMode &= ~(ScannerConstants.SCAN_MODE_ARCHIVED | ScannerConstants.SCAN_MODE_ARCHIVED_ONLYEXECUTABLE);
                }
                scanCheckboxLogString = "'Scan: check archives' is ";
                Settings.setOdsScanMode(this, mSampleAntivirusSettings.mScanMode);
                break;
            case R.id.ScanUds:
                // Updating of "check in cloud while scanning" check box
                if (((CheckBox) v).isChecked()) {
                    mSampleAntivirusSettings.mScanMode |= ScannerConstants.SCAN_MODE_ALLOW_UDS;
                } else {
                    mSampleAntivirusSettings.mScanMode &= ~ScannerConstants.SCAN_MODE_ALLOW_UDS;
                }
                scanCheckboxLogString = "'Scan: check in cloud' is ";
                Settings.setOdsScanMode(this, mSampleAntivirusSettings.mScanMode);
                break;
            case R.id.ScanSkipRiskwareAdware:
                if (((CheckBox) v).isChecked()) {
                    mSampleAntivirusSettings.mScanMode |= ScannerConstants.SCAN_MODE_SKIP_RISKWARE_ADWARE;
                } else {
                    mSampleAntivirusSettings.mScanMode &= ~ScannerConstants.SCAN_MODE_SKIP_RISKWARE_ADWARE;
                }
                scanCheckboxLogString = "'Scan: skip riskware adware' is ";
                Settings.setOdsScanMode(this, mSampleAntivirusSettings.mScanMode);
                break;
            case R.id.ScanDeepScan:
                // Updating of "deep scan" check box
                if (((CheckBox) v).isChecked()) {
                    mSampleAntivirusSettings.mScanMode |= ScannerConstants.SCAN_MODE_DEEP_SCAN_ENABLED;
                } else {
                    mSampleAntivirusSettings.mScanMode &= ~ScannerConstants.SCAN_MODE_DEEP_SCAN_ENABLED;
                }
                scanCheckboxLogString = "'Scan: deep scan is ";
                Settings.setOdsScanMode(this, mSampleAntivirusSettings.mScanMode);
                break;
            case R.id.ScanTryCure:
                // Updating of "try cure while scanning" check box
                if (((CheckBox) v).isChecked()) {
                    mSampleAntivirusSettings.mCleanMode = ScannerConstants.CLEAN_MODE_CLEAN;
                } else {
                    mSampleAntivirusSettings.mCleanMode = ScannerConstants.CLEAN_MODE_DONOTCLEAN;
                }
                scanCheckboxLogString = "'Scan: try cure' is ";
                Settings.setOdsCleanMode(this, mSampleAntivirusSettings.mCleanMode);
                break;

            case R.id.ScanSuspicious:
                if (((CheckBox) v).isChecked()) {
                    mSampleAntivirusSettings.mScanMode |= ScannerConstants.SCAN_MODE_DETECT_SUSPICIOUS;
                } else {
                    mSampleAntivirusSettings.mScanMode &= ~ScannerConstants.SCAN_MODE_DETECT_SUSPICIOUS;
                }
                scanCheckboxLogString = "'Scan: suspicious' is ";
                Settings.setOdsScanMode(this, mSampleAntivirusSettings.mScanMode);
                break;
            case R.id.StartScan:
                processScanButtonClick();
                break;
            case R.id.PauseScan:
                if (!mAntivirusSample.isPaused()) {
                    mAntivirusSample.pauseScan();
                    mPauseScanCommandCaptionView.setText(R.string.str_antivirus_resume_scan_button);
                } else {
                    mAntivirusSample.resumeScan();
                    mPauseScanCommandCaptionView.setText(R.string.str_antivirus_pause_scan_button);
                }
                break;
            case R.id.deleteIfNotCured:
                Settings.setActionIfNotCured(this, Settings.DELETE_THREAT_ACTION);
                break;
            case R.id.quarantineIfNotCured:
                Settings.setActionIfNotCured(this, Settings.QUARANTINE_THREAT_ACTION);
                break;
            case R.id.skipIfNotCured:
                Settings.setActionIfNotCured(this, Settings.SKIP_THREAT_ACTION);
                break;
            case R.id.ScanInstalledAppsRadio:
                mSampleAntivirusSettings.mMemoryPartition = AntivirusSample.SCAN_INSTALLED_APPS;
                Settings.setScanAction(this, AntivirusSample.SCAN_INSTALLED_APPS);
                break;
            case R.id.ScanInstalledAppRadio:
                doSelectApplication();
                break;
            case R.id.ScanMemory:
                mSampleAntivirusSettings.mMemoryPartition = AntivirusSample.SCAN_PHONE_MEMORY;
                Settings.setScanAction(this, AntivirusSample.SCAN_PHONE_MEMORY);
                break;
            case R.id.ScanSelectedFolder:
                doSelectFolder();
                break;
            case R.id.ScanAll:
                mSampleAntivirusSettings.mMemoryPartition = AntivirusSample.SCAN_ALL_FILES;
                Settings.setScanAction(this, AntivirusSample.SCAN_ALL_FILES);
                break;
            case R.id.ScanFile:
                doSelectFile();
                break;
            default:
                break;
        }
        
        if (scanCheckboxLogString != null) {
            Log.d(TAG, scanCheckboxLogString
                    + (((CheckBox) v).isChecked() ? "enabled" : "disabled"));
        }
    }

    @Override
    public String getPrevFragmentDialogTag() {
        return mPrevDialogTag;
    }

    @Override
    public void setPrevFragmentDialogTag(String tag) {
        mPrevDialogTag = tag;        
    }

    @Override
    public DialogFragment onCreateDialogFragment(int dialogId, Bundle bundle) {
        DialogFragment dialog = null;
        
        switch (dialogId) {
        case DialogsSupplier.DIALOG_ANTIVIRUS_CHECK_URL:
            dialog = new AntivirusCheckUrlDialog();
            break;
        case DialogsSupplier.DIALOG_ANTIVIRUS_THREAT_DESCRIPTION:
            dialog = new AntivirusThreatInfoDialog();
            break;
        default:
            break;
        }
        return dialog;
    }

    private void initMultiScanUi() {

        mSampleAntivirusSettings.mMultithreaded = Settings.isMultithreadedModeEnabled(this, false);
        mSampleAntivirusSettings.mMultithreadedAddApps = Settings.isMultithreadedModeAddApps(this, false);

        mSampleAntivirusSettings.mExploringThreadsCount = Settings.getExploringThreadsCount(this, MIN_THREADS_COUNT);
        mSampleAntivirusSettings.mScanningThreadsCount = Settings.getScanningThreadsCount(this, MIN_THREADS_COUNT);


        mMultithreadScanCheckbox = (CheckBox)findViewById(R.id.MultithreadedScan);
        mMultithreadScanCheckbox.setOnClickListener(this);
        mMultithreadScanCheckbox.setChecked(mSampleAntivirusSettings.mMultithreaded);

        mMultithreadScanAddAppsCheckbox = (CheckBox)findViewById(R.id.MultithreadedScanAddApplications);
        mMultithreadScanAddAppsCheckbox.setOnClickListener(this);
        mMultithreadScanAddAppsCheckbox.setChecked(mSampleAntivirusSettings.mMultithreadedAddApps);

        mExploringThreadsCountPicker = (NumberPicker) findViewById(R.id.ExploringThreads);
        mExploringThreadsCountPicker.setOnValueChangedListener(this);
        mExploringThreadsCountPicker.setMaxValue(MAX_THREADS_COUNT);
        mExploringThreadsCountPicker.setMinValue(MIN_THREADS_COUNT);
        mExploringThreadsCountPicker.setValue(mSampleAntivirusSettings.mExploringThreadsCount);

        mScanningThreadsCountPicker = (NumberPicker) findViewById(R.id.ScanningThreads);
        mScanningThreadsCountPicker.setOnValueChangedListener(this);
        mScanningThreadsCountPicker.setMaxValue(MAX_THREADS_COUNT);
        mScanningThreadsCountPicker.setMinValue(MIN_THREADS_COUNT);
        mScanningThreadsCountPicker.setValue(mSampleAntivirusSettings.mScanningThreadsCount);

        updateMultiScanUi(mSampleAntivirusSettings.mMultithreaded);
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
        switch (picker.getId()) {
            case R.id.ExploringThreads:
                mSampleAntivirusSettings.mExploringThreadsCount = newVal;
                Settings.setExploringThreadsCount(this, newVal);
                break;
            case R.id.ScanningThreads:
                mSampleAntivirusSettings.mScanningThreadsCount = newVal;
                Settings.setScanningThreadsCount(this, newVal);
                break;
            default:
                break;
        }
    }

    private void updateMultiScanUi(boolean multiScanEnabled) {

        mSampleAntivirusSettings.mMultithreaded = multiScanEnabled;
        Settings.setMultithreadedModeEnabled(this, multiScanEnabled);

        int unwantedItemsVisibility = multiScanEnabled ? View.GONE : View.VISIBLE;
        int multiscanItemsVisibility = multiScanEnabled ? View.VISIBLE : View.GONE;

        findViewById(R.id.ScanDeepScan).setVisibility(unwantedItemsVisibility);
        findViewById(R.id.ScanTryCure).setVisibility(unwantedItemsVisibility);
        mScanInstalledApps.setVisibility(unwantedItemsVisibility);
        mScanInstalledApp.setVisibility(unwantedItemsVisibility);
        mScanFile.setVisibility(unwantedItemsVisibility);

        mMultithreadScanAddAppsCheckbox.setVisibility(multiscanItemsVisibility);
        findViewById(R.id.ExploringThreadsLayout).setVisibility(multiscanItemsVisibility);
        findViewById(R.id.ScanningThreadsLayout).setVisibility(multiscanItemsVisibility);

        if (multiScanEnabled) {
            mScanAll.setChecked(true);
        }
    }
}
