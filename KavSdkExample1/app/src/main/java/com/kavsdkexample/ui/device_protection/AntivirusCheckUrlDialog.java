package com.kavsdkexample.ui.device_protection;

import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.kavsdk.antivirus.Scanner;
import com.kavsdk.antivirus.ScannerConstants;
import com.kavsdk.antivirus.ScannerEventListener;
import com.kavsdk.antivirus.ThreatInfo;
import com.kavsdk.antivirus.ThreatType;
import com.kavsdk.antivirus.UrlScanParams;
import com.kavsdkexample.R;
import com.kavsdkexample.app.SdkSampleApplication;
import com.kavsdkexample.controls.AutoCompleteTextViewShowingDropDownAlways;
import com.kavsdkexample.shared.dialogs.BaseAsyncOperationDialog;
import com.kavsdkexample.storage.Settings;

import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * This dialog checks the content of an entered URL and
 * shows the result in a toast.
 */
public class AntivirusCheckUrlDialog extends BaseAsyncOperationDialog implements ScannerEventListener {
    
    private static final String DEFAULT_USER_AGENT = System.getProperty("http.agent");
    
    private String mUrl;
    private Scanner mScanner;
    private String mResult;
    
    private TextView mUrlEditText;
    AutoCompleteTextViewShowingDropDownAlways mUserAgentText;
    
    private final Map<String, String> mUserAgentsMap = new LinkedHashMap<String, String>();
    
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setTitle(R.string.str_antivirus_scan_url);
        return dialog;
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mScanner = ((SdkSampleApplication)getActivity().getApplication()).getAntivirus().createScanner();
        
        View view = inflater.inflate(R.layout.antivirus_check_url_dialog, container, false);

        mUrlEditText = (TextView) view.findViewById(R.id.textUrl);
        
        mUserAgentsMap.put(getString(R.string.str_antivirus_check_url_default_user_agent_title), 
                           DEFAULT_USER_AGENT);
        mUserAgentsMap.put(getString(R.string.str_antivirus_check_url_chrome_mobile_user_agent), 
                           getString(R.string.str_antivirus_check_url_chrome_mobile_title));
        mUserAgentsMap.put(getString(R.string.str_antivirus_check_url_chrome_desktop_user_agent), 
                           getString(R.string.str_antivirus_check_url_chrome_desktop_title));
        
        mUserAgentText = (AutoCompleteTextViewShowingDropDownAlways) view.findViewById(R.id.textWithDropDown);
        mUserAgentText.setAdapter(new ArrayAdapter<String>(getActivity(), 
                android.R.layout.simple_dropdown_item_1line, mUserAgentsMap.keySet().toArray(new String[] {})));

        Button checkButton = (Button) view.findViewById(R.id.checkButton);
        checkButton.setOnClickListener(new View.OnClickListener() {         
            @Override
            public void onClick(View v) {
                checkAndShowResult();
            }
        });

        Button cancelButton = (Button) view.findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {            
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });     
        return view;
    }

    @Override
    protected String check() {
        try {
            mUrl = mUrlEditText.getText().toString();
            URL url = new URL(mUrl);
            int scanMode = Settings.getOdsScanMode(mContext, 0);
            
            String userAgentString = mUserAgentText.getText().toString();
            if (userAgentString.isEmpty()) {
                mScanner.scanUrl(url, scanMode, this, true);
                
            } else {
                String predefined = mUserAgentsMap.get(userAgentString);
                if (predefined != null) {
                    userAgentString = predefined;
                }
                mScanner.scanUrl(url, new SampleUrlScanParams(userAgentString), scanMode, this, true);
            }

        } catch (MalformedURLException e) {
            mResult = mContext.getString(R.string.str_antivitus_check_url_dialog_url_is_not_valid);
        } catch (URISyntaxException e) {
            mResult = String.format(mContext.getString(R.string.str_antivitus_check_url_dialog_url_is_not_valid_with_message), e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {           
            mResult = mContext.getString(R.string.str_antivitus_check_url_dialog_connection_error);
            e.printStackTrace();
        }
        return mResult == null ? mContext.getString(R.string.str_antivitus_check_url_dialog_everything_is_ok)
                : mResult;
    }

    @Override
    public int onScanEvent(int eventId, int param, ThreatInfo threat, ThreatType threatType) {
        if (threatType != ThreatType.None) {
            
            mResult = String.format(getString(R.string.str_antivitus_check_url_dialog_smth_detected), 
                                    TextUtils.isEmpty(threat.getVirusName()) ? threat.getObjectName() : threat.getVirusName());
        }
        return ScannerConstants.EVENT_RESULT_OK;
    }

    private static class SampleUrlScanParams implements UrlScanParams {

        private final HttpParams mHttpParams;
        
        SampleUrlScanParams(String userAgent) {
            mHttpParams = new BasicHttpParams();
            mHttpParams.setParameter(CoreProtocolPNames.USER_AGENT, userAgent);
        }
        
        @Override
        public HttpParams getHttpParams() {
            return mHttpParams;
        }
    }
}