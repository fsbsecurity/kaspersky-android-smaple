package com.kavsdkexample.ui;

import com.kavsdk.KavSdk;
import com.kavsdkexample.R;
import com.kavsdkexample.shared.AppStatus;
import com.kavsdkexample.shared.activities.BaseActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ProxyAuthActivity extends BaseActivity {
    private TextView mTextViewLogin;
    private TextView mTextViewPassword;
    private String mLogin;
    private String mPassword;

    @Override
    public void initUi(Bundle savedInstanceState) {
        setContentView(R.layout.proxy_data_dialog);
        Button checkButton = (Button) findViewById(R.id.checkButton);

        mTextViewLogin = (TextView) findViewById(R.id.loginEditText);
        mTextViewPassword = (TextView) findViewById(R.id.passwordEditText);

        checkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLogin = mTextViewLogin.getText().toString();
                mPassword = mTextViewPassword.getText().toString();

                KavSdk.setProxyAuthCredentials(mLogin, mPassword);
                finish();
            }
        });
    }

    @Override
    public void initComponent(AppStatus appStatus, String additionalInfo) {

    }
}
