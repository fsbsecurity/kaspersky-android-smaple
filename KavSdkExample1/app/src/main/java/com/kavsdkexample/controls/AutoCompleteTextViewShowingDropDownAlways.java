package com.kavsdkexample.controls;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.AutoCompleteTextView;

public class AutoCompleteTextViewShowingDropDownAlways extends AutoCompleteTextView {
    
    public AutoCompleteTextViewShowingDropDownAlways(Context context) {
        super(context);
    }
    
    public AutoCompleteTextViewShowingDropDownAlways(Context context, AttributeSet arg1) {
        super(context, arg1);
    }

    public AutoCompleteTextViewShowingDropDownAlways(Context context, AttributeSet arg1, int arg2) {
        super(context, arg1, arg2);
    }

    @Override
    public boolean enoughToFilter() {
        return true;
    }
    
    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);
        if (focused) {
            performFiltering(getText(), 0);
        }
    }
    
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        performFiltering(getText(), 0);
        return super.onTouchEvent(event);
    }
    
}