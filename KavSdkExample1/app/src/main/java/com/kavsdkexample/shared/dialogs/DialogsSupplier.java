package com.kavsdkexample.shared.dialogs;

import java.io.IOException;
import java.util.Set;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.v4.app.DialogFragment;
import android.text.InputType;
import android.text.TextUtils;
import android.widget.EditText;

import com.kaspersky.components.urlchecker.UrlCategory;
import com.kaspersky.components.urlchecker.UrlCategoryExt;
import com.kavsdk.antivirus.Antivirus;
import com.kavsdk.antivirus.QuarantineException;
import com.kavsdk.antivirus.systemsecurity.SystemSecurity;
import com.kavsdk.antivirus.systemsecurity.SystemSecurityCheckResult;
import com.kavsdk.antivirus.systemsecurity.SystemSecurityVerdict;
import com.kavsdk.fingerprint.SmFingerprint;
import com.kavsdk.firmware.FirmwareCheckResult;
import com.kavsdk.firmware.FirmwareChecker;
import com.kavsdk.license.SdkLicenseViolationException;
import com.kavsdk.webfilter.WebFilterControl;
import com.kavsdk.wifi.WifiReputation;
import com.kavsdkexample.R;
import com.kavsdkexample.app.SdkSampleApplication;
import com.kavsdkexample.ui.device_protection.AntivirusCheckUrlDialog;
import com.kavsdkexample.ui.device_protection.QuarantineActivity;
import com.kavsdkexample.ui.device_protection.RootCheckDialog;
import com.kavsdkexample.ui.network_protection.CheckCertificateDialog;
import com.kavsdkexample.ui.network_protection.FinancialCategoryDialog;
import com.kavsdkexample.ui.network_protection.UrlReputationDialog;

/**
 * This is a class responsible for providing dialogs for different situations within the application.
 * All the dialogs are used with their wrappers such as {@link AlertDialogFragment}, {@link com.kavsdkexample.ui.network_protection.CheckCertificateDialog},
 * {@link com.kavsdkexample.ui.network_protection.FinancialCategoryDialog}, etc.
 */
public final class DialogsSupplier {
    /*
     * Dialogs IDs
     */
    public static final int DIALOG_WF_CATEGORY                  = 1;
    public static final int DIALOG_REMOVE_WF_EXCLUDE_ITEM       = 2;
    public static final int DIALOG_ROOT_CHECK                   = 3;
    public static final int DIALOG_WIFI_CHECK                   = 4;
    public static final int DIALOG_CERTIFICATE_CHECK            = 5;
    public static final int DIALOG_REPUTATION_CHECK             = 6;
    public static final int DIALOG_FINANCIAL_CHECK              = 7;
    public static final int DIALOG_USB_DEBUG_CHECK              = 8;
    public static final int DIALOG_NON_MARKET_APPS_CHECK        = 9;
    public static final int DIALOG_PROCESS_QUARANTUNE_ITEM      = 10;
    public static final int DIALOG_ADD_PHONE_NUMBER             = 11;
    public static final int DIALOG_ANTIVIRUS_CHECK_URL          = 12;
    public static final int DIALOG_CHECK_FIRMWARE               = 13;
    public static final int DIALOG_ANTIVIRUS_THREAT_DESCRIPTION = 14;
    public static final int DIALOG_ENTER_CLIENT_USER_ID         = 15;
    public static final int DIALOG_CHECK_SDKSTATUS              = 16;
    public static final int DIALOG_DEVICE_PASSWORD_QUALITY_CHECK= 17;
    public static final int DIALOG_ENTER_NEW_PROXY_PORT         = 18;
    public static final int DIALOG_REPUTATION_EXT_CHECK         = 19;

    /* A list of all available URL categories */
    static final UrlCategory[] WF_CATEGORIES = new UrlCategory[]
            {
                UrlCategory.AdultContent,
                UrlCategory.IllegalSoft,
                UrlCategory.AlcoholTobaccoNarcotics,
                UrlCategory.Violence,
                UrlCategory.Profanity,
                UrlCategory.Weapons,
                UrlCategory.Gambling,
                UrlCategory.ChatsForumsAndIM,
                UrlCategory.WebMail,
                UrlCategory.ShopsAndAuctions,
                UrlCategory.SocialNet,
                UrlCategory.Recruitment,
                UrlCategory.Anonymizers,
                UrlCategory.Payments,
                UrlCategory.CasualGames,
                UrlCategory.Counterfeit,
                UrlCategory.SoftwareAudioVideo,
                UrlCategory.GamblingLotteriesSweepstakes,
                UrlCategory.InternetCommunicationMedia,
                UrlCategory.ElectronicCommerce,
                UrlCategory.ComputerGames,
                UrlCategory.ReligionsAndReligiousAssociations,
                UrlCategory.NewsMedia,
                UrlCategory.PornoAndErotic,
                UrlCategory.Nudism,
                UrlCategory.Lingerie,
                UrlCategory.SexEducation,
                UrlCategory.Dating18plus,
                UrlCategory.LGBT,
                UrlCategory.SexShops,
                UrlCategory.Narcotics,
                UrlCategory.Alcohol,
                UrlCategory.Tobacco,
                UrlCategory.CultureAndSociety,
                UrlCategory.GovernmentPoliticsLaws,
                UrlCategory.HomeAndFamily,
                UrlCategory.Military,
                UrlCategory.RestaurantsCafeFood,
                UrlCategory.AstrologyAndEsoterica,
                UrlCategory.Torrents,
                UrlCategory.FileSharing,
                UrlCategory.AudioAndVideo,
                UrlCategory.InformationTechnologies,
                UrlCategory.SearchEnginesAndServices,
                UrlCategory.HostingAndDomains,
                UrlCategory.Ads,
                UrlCategory.Banks,
                UrlCategory.RentRealEstateServices,
                UrlCategory.Phishing,
                UrlCategory.Malware
            };
    
    /* Quarantine actions */
    private static final int REMOVE_ACTION = 0;
    private static final int RESTORE_ACTION = 1;

    private DialogsSupplier(){}

    public static Dialog getWebFilterCategoryDialog(final Context context, final WebFilterControl webFilterControl) {

        final CharSequence[] categoryNames;
        final boolean[] checkedItems;

        if (webFilterControl.isExtendedCategoriesEnabled()) {
            categoryNames = new CharSequence[UrlCategoryExt.values().length];
            checkedItems = new boolean[UrlCategoryExt.values().length];
            Set<UrlCategoryExt> enabledCategories = webFilterControl.getEnabledCategoriesExt();

            for (int i = 0; i < UrlCategoryExt.values().length; ++i) {
                categoryNames[i] = UrlCategoryExt.values()[i].toString();
                checkedItems[i] = enabledCategories.contains(UrlCategoryExt.values()[i]);
            }
        } else {
            categoryNames = new CharSequence[WF_CATEGORIES.length];
            checkedItems = new boolean[WF_CATEGORIES.length];
            Set<UrlCategory> enabledCategories = webFilterControl.getEnabledCategories();

            for (int i = 0; i < WF_CATEGORIES.length; ++i) {
                categoryNames[i] = WF_CATEGORIES[i].toString();
                checkedItems[i] = enabledCategories.contains(WF_CATEGORIES[i]);
            }
        }
        return new AlertDialog.Builder(context)
                .setTitle(R.string.str_web_filter_category_dialog_title)
                .setMultiChoiceItems(categoryNames, checkedItems,
                        new OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                    int which, boolean isChecked) {

                                if (webFilterControl.isExtendedCategoriesEnabled()) {
                                    if (isChecked) {
                                        webFilterControl.setCategoryEnabled(UrlCategoryExt.values()[which]);
                                    } else {
                                        webFilterControl.setCategoryDisabled(UrlCategoryExt.values()[which]);
                                    }
                                } else {
                                    if (isChecked) {
                                        webFilterControl.setCategoryEnabled(WF_CATEGORIES[which]);
                                    } else {
                                        webFilterControl.setCategoryDisabled(WF_CATEGORIES[which]);
                                    }
                                }
                                webFilterControl.saveCategories();
                            }
                        }).setPositiveButton(R.string.str_web_filter_category_dialog_positive, null).create();
    }
    
    public static Dialog getRemoveWebFilterExcludeItem(Context context, final WebFilterControl webFilterControl,
            final String urlToDelete, final UiUpdater updater) {
        return new AlertDialog.Builder(context)
            .setTitle(R.string.str_remove_web_filter_dialog_title)
            .setMessage(R.string.str_remove_web_filter_dialog_message)
            .setPositiveButton(R.string.str_remove_web_filter_dialog_positive,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog,
                                            int which) {
                            if (urlToDelete != null) {
                                // Do no rely on index here, because there is a
                                // possibility that exclusion list could
                                // be changed by some other task
                                // while user was watching on the dialog
                                for (int i = 0; i < webFilterControl.getExclusionsCount(); ++i) {
                                    if (webFilterControl.getExclusion(i).equals(urlToDelete)) {
                                        webFilterControl.removeExclusion(i);
                                        webFilterControl.saveExclusions();
                                        // Refresh listView
                                        updater.updateUi();
                                        break;
                                    }
                                }
                            }
                            dialog.dismiss();
                        }
                    }).setNegativeButton(R.string.str_remove_web_filter_dialog_negative, null).create();
    }
    
    public static DialogFragment getRootCheckDialog() {
        return new RootCheckDialog();
    }

    static Dialog getFirmwareCheckDialog(Context context) {
        final StringBuilder msg = new StringBuilder();
        try {
            final FirmwareChecker checker    = new FirmwareChecker();
            final FirmwareCheckResult result = checker.checkFirmware();

            msg.append(context.getString(R.string.str_firmware_info, android.os.Build.MODEL, android.os.Build.VERSION.INCREMENTAL));
            
            switch (result.getVerdict()) {
                case Trusted:
                    msg.append(context.getString(R.string.str_trusted_firmware_installed));
                    msg.append(" ");
                    if (result.isUpdateAvalable()) {
                        msg.append(context.getString(R.string.str_new_firmware_available));
                        msg.append(result.getUpdateVersion());
                    } else {
                        msg.append(context.getString(R.string.str_latest_firmware_istalled));
                    }
                    break;
                case Untrusted:
                    msg.append(context.getString(R.string.str_untrusted_firmware_installed));
                    break;
                case Unknown:
                    msg.append(context.getString(R.string.str_unknown_firmware_installed));
                    break;
                default:
                    break;
            }
            
        } catch (SdkLicenseViolationException e) {
            msg.append(context.getString(R.string.str_lic_expired));
        } catch (IOException e) {
            msg.append(context.getString(R.string.str_firmware_service_na));
            e.printStackTrace();
        }
        return new AlertDialog.Builder(context).setTitle(R.string.str_firmware_title)
                .setMessage(msg).create();
    }

    public static Dialog getWiFiCheckDialog(Context context) {
        final StringBuilder msg = new StringBuilder();
        final ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo wifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        try {
            if (wifi.isConnected()) {
                final WifiReputation wifiReputation = new WifiReputation(context);
                msg.append(context.getString(wifiReputation.isCurrentNetworkSafe() ? R.string.str_wifi_check_dialog_safe : R.string.str_wifi_check_dialog_not_safe));
                final WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

                SmFingerprint fingerprint = new SmFingerprint(context);

                if (wifiManager != null) {
                    final WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                    msg.append("\n\n");
                    msg.append(String.format("SSID: %s\n", wifiInfo.getSSID()));
                    msg.append(String.format("BSSID: %s\n", wifiInfo.getBSSID()));
                    msg.append(String.format("MAC: %s\n",  fingerprint.getPersistentProperties().wifiMAC));
                    msg.append(String.format("Supplicant state: %s\n", wifiInfo.getSupplicantState().name()));
                    msg.append(String.format("RSSI: %d\n", wifiInfo.getRssi()));
                    msg.append(String.format("Link speed: %d\n", wifiInfo.getLinkSpeed()));
                    msg.append(String.format("Network ID: %d\n", wifiInfo.getNetworkId()));
                    msg.append(String.format("Hidden SSID: %s\n", wifiInfo.getHiddenSSID() ? "yes" : "no"));
                }
            } else {
                msg.append(context.getString(R.string.str_wifi_check_dialog_not_connected));
            }
        } catch (SdkLicenseViolationException e) {
            msg.append(context.getString(R.string.str_lic_expired));
        } catch (IOException e) {
            msg.append(context.getString(R.string.str_wifi_check_dialog_service_unavailable));
        }
        return new AlertDialog.Builder(context).setTitle(R.string.str_wifi_check_dialog_title)
                .setMessage(msg).create();
    }
    
    public static DialogFragment getCertificateCheckDialog() {
        return new CheckCertificateDialog();
    }
    
    public static DialogFragment getReputationCheckDialog() {
        return new UrlReputationDialog();
    }

    public static DialogFragment getReputationExtCheckDialog() {
        UrlReputationDialog dlg = new UrlReputationDialog();
        dlg.enableExtendedCategories(true);
        return dlg;
    }
    
    public static DialogFragment getFinancialCheckDialog() {
        return new FinancialCategoryDialog();
    }
    
    public static DialogFragment getAntivirusCheckUrlDialog() {
        return new AntivirusCheckUrlDialog();
    }

    public static Dialog getUsbDebugCheckDialog(Context context) {
        boolean isUSBDebugOn = false;
        int messageId;
        try {
            SystemSecurityCheckResult result = (new SystemSecurity(context))
                    .checkSystemSettings();

            isUSBDebugOn = (result.getVerdict() == SystemSecurityVerdict.UnSafe && (result
                    .getExtendedVerdict() & SystemSecurityCheckResult.USB_DEBUG_ON_FLAG) != 0);

            messageId = isUSBDebugOn ? R.string.str_usb_debug_on
                    : R.string.str_usb_debug_off;
        } catch (SdkLicenseViolationException e) {
            messageId = R.string.str_license_expired_error;
        }
        return new AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.str_usb_debug_dialog_title))
                .setMessage(context.getString(messageId)).create();
    }

    public static Dialog getNonMarketAppsCheckDialog(Context context) {
        boolean allowed = false;
        int messageId;

        try {
            SystemSecurityCheckResult result = (new SystemSecurity(context)).checkSystemSettings();

            int flag = SystemSecurityCheckResult.INSTALL_NON_MARKET_APPS_ALLOWED_FLAG;

            allowed = (result.getVerdict() == SystemSecurityVerdict.UnSafe && (result
                    .getExtendedVerdict() & flag) != 0);

            messageId = allowed ? R.string.str_non_market_apps_on
                    : R.string.str_non_market_apps_off;
        } catch (SdkLicenseViolationException e) {
            messageId = R.string.str_license_expired_error;
        }

        return new AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.str_non_market_apps_dialog_title))
                    .setMessage(context.getString(messageId)).create();
    }

    public static Dialog getDevicePasswordQualityCheckDialog(Context context) {
        boolean qualityLow = false;
        int messageId;

        try {
            SystemSecurityCheckResult result = (new SystemSecurity(context)).checkSystemSettings();

            int flag = SystemSecurityCheckResult.DEVICE_PASSWORD_QUALITY_LOW_FLAG;

            qualityLow = (result.getVerdict() == SystemSecurityVerdict.UnSafe && (result
                    .getExtendedVerdict() & flag) != 0);

            messageId = qualityLow ? R.string.str_device_password_quality_low_on
                    : R.string.str_device_password_quality_low_off;
        } catch (SdkLicenseViolationException e) {
            messageId = R.string.str_license_expired_error;
        }

        return new AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.str_device_password_quality_low_dialog_title))
                    .setMessage(context.getString(messageId)).create();
    }

    public static Dialog getProcessQuarantineItemDialog(final Context context, final Antivirus kavComponent,
            final String itemName, final String customRestorePath, final UiUpdater updater) {
        return new AlertDialog.Builder(context)
        .setItems(context.getResources().getStringArray(R.array.quarantine_user_actions), new DialogInterface.OnClickListener() {                               
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                case REMOVE_ACTION:
                    try {
                        kavComponent.removeFromQuarantine(itemName);
                    } catch (QuarantineException e) {
                        e.printStackTrace();
                    }
                    break;
                case RESTORE_ACTION:
                    try {
                        kavComponent.restoreFromQuarantine(itemName, customRestorePath);
                    } catch (QuarantineException e) {
                        e.printStackTrace();
                        if (TextUtils.isEmpty(customRestorePath)) {
                            QuarantineActivity.showPromptSelectCustomPath(context);
                        }
                    }
                    break;
                default:
                    break;
                }
                dialog.dismiss();
                updater.updateUi();
            }
        })
        .setPositiveButton(context.getString(R.string.str_dialog_close_button), null)
        .create();
    }

    public static Dialog getSetupProxyPortDialog(final Context context) {
        final EditText input = new EditText(context);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        input.setRawInputType(Configuration.KEYBOARD_12KEY);

        return new AlertDialog.Builder(context)
            .setTitle(context.getString(R.string.webfilter_new_proxy_port_dialog_title))
            .setView(input)
            .setPositiveButton(context.getString(R.string.ok_button_text), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    String str = input.getText().toString();
                    if ("".equals(str)) {
                        return;
                    }
                    int wifiPort = Integer.parseInt(str);
                    SdkSampleApplication app = (SdkSampleApplication) context.getApplicationContext();
                    try {
                        app.changePortNumber(wifiPort);
                        app.reinitWebFilterControl();
                    } catch (SdkLicenseViolationException e) {
                        e.printStackTrace();
                    }
                }
            })
            .create();
    }
}
