package com.kavsdkexample.shared;

public interface OnAddItemToAntispamListener {
    void onAddItem(String phoneMask, String textMask, boolean toWhiteList);
}
