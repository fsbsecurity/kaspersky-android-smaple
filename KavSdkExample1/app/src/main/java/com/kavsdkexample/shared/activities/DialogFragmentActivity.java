package com.kavsdkexample.shared.activities;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.kavsdkexample.R;

/**
 * This is a base class for other fragments in our sample application. 
 * The class provides to fragments the ability to show fragment dialogs
 */
public abstract class DialogFragmentActivity extends BaseActivity {
    private String mPrevDialogTag;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /*
         * Assume that only one dialog per activity should be shown.
         * In order to present a new dialog, there is a need to find and remove the previous one
         */
    public String getPrevFragmentDialogTag() {
        return mPrevDialogTag;
    }

    public void setPrevFragmentDialogTag(String tag) {
        mPrevDialogTag = tag;
    }
    
    public abstract DialogFragment onCreateDialogFragment(int dialogId, Bundle bundle);

    private FragmentManager mManager = getSupportFragmentManager();
    
    /**
     * This method shows a fragment dialog with a specified id
     */
    public void showFragmentDialog(int dialogId) {
        showFragmentDialog(dialogId, null);
    }
    
    public void showFragmentDialog(int dialogId, Bundle bundle) {
        // We show only one dialog at a time
        // Removing the previous dialog
        removePrevDialog();
    
        // Showing a new one
        DialogFragment dialog = onCreateDialogFragment(dialogId, bundle);
        dialog.setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogTheme);
        String tag = Integer.toString(dialogId);
        FragmentTransaction ft = mManager.beginTransaction();
        ft.add(dialog, tag);
        ft.commitAllowingStateLoss();
        setPrevFragmentDialogTag(tag);
    }
    
    @Override
    public void onStop() {
        super.onStop();
        removePrevDialog();
    }
    
    private void removePrevDialog() {
        Fragment prev = mManager.findFragmentByTag(getPrevFragmentDialogTag());
        if (prev != null && prev instanceof DialogFragment) {
            ((DialogFragment) prev).dismissAllowingStateLoss();
            setPrevFragmentDialogTag(null);
        }
    }
}
