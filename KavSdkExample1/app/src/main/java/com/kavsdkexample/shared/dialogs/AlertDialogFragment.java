package com.kavsdkexample.shared.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.kavsdk.webfilter.WebFilterControl;
import com.kavsdkexample.app.SdkSampleApplication;

/**
 * This is a sample fragment dialog implementation. 
 * The class converts dialogs to dialog fragments.
 */
public class AlertDialogFragment extends DialogFragment {
    /* 
     * A tag for ID of the dialog to be shown 
     * {@see DialogsSupplier} 
     */
    private static final String DIALOG_ID = "id";
    
    public static AlertDialogFragment newInstance(int id) {
        AlertDialogFragment dialog = new AlertDialogFragment();
        
        // Supply a dialog id input as an argument.
        Bundle args = new Bundle();
        args.putInt(DIALOG_ID, id);
        dialog.setArguments(args);

        return dialog;
    }
    
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int id = getArguments().getInt(DIALOG_ID);
        switch (id) {
            case DialogsSupplier.DIALOG_WF_CATEGORY:
                WebFilterControl webFilterControl = ((SdkSampleApplication)getActivity().getApplication()).getWebFilterControl();
                return DialogsSupplier.getWebFilterCategoryDialog(getActivity(), webFilterControl);
            case DialogsSupplier.DIALOG_WIFI_CHECK:
                return DialogsSupplier.getWiFiCheckDialog(getActivity());
            case DialogsSupplier.DIALOG_USB_DEBUG_CHECK:
                return DialogsSupplier.getUsbDebugCheckDialog(getActivity());
            case DialogsSupplier.DIALOG_NON_MARKET_APPS_CHECK:
                return DialogsSupplier.getNonMarketAppsCheckDialog(getActivity());
            case DialogsSupplier.DIALOG_DEVICE_PASSWORD_QUALITY_CHECK:
                return DialogsSupplier.getDevicePasswordQualityCheckDialog(getActivity());
            case DialogsSupplier.DIALOG_CHECK_FIRMWARE:
                return DialogsSupplier.getFirmwareCheckDialog(getActivity());
            case DialogsSupplier.DIALOG_ENTER_NEW_PROXY_PORT:
                return DialogsSupplier.getSetupProxyPortDialog(getActivity());
            default:
                throw new IllegalStateException("Unexpected dialog id"); 
        }
    }
}
