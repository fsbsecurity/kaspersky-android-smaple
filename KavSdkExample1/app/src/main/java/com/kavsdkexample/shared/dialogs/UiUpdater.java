package com.kavsdkexample.shared.dialogs;

/**
 * This is an interface for asking to update or refresh UI
 */
public interface UiUpdater {
    void updateUi();
}
