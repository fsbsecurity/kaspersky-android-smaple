package com.kavsdkexample.shared.dialogs;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.widget.Toast;

import com.kavsdkexample.R;

/*
 * Base dialog for running time-consuming operation and showing result in Toast. 
 */
public abstract class BaseAsyncOperationDialog extends DialogFragment {
    protected Context mContext;
    private Handler mHandler;
    private ProgressDialog mProgressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandler = new Handler();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity.getApplicationContext();
    }

    /**
     * Implement this method to perform time-consuming operation
     * @return operation result message that will be shown in Toast.  
     */
    protected abstract String check();
    
    protected void checkAndShowResult() {
        /*
         * Checking can be time consuming.
         * So, show a "thinking" process dialog and process verification from a new thread
         */
        
        mProgressDialog = ProgressDialog.show(getActivity(), "", getString(R.string.app_loading));
        new Thread() {
            @Override
            public void run() {

                final String result = check();

                mHandler.post( new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(mContext, result, Toast.LENGTH_LONG).show();
                            mProgressDialog.dismiss();
                            dismissAllowingStateLoss();
                        }
                    });
                }
        }.start();  
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
