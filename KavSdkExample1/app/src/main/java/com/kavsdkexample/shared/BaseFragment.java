package com.kavsdkexample.shared;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kavsdkexample.R;
import com.kavsdkexample.app.SdkSampleApplication;

public abstract class BaseFragment extends Fragment implements InitializedComponent {
    protected ComponentInitializer mInitializer;
    private ProgressDialog mProgressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final SdkSampleApplication app = ((SdkSampleApplication) getActivity().getApplication());
        View rootView = initUi(inflater, container, savedInstanceState);
        mInitializer = new ComponentInitializer();
        mInitializer.init(app, this);
        return rootView;
    }
    
    public abstract View initUi(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);

    public void showProgressDialog() {
        mProgressDialog = new ProgressDialog(getActivity()) {
            public boolean onKeyDown(int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    return true;
                }
                return false;
            }
        };
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setMessage(getString(R.string.str_main_activity_loading_application));
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.cancel();
        }
    }
}
