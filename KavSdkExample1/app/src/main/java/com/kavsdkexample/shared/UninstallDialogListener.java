package com.kavsdkexample.shared;

import com.kavsdk.antivirus.ThreatInfo;

public interface UninstallDialogListener {
    void onHide();
    void onShow();
    void onUninstall(ThreatInfo threatInfo, boolean success);
}
