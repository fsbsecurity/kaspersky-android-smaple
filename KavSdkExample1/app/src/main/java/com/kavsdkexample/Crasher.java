package com.kavsdkexample;

public final class Crasher {
    
    static{
        System.loadLibrary("SdkMainActivity");
    }
    private Crasher() {}
    public static void invokeJavaCrash() {
        throw new RuntimeException();
    }
        
    public static native void invokeNativeCrash();
}
