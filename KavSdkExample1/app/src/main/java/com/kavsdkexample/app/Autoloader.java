package com.kavsdkexample.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class Autoloader extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent serviceIntent = new Intent(context, SdkService.class);
        context.startService(serviceIntent);
    }
}
