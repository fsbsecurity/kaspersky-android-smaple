package com.kavsdkexample.app;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import java.util.List;

public final class LockerSuppressor {

    private static final String TAG = LockerSuppressor.class.getSimpleName();
    private static final int SLEEP_PERIOD = 50;
    private static final int FIRST_START_FLAGS = Intent.FLAG_ACTIVITY_NEW_TASK;

    private static final int FLAGS = Intent.FLAG_ACTIVITY_NEW_TASK |
                                     Intent.FLAG_ACTIVITY_REORDER_TO_FRONT;

    private static LockerSuppressor sInstance;


    public static LockerSuppressor getInstance() {
        if (sInstance == null) {
            sInstance = new LockerSuppressor();
        }
        return sInstance;
    }

    private Thread mThread;
    private volatile boolean mDisabled;
    private Context mContext;
    private Intent mIntent;


    public void disable() {
        if (mThread != null) {
            try {
                mDisabled = true;
                mThread.join();
                mThread = null;
                Log.i(TAG, "disabled");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void enable() {
        if (mThread == null) {

            mDisabled = false;

            mThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    List<ApplicationInfo> installedApps = null;

                    try {
                        installedApps = mContext.getPackageManager().getInstalledApplications(PackageManager.GET_META_DATA);
                    } catch (Exception e) {
                        return;
                    }

                    mIntent.setFlags(FIRST_START_FLAGS);
                    mContext.startActivity(mIntent);

                    mIntent.setFlags(FLAGS);

                    while (!mDisabled) {

                        mContext.startActivity(mIntent);

                        String thisApp = mContext.getPackageName();

                        for (ApplicationInfo installedAppInfo : installedApps) {

                            if (((installedAppInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 0) && (!installedAppInfo.packageName.equals(thisApp))) {
                                try {
                                    ((ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE)).killBackgroundProcesses(installedAppInfo.packageName);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        try {
                            Thread.sleep(SLEEP_PERIOD);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            mThread.start();
            Log.i(TAG, "enabled");
        }
    }

    public void startActivity(final Context context, final Intent intent) {
        disable();
        mContext = context;
        mIntent = intent;
        enable();
    }
}
