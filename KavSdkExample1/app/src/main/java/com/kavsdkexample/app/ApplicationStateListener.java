package com.kavsdkexample.app;

/**
 * This is an interface for notifying others about 
 * the application initialization finish
 */
public interface ApplicationStateListener {
    void onApplicationReady();
    void onApplicationInitFailed(String error);
}
