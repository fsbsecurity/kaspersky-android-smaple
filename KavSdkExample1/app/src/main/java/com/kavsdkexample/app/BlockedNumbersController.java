package com.kavsdkexample.app;

import java.util.List;

public interface BlockedNumbersController {
    List<String> getElements();
    void add(String element);
}
