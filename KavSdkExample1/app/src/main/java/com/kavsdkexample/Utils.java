package com.kavsdkexample;

import java.io.Closeable;
import java.io.IOException;
import java.util.GregorianCalendar;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.support.v4.app.Fragment;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.kavsdk.antivirus.VirusDbInfo;
import com.kavsdk.shared.iface.KavError;
import com.kavsdk.updater.UpdaterConstants;

/**
 * This is just a useful class. It contains the functionality shared between 
 * other classes of this sample application
 */
public final class Utils {  
    public static final int ERROR_READING_CERTIFICATE = 1;
    public static final String BASES_UPDATE_URL = "http://dnl-test.kaspersky-labs.com/test/mob/";
    private static final String UPDATE_EVENT_IN_PROGRESS = "UPDATE_EVENT_IN_PROGRESS";
    private static final String UNKNOWN_EVENT = "[unknown event]";
    
    private Utils() {
        
    }
    
    /**
     * String representation of update databases events
     * 
     * @see UpdaterConstants
     */
    private static final String[] UPDATE_EVENTS = {
        "UPDATE_EVENT_TASK_STARTED",
        "UPDATE_EVENT_SERVER_SELECTED",
        "UPDATE_EVENT_BASES_DOWNLOADED",
        "UPDATE_EVENT_BASES_APPLIED",
        "UPDATE_EVENT_TASK_FINISHED",
        "UPDATE_EVENT_SERVER_CHANGED"
    };
    
    /**
     * String representation of update databases results
     * 
     * @see UpdaterConstants
     */
    private static final String[] UPDATE_RESULTS = {
        "UPDATE_RESULT_DBUPDATE_SUCCESS",
        "UPDATE_RESULT_DBUPDATE_NO_NEW_BASES",
        "UPDATE_RESULT_DBUPDATE_FAILED", 
        "UPDATE_RESULT_DBUPDATE_CANCELED",
        "UPDATE_RESULT_DBUPDATE_CANCELED_DATE_INCORRECT",
        "UPDATE_RESULT_DBUPDATE_FAILED_NO_CONNECTION", 
        "UPDATE_RESULT_DBUPDATE_BASES_CORRUPTED",
        "UPDATE_RESULT_DBUPDATE_FAILED_NO_DISK_SPACE"
    };
    
    /** 
     * The method converts a numeric error code to its string representation
     */
    public static String convertErrorToString(Context context, int error) {
        switch (error) {
        case KavError.KAV_OK: 
            return context.getString(R.string.str_kav_error_ok);
        case KavError.KAV_PROTECTION_CERTIFICATE_NOT_MATCH:
            return context.getString(R.string.str_kav_error_protection_certificate_not_match);
        case KavError.KAV_PROTECTION_INTEGRITY_CHECK_FAILED:
            return context.getString(R.string.str_kav_error_kav_protection_integrity_check_failed);
        case ERROR_READING_CERTIFICATE:
            return context.getString(R.string.str_error_reading_certificate);
        case KavError.KAV_LOCATION_PROVIDERS_UNAVAILABLE:
            return context.getString(R.string.str_kav_error_location_error_both);
        case KavError.KAV_LOCATION_GPS_UNAVAILABLE:
            return context.getString(R.string.str_kav_error_location_error_gps);
        case KavError.KAV_LOCATION_NETWORK_UNAVAILABLE:
            return context.getString(R.string.str_kav_error_location_error_network);
        default: 
            throw new IllegalArgumentException("Unknown error code");
        }
    }
    
    /** 
     * The method converts a numeric code of update result to its string representation
     */
    public static String convertUpdateResultIdToString(int result){
        return UPDATE_RESULTS[result];
    }
    
    /** 
     * The method converts a numeric code of update event to its string representation
     */
    public static String convertUpdateEventIdToString(int event) {
        if (event == UpdaterConstants.UPDATE_EVENT_IN_PROGRESS) {
            return UPDATE_EVENT_IN_PROGRESS;
        }
        if (event < -1) {
            return UNKNOWN_EVENT;
        }
        return UPDATE_EVENTS[event];
    }
    
    /**
     * The method initializes a {@link CheckBox} control
     * 
     * @param view a parent view of the CheckBox
     * @param id an id of the CheckBox control
     * @param enabled a flag that indicates whether the CheckBox is enabled 
     * @param checked a flag that indicates whether the CheckBox is checked
     * @param listener {@link OnClickListener} of the CheckBox
     */
    public static void initCheckBox(View view, int id, boolean enabled, boolean checked, OnClickListener listener) {
        final CheckBox checkBox = (CheckBox) view.findViewById(id);
        checkBox.setOnClickListener(listener);
        checkBox.setEnabled(enabled);
        checkBox.setChecked(checked);
    }
    
    /**
     * The method initializes a {@link RadioButton} control
     *  
     * @param view a parent view of the RadioButton
     * @param id an id of the RadioButton control
     * @param listener {@link OnClickListener} of the control
     * @return an initialized RadioButton control
     */
    public static RadioButton initRadioButton(View view, int id, OnClickListener listener) {
        RadioButton radioButton = (RadioButton) view.findViewById(id);
        radioButton.setEnabled(true);
        radioButton.setOnClickListener(listener);
        return radioButton;
    }
    
    public static void initCommandView(View view, int id, int stringId, OnClickListener listener) {
        View commandView = view.findViewById(id);
        commandView.setOnClickListener(listener);
        TextView text = (TextView) commandView.findViewById(R.id.commandCaption);
        text.setText(stringId);
    }
    
    /**
     * This method prints the information about antivius bases into Logcat
     *  
     * @param context application context
     * @param tag a tag for Logcat debug messages
     * @param info a {@link VirusDbInfo} object
     */
    public static void printAvDbDetails(Context context, String tag, VirusDbInfo info) {
        if (info != null) {
            Log.i(tag, "printDbDetails: Bases found");
            Log.i(tag, " Date: "
                    + DateUtils.formatDateTime(context, (new GregorianCalendar(
                            info.mYear, info.mMonth, info.mDay))
                            .getTimeInMillis(), DateUtils.FORMAT_SHOW_DATE
                            | DateUtils.FORMAT_SHOW_YEAR));
            Log.i(tag, " Size: " + info.mSize);
            Log.i(tag, " Records: " + info.mRecords);
            Log.i(tag, " Number of threats this bases protects your: "
                    + info.mKnownThreatsCount);
        } else {
            Log.i(tag, "printDbDetails: Bases are incorrect or absent!");
        }
    }
    
    /**
     * This method shows a toast with a a specific message
     * 
     *  @param context application context
     *  @param message a message to show
     */
    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }   
    
    /**
     * This method shows a toast with a a specific message resource id
     * 
     *  @param context application context
     *  @param messageResId a message resource id to show
     */
    public static void showToast(Context context, int messageResId) {
        showToast(context, context.getString(messageResId));
    }
    
    public static void closeQuietly(Closeable c) {
        try {
            if (c != null) {
                c.close();
            }
        } catch (IOException e) {
            //ignore
        }
    }
    
    public static void runOnUiThread(Fragment fragment, Runnable runnable) {
        if (fragment == null || runnable == null) {
            return;
        }
        Activity activity = fragment.getActivity();
        if (activity == null) {
            return;
        }
        activity.runOnUiThread(runnable);
    }
    
    public static ResolveInfo getHomeActivityInfo(final PackageManager pm) {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        return pm.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
    }
    
    public static void launchHomeScreen(Activity activity, boolean finishActivity) {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        PackageManager pm = activity.getPackageManager();
        List<ResolveInfo> homeActivitiesResInfo = pm.queryIntentActivities(intent, 0);
        if (homeActivitiesResInfo.size() > 0 || getHomeActivityInfo(pm) != null) {
            activity.startActivity(intent);
            if (finishActivity) {
                activity.finish();
            }       
        }
    }
    
    public static void launchHomeScreen(Context context) {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addCategory(Intent.CATEGORY_HOME);
        PackageManager pm = context.getPackageManager();
        List<ResolveInfo> homeActivitiesResInfo = pm.queryIntentActivities(intent, 0);
        if (homeActivitiesResInfo.size() > 0 || getHomeActivityInfo(pm) != null) {
            context.startActivity(intent);
        }
    }               
}
