package com.kavsdkexample.samples;

import com.kavsdk.antivirus.ScannerConstants;
import com.kavsdk.antivirus.ScannerEventListener;
import com.kavsdk.antivirus.ScannerSuspiciousEventListener;
import com.kavsdk.antivirus.SuspiciousThreatType;
import com.kavsdk.antivirus.ThreatInfo;
import com.kavsdk.antivirus.ThreatType;
import com.kavsdkexample.app.SdkService.ScannerListener;

/**
 * Helper class for interaction between Scanner and GUI 
 */
public class ScannerEventListenerSample implements ScannerEventListener, ScannerSuspiciousEventListener {
    
    private final ScannerListener mScannerListener;
    private volatile boolean mCancelRequested;
    
    private int mFilesCount;
    private int mCured;
    private int mSkippedObjects;
    private int mCheckedObjects;
    private int mVirusesDetected;
    private int mSuspiciousCount;

    public ScannerEventListenerSample(ScannerListener scannerListener) {
        mScannerListener = scannerListener;
    }
    
    public boolean isCancelled() {
        return mCancelRequested;
    }

    public void requestCancel() {
        mCancelRequested = true;
    }
    
    @Override
    public int onScanEvent(int eventId, int result, ThreatInfo threat, ThreatType threatType) {
        
        updateCounters(eventId, result);
        
        mScannerListener.onEvent(eventId, result, threat, threatType);

        if (mCancelRequested) {
            return ScannerConstants.EVENT_RESULT_CANCEL_ALL;
        
        } else {
            return ScannerConstants.EVENT_RESULT_OK;
        }
    }
    
    @Override
    public void onScanEvent(ThreatInfo threat, SuspiciousThreatType threatType) {
        ++mSuspiciousCount;

        mScannerListener.onSuspiciousEvent(threat, threatType);
    }

    private void updateCounters(int eventId, int result) {
        switch (eventId) {
        case ScannerConstants.EVENT_OBJECT_RESULT:
            ++mFilesCount;
            break;

        case ScannerConstants.EVENT_CLEANED:
            ++mCured;
            break;

        case ScannerConstants.EVENT_RESULT:
            switch (result) {
            case ScannerConstants.SCAN_RESULT_NONSCANNED:
            case ScannerConstants.SCAN_RESULT_FAILURE:
            case ScannerConstants.SCAN_RESULT_SKIPPED:
            case ScannerConstants.SCAN_RESULT_CANCELED:
            case ScannerConstants.SCAN_RESULT_INVALID_DATA:
            case ScannerConstants.SCAN_RESULT_ACCESSDENIED:
                ++mSkippedObjects;
                break;

            case ScannerConstants.SCAN_RESULT_CLEAN:
            case ScannerConstants.SCAN_RESULT_INFECTED:
            case ScannerConstants.SCAN_RESULT_DISINFECTED:
                ++mCheckedObjects;
                break;
            default:
                break;
            }
            break;

        case ScannerConstants.EVENT_DETECT:
            ++mVirusesDetected;
            break;
        default:
            break;
        }
    }
    
    public void resetCounters() {
        mCancelRequested = false;
        mFilesCount = 0;
        mCured = 0;
        mSkippedObjects = 0;
        mCheckedObjects = 0;
        mVirusesDetected = 0;
        mSuspiciousCount = 0;
    }

    public int getFilesCount() {
        return mFilesCount;
    }
    
    public int getCuredCount() {
        return mCured;
    }
    
    public int getSkippedObjectsCount() {
        return mSkippedObjects;
    }
    
    public int getCheckedObjectsCount() {
        return mCheckedObjects;
    }
    
    public int getDetectsCount() {
        return mVirusesDetected;
    }

    public int getSuspiciousCount() {
        return mSuspiciousCount;
    }
}
