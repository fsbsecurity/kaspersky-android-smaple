package com.kavsdkexample.samples;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;

import com.kavsdk.antivirus.Antivirus;
import com.kavsdk.antivirus.multithread.MultithreadedScanEventListener;
import com.kavsdk.antivirus.multithread.MultithreadedScanner;
import com.kavsdk.antivirus.Scanner;
import com.kavsdk.antivirus.ScannerConstants;
import com.kavsdk.antivirus.SuspiciousThreatType;
import com.kavsdk.antivirus.ThreatInfo;
import com.kavsdk.antivirus.ThreatType;
import com.kavsdk.antivirus.multithread.ObjectInfo;
import com.kavsdk.antivirus.multithread.ObjectStatus;
import com.kavsdk.antivirus.multithread.ScanTarget;
import com.kavsdkexample.app.SdkService;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Vector;

public class AntivirusSample implements SdkService.ScannerListener {

    public static final int SCAN_PHONE_MEMORY = 0;
    public static final int SCAN_SD_CARD = 1;
    public static final int SCAN_ALL_FILES = 2;
    public static final int SCAN_INSTALLED_APPS = 3;
    public static final int SCAN_INSTALLED_APP = 4;
    public static final int SCAN_SELECTED_FOLDER = 5;
    public static final int SCAN_FILE = 6;
    private static final String TAG = AntivirusSample.class.getSimpleName();

    private final Context mContext;
    
    private final Object mLock = new Object();
    private volatile boolean mStopped = true;
    private volatile boolean mPaused ;
    private volatile boolean mWaiting;

    private final Set<WeakReference<Listener>> mListeners = new HashSet<WeakReference<Listener>>();

    private Settings mSettings = new Settings();

    private final Antivirus mAntivirusComponent;
    private final Scanner mOdsScanner;
    private final ScannerEventListenerSample mScannerListener;

    private MultithreadedScanner mMultithreadScanner;
    private final MultiScannerListener mMultiScannerListener;
    
    private ScanResults mScanResults;
    
    private final Vector<ThreatInfo> mDetectedApplications = new Vector<ThreatInfo>();
    private final List<ThreatInfo> mAppDetectedFromAppMonitor = new ArrayList<ThreatInfo>();
    /*
     * Current scan statistics
     */
    private int mFilesDeleted;
    private int mFilesQuarantined;
    
    public AntivirusSample(Context context, Antivirus antivirus) {
        mContext = context;
        mAntivirusComponent = antivirus;
        mOdsScanner = mAntivirusComponent.createScanner();
        mScannerListener = new ScannerEventListenerSample(this);
        mMultiScannerListener = new MultiScannerListener();
    }

    // This method is used by kashell. Do not remove.
    public Scanner getOdsScanner() {
        return mOdsScanner;
    }
    
    /*
     * Starts the scanning process. 
     * Invoked by pressing "Scan" button
     */
    public void startScan(Settings settings) {
        synchronized (mLock) {
            Log.d(TAG, "startRegularScan was called!");
            if (!mStopped || mWaiting) {
                return;
            }
            Log.d(TAG, "Starting scan...");
            mStopped = false;
            mWaiting = true;
            mSettings = settings.clone();
            new ScanThread().start();
        }
    }

    public void stopScan() {
        synchronized (mLock) {
            Log.d(TAG, "stopScan was called!");
            if (mStopped || mWaiting) {
                return;
            }
            Log.d(TAG, "Stopping scan...");
            mWaiting = true;
            mStopped = true;

            if (mSettings.mMultithreaded) {
                if (mMultithreadScanner.isScanInProgress()) {
                    mMultithreadScanner.stopScan();
                }
            } else {
                if (mOdsScanner.isScanInProgress()) {
                    mOdsScanner.stopScan();
                }
            }
            mPaused = false;
            mLock.notify();
        }
    }

    public boolean isStopped() {
        return mStopped;
    }

    public void pauseScan() {
        synchronized (mLock) {
            Log.d(TAG, "pauseScan was called!");
            if (mStopped || mPaused) {
                return;
            }
            Log.d(TAG, "Pausing scan...");
            mPaused = true;

            if (mSettings.mMultithreaded) {
                if (mMultithreadScanner.isScanInProgress()) {
                    mMultithreadScanner.pauseScan();
                }

            } else {
                if (mOdsScanner.isScanInProgress()) {
                    mOdsScanner.pauseScan();
                }
            }
        }
    }
    
    public void resumeScan() {

        synchronized (mLock) {
            Log.d(TAG, "resumeScan was called!");
            if (mStopped || !mPaused) {
                return;
            }
            Log.d(TAG, "Resuming scan...");
            if (mSettings.mMultithreaded) {
                if (mMultithreadScanner.isScanInProgress()) {
                    mMultithreadScanner.resumeScan();
                }
            } else {
                if (mOdsScanner.isScanInProgress()) {
                    mOdsScanner.resumeScan();
                }
            }

            mPaused = false;
            mLock.notify();
        }
    }
    
    
    public boolean isPaused() {
        return mPaused;
    }

    public void addToAppDetectedListFromAppMonitor(ThreatInfo threat) {
        mAppDetectedFromAppMonitor.add(threat);
    }
    
    public List<ThreatInfo> getAppDetectedListFromAppMonitor() {
        return mAppDetectedFromAppMonitor;
    }

    @Override
    public void onEvent(int eventId, int result, ThreatInfo threat, ThreatType threatType) {
        final String virusName = threat.getVirusName();
        final String objectName = threat.getObjectName();

        switch (eventId) {
        case ScannerConstants.EVENT_OBJECT_RESULT:
            // Count the number of scanned files for a progress bar
            Log.d(TAG, " Scan: EVENT_OBJECT_RESULT  result = " + result);
            break;

        case ScannerConstants.EVENT_CLEANED:
            Log.d(TAG, " Scan: CLEANED virus '" + virusName + "' in '" + objectName + "'");
            break;

        case ScannerConstants.EVENT_CLEANFAILED:
            Log.d(TAG, " Scan: virus '" + virusName + "' in '" + objectName + "' CAN'T BE CLEANED");
            processThreat(threat);
            break;

        case ScannerConstants.EVENT_BEGIN:
            // This is a good place to update a progress bar
            break;

        case ScannerConstants.EVENT_DETECT:
            Log.d(TAG, " Scan: detected virus '" + virusName + "' in '" + objectName + "'");

            if (mSettings.mCleanMode == ScannerConstants.CLEAN_MODE_DONOTCLEAN) {
                processThreat(threat);
            }
            break;
        default:
            break;
        }
    }

    /*
     * Events of suspicious files detections fall here
     */
    @Override
    public void onSuspiciousEvent(ThreatInfo threat, SuspiciousThreatType threatType) {
        Log.d(TAG, " Scan: detected suspicious (" + threatType.toString() + ") '" + threat.getVirusName() + "' in '" + threat.getObjectName() + "'");
        if (threat.isCloudCheckFailed()) {
            Log.e(TAG, String.format("Object '%s' was not scanned in cloud", threat.getObjectName()));
        }
    }

    public void removeNonpersitentThreat(ThreatInfo threat, Activity activity) {
        mAntivirusComponent.removeNonpersistentThreat(threat, activity);
    }

    public void removeDeviceAdminThreat(ThreatInfo threat, Activity activity) {
        mAntivirusComponent.removeDeviceAdminThreat(threat, activity);
    }
    
    public void removeThreat(ThreatInfo threat) {
        mAntivirusComponent.removeThreat(threat);
    }
    
    public boolean isCloudAvailable() {
        return mAntivirusComponent.isCloudCheckAvailable();
    }
    
    /*
     * Process the detected threat
     */
    private void processThreat(ThreatInfo threat) {

        if (threat.isApplication()) {
            mDetectedApplications.add(threat);
        } else {
            if (mSettings.mDeleteIfNotCured) {
                removeFile(threat);
            } else if (mSettings.mQuarantineIfNotCured) {
                moveFileToQuarantine(threat);
            }
        }
    }
    
    private boolean removeFile(ThreatInfo threat) {
        final String fileName = threat.getFileFullPath();
        try {
            Log.d(TAG, " Scan: removing file '" + fileName + "'");
            boolean deleted = mAntivirusComponent.removeThreat(threat);
            if (!deleted) {
                Log.e(TAG, "Can not remove file '" + fileName + "'");
                Log.e(TAG, "Try to remove it manually");
            } else {
                ++mFilesDeleted;
            }
            
            return true;
        } catch (Exception ex) {
            Log.e(TAG, " Scan: removing file '" + fileName + "' failed!");
            Log.e(TAG, ex.toString());
            return false;
        }
    }

    private boolean moveFileToQuarantine(ThreatInfo threat) {
        final String fileName = threat.getFileFullPath();
        try {
            Log.d(TAG, " Scan: move file '" + fileName + "' to quarantine");
            mAntivirusComponent.addToQuarantine(threat);
            mFilesQuarantined++;
            notifyScan(ListenerNotifier.Quarantine);
            return true;
        } catch (Exception ex) {
            Log.e(TAG, " Scan: moving file '" + fileName + "' to quarantine failed!");
            Log.e(TAG, ex.toString());
            return false;
        }
    }

    public List<ApplicationInfo> getApplicationsForScan() {

        PackageManager pm = mContext.getPackageManager();
        
        ApplicationInfo thisApp = null;
        try {
            thisApp = pm.getApplicationInfo(mContext.getPackageName(), 0);
        } catch (NameNotFoundException e) {
            e.printStackTrace(); //couldn't happen
        }
        
        List<ApplicationInfo> installedApps = pm.getInstalledApplications(0);
        Iterator<ApplicationInfo> iter = installedApps.iterator();
        
        while (iter.hasNext()) {
            ApplicationInfo currentApp = iter.next();

            // We do not scan our application, systems services and system applications 
            if (currentApp.packageName.equals(thisApp.packageName) ||
                    (currentApp.uid == 0) ||
                currentApp.sourceDir.startsWith("/system")) {
                iter.remove();
            }
        }
        return installedApps;
    }

    public void addListener(Listener listener) {
        synchronized (mListeners) {
            mListeners.add(new WeakReference<Listener>(listener));
        }
    }
    
    public void removeListener(Listener listener) {
        synchronized (mListeners) {
            Iterator<WeakReference<Listener>> iter = mListeners.iterator();
            while (iter.hasNext()) {
                WeakReference<Listener> ref = iter.next();
                Listener temp = ref.get();
                if (temp == null || temp.equals(listener)) {
                    iter.remove();
                }
            }
        }
    }

    private void notifyScan(final ListenerNotifier notifier) {
        synchronized (mListeners) {
            Iterator<WeakReference<Listener>> iter = mListeners.iterator();
            while (iter.hasNext()) {
                WeakReference<Listener> ref = iter.next();
                Listener listener = ref.get();
                if (listener != null) {
                    notifier.notifyListener(listener);
                } else {
                    iter.remove();
                }
            }
        }
    }

    public List<ThreatInfo> getDetectedApplicationList() {
        return mDetectedApplications;
    }

    public ScanResults getScanResults() {
        return mScanResults;
    }

    /**************************************Inner classes*******************************************/
    private class ScanThread extends Thread {

        private String getFolderToScan(ArrayList<String> excludedFolders) {
            // Use the root directory as a starting one
            String folderName = "/";

            switch (mSettings.mMemoryPartition) {
                case AntivirusSample.SCAN_SD_CARD:
                    folderName = mSettings.mPathToScan;
                    Log.d(TAG, "Scan started: SD_CARD " + mSettings.mPathToScan);
                    break;

                case AntivirusSample.SCAN_SELECTED_FOLDER:
                    try {
                        folderName = new File(mSettings.mPathToScan).getCanonicalPath();
                    } catch (IOException e) {
                        folderName = mSettings.mPathToScan;
                    }
                    Log.d(TAG, "Scan started: SELECTED_FOLDER " + mSettings.mPathToScan + " (" + folderName + ")");
                    break;

                case AntivirusSample.SCAN_PHONE_MEMORY:
                    excludedFolders.addAll(com.kavsdk.utils.Utils.getStoragePaths(Integer.MAX_VALUE));
                    Log.d(TAG, "Scan started: PHONE_MEMORY");
                    break;

                default:
                    Log.d(TAG, "Scan started: ALL_FILES");
                    break;
            }
            return folderName;
        }

        private void startRegularScan() {
            if (mSettings.mMemoryPartition == AntivirusSample.SCAN_INSTALLED_APPS) {
                mDetectedApplications.clear();
                Log.d(TAG, "Scan started: INSTALLED_APPS");
                final List<ApplicationInfo> appList = getApplicationsForScan();


                for (ApplicationInfo appInfo : appList) {
                    synchronized (mLock) {
                        while (mPaused) {
                            try {
                                mLock.wait();
                            } catch (InterruptedException e) {
                            }
                        }
                    }
                    if (mStopped || mScannerListener.isCancelled()) {
                        break;
                    }
                    ScannerEventListenerSample suspListener = (mSettings.mScanMode & ScannerConstants.SCAN_MODE_DETECT_SUSPICIOUS) != 0 ? mScannerListener : null;
                    mOdsScanner.scanInstalledApplication(appInfo, mSettings.mCleanMode, mScannerListener, suspListener, false, mSettings.mScanMode);
                }

            } else if (mSettings.mMemoryPartition == AntivirusSample.SCAN_INSTALLED_APP) {
                mDetectedApplications.clear();
                Log.d(TAG, "Scan started: INSTALLED_APP " + mSettings.mPathToScan);
                PackageManager pm = mContext.getPackageManager();
                ApplicationInfo appInfo = null;
                try {
                    appInfo = pm.getApplicationInfo(mSettings.mPathToScan, 0);
                } catch (NameNotFoundException e) {
                    e.printStackTrace(); //couldn't happen
                }
                if (appInfo != null) {
                    ScannerEventListenerSample suspListener = (mSettings.mScanMode & ScannerConstants.SCAN_MODE_DETECT_SUSPICIOUS) != 0 ? mScannerListener : null;
                    mOdsScanner.scanInstalledApplication(appInfo, mSettings.mCleanMode, mScannerListener, suspListener, false, mSettings.mScanMode);
                }
            } else if (mSettings.mMemoryPartition == AntivirusSample.SCAN_FILE) {
                Log.d(TAG, "Scan started: FILE " + mSettings.mPathToScan);
                mOdsScanner.scanFile(mSettings.mPathToScan, mSettings.mScanMode, mSettings.mCleanMode, mScannerListener, mScannerListener, false);
            } else {
                ArrayList<String> excluded = new ArrayList<>();
                String folderName = getFolderToScan(excluded);
                String[] excludeFolders = excluded.toArray(new String[excluded.size()]);
                mOdsScanner.scanFolder(folderName, mSettings.mScanMode, mSettings.mCleanMode, mScannerListener, mScannerListener, excludeFolders, false);
            }
        }

        void startMultithreadedScan() {

            mMultithreadScanner = mAntivirusComponent.createMultithreadedScanner(
                        mSettings.mExploringThreadsCount, mSettings.mScanningThreadsCount);
            ArrayList<String> excluded = new ArrayList<>();
            File folder = new File(getFolderToScan(excluded));

            ScanTarget target = new ScanTarget();
            target.setFolder(folder);
            target.setFoldersToExclude(excluded);

            if (mSettings.mMultithreadedAddApps) {
                mDetectedApplications.clear();
                target.setApplications(getApplicationsForScan());
            }

            mMultithreadScanner.scan(target, mSettings.mScanMode, mMultiScannerListener);

            mMultithreadScanner = null;
        }

        private ScanResults getScanResults() {

            ScanResults results;

            if (mSettings.mMultithreaded) {

                results = new ScanResults(
                    System.currentTimeMillis(),
                    mMultiScannerListener.getFilesScanned(),
                    mMultiScannerListener.getObjectsScanned(),
                    mMultiScannerListener.getObjectsSkipped(),
                    mMultiScannerListener.getDetectsCount(),
                    0,
                    mFilesDeleted,
                    mFilesQuarantined,
                    mMultiScannerListener.getSuspiciousCount());

            } else {

                results = new ScanResults(
                    System.currentTimeMillis(),
                    mScannerListener.getFilesCount(),
                    mScannerListener.getCheckedObjectsCount(),
                    mScannerListener.getSkippedObjectsCount(),
                    mScannerListener.getDetectsCount(),
                    mScannerListener.getCuredCount(),
                    mFilesDeleted,
                    mFilesQuarantined,
                    mScannerListener.getSuspiciousCount());
            }

            Log.d(TAG, "Scan finished");
            Log.d(TAG, " Files found: "         + results.mFilesCount);
            Log.d(TAG, " Objects scanned: "     + results.mCheckedObjects);
            Log.d(TAG, " Objects skipped: "     + results.mSkippedObjects);
            Log.d(TAG, " Viruses found: "       + results.mVirusesDetected);
            Log.d(TAG, " Objects cured: "       + results.mCuredObjects);
            Log.d(TAG, " Files deleted: "       + results.mFilesDeleted);
            Log.d(TAG, " Files quarantineed: "  + results.mFilesQuarantined);
            Log.d(TAG, " Suspicious: "          + results.mSuspiciousCount);

            return results;
        }

        @Override
        public void run() {
            mWaiting = false;
            notifyScan(ListenerNotifier.StartScan);

            if (mSettings.mMultithreaded) {
                startMultithreadedScan();
            } else {
                startRegularScan();
            }

            mScanResults = getScanResults();

            mScannerListener.resetCounters();
            mMultiScannerListener.resetCounters();

            mFilesDeleted = 0;
            mFilesQuarantined = 0;
            mStopped = true;
            mWaiting = false;
            notifyScan(ListenerNotifier.StopScan);
        }
    }
    
    public static class ScanResults {
        public final long mFinishTime;
        public final int mFilesCount;
        public final int mCheckedObjects;
        public final int mSkippedObjects;
        public final int mVirusesDetected;
        public final int mCuredObjects;
        public final int mFilesDeleted;
        public final int mFilesQuarantined;
        public final int mSuspiciousCount;

        public ScanResults(
                long finishTime,
                int filesCount,
                int checkedObjects,
                int filesSkipped,
                int virusesDetected,
                int curedFiles,
                int filesDeleted,
                int filesQuarantined,
                int suspiciousCount) {
            
            mFinishTime       = finishTime;
            mFilesCount       = filesCount;
            mCheckedObjects   = checkedObjects;
            mSkippedObjects = filesSkipped;
            mVirusesDetected  = virusesDetected;
            mCuredObjects = curedFiles;
            mFilesDeleted     = filesDeleted;
            mFilesQuarantined = filesQuarantined;
            mSuspiciousCount  = suspiciousCount;
        }
    }
    
    public static class Settings implements Cloneable {
        public boolean mDeleteIfNotCured;
        public boolean mQuarantineIfNotCured;
        public String mPathToScan;
        public int mMemoryPartition;
        public boolean mMultithreaded;
        public boolean mMultithreadedAddApps;
        public int mExploringThreadsCount = 1;
        public int mScanningThreadsCount  = 2;
        /*
         * Let's scan archives, use KSN, detect suspicious files
         * Note: antivirus doesn't unpack archives by default
         */
        public int mScanMode = ScannerConstants.SCAN_MODE_ARCHIVED | ScannerConstants.SCAN_MODE_ALLOW_UDS | ScannerConstants.SCAN_MODE_DETECT_SUSPICIOUS;
        
        /* Let's try to cure infected objects */
        public int mCleanMode = ScannerConstants.CLEAN_MODE_CLEAN;
        
        public Settings clone() {
            try {
                return (Settings) super.clone();
            } catch (CloneNotSupportedException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    public interface Listener {
        void onStartScan();

        void onQuarantine();

        void onStopScan();
    }
    
    private enum ListenerNotifier {
        StartScan {
            @Override
            public void notifyListener(final Listener listener) {
                listener.onStartScan();
            }
        },
        Quarantine {
            @Override
            public void notifyListener(final Listener listener) {
                listener.onQuarantine();
            }
        },
        StopScan {
            @Override
            public void notifyListener(final Listener listener) {
                listener.onStopScan();
            }
        };
        
        public abstract void notifyListener(Listener listener);
    }

    private class MultiScannerListener implements MultithreadedScanEventListener {

        private volatile int mObjectsScanned;
        private volatile int mFilesScanned;
        private volatile int mSuspiciousCount;
        private volatile int mDetectsCount;
        private volatile int mObjectsSkipped;

        @Override
        public void onMalwareDetected(ObjectInfo info, ThreatInfo threatInfo, ThreatType threatType) {
            Log.w(TAG, " Scan: detected virus '" + threatInfo.getVirusName() + "' in '" + info.getObjectName() + "'");

            mDetectsCount++;
            processThreat(threatInfo);
            info.release();
        }

        @Override
        public void onSuspiciousDetected(ObjectInfo info, ThreatInfo threat, SuspiciousThreatType threatType) {
            Log.d(TAG, " Scan: detected suspicious (" + threatType.toString() + ") '" + threat.getVirusName() + "' in '" + threat.getObjectName() + "'");
            if (threat.isCloudCheckFailed()) {
                Log.e(TAG, String.format("Object '%s' was not scanned in cloud", threat.getObjectName()));
            }

            mSuspiciousCount++;
            info.release();
        }

        @Override
        public void onScanObjectBegin(ObjectInfo info) {
            info.release();
        }

        @Override
        public void onScanObjectEnd(ObjectInfo info, ObjectStatus status) {
            if (info.getFileFullPath().equals(info.getObjectName())) {
                mFilesScanned++;
            }

            if (status == ObjectStatus.Ok) {
                mObjectsScanned++;
            } else {
                mObjectsSkipped++;
            }

            info.release();
        }

        public int getObjectsScanned() {
            return mObjectsScanned;
        }

        public int getFilesScanned() {
            return mFilesScanned;
        }

        public int getSuspiciousCount() {
            return mSuspiciousCount;
        }

        public int getDetectsCount() {
            return mDetectsCount;
        }

        public int getObjectsSkipped() {
            return mObjectsSkipped;
        }

        public void resetCounters() {
            mObjectsScanned = 0;
            mObjectsSkipped = 0;
            mFilesScanned = 0;
            mSuspiciousCount = 0;
            mDetectsCount = 0;
        }
    }
}
