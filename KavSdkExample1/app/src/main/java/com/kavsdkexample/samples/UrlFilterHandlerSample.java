package com.kavsdkexample.samples;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import android.content.Context;
import android.content.res.Resources;

import com.kaspersky.components.urlchecker.UrlInfo;
import com.kaspersky.components.urlfilter.UrlFilterHandler;
import com.kavsdkexample.R;
import com.kavsdkexample.Utils;

/**
 * Demonstration of how to implement a {@link UrlFilterHandler}.
 */
public class UrlFilterHandlerSample implements UrlFilterHandler {
    /* Application context */
    private final Context mContext;
    
    public UrlFilterHandlerSample(Context context) {
        mContext = context;
    }

    @Override
    public InputStream getBlockPageData(String url, UrlInfo urlInfo) {
        // Reading the blocking page from resources
        Resources resources = mContext.getResources();
        InputStream page = resources.openRawResource(R.raw.permission_denied);

        ByteArrayOutputStream baos = null;
        BufferedReader reader = null;
        try {
            baos = new ByteArrayOutputStream();
            reader = new BufferedReader(new InputStreamReader(page, Charset.defaultCharset()));

            // We do not store all localized pages
            // and replace the phrases in the page by the needed ones 
            // You can load localized raw resources instead
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.indexOf("${BLOCK_PAGE_TITLE}") != -1) {
                    line = line.replace("${BLOCK_PAGE_TITLE}",
                            resources.getString(R.string.app_name));
                } else if (line.indexOf("${BLOCK_PAGE_HEADER}") != -1) {
                    line = line.replace("${BLOCK_PAGE_HEADER}", "Block title");
                } else if (line.indexOf("${BLOCK_PAGE_TEXT}") != -1) {
                    line = line.replace("${BLOCK_PAGE_TEXT}", "Block text");
                } else if (line.indexOf("${BLOCK_PAGE_URL}") != -1) {
                    line = line.replace("${BLOCK_PAGE_URL}", url);
                } else if (line.indexOf("${ICON_FILE}") != -1) {
                } else if (line.indexOf("${BLOCK_PAGE_ADD_EXCLUSION}") != -1) {
                    line = line.replace("${BLOCK_PAGE_ADD_EXCLUSION}",
                            "Add exclusion");
                }
                baos.write(line.getBytes(Charset.defaultCharset()));
            }
            page = new ByteArrayInputStream(baos.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            Utils.closeQuietly(reader);
            Utils.closeQuietly(baos);
        }
        return page;
    }
}
