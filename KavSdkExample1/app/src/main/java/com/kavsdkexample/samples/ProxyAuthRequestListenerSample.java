package com.kavsdkexample.samples;

import android.content.Context;
import android.content.Intent;

import com.kavsdk.ProxyAuthRequestListener;
import com.kavsdkexample.ui.ProxyAuthActivity;

public class ProxyAuthRequestListenerSample implements ProxyAuthRequestListener {
    private final Context mContext;

    public ProxyAuthRequestListenerSample(Context context) {
        mContext = context;
    }
        
    @Override
    public void onProxyAuthRequired() {
        final Intent intent = new Intent(mContext, ProxyAuthActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        
        mContext.startActivity(intent);
    }
}
