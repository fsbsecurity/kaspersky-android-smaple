package com.kavsdkexample;

public class DuplicateKeyException extends Exception {

    private static final long serialVersionUID = 9024593073567649132L;

    public DuplicateKeyException(){
        
    }
    
    public DuplicateKeyException(String detailedMessage) {
        super(detailedMessage);
    }
    
    public DuplicateKeyException(String detailedMessage, Throwable throwable) {
        super(detailedMessage, throwable);
    }
}
