package com.kavsdkexample.ui.device_protection;

import java.io.File;
import java.util.Set;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.Toast;

import com.kavsdk.license.SdkLicenseViolationException;
import com.kavsdkexample.DuplicateKeyException;
import com.kavsdkexample.R;
import com.kavsdkexample.samples.FolderMonitorSample;
import com.kavsdkexample.app.SdkSampleApplication;
import com.kavsdkexample.shared.AppStatus;
import com.kavsdkexample.shared.activities.BaseActivity;
import com.kavsdkexample.shared.dialogs.BaseDialogFragment;
import com.kavsdkexample.shared.dialogs.OnDialogFragmentResultListener;

/**
 * Example of using the anti-virus to automatically check when add or change the files in a
 * directory
 */
public class FolderMonitorActivity extends BaseActivity implements
        OnDialogFragmentResultListener, OnCheckedChangeListener {
    private static final String TAG = FolderMonitorActivity.class.getSimpleName();
    private static final boolean DEBUG = true;
    private static final int REQUEST_ADD_FOLDER = 0;
    
    private FolderMonitorSample mFolderMonitor;
    
    private CheckBox mScanUdsCheckBox;
    private CheckBox mUnpackArchivesCheckBox;
    private CheckBox mSkipRiskwareCheckbox;
    private CheckBox mCureInfectedCheckbox;
    
    private ListView mList;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, FolderMonitorActivity.class);
    }

    @Override
    public void initComponent(AppStatus appStatus, String additionalInfo) {
        mScanUdsCheckBox.setOnCheckedChangeListener(this);
        mUnpackArchivesCheckBox.setOnCheckedChangeListener(this);
        mSkipRiskwareCheckbox.setOnCheckedChangeListener(this);
        mCureInfectedCheckbox.setOnCheckedChangeListener(this);

        mFolderMonitor = ((SdkSampleApplication)getApplication()).getService().getSampleFolderMonitor();
        mScanUdsCheckBox.setChecked(mFolderMonitor.isUdsEnabled());
        mUnpackArchivesCheckBox.setChecked(mFolderMonitor.isUnpackArchives());
        mSkipRiskwareCheckbox.setChecked(mFolderMonitor.isSkipRiskware());
        mCureInfectedCheckbox.setChecked(mFolderMonitor.isCureInfected());
        updateAdapter();
    }

    @Override
    public void initUi(Bundle savedInstanceState) {
        setContentView(R.layout.folder_monitor_activity);

        mScanUdsCheckBox = (CheckBox) findViewById(R.id.cbScanUdsAllow);
        mUnpackArchivesCheckBox = (CheckBox) findViewById(R.id.cbUnpackArchives);
        mSkipRiskwareCheckbox = (CheckBox) findViewById(R.id.cbSkipRiskware);
        mCureInfectedCheckbox = (CheckBox) findViewById(R.id.cbCureInfected);


                
        mList = (ListView) findViewById(R.id.lstFolderMonitors);
        mList.setEmptyView(findViewById(R.id.empty_list_view));
        mList.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                @SuppressWarnings("unchecked")
                ArrayAdapter<String> adapter = (ArrayAdapter<String>) parent.getAdapter();
                doDeleteFolderMonitor(adapter.getItem(position));
            }
        });

        findViewById(R.id.btnAddFolder).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                doAddFolder();
            }
        });

        findViewById(R.id.btnClearAll).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                doClearAll();
            }
        });
    }

    private void updateAdapter() {
        Set<String> folders = mFolderMonitor.getFolders();
        String[] items = folders.toArray(new String[folders.size()]);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, items);
        mList.setAdapter(adapter);
    }

    protected void doAddFolder() {
        File initDir = getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);
        if (initDir == null) {
            initDir = getFilesDir();
        }
        DirectoryChooserFragment.show(this, initDir, DirectoryChooserFragment.DIRECTORY_MODE, REQUEST_ADD_FOLDER);
    }

    private void doDeleteFolderMonitor(final String path) {
        AlertDialog.Builder b = new Builder(this);
        b.setTitle(R.string.app_name);
        b.setMessage(R.string.str_av_folder_monitor_delete_selected_folder_motitor);
        b.setNegativeButton(android.R.string.cancel, null);
        b.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                mFolderMonitor.deleteFolderMonitor(path);
                updateAdapter();
            }
        });
        b.setCancelable(true);
        b.create().show();

    }

    protected void doClearAll() {
        if (mFolderMonitor.getFolders().size() > 0) {
            AlertDialog.Builder b = new Builder(this);
            b.setTitle(R.string.app_name);
            b.setMessage(R.string.str_av_folder_monitor_delete_all_folder_motitors);
            b.setNegativeButton(android.R.string.cancel, null);
            b.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mFolderMonitor.clearAllFolderMonitors();
                    updateAdapter();
                }
            });
            b.setCancelable(true);
            b.create().show();
        }
    }

    @Override
    public void onDialogFragmentResult(BaseDialogFragment dialog, Object data) {
        if (dialog instanceof DirectoryChooserFragment) {
            File dir = (File) data;
            if (DEBUG) {
                Log.d(TAG, "selected dir = " + dir.getAbsolutePath());
            }
            try {
                mFolderMonitor.addFolderMonitor(dir);
                updateAdapter();
            } catch (SdkLicenseViolationException e) {
                showToast(R.string.str_error_license_absent_or_expired);
            } catch (DuplicateKeyException e) {
                showToast(R.string.str_av_folder_monitor_directory_already_monitored);
            }
        }
    }

    private void showToast(int resId) {
        showToast(getString(resId));
    }

    private void showToast(final String msg) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                Toast.makeText(FolderMonitorActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        try {
            switch (buttonView.getId()) {
            case R.id.cbScanUdsAllow:
                mFolderMonitor.setUdsEnabled(isChecked);
                break;
            case R.id.cbUnpackArchives:
                mFolderMonitor.setUnpackArchives(isChecked);
                break;
            case R.id.cbSkipRiskware:
                mFolderMonitor.setSkipRiskware(isChecked);
                break;
            case R.id.cbCureInfected:
                mFolderMonitor.setCureInfected(isChecked);
                break;
            default:
                break;
            }
            updateAdapter();
        } catch (SdkLicenseViolationException e) {
            showToast(R.string.str_error_license_absent_or_expired);
        }
    }
}
