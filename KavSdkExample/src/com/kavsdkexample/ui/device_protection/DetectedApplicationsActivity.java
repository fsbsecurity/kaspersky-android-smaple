package com.kavsdkexample.ui.device_protection;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.kavsdk.antivirus.ThreatInfo;
import com.kavsdk.shared.SdkUtils;
import com.kavsdkexample.R;
import com.kavsdkexample.app.LockerSuppressor;
import com.kavsdkexample.app.SdkSampleApplication;
import com.kavsdkexample.samples.AntivirusSample;
import com.kavsdkexample.shared.AppStatus;
import com.kavsdkexample.shared.UninstallDialogListener;
import com.kavsdkexample.shared.activities.BaseActivity;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class DetectedApplicationsActivity extends BaseActivity implements UninstallDialogListener, OnItemClickListener {

    private static final String SCHEME_PACKAGE = "package";
    public static final String WITH_SUPPRESSION_EXTRA = "with_suppression";
    private static final String INTENT_EXTRA_KEY = "AppMonitor";
    private static final String TAB_OAS_ID = "oas";
    private static final String TAB_ODS_ID = "ods";
    
    private ListView mListView;
    private ListView mListViewAppMonitor;
    
    private AntivirusSample mAntivirus;
    private AppListAdapter mAdapter;
    private AppListAdapter mAdapterMonitor;
    
    private List<ThreatInfo> mAppList;
    private List<ThreatInfo> mAppListMonitor;
    
    private volatile List<ThreatInfo> mAppsToRemove;

    private BroadcastReceiver mPackageRemovedReceiver;

    private boolean mWithSuppression;
    
    public static Intent newStartIntent(Context context) {
        return new Intent(context, DetectedApplicationsActivity.class);
    }

    private void start(Intent intent) {
        mWithSuppression = intent.getBooleanExtra(WITH_SUPPRESSION_EXTRA, false);

        if (!getIntent().getBooleanExtra(INTENT_EXTRA_KEY, false)) {
            mAppList = mAntivirus.getDetectedApplicationList();
            mAdapter.notifyDataSetChanged();
        } else {
            mAppListMonitor = mAntivirus.getAppDetectedListFromAppMonitor();
            mAdapterMonitor.notifyDataSetChanged();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        start(getIntent());
    }

    private void removeTopThreat() {
        if (!mAppsToRemove.isEmpty()) {

            ThreatInfo threatInfo = (ThreatInfo) mAppsToRemove.get(0);
            mAppsToRemove.remove(0);
            mAntivirus.removeNonpersitentThreat(threatInfo, DetectedApplicationsActivity.this);
        }
    }

    @Override
    public void initUi(Bundle savedInstanceState) {
        setContentView(R.layout.detected_applications_activity);

        TabHost tabs = (TabHost) findViewById(android.R.id.tabhost);
        tabs.setup();

        TabHost.TabSpec spec = tabs.newTabSpec(TAB_ODS_ID);

        spec.setContent(R.id.tab1);
        spec.setIndicator(getString(R.string.tab_name_ods));
        tabs.addTab(spec);

        spec = tabs.newTabSpec(TAB_OAS_ID);

        spec.setContent(R.id.tab2);
        spec.setIndicator(getString(R.string.tab_name_oas));
        tabs.addTab(spec);

        mListView = (ListView) findViewById(R.id.applicationsListView);
        mListViewAppMonitor = (ListView) findViewById(R.id.applicationsMonitorListView);

        mListView.setOnItemClickListener(this);
        mListView.setEmptyView(findViewById(R.id.appListEmptyView));

        mListViewAppMonitor.setOnItemClickListener(this);
        mListViewAppMonitor.setEmptyView(findViewById(R.id.appListMonitorEmptyView));

        Button removeallButton = (Button)findViewById(R.id.detected_applications_activity_removeall_button);
        removeallButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mWithSuppression) {
                    LockerSuppressor.getInstance().disable();
                }

                mAppsToRemove = Collections.synchronizedList(new ArrayList<ThreatInfo>());
                mAppsToRemove.addAll(mAppList);
                mAppsToRemove.addAll(mAppListMonitor);
                finish();
                removeTopThreat();
            }
        });

        Button closeButton = (Button)findViewById(R.id.detected_applications_activity_close_button);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SdkSampleApplication.from(DetectedApplicationsActivity.this).setLockerSuppressionStep(LockerSuppressionStep.Scanning);
                if (mWithSuppression) {
                    LockerSuppressor.getInstance().disable();
                }
                finish();
            }
        });

        if (!getIntent().getBooleanExtra(INTENT_EXTRA_KEY, false)) {
            tabs.setCurrentTabByTag(TAB_ODS_ID);
        } else {
            tabs.setCurrentTabByTag(TAB_OAS_ID);
        }

        mPackageRemovedReceiver = new PackageRemovedReceiver(this);
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_PACKAGE_REMOVED);
        filter.addDataScheme(SCHEME_PACKAGE);

        registerReceiver(mPackageRemovedReceiver, filter);
    }

    private void removeDeadApps(List<ThreatInfo> list) {
        Iterator<ThreatInfo> iter = list.iterator();
        while (iter.hasNext()) {
            ThreatInfo threat = iter.next();
            if (!SdkUtils.isPackageExisted(this, threat.getPackageName())) {
                iter.remove();
            }
        }
    }

    @Override
    public void initComponent(AppStatus appStatus, String additionalInfo) {
        SdkSampleApplication.from(this).getAppUninstallingHandlerSample().setListener(this);
        mAntivirus = SdkSampleApplication.from(this).getService().getAntivirusSample();
        mAppList = mAntivirus.getDetectedApplicationList();
        mAppListMonitor = mAntivirus.getAppDetectedListFromAppMonitor();
        removeDeadApps(mAppList);
        removeDeadApps(mAppListMonitor);
        mAdapter = new AppListAdapter(this, mAppList);
        mAdapterMonitor = new AppListAdapter(this, mAppListMonitor);
        mListView.setAdapter(mAdapter);
        mListViewAppMonitor.setAdapter(mAdapterMonitor);
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mPackageRemovedReceiver);
    }
    
    @Override
    protected void onNewIntent(Intent intent) {
        start(intent);
    };

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        
        final ThreatInfo threatInfo;
        
        if (parent.getId() == R.id.applicationsListView) {
            threatInfo = (ThreatInfo) mAdapter.getItem(position);
        } else {
            threatInfo = (ThreatInfo) mAdapterMonitor.getItem(position);
        }
        
        builder.setMessage(getString(R.string.str_antivirus_delete_app_warning, threatInfo.getPackageName()));
        builder.setNegativeButton(android.R.string.cancel, null);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (mWithSuppression) {
                    LockerSuppressor.getInstance().disable();
                }
                if (threatInfo.isDeviceAdminThreat(DetectedApplicationsActivity.this)) {
                    mAntivirus.removeDeviceAdminThreat(threatInfo, DetectedApplicationsActivity.this);
                } else {
                    mAntivirus.removeThreat(threatInfo);
                }
            }
        });
        builder.create().show();
    }
    
    private void onPackageRemoved(String packageName) {
        
        ThreatInfo itemToDelete = null;
        
        if (!mAppList.isEmpty()) {
            for (ThreatInfo info : mAppList) {
                if (info.getPackageName().equals(packageName)) {
                    itemToDelete = info;
                    break;
                }
            }
            
            if (itemToDelete != null) {
                mAppList.remove(itemToDelete);
                mAdapter.notifyDataSetChanged();
            }
        } 
        
        if (!mAppListMonitor.isEmpty()) {
            for (ThreatInfo info : mAppListMonitor) {
                if (info.getPackageName().equals(packageName)) {
                    itemToDelete = info;
                    break;
                }
            }
            
            if (itemToDelete != null) {
                mAppListMonitor.remove(itemToDelete);
                mAdapterMonitor.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onHide() {
        if (mWithSuppression) {
            LockerSuppressor.getInstance().enable();
        }
    }

    @Override
    public void onShow() {
        if (mWithSuppression) {
            LockerSuppressor.getInstance().disable();
        }
    }

    @Override
    public void onUninstall(ThreatInfo threatInfo, boolean success) {

        if (success) {
            Toast.makeText(this, threatInfo.getPackageName() + ", uninstall finished successfully", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, threatInfo.getPackageName() + ", uninstall was canceled or failed", Toast.LENGTH_SHORT).show();
        }

        if (mAppsToRemove != null && !mAppsToRemove.isEmpty()) {
            removeTopThreat();
        } else {
            if (mWithSuppression) {
                LockerSuppressor.getInstance().enable();
            }
        }
    }
    
    private static class PackageRemovedReceiver extends BroadcastReceiver {

        final WeakReference<DetectedApplicationsActivity> mActivity;
        
        PackageRemovedReceiver(DetectedApplicationsActivity activity) {
            mActivity = new WeakReference<DetectedApplicationsActivity>(activity);
        }
        
        @Override
        public void onReceive(Context context, Intent intent) {
            final DetectedApplicationsActivity activity = mActivity.get();
            if (activity != null) {
                activity.onPackageRemoved(intent.getData().toString().replace(SCHEME_PACKAGE + ":", ""));
            }
        }
    }
    
    private static class AppListAdapter extends BaseAdapter {

        private final List<ThreatInfo> mAppList;
        private final Context mContext;
        
        public AppListAdapter(Context context, List<ThreatInfo> appList) {
            mContext = context;
            mAppList = appList;
        }
        
        @Override
        public int getCount() {
            return mAppList.size();
        }

        @Override
        public Object getItem(int position) {
            return mAppList.get(position);
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            
            View listItem = null;
            ViewHolder holder = null;
            
            if (convertView != null && convertView.getTag() instanceof ViewHolder) {
                listItem = convertView;
                holder = (ViewHolder) convertView.getTag();
            } else {
                listItem = LayoutInflater.from(mContext).inflate(android.R.layout.simple_list_item_2, parent, false);
                TextView title = (TextView) listItem.findViewById(android.R.id.text1);
                TextView virusName = (TextView) listItem.findViewById(android.R.id.text2);
                holder = new ViewHolder(title, virusName);
                listItem.setTag(holder);
            }
            
            ThreatInfo info = mAppList.get(position);
            holder.mTitleText.setText(info.getPackageName());
            holder.mVirusName.setText(info.getVirusName());
            
            return listItem;
        }
        
        @Override
        public long getItemId(int position) {
            return 0;
        }
        
        private static class ViewHolder {
            
            ViewHolder(TextView title, TextView virusName) {
                mTitleText = title;
                mVirusName = virusName;
            }
            
            TextView mTitleText;
            TextView mVirusName;
        }
    }
}
