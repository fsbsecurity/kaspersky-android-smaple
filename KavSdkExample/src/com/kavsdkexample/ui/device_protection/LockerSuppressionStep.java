package com.kavsdkexample.ui.device_protection;

public enum LockerSuppressionStep {
    Scanning,
    Killing
}
