package com.kavsdkexample.ui.device_protection;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Toast;

import com.kavsdk.KavSdk;
import com.kavsdk.accessibility.AccessibilityStateListener;
import com.kavsdk.accessibility.OpenAccessibilitySettingsException;
import com.kavsdk.appcategorizer.AppCategory;
import com.kavsdk.appcontrol.AppControlFactory;
import com.kavsdk.appcontrol.AppControlMode;
import com.kavsdk.appcontrol.AppControlItem;
import com.kavsdk.appcontrol.AppControlList;
import com.kavsdkexample.R;
import com.kavsdkexample.app.SdkSampleApplication;
import com.kavsdkexample.shared.AppStatus;
import com.kavsdkexample.shared.activities.BaseActivity;
import com.kavsdkexample.storage.Settings;

public class AppControlActivity extends BaseActivity {

    private static final String TAG = AppControlActivity.class.getSimpleName();
    private static final String ADD_DIALOG_ID = "ADD_DIALOG_TAG";
    private static final String DELETE_DIALOG_ID = "DELETE_DIALOG_TAG";
    
    private static final int INDEX_BLACK_LIST_MODE = 0;
    private static final int INDEX_WHITE_LIST_MODE = 1;
    private static final int INDEX_BOTH_LISTS_MODE = 2;
        
    private ItemsAdapter mItemsAdapter;
    private AppControlList mSelectedList;
    private int mSelectedIndex; 
    
    private AppControlManager getAppControlManager() {
        final SdkSampleApplication app = SdkSampleApplication.from(this);
        return app.getAppControlManager();
    }
            
    @Override
    public void initComponent(AppStatus status, String additionalInfo) {
        
    }
    
    @Override
    public void initUi(Bundle savedInstanceState) {     
        
        setContentView(R.layout.appcontrol_fragment);       
        
        //Enable appcontrol checkbox
        final CheckBox enableCheckBox = (CheckBox) findViewById(R.id.appcontrol_enabled_checkbox);
        enableCheckBox.setChecked(Settings.isAppControlEnabled(getApplicationContext(), false));
        enableCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                getAppControlManager().enableAppControl(isChecked);
                Settings.setAppControlEnabled(getApplicationContext(), isChecked);
            }
        });

        //Use window manager checkbox
        final CheckBox useWindowManagerCheckBox = (CheckBox) findViewById(R.id.appcontrol_usewindowmanager_checkbox);
        useWindowManagerCheckBox.setChecked(Settings.useAppControlWindowManager(getApplicationContext(), false));
        useWindowManagerCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                getAppControlManager().enableWindowManager(isChecked);
                Settings.setUsingWindowManager(getApplicationContext(), isChecked);
            }
        });

        //Open accessibility settings button
        Button openSettingsButton = (Button) findViewById(R.id.appcontrol_openaccessibility_button);
        openSettingsButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    KavSdk.getAccessibility().openSettings();
                } catch (OpenAccessibilitySettingsException e) {
                    e.printStackTrace();
                }

                KavSdk.getAccessibility().setStateListener(new AccessibilityStateListener() {
                    @Override
                    public void onAccessibilityStateChanged(boolean enabled) {
                        Log.i(TAG, String.format("Accessibility is %s", enabled ? "enabled" : "disabled"));
                    }
                });

                boolean enabled = KavSdk.getAccessibility().isSettingsOn();
                Log.i(TAG, String.format("Accessibility is %s", enabled ? "enabled" : "disabled"));
            }
        });

        //Appcontrol mode spinner
        final Spinner listModeSpinner = (Spinner) findViewById(R.id.appcontrol_mode_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this,
                R.array.appcontrol_modes, 
                android.R.layout.simple_spinner_item);          
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        listModeSpinner.setAdapter(adapter);
        
        AppControlMode mode = getAppControlManager().getMode();
        
        switch (mode) {
        case BlackList:
            listModeSpinner.setSelection(INDEX_BLACK_LIST_MODE);
            break;
        case WhiteList:
            listModeSpinner.setSelection(INDEX_WHITE_LIST_MODE);
            break;
        case BothLists:
            listModeSpinner.setSelection(INDEX_BOTH_LISTS_MODE);            
            break;
        default:
            break;
        }
                        
        listModeSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                    int position, long id) {

                ((TextView)listModeSpinner.getChildAt(0)).setTextColor(Color.BLACK);

                switch (position) {
                case INDEX_BLACK_LIST_MODE:
                    getAppControlManager().setMode(AppControlMode.BlackList);
                    break;
                case INDEX_WHITE_LIST_MODE:
                    getAppControlManager().setMode(AppControlMode.WhiteList);
                    break;
                case INDEX_BOTH_LISTS_MODE:
                    getAppControlManager().setMode(AppControlMode.BothLists);
                    break;
                default:
                    break;
            }
                
                getAppControlManager().saveChanges();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                
            }
        });
        
        //Add item button
        Button addButton = (Button) findViewById(R.id.appcontrol_additem_button);
        addButton.setOnClickListener(new OnClickListener() {
            
            @Override
            public void onClick(View v) {
                AddItemDialogFragment dialog = new AddItemDialogFragment();
                dialog.show(getSupportFragmentManager(), ADD_DIALOG_ID);
            }
        }); 
        
        ListView itemsList = (ListView) findViewById(R.id.appcontrol_items_list);
        mItemsAdapter = new ItemsAdapter(this);
        itemsList.setAdapter(mItemsAdapter);
        
        itemsList.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (!view.isEnabled()) {
                    return;
                }

                boolean hitItem = false;
                
                if (position > 0 && position < (getAppControlManager().getBlackList().getItemsCount() + 1)) {
                    mSelectedList = getAppControlManager().getBlackList();
                    mSelectedIndex = position - 1;
                    hitItem = true;
                }
                
                if (position > (getAppControlManager().getBlackList().getItemsCount() + 1)) {
                    mSelectedList = getAppControlManager().getWhiteList();
                    mSelectedIndex = position - getAppControlManager().getBlackList().getItemsCount() - 2;
                    hitItem = true;
                }
                
                if (hitItem) { 
                    new DeleteDialogFragment().show(getSupportFragmentManager(), DELETE_DIALOG_ID);
                }
            }
        });     
    }
    
    public static class DeleteDialogFragment extends DialogFragment {

        private AppControlActivity mAppControlActivity;

        @Override
        public void onResume() {
            super.onResume();
            if (getActivity() instanceof AppControlActivity) {
                mAppControlActivity = (AppControlActivity)getActivity();
            } else {
                dismiss();
            }
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            setRetainInstance(true);

            return new AlertDialog.Builder(getActivity())
                .setTitle(R.string.str_appcontrol_delete_dialog_title)
                .setMessage(R.string.str_appcontrol_delete_dialog_confirm)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {                        
                    
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mAppControlActivity.mSelectedList.deleteItem(mAppControlActivity.mSelectedIndex);
                        mAppControlActivity.mItemsAdapter.notifyDataSetChanged();
                        mAppControlActivity.getAppControlManager().saveChanges();
                    }
                    
                })
                .setNegativeButton(android.R.string.no, null)
                .create();
        }
        
         @Override
         public void onDestroyView() {

             if (getDialog() != null && getRetainInstance()) {
                 getDialog().setOnDismissListener(null);
             }
             super.onDestroyView();
         } 
    }   
    
    public static class AddItemDialogFragment extends DialogFragment {

        private static final int INDEX_BLACK_LIST = 0;
        private static final int INDEX_WHITE_LIST = 1;
        
        private static AppControlActivity sAppControlActivity;

        @Override
        public void onResume() {
            super.onResume();
            if (getActivity() instanceof AppControlActivity) {
                sAppControlActivity = (AppControlActivity)getActivity();
            } else {
                dismiss();
            }
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Context context = getActivity();
            final Dialog dialog = new Dialog(context, R.style.DialogTheme);

            dialog.setContentView(R.layout.appcontrol_add_item_dialog);
            dialog.setTitle(R.string.str_appcontrol_add_item_button);
            setRetainInstance(true);        
            
            final Spinner listTypeSpinner = (Spinner) dialog.findViewById(R.id.appcontrol_add_dialog_list_type_spinner);
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                    context,
                    R.array.appcontrol_lists, 
                    android.R.layout.simple_spinner_item);          
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            listTypeSpinner.setAdapter(adapter);
            
            
            final Spinner categorySpinner = (Spinner) dialog.findViewById(R.id.appcontrol_add_dialog_item_category_spinner);
            adapter = ArrayAdapter.createFromResource(
                    context,
                    R.array.appcontrol_item_categories, 
                    android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            categorySpinner.setAdapter(adapter);

            final EditText packageNameText = (EditText) dialog.findViewById(R.id.appcontrol_add_dialog_package_name_text);
            
            Button addButton = (Button) dialog.findViewById(R.id.appcontrol_add_dialog_add_button);
            addButton.setOnClickListener(new View.OnClickListener() {           
                
                @Override
                public void onClick(View v) {

                    String packageName = packageNameText.getText().toString();
                    if (packageName.isEmpty()) {
                        packageName = null;
                    }

                    AppCategory category = null;
                    int selectedCategoryIndex = categorySpinner.getSelectedItemPosition();
                    if (selectedCategoryIndex != 0) { //0 is "any"
                        category = AppCategory.values()[selectedCategoryIndex - 1];
                    }

                    if (packageName == null && category == null) {
                        Toast.makeText(getActivity(), "Please specify package or category", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    AppControlItem item = AppControlFactory.createItem(packageName, category);

                    switch (listTypeSpinner.getSelectedItemPosition()) {
                    case INDEX_BLACK_LIST:
                        sAppControlActivity.getAppControlManager().getBlackList().addItem(item);
                        break;
                    case INDEX_WHITE_LIST:
                        sAppControlActivity.getAppControlManager().getWhiteList().addItem(item);
                        break;
                    default:
                        throw new RuntimeException("Wrong index");                      
                    }

                    sAppControlActivity.mItemsAdapter.notifyDataSetChanged();
                    sAppControlActivity.getAppControlManager().saveChanges();
                    dismiss();
                }
            });
        
            Button cancelButton = (Button) dialog.findViewById(R.id.appcontrol_add_dialog_cancel_button);
            cancelButton.setOnClickListener(new View.OnClickListener() {            
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                }
            });
            
            return dialog;
        }
        
         @Override
         public void onDestroyView() {

             if (getDialog() != null && getRetainInstance()) {
                 getDialog().setOnDismissListener(null);
             }
             super.onDestroyView();
         }      
    }
    
    private class ItemsAdapter extends BaseAdapter {
                
        private static final int TYPE_ITEM = 0;
        private static final int TYPE_SEPARATOR = 1;        
        private static final int TYPE_COUNT = TYPE_SEPARATOR + 1;
        
        private LayoutInflater mInflater;
        private Resources mResources;
       
        public ItemsAdapter(Context context) {
            mInflater = LayoutInflater.from(context);
            mResources = context.getResources();
        }
        
        @Override
        public int getViewTypeCount() {
            return TYPE_COUNT;
        }
        
        @Override
        public int getItemViewType(int position) {
            if (position == 0 ||
                position == getAppControlManager().getBlackList().getItemsCount() + 1) {
                return TYPE_SEPARATOR;
            }
            
            return TYPE_ITEM;
        }
        
        @Override
        public int getCount() {
            return getAppControlManager().getBlackList().getItemsCount() + getAppControlManager().getWhiteList().getItemsCount() + 2;           
        }
    
        @Override
        public Object getItem(int position) {
            return position;
        }
    
        @Override
        public long getItemId(int position) {
            return position;
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            int type = getItemViewType(position);
            if (convertView == null) {
                holder = new ViewHolder();
                switch(type) {
                case TYPE_ITEM:
                    convertView = mInflater.inflate(android.R.layout.simple_list_item_1, parent, false);
                    holder.textView = (TextView)convertView.findViewById(android.R.id.text1);
                    holder.textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.text_small));
                    break;
                case TYPE_SEPARATOR:
                    convertView = mInflater.inflate(R.layout.appcontrol_items_separator, null);
                    holder.textView = (TextView)convertView.findViewById(R.id.appcontrol_separator_text);
                    break;
                default:
                    throw new RuntimeException("Wrong item type");
                }
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder)convertView.getTag();
            }
                    
            if (position == 0) {            
                //Black list header
                holder.textView.setText(mResources.getString(R.string.str_appcontrol_blacklist_header));
            
            } else if (position < getAppControlManager().getBlackList().getItemsCount() + 1) {              
                //Black list itself
                int index = position - 1;
                AppControlItem item = getAppControlManager().getBlackList().getItem(index);
                holder.textView.setText(getItemTitle(item));
                
            } else if (position == getAppControlManager().getBlackList().getItemsCount() + 1) {
                //White list header
                holder.textView.setText(mResources.getString(R.string.str_appcontrol_whitelist_header));
                
            } else {
                //Black list itself
                int index = position - getAppControlManager().getBlackList().getItemsCount() - 2;
                AppControlItem item = getAppControlManager().getWhiteList().getItem(index);
                holder.textView.setText(getItemTitle(item));
            }
                                
            convertView.setEnabled(parent.isEnabled());
            return convertView;
        }

        private String getItemTitle(AppControlItem item) {

            String itemPackage = item.getPackage();
            AppCategory itemCategory = item.getCategory();

            StringBuilder sb = new StringBuilder();
            sb.append(itemPackage != null ? itemPackage : getString(R.string.str_appcontrol_any_package)).append(", ");
            sb.append(itemCategory != null ? itemCategory : getString(R.string.str_appcontrol_any_category));

            return sb.toString();
        }
    }

     public static class ViewHolder {
            public TextView textView;
     }
}
