package com.kavsdkexample.ui.device_protection;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.kavsdkexample.R;
import com.kavsdkexample.app.LockerSuppressor;
import com.kavsdkexample.app.SdkSampleApplication;
import com.kavsdkexample.samples.AntivirusSample;
import com.kavsdkexample.shared.AppStatus;
import com.kavsdkexample.shared.activities.BaseActivity;

public class KillLockerActivity extends BaseActivity implements AntivirusSample.Listener {

    private AntivirusSample mAntivirusSample;
    private AntivirusSample.Settings mSampleAntivirusSettings;
    private Button mScanButton;
    private SdkSampleApplication mApp;

    @Override
    public void initUi(Bundle savedInstanceState) {
        setContentView(R.layout.kill_locker_activity);

        mScanButton = (Button)findViewById(R.id.kill_locker_activity_scan_button);
        mScanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAntivirusSample.startScan(mSampleAntivirusSettings);
            }
        });

        Button cancelButton = (Button)findViewById(R.id.kill_locker_activity_cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LockerSuppressor.getInstance().disable();
                mAntivirusSample.stopScan();
                finish();
            }
        });

        if (mAntivirusSample != null) {
            mScanButton.setEnabled(mAntivirusSample.isStopped());
        }
    }

    @Override
    public void initComponent(AppStatus appStatus, String additionalInfo) {
        mApp = ((SdkSampleApplication)getApplication());

        mSampleAntivirusSettings = new AntivirusSample.Settings();
        mSampleAntivirusSettings.mMemoryPartition = AntivirusSample.SCAN_INSTALLED_APPS;

        mAntivirusSample = mApp.getService().getAntivirusSample();
        mAntivirusSample.addListener(this);
        mScanButton.setEnabled(mAntivirusSample.isStopped());
    }

    @Override
    public void onStartScan() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mScanButton.setEnabled(false);
            }
        });
    }

    @Override
    public void onQuarantine() {

    }

    @Override
    public void onStopScan() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mScanButton.setEnabled(true);
            }
        });

        if (!mAntivirusSample.getDetectedApplicationList().isEmpty()) {
            Intent intent = DetectedApplicationsActivity.newStartIntent(getApplicationContext());
            intent.putExtra(DetectedApplicationsActivity.WITH_SUPPRESSION_EXTRA, true);
            LockerSuppressor.getInstance().startActivity(getApplicationContext(), intent);
            mApp.setLockerSuppressionStep(LockerSuppressionStep.Killing);
        }
    }
}
