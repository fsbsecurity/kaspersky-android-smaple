package com.kavsdkexample.ui.device_protection;

import java.io.File;
import java.util.List;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TabHost;

import com.kavsdk.antivirus.Antivirus;
import com.kavsdk.antivirus.AntivirusInstance;
import com.kavsdk.antivirus.MonitorItem;
import com.kavsdk.antivirus.MonitorNotifyConstants;
import com.kavsdkexample.R;
import com.kavsdkexample.shared.AppStatus;
import com.kavsdkexample.shared.activities.BaseActivity;
import com.kavsdkexample.shared.dialogs.BaseDialogFragment;
import com.kavsdkexample.shared.dialogs.OnDialogFragmentResultListener;

/**
 * Example of using the anti-virus to automatically check when add or change the files in a
 * directory
 */
public class RtpMonitorDirectoryManagerActivity extends BaseActivity implements
        OnDialogFragmentResultListener {
    private static final String TAG = RtpMonitorDirectoryManagerActivity.class.getSimpleName();
    private static final boolean DEBUG = false;
    private static final int REQUEST_ADD_DIRECTORY = 0;
    private static final int REQUEST_ADD_EXCLUSION = 1;
    
    private Antivirus mAntivirus;
    private ArrayAdapter<String> mDirectoriesAdapter;
    private ArrayAdapter<String> mExclusionAdapter;
    
    private ListView mDirectoryList;
    private ListView mExclusionList;
    private CheckBox mChbNotifyAccess;
    private CheckBox mChbNotifyModify;
    private CheckBox mChbNotifyAttrib;
    private CheckBox mChbNotifyCloseWrite;
    private CheckBox mChbNotifyCloseNoWrite;
    private CheckBox mChbNotifyOpen;
    private CheckBox mChbNotifyMovedFrom;
    private CheckBox mChbNotifyMovedTo;
    private CheckBox mChbNotifyCreate;
    private CheckBox mChbNotifyDelete;
    private CheckBox mChbNotifyMoveSelf;
    private CheckBox mChbNotifyDeleteSelf;
    
    public static Intent getStartIntent(Context context) {
        return new Intent(context, RtpMonitorDirectoryManagerActivity.class);
    }

    @Override
    public void initComponent(AppStatus appStatus, String additionalInfo) {
        mAntivirus = AntivirusInstance.getInstance();
        updateDirectoriesAdapter();
        updateExclusionsAdapter();
    }

    @Override
    public void initUi(Bundle savedInstanceState) {
        setContentView(R.layout.rtp_monitor_directory_manager_activity);

        TabHost tabs = (TabHost) findViewById(android.R.id.tabhost);

        tabs.setup();

        TabHost.TabSpec spec = tabs.newTabSpec("addDirectoryTab");
        spec.setContent(R.id.addDirectoryTab);
        spec.setIndicator(getString(R.string.str_av_rtp_monitor_add_directory_tab));
        tabs.addTab(spec);
        
        spec = tabs.newTabSpec("directoriesTab");
        spec.setContent(R.id.directoriesTab);
        spec.setIndicator(getString(R.string.str_av_rtp_monitor_directories_tab));
        tabs.addTab(spec);

        spec = tabs.newTabSpec("exclusionsTab");
        spec.setContent(R.id.exclusionsTab);
        spec.setIndicator(getString(R.string.str_av_rtp_monitor_exclusions_tab));
        tabs.addTab(spec);

        tabs.setCurrentTab(0);
        
        mChbNotifyAccess = (CheckBox) findViewById(R.id.cbNotifyAccess);
        mChbNotifyModify = (CheckBox) findViewById(R.id.cbNotifyModify);
        mChbNotifyAttrib = (CheckBox) findViewById(R.id.cbNotifyAttrib);
        mChbNotifyCloseWrite = (CheckBox) findViewById(R.id.cbNotifyCloseWrite);
        mChbNotifyCloseNoWrite = (CheckBox) findViewById(R.id.cbNotifyCloseNoWrite);
        mChbNotifyOpen = (CheckBox) findViewById(R.id.cbNotifyOpen);
        mChbNotifyMovedFrom = (CheckBox) findViewById(R.id.cbNotifyMovedFrom);
        mChbNotifyMovedTo = (CheckBox) findViewById(R.id.cbNotifyMovedTo);
        mChbNotifyCreate = (CheckBox) findViewById(R.id.cbNotifyCreate);
        mChbNotifyDelete = (CheckBox) findViewById(R.id.cbNotifyDelete);
        mChbNotifyMoveSelf = (CheckBox) findViewById(R.id.cbNotifyMoveSelf);
        mChbNotifyDeleteSelf = (CheckBox) findViewById(R.id.cbNotifyDeleteSelf);
        
        mDirectoryList = (ListView) findViewById(R.id.lstMonitoredFolders);
        mDirectoryList.setEmptyView(findViewById(R.id.empty_list_view));
        mDirectoryList.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                @SuppressWarnings("unchecked")
                ArrayAdapter<String> adapter = (ArrayAdapter<String>) parent.getAdapter();
                doDeleteFolder(adapter.getItem(position));
            }
        });
        
        findViewById(R.id.btnAddFolder).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                doAddFolder();
            }
        });

        mExclusionList = (ListView) findViewById(R.id.lstExcludedFolders);
        mExclusionList.setEmptyView(findViewById(R.id.empty_list_view_2));
        mExclusionList.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                @SuppressWarnings("unchecked")
                ArrayAdapter<String> adapter = (ArrayAdapter<String>) parent.getAdapter();
                doDeleteExclusion(adapter.getItem(position));
            }
        });
        
        findViewById(R.id.btnAddExclusionFolder).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                doAddExclusion();
            }
        });
    }
    
    private int getNotifyFlags() {
        int flags = 0;
        if (mChbNotifyAccess.isChecked()) {
            flags |= MonitorNotifyConstants.NOTIFY_ACCESS;
        }
        if (mChbNotifyModify.isChecked()) {
            flags |= MonitorNotifyConstants.NOTIFY_MODIFY;
        }
        if (mChbNotifyAttrib.isChecked()) {
            flags |= MonitorNotifyConstants.NOTIFY_ATTRIB;
        }
        if (mChbNotifyCloseWrite.isChecked()) {
            flags |= MonitorNotifyConstants.NOTIFY_CLOSE_WRITE;
        }
        if (mChbNotifyCloseNoWrite.isChecked()) {
            flags |= MonitorNotifyConstants.NOTIFY_CLOSE_NOWRITE;
        }
        if (mChbNotifyOpen.isChecked()) {
            flags |= MonitorNotifyConstants.NOTIFY_OPEN;
        }
        if (mChbNotifyMovedFrom.isChecked()) {
            flags |= MonitorNotifyConstants.NOTIFY_MOVED_FROM;
        }
        if (mChbNotifyMovedTo.isChecked()) {
            flags |= MonitorNotifyConstants.NOTIFY_MOVED_TO;
        }
        if (mChbNotifyCreate.isChecked()) {
            flags |= MonitorNotifyConstants.NOTIFY_CREATE;
        }
        if (mChbNotifyDelete.isChecked()) {
            flags |= MonitorNotifyConstants.NOTIFY_DELETE;
        }
        if (mChbNotifyMoveSelf.isChecked()) {
            flags |= MonitorNotifyConstants.NOTIFY_MOVE_SELF;
        }
        if (mChbNotifyDeleteSelf.isChecked()) {
            flags |= MonitorNotifyConstants.NOTIFY_DELETE_SELF;
        }
        return flags;
    }
    
    private void updateDirectoriesAdapter() {
        List<MonitorItem> folders = mAntivirus.getMonitoredDirectories();
        String[] items = new String[folders.size()];
        int i = 0;
        for (MonitorItem item : folders) {
            items[i] = item.getPath();
            ++i;
        }
        mDirectoriesAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, items);
        mDirectoryList.setAdapter(mDirectoriesAdapter);
    }

    private void updateExclusionsAdapter() {
        List<String> folders = mAntivirus.getMonitorExclusions();
        String[] items = folders.toArray(new String[folders.size()]);
        mExclusionAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, items);
        mExclusionList.setAdapter(mExclusionAdapter);
    }
    
    protected void doAddExclusion() {
        File initDir = getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);
        if (initDir == null) {
            initDir = getFilesDir();
        }
        DirectoryChooserFragment.show(this, initDir, DirectoryChooserFragment.DIRECTORY_MODE, REQUEST_ADD_EXCLUSION);
    }
    
    private void doDeleteExclusion(final String path) {
        AlertDialog.Builder b = new Builder(this);
        b.setTitle(R.string.app_name);
        b.setMessage(R.string.str_av_folder_monitor_delete_selected_folder_motitor);
        b.setNegativeButton(android.R.string.cancel, null);
        b.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mAntivirus.removeExclusionFromMonitor(path);
                updateExclusionsAdapter();
            }
        });
        b.setCancelable(true);
        b.create().show();
    }
    
    protected void doAddFolder() {
        File initDir = getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);
        if (initDir == null) {
            initDir = getFilesDir();
        }
        DirectoryChooserFragment.show(this, initDir, DirectoryChooserFragment.DIRECTORY_MODE, REQUEST_ADD_DIRECTORY);
    }

    private void doDeleteFolder(final String path) {
        AlertDialog.Builder b = new Builder(this);
        b.setTitle(R.string.app_name);
        b.setMessage(R.string.str_av_folder_monitor_delete_selected_folder_motitor);
        b.setNegativeButton(android.R.string.cancel, null);
        b.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mAntivirus.removeDirectoryFromMonitor(path);
                updateDirectoriesAdapter();
            }
        });
        b.setCancelable(true);
        b.create().show();
    }

    @Override
    public void onDialogFragmentResult(BaseDialogFragment dialog, Object data) {
        
        if (dialog instanceof DirectoryChooserFragment) {
            File dir = (File) data;
            int requestId = ((DirectoryChooserFragment)dialog).getRequestId();
            if (DEBUG) {
                Log.d(TAG, "selected dir = " + dir.getAbsolutePath());
            }
            if (requestId == REQUEST_ADD_DIRECTORY) {
                mAntivirus.addDirectoryToMonitor(dir.getPath(), getNotifyFlags());
                updateDirectoriesAdapter();
            } else if (requestId == REQUEST_ADD_EXCLUSION) {
                mAntivirus.addExclusionToMonitor(dir.getPath());
                updateExclusionsAdapter();
            }
        }
    }
}
