package com.kavsdkexample.ui.plugins;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.kavsdk.KavSdk;
import com.kavsdk.plugins.KfpPluginParams;
import com.kavsdk.plugins.Plugin;
import com.kavsdkexample.BuildConfig;
import com.kavsdkexample.R;
import com.kavsdkexample.shared.AppStatus;
import com.kavsdkexample.shared.activities.BaseActivity;
import com.kavsdkexample.ui.NoticeDialogFragment;

public class PluginConsoleActivity extends BaseActivity implements NoticeDialogFragment.NoticeDialogListener {
    private static final String TAG = PluginConsoleActivity.class.getSimpleName();

    private static final String EXTRA_PLUGIN_NAME = "plugin_name";

    private Plugin mPlugin;
    private Spinner mSpinParamName;
    private EditText mEtParamValue;

    public static Intent getStartIntent(Context context, Plugin plugin) {
        Intent i = new Intent(context, PluginConsoleActivity.class);
        i.putExtra(EXTRA_PLUGIN_NAME, plugin.getName());
        return i;
    }

    private static Plugin findPlugin(Intent intent) {
        String pluginName = intent.getStringExtra(EXTRA_PLUGIN_NAME);
        for (Plugin plugin : KavSdk.getPluginManager().getPlugins()) {
            if (pluginName.equals(plugin.getName())) {
                return plugin;
            }
        }
        return null;
    }

    private List<String> getPredefinedParams() {
        List<String> lst = new ArrayList<String>();
        for (Field f : KfpPluginParams.class.getDeclaredFields()) {
            if (Modifier.isStatic(f.getModifiers()) && f.getName().startsWith("PARAM_NAME")) {
                try {
                    lst.add(f.get(mPlugin).toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return lst;
    }

    @Override
    public void initComponent(AppStatus appStatus, String additionalInfo) {
        mPlugin = findPlugin(getIntent());
        if (mPlugin == null) {
            Toast.makeText(this, R.string.str_plugin_already_unloaded, Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    public void initUi(Bundle savedInstanceState) {
        setContentView(R.layout.plugin_console_activity);
        initControls();
    }

    private void initControls() {
        mSpinParamName = (Spinner) findViewById(R.id.spinParamName);
        ArrayAdapter<String> adaper = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, getPredefinedParams());
        adaper.setDropDownViewResource(android.R.layout.select_dialog_item);
        mSpinParamName.setAdapter(adaper);
        mEtParamValue = (EditText) findViewById(R.id.etParamValue);
        Button btn = (Button) findViewById(R.id.btnSetData);
        btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                onSetDataClick();
            }
        });
    }

    protected void onSetDataClick() {
        final String paramName = (String) mSpinParamName.getSelectedItem();
        final String value = mEtParamValue.getText().toString();
        if (KfpPluginParams.PARAM_NAME_KFP_ADD_STATISTIC_TARGET.equals(paramName)) {
            AddStasticTargetFragmentDialog dialogFragment = new AddStasticTargetFragmentDialog();
            dialogFragment.show(getSupportFragmentManager(), null);
        } else if (KfpPluginParams.PARAM_NAME_KFP_PLUGIN_SEND_MOB_APP_ACTION.equals(paramName)) {
            MobAppActionFragmentDialog dialogFragment = new MobAppActionFragmentDialog();
            dialogFragment.show(getSupportFragmentManager(), null);
        } else {
            boolean result = mPlugin.setData(paramName, value);
            showResult(paramName, result);
        }
    }

    private void showResult(String name, boolean result) {
        String msg = getString(R.string.str_plugin_console_set_data_result_frm, name, result);
        Log.d(TAG, "set data result: " + msg);
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        if (dialog instanceof AddStasticTargetFragmentDialog) {
            String url = ((AddStasticTargetFragmentDialog) dialog).getUrl();
            String certPath = ((AddStasticTargetFragmentDialog) dialog).getBase64Cert();
            String consumerId = ((AddStasticTargetFragmentDialog) dialog).getConsumerId();
            addStatTarget(url, certPath, consumerId);

        } else if (dialog instanceof MobAppActionFragmentDialog) {
            Object data = KfpPluginParams.createMobAppActionEventObject(
                    ((MobAppActionFragmentDialog) dialog).getActionType(),
                    ((MobAppActionFragmentDialog) dialog).getActionResult(),
                    ((MobAppActionFragmentDialog) dialog).getDescription()
            );
            boolean result = mPlugin.setData(KfpPluginParams.PARAM_NAME_KFP_PLUGIN_SEND_MOB_APP_ACTION, data);
            showResult(KfpPluginParams.PARAM_NAME_KFP_PLUGIN_SEND_MOB_APP_ACTION, result);
        }
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        if (dialog instanceof AddStasticTargetFragmentDialog) {
            showResult(KfpPluginParams.PARAM_NAME_KFP_ADD_STATISTIC_TARGET, false);
        } else if (dialog instanceof MobAppActionFragmentDialog) {
            showResult(KfpPluginParams.PARAM_NAME_KFP_PLUGIN_SEND_MOB_APP_ACTION, false);
        }
    }

    private void addStatTarget(String url, String certPath, String consumerId) {
        BufferedReader reader = null;
        String errorMsg = null;
        try {

            reader = new BufferedReader(new InputStreamReader(new FileInputStream(certPath), Charset.defaultCharset()));
            StringBuffer stringBuffer = new StringBuffer();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuffer.append(line);
            }
            byte[] cert = Base64.decode(stringBuffer.toString(), Base64.DEFAULT);

            Object data = KfpPluginParams.createStatTargetObject(url, cert, consumerId);
            boolean result = mPlugin.setData(KfpPluginParams.PARAM_NAME_KFP_ADD_STATISTIC_TARGET, data);
            showResult(KfpPluginParams.PARAM_NAME_KFP_ADD_STATISTIC_TARGET, result);
            return;
        } catch (IllegalArgumentException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            errorMsg = "Incorrect base64-encoded certificate";
        } catch (FileNotFoundException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            errorMsg = "Certificate file was not found";
        } catch (IOException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            errorMsg = "Error occured during work with cert file";
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        Toast.makeText(this, errorMsg, Toast.LENGTH_LONG).show();
    }
}
