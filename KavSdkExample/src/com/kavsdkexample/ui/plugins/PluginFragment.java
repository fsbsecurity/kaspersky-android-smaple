package com.kavsdkexample.ui.plugins;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.kavsdk.KavSdk;
import com.kavsdk.plugins.Plugin;
import com.kavsdk.plugins.PluginManager;
import com.kavsdkexample.R;
import com.kavsdkexample.shared.AppStatus;
import com.kavsdkexample.shared.BaseFragment;

public class PluginFragment extends BaseFragment {

    private ListView mListView;
    private View mEmptyHolder;

    @Override
    public void initComponent(AppStatus appStatus, String additionalInfo) {
        if (appStatus != AppStatus.InitFailed) {
            refreshAdapter(KavSdk.getPluginManager());
        }
    }

    @Override
    public View initUi(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.plugin_fragment, container, false);
        initControls(root);
        return root;
    }

    private void initControls(View root) {
        mListView = (ListView) root.findViewById(android.R.id.list);
        mListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Plugin plugin = ((PluginAdapter) parent.getAdapter()).getItem(position);
                openPlugin(plugin);
            }
        });
        mEmptyHolder = root.findViewById(R.id.plugin_empty_holder);
    }

    protected void openPlugin(Plugin plugin) {
        Activity activity = getActivity();
        if (activity != null) {
            activity.startActivity(PluginConsoleActivity.getStartIntent(activity, plugin));
        }
    }

    private void refreshAdapter(PluginManager pluginManager) {
        PluginAdapter adapter = new PluginAdapter(getActivity(), pluginManager.getPlugins());
        mListView.setAdapter(adapter);
        mEmptyHolder.setVisibility(adapter.getCount() > 0 ? View.GONE : View.VISIBLE);
    }

    private static class PluginAdapter extends BaseAdapter {

        private final List<Plugin> mPlugins;
        private final LayoutInflater mLi;

        PluginAdapter(Context context, List<Plugin> plugins) {
            mLi = LayoutInflater.from(context);
            mPlugins = plugins;
        }

        @Override
        public int getCount() {
            return mPlugins.size();
        }

        @Override
        public Plugin getItem(int position) {
            return mPlugins.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            ViewHolder holder = null;

            if (v == null) {
                v = mLi.inflate(android.R.layout.simple_list_item_1, parent, false);
                holder = new ViewHolder(v);
                v.setTag(holder);
            } else {
                holder = (ViewHolder) v.getTag();
            }
            Plugin plugin = getItem(position);
            holder.mDesc.setText(String.format("%s (%s)", plugin.getName(), plugin.getVersion()));
            return v;
        }

        private static class ViewHolder {
            private final TextView mDesc;

            ViewHolder(View row) {
                mDesc = (TextView) row.findViewById(android.R.id.text1);
            }
        }
    }
}
