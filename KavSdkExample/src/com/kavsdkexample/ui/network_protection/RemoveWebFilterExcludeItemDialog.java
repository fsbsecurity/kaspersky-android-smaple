package com.kavsdkexample.ui.network_protection;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.kavsdk.webfilter.WebFilterControl;
import com.kavsdkexample.app.SdkSampleApplication;
import com.kavsdkexample.shared.dialogs.DialogsSupplier;
import com.kavsdkexample.shared.dialogs.UiUpdater;

/**
 * This is a dialog for removing a URL from the list of exclusions
 */
public class RemoveWebFilterExcludeItemDialog extends DialogFragment {
    public static final String ARG_URL_TO_DELETE = "url_to_delete";
    
    /*
     * A notifier used for GUI updated after the exclusions list changes
     */
    private UiUpdater mUiUpdater;
    
    public static RemoveWebFilterExcludeItemDialog newInstance(Bundle bundle) {
        RemoveWebFilterExcludeItemDialog dialog = new RemoveWebFilterExcludeItemDialog();
        
        // Supply the information about the URL to be removed
        dialog.setArguments(bundle);
        
        return dialog;
    }
    
    public RemoveWebFilterExcludeItemDialog setOnListUpdatedListener(UiUpdater updater) {
        mUiUpdater = updater;
        return this;
    }
    
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Activity act = getActivity();
        WebFilterControl webFilterControl = ((SdkSampleApplication)act.getApplication()).getWebFilterControl();
        String urlToDelete = getArguments().getString(ARG_URL_TO_DELETE);
        return DialogsSupplier.getRemoveWebFilterExcludeItem(act, webFilterControl, urlToDelete, mUiUpdater);
    }
}
