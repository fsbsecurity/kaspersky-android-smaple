package com.kavsdkexample.ui.network_protection;

import com.kavsdk.webfilter.WebFilterControl;
import com.kavsdkexample.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.lang.ref.WeakReference;

/**
 * This is a listView adapter for web filter exclusions
 */
public class WfItemsAdapter extends BaseAdapter {
    private final LayoutInflater mInflater;
    private final WeakReference<WebFilterControl> mWebFilterControlRef;

    public WfItemsAdapter(Context context, WebFilterControl webFilterControl) {
        mInflater = LayoutInflater.from(context);
        mWebFilterControlRef = new WeakReference<WebFilterControl>(webFilterControl);
    }

    @Override
    public int getCount() {
        final WebFilterControl webFilterControl = mWebFilterControlRef.get();
        if (webFilterControl != null) {
            return webFilterControl.getExclusionsCount();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mInflater.inflate(
                    android.R.layout.simple_list_item_1, null);
        }
        TextView textView = (TextView) convertView.findViewById(android.R.id.text1);
        textView.setTextColor(R.color.text_color);

        final WebFilterControl webFilterControl = mWebFilterControlRef.get();
        String text = null;
        if (webFilterControl != null) {
            // An exclusion item consist of only its name
            // Getting the name
            text = webFilterControl.getExclusion(position);
        }
        // And showing it
        if (text != null) {
            textView.setText(text);
        }

        return convertView;
    }
}
