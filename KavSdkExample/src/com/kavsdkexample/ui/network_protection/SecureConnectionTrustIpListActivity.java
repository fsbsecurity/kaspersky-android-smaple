package com.kavsdkexample.ui.network_protection;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.io.StreamCorruptedException;
import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import com.kavsdkexample.NetUtils;
import com.kavsdkexample.R;
import com.kavsdkexample.Utils;

public class SecureConnectionTrustIpListActivity extends ListActivity {

    static final String EXTRA_TRUSTED_URLS = "trusted_urls";

    private static final String TRUSTED_URLS_FILENAME = Environment.getExternalStorageDirectory() + "/trusted_urls"; 
    private static final File TRUSTED_URLS_FILE = new File(TRUSTED_URLS_FILENAME);
    
    private ArrayList<String> mTrustedUrls;
    private EditText mNewIpEditText;

    public static Intent getIntent(Context context, ArrayList<String> trustedUrls) {
        Intent i = new Intent(context, SecureConnectionTrustIpListActivity.class);
        i.putExtra(EXTRA_TRUSTED_URLS, trustedUrls);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.secure_connection_trustip_listactivity);
        initControl();
    }

    public static void saveTrustedUrls(ArrayList<String> urls) {
        ObjectOutputStream out = null;
        try {
            FileOutputStream fos = new FileOutputStream(TRUSTED_URLS_FILE);
            BufferedOutputStream bos = new BufferedOutputStream(fos);
            out = new ObjectOutputStream(bos);
            out.writeObject(urls);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            Utils.closeQuietly(out);
        }
    }

    @SuppressWarnings("unchecked")
    public static ArrayList<String> loadTrustedUrls() {
        ObjectInputStream in = null;
        try {
            FileInputStream fos = new FileInputStream(TRUSTED_URLS_FILE);
            BufferedInputStream bos = new BufferedInputStream(fos);
            in = new ObjectInputStream(bos);
            return (ArrayList<String>) in.readObject();
        } catch (OptionalDataException e) {
            e.printStackTrace();
        } catch (StreamCorruptedException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            Utils.closeQuietly(in);         
        }
        return new ArrayList<String>();
    }
    
    @Override
    public void onStop() {
        super.onStop();
        saveTrustedUrls(mTrustedUrls);
    }
    
    @Override
    public boolean onKeyDown(int keyCode, android.view.KeyEvent event) {

        if (KeyEvent.KEYCODE_BACK == keyCode) {
            Intent i = new Intent();
            i.putExtra(EXTRA_TRUSTED_URLS, mTrustedUrls);
            setResult(RESULT_OK, i);
        }

        return super.onKeyDown(keyCode, event);
    }

    private void initControl() {

        mNewIpEditText = (EditText) findViewById(R.id.newIpText);
        Button btn = (Button) findViewById(R.id.addIpButton);
        btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                doAddItem();
            }
        });

        mTrustedUrls = getIntent().getExtras().getStringArrayList(EXTRA_TRUSTED_URLS);
        if (mTrustedUrls == null) {
            mTrustedUrls = new ArrayList<String>();
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, mTrustedUrls);
        setListAdapter(adapter);
        getListView().setOnItemLongClickListener(new OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View v, int position, long id) {
                doRemoveItem(position);
                return true;
            }
        });

    }

    @SuppressWarnings("unchecked")
    protected void doAddItem() {

        String ip = mNewIpEditText.getText().toString();
        if (NetUtils.isValidIPv4(ip)) {

            mTrustedUrls.add(ip);
            mNewIpEditText.setText("");
            ((ArrayAdapter<String>) getListAdapter()).notifyDataSetChanged();

        }
    }

    protected void doRemoveItem(final int position) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.str_msg_delete_selected_item);
        builder.setNegativeButton(android.R.string.no, null);
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

            @SuppressWarnings("unchecked")
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mTrustedUrls.remove(position);
                ((ArrayAdapter<String>) SecureConnectionTrustIpListActivity.this.getListAdapter())
                        .notifyDataSetChanged();

            }
        });
        builder.create().show();

    }
}
