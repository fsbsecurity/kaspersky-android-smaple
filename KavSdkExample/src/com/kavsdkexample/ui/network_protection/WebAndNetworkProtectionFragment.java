package com.kavsdkexample.ui.network_protection;

import com.kavsdkexample.R;
import com.kavsdkexample.shared.AppStatus;
import com.kavsdkexample.shared.activities.DialogFragmentActivity;
import com.kavsdkexample.Utils;
import com.kavsdkexample.shared.dialogs.DialogsSupplier;
import com.kavsdkexample.shared.BaseFragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;


public class WebAndNetworkProtectionFragment extends BaseFragment implements OnClickListener {
    
    @Override
    public View initUi(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.web_and_network_protection_fragment, container, false);
        
        Utils.initCommandView(view, R.id.open_webfilter_button, R.string.str_web_open_webfilter, this);
        Utils.initCommandView(view, R.id.open_antispam_button, R.string.str_web_open_antispam, this);
        Utils.initCommandView(view, R.id.open_dns_checker_button, R.string.str_dsc_open_dns_checker, this);
        Utils.initCommandView(view, R.id.test_secure_connection_button, R.string.str_secureconnection_test_button, this);
        Utils.initCommandView(view, R.id.ScanUrl, R.string.str_antivirus_scan_url, this);
        Utils.initCommandView(view, R.id.check_url_sertef_button, R.string.str_dsc_check_certificate_url, this);
        Utils.initCommandView(view, R.id.check_url_reputation_button, R.string.main_check_url_button, this);
        Utils.initCommandView(view, R.id.check_url_reputation_ext_button, R.string.main_check_url_ext_button, this);
        Utils.initCommandView(view, R.id.check_financial_category_button, R.string.main_check_bank_url_button, this);

        return view;
    }

    @Override
    public void initComponent(AppStatus appStatus, String additionalInfo) {

    }

    @Override
    public void onClick(View v) {
        DialogFragmentActivity activity = (DialogFragmentActivity) getActivity();
        switch (v.getId()) {
        case R.id.open_webfilter_button:
            startActivity(new Intent(activity, WebFilterActivity.class));
            break;
        case R.id.open_antispam_button:
            startActivity(new Intent(activity, AntispamActivity.class));
            break;
        case R.id.open_dns_checker_button:
            startActivity(new Intent(activity, DnsCheckerActivity.class));
            break;
        case R.id.test_secure_connection_button:
            startActivity(new Intent(activity, SecureConnectionActivity.class));
            break;
        case R.id.ScanUrl:
            activity.showFragmentDialog(DialogsSupplier.DIALOG_ANTIVIRUS_CHECK_URL);
            break;
        case R.id.check_url_sertef_button:
            activity.showFragmentDialog(DialogsSupplier.DIALOG_CERTIFICATE_CHECK);
            break;
        case R.id.check_url_reputation_button:
            activity.showFragmentDialog(DialogsSupplier.DIALOG_REPUTATION_CHECK);
            break;
        case R.id.check_url_reputation_ext_button:
            activity.showFragmentDialog(DialogsSupplier.DIALOG_REPUTATION_EXT_CHECK);
            break;
        case R.id.check_financial_category_button:
            activity.showFragmentDialog(DialogsSupplier.DIALOG_FINANCIAL_CHECK);
            break;
        default:
            break;
        }
    }

}
