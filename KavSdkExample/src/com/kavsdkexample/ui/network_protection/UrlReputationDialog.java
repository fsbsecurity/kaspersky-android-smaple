package com.kavsdkexample.ui.network_protection;

import java.util.List;

import android.widget.ArrayAdapter;

import com.kaspersky.components.urlchecker.UrlCategory;
import com.kaspersky.components.urlchecker.UrlInfo;
import com.kavsdk.license.SdkLicenseViolationException;
import com.kaspersky.components.urlchecker.UrlCategoryExt;
import com.kavsdk.urlchecker.UrlCheckService;
import com.kavsdkexample.R;
import com.kavsdkexample.shared.dialogs.BaseSimpleDialog;

/**
 * This dialog checks a reputation of an entered URL and
 * shows the result in a toast.
 */
public class UrlReputationDialog extends BaseSimpleDialog {
    
    /** 
     * This method checks the URL reputation of an URL entered into the edit box.
     * In order to verify a URL, SDK asks a KSN server via a network. 
     * Therefore, this method has to be invoked from a non-GUI thread
     * 
     * @return a verdict about a "goodness" of the URL
     */
    @Override
    public String check() { 
        try {
            final UrlCheckService service = new UrlCheckService(mContext);
            String url = mTextView.getText().toString();

            UrlInfo result;
            if (mExtendedCategoriesEnabled) {
                result = service.checkUrlExt(url);
            } else {
                result = service.checkUrl(url);
            }

            if (result != null) {
                return mContext.getString(R.string.str_url_reputation_dialog_result) + getInfo(result);
            } else {
                return mContext.getString(R.string.str_url_reputation_dialog_result_null);
            }

        } catch (SdkLicenseViolationException e) {
            return mContext.getString(R.string.str_url_reputation_dialog_result_exception) + e.toString();
        } catch (Throwable e) {
            return mContext.getString(R.string.str_url_reputation_dialog_result_exception) + e.toString();
        }
    }

    private boolean mExtendedCategoriesEnabled;

    public void enableExtendedCategories(boolean enabled) {
        mExtendedCategoriesEnabled = enabled;
    }
    
    /**
     * Returns the string representation of a URL reputation
     */
    private static final String getInfo(UrlInfo urlInfo) {
        StringBuilder result = new StringBuilder();
        result.append("Verdict: ");
        switch (urlInfo.mVerdict) {
            case UrlInfo.VERDICT_UNKNOWN:
                result.append("UNKNOWN");
                break;
            case UrlInfo.VERDICT_GOOD:
                result.append("GOOD");
                break;
            case UrlInfo.VERDICT_BAD:
                result.append("BAD");
                break;
            default:
                throw new IllegalArgumentException("Illegal verdict");
        }
        result.append(", categories: ");

        if (urlInfo.mCategoriesExt == null) {
            List<UrlCategory> categories = UrlCategory.getCategoriesByMask(urlInfo.mCategories);
            for (UrlCategory category : categories) {
                result.append(" ");
                result.append(category.name());
            }
        } else {
            for (UrlCategoryExt category : urlInfo.mCategoriesExt) {
                result.append(" ");
                result.append(category.name());
            }
        }
        return result.toString();
    }

    @Override
    protected int getHintTextId() {
        return R.string.str_url_reputation_dialog_enter_url;
    }

    @Override
    protected ArrayAdapter<?> getAdapter() {
        return null;
    }

    @Override
    protected int getTitleId() {
        return R.string.main_check_url_button;
    }
}