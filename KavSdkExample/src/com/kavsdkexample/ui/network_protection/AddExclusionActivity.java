package com.kavsdkexample.ui.network_protection;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.kavsdkexample.ui.SdkMainActivity;

/**
 * This is a sample activity used for adding URL exclusions.
 *      The activity serves a {@link Intent.CATEGORY_BROWSABLE} 
 *      action (specified in Manifest) 
 */
public class AddExclusionActivity extends Activity {
    
    /*
     * This method is invoked by Android when URLs start to open.
     * The method forwards the URLs to the main activity
     * for further handling
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent newIntent = new Intent(this, SdkMainActivity.class);
        String url = getIntent().getStringExtra("url");
        if (url != null) {
            newIntent.putExtra(SdkMainActivity.EXTRA_URL, url);
        }
        startActivity(newIntent);
        finish();
    }
}
