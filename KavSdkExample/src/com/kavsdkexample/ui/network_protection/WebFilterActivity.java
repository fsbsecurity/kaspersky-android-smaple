package com.kavsdkexample.ui.network_protection;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.kavsdk.KavSdk;
import com.kavsdk.accessibility.OpenAccessibilitySettingsException;
import com.kavsdk.license.SdkLicenseViolationException;
import com.kavsdk.webfilter.WebFilterControl;
import com.kavsdkexample.R;
import com.kavsdkexample.app.SaveModeForegroundService;
import com.kavsdkexample.app.SdkSampleApplication;
import com.kavsdkexample.shared.AppStatus;
import com.kavsdkexample.shared.activities.DialogFragmentActivity;
import com.kavsdkexample.shared.dialogs.AlertDialogFragment;
import com.kavsdkexample.shared.dialogs.DialogsSupplier;
import com.kavsdkexample.shared.dialogs.UiUpdater;
import com.kavsdkexample.storage.Settings;

public class WebFilterActivity extends DialogFragmentActivity implements ChangeProxyPortListener {

    private static final int LOLLIPOP_MR1 = 22;
    /* Titles for different states of buttons */
    private static final int RESTORE_OLD_WIFI_PROXY_STRING = R.string.str_webfilter_restore_wifi_proxy_button;
    private static final int ENABLE_NEW_WIFI_PROXY_STRING = R.string.str_webfilter_enable_wifi_proxy_button;
    private static final int RESTORE_OLD_APN_PROXY_STRING = R.string.str_webfilter_restore_apn_proxy_button;
    private static final int ENABLE_NEW_APN_PROXY_STRING = R.string.str_webfilter_enable_apn_proxy_button;

    /* Filtering options parameter. You can change it and try different correct flags combinations */
    private static final int WEB_FILTER_FLAGS = WebFilterControl.FILTER_BROWSERS_ONLY;

    private ViewGroup mSettingsViewGroup;
    private ListView mWfExcludeItemsList;
    private CheckBox mWebFilterEnabledCheckBox;
    private CheckBox mExtCategoriesEnabledCheckBox;
    private EditText mWebFilterPortEditText;
    private View mSwitchWifiProxyStateView;
    private View mSwitchApnProxyStateView;

    private WfItemsAdapter mExclusionsAdapter;

    private boolean mApnProxyEnabled;
    private boolean mWifiProxyEnabled;

    public void onExclusionsChanged() {
        mExclusionsAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        SdkSampleApplication app = SdkSampleApplication.from(WebFilterActivity.this);
        updateUiState(app.getWebFilterControl().isEnabled());
    }

    @Override
    public void initUi(Bundle savedInstanceState) {

        final SdkSampleApplication app = SdkSampleApplication.from(WebFilterActivity.this);
        app.getSdkLocalStatusObserver().setWebFilterActivityReference(this);
        final WebFilterControl webFilterControl = app.getWebFilterControl();

        setContentView(R.layout.webfilter_fragment);

        mWfExcludeItemsList = (ListView) findViewById(R.id.wfItemsList);

        mSettingsViewGroup = (ViewGroup)LayoutInflater.from(this).inflate(R.layout.webfilter_settings, mWfExcludeItemsList, false);
        mWfExcludeItemsList.addHeaderView(mSettingsViewGroup);

        mExclusionsAdapter = new WfItemsAdapter(this, webFilterControl);
        mWfExcludeItemsList.setAdapter(mExclusionsAdapter);

        mWfExcludeItemsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (!view.isEnabled()) {
                    return;
                }
                Bundle bundle = new Bundle();
                String url = app.getWebFilterControl().getExclusion(position - 1); //0 is ListView header
                bundle.putString(RemoveWebFilterExcludeItemDialog.ARG_URL_TO_DELETE, url);
                showFragmentDialog(DialogsSupplier.DIALOG_REMOVE_WF_EXCLUDE_ITEM, bundle);
            }
        });

        mWebFilterPortEditText = ((EditText) findViewById(R.id.webFilterProxyPort));

        mWebFilterEnabledCheckBox = (CheckBox) findViewById(R.id.WfEnable);
        mWebFilterEnabledCheckBox.setChecked(webFilterControl.isEnabled());

        mWebFilterEnabledCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                app.getWebFilterControl().enable(isChecked);
                Settings.setWebFilterEnabled(WebFilterActivity.this, isChecked);
                updateUiState(isChecked);

                if (isChecked) {
                    showFragmentDialog(DialogsSupplier.DIALOG_WF_CATEGORY);
                }
            }
        });

        mExtCategoriesEnabledCheckBox = (CheckBox) findViewById(R.id.WfExtCategories);
        mExtCategoriesEnabledCheckBox.setChecked(webFilterControl.isExtendedCategoriesEnabled());
        mExtCategoriesEnabledCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                webFilterControl.enableExtendedCategories(isChecked);
                webFilterControl.saveCategories();
            }
        });

        mApnProxyEnabled = Settings.getApnProxyEnabled(this, false);
        mWifiProxyEnabled = Settings.getWifiProxyEnabled(this, false);

        mSwitchApnProxyStateView = findViewById(R.id.switchApnProxyState);
        final TextView switchApnProxyStateCommandCaptionView = (TextView) mSwitchApnProxyStateView.findViewById(R.id.commandCaption);
        updateButtonText(mApnProxyEnabled,
                switchApnProxyStateCommandCaptionView, RESTORE_OLD_APN_PROXY_STRING,
                ENABLE_NEW_APN_PROXY_STRING);

        final View accessibilityEnabledButton = findViewById(R.id.accessibilyEnableButton);
        final TextView accessibilityEnabledText = (TextView) accessibilityEnabledButton.findViewById(R.id.commandCaption);
        accessibilityEnabledText.setText(R.string.str_antivirus_accessibility_enable);
        accessibilityEnabledButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    KavSdk.getAccessibility().openSettings();
                } catch (OpenAccessibilitySettingsException e) {
                    e.printStackTrace();
                }
            }
        });
        mSwitchApnProxyStateView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final WebFilterControl webFilter = app.getWebFilterControl();
                if (mApnProxyEnabled) {
                    webFilter.restoreApnProxySettings();
                } else {
                    webFilter.applyApnProxySettings();
                }
                mApnProxyEnabled = !mApnProxyEnabled;
                Settings.setApnProxyEnabled(WebFilterActivity.this, mApnProxyEnabled);
                updateButtonText(mApnProxyEnabled,
                        switchApnProxyStateCommandCaptionView,
                        RESTORE_OLD_APN_PROXY_STRING,
                        ENABLE_NEW_APN_PROXY_STRING);
            }
        });

        mSwitchWifiProxyStateView = findViewById(R.id.switchWifiProxyState);
        final TextView switchWifiProxyStateCommandCaptionView = (TextView) mSwitchWifiProxyStateView.findViewById(R.id.commandCaption);
        updateButtonText(mWifiProxyEnabled,
                switchWifiProxyStateCommandCaptionView, RESTORE_OLD_WIFI_PROXY_STRING,
                ENABLE_NEW_WIFI_PROXY_STRING);

        if (android.os.Build.VERSION.SDK_INT <= LOLLIPOP_MR1) {
            mSwitchWifiProxyStateView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    SdkSampleApplication app = SdkSampleApplication.from(WebFilterActivity.this);
                    final WebFilterControl webFilter = app.getWebFilterControl();
                    mWifiProxyEnabled = !mWifiProxyEnabled;

                    if (mWifiProxyEnabled) {
                        String port = mWebFilterPortEditText.getText().toString();
                        app.changePortNumber(port.isEmpty() ? SdkSampleApplication.PORT_NUMBER : Integer.parseInt(port));
                        webFilter.applyWifiProxySettings();
                    } else {
                        webFilter.restoreWifiProxySettings();
                    }

                    Settings.setWifiProxyEnabled(WebFilterActivity.this, mWifiProxyEnabled);
                    updateButtonText(mWifiProxyEnabled,
                            switchWifiProxyStateCommandCaptionView,
                            RESTORE_OLD_WIFI_PROXY_STRING,
                            ENABLE_NEW_WIFI_PROXY_STRING);

                    try {
                        app.reinitWebFilterControl();
                    } catch (SdkLicenseViolationException e) {
                        e.printStackTrace();
                    }
                }
            });

        } else {
            mSwitchWifiProxyStateView.setEnabled(false);
        }

        View selCategoryCommandView = findViewById(R.id.selCategoryButton);
        final TextView selCategoryCommandCaptionView = (TextView) selCategoryCommandView.findViewById(R.id.commandCaption);
        selCategoryCommandCaptionView.setText(R.string.str_webfilter_view_or_select_categories);
        selCategoryCommandView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showFragmentDialog(DialogsSupplier.DIALOG_WF_CATEGORY);
            }
        });

        CheckBox ignorePowerSaveModeCheckBox = (CheckBox) findViewById(R.id.WfIgnorePowerSaveMode);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            ignorePowerSaveModeCheckBox.setVisibility(View.GONE);
            findViewById(R.id.WfIgnorePowerSaveModeInfo).setVisibility(View.GONE);
        } else {
            ignorePowerSaveModeCheckBox.setChecked(webFilterControl.isPowerSaveModeIgnored());
            ignorePowerSaveModeCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    app.getWebFilterControl().setPowerSaveModeIgnored(isChecked);

                    /*
                     * In power save mode only foreground application has network access.
                     *
                     * If webfilter is enabled and your application is in background,
                     * any foreground application may fail network operations,
                     * because your application (with webfilter) won't have network access.
                     *
                     * To avoid this in KavSdkExample we start a foreground service, so KavSdkExample
                     * will have network access in power save mode even it's in background.
                     */

                    if (isChecked) {
                        SaveModeForegroundService.start(WebFilterActivity.this);
                    } else {
                        SaveModeForegroundService.stop(WebFilterActivity.this);
                    }
                }
            });
        }

        updateUiState(webFilterControl.isEnabled());
    }

    @Override
    public void initComponent(AppStatus status, String additionalInfo) {
    }

    @SuppressWarnings("all")
    private boolean isProxyEnabled() {
        return (WEB_FILTER_FLAGS & WebFilterControl.DISABLE_PROXY) == 0;
    }

    private void updateUiState(boolean enabled) {

        for (int i = 0; i < mSettingsViewGroup.getChildCount(); ++i) {
            mSettingsViewGroup.getChildAt(i).setEnabled(enabled);
        }

        mWfExcludeItemsList.setEnabled(enabled);

        if (enabled) {
            if (isProxyEnabled()) {
                // A possibility to set up APN proxy server existed before Android API 14 (4.0).
                // Then it was replaced by a possibility to set up an WI-FI proxy server, but not both simultaneously
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                    mSwitchWifiProxyStateView.setEnabled(false);
                } else {
                    mSwitchApnProxyStateView.setEnabled(false);
                }
            } else {
                mSwitchWifiProxyStateView.setEnabled(false);
                mSwitchApnProxyStateView.setEnabled(false);
            }
        } else {
            mWebFilterEnabledCheckBox.setEnabled(true);
        }

        mExclusionsAdapter.notifyDataSetChanged();
    }

    private void updateButtonText(boolean enabled, TextView button, int textOn, int textOff) {
        button.setText(enabled ? getString(textOn) : getString(textOff));
    }

    @Override
    public DialogFragment onCreateDialogFragment(int dialogId, Bundle bundle) {
        DialogFragment dialog = null;
        switch (dialogId) {
        case DialogsSupplier.DIALOG_WF_CATEGORY:
            dialog = AlertDialogFragment.newInstance(DialogsSupplier.DIALOG_WF_CATEGORY);
        break;

        case DialogsSupplier.DIALOG_REMOVE_WF_EXCLUDE_ITEM:
            dialog = RemoveWebFilterExcludeItemDialog.newInstance(bundle)
                .setOnListUpdatedListener(new UiUpdater() {
                    @Override
                    public void updateUi() {
                        // Refreshing of the exclusion list view
                        onExclusionsChanged();
                    }
                });
            break;

        case DialogsSupplier.DIALOG_ENTER_NEW_PROXY_PORT:
            dialog = AlertDialogFragment.newInstance(DialogsSupplier.DIALOG_ENTER_NEW_PROXY_PORT);
            break;
        default:
            break;
        }
        return dialog;
    }

    @Override
    public void changeProxyPort() {
        SdkSampleApplication app = SdkSampleApplication.from(WebFilterActivity.this);
        app.getWebFilterControl().restoreWifiProxySettings();
        showFragmentDialog(DialogsSupplier.DIALOG_ENTER_NEW_PROXY_PORT);
    }
}
