package com.kavsdkexample.ui.network_protection;

import android.widget.ArrayAdapter;

import com.kaspersky.components.financialcategorizer.FinanceCategory;
import com.kavsdk.license.SdkLicenseViolationException;
import com.kavsdk.urlchecker.UrlCheckService;
import com.kavsdkexample.R;
import com.kavsdkexample.shared.dialogs.BaseSimpleDialog;

/**
 * This is a dialog for showing the functionality of the URL checker.
 * The dialog asks the user to enter a URL, checks it and shows the result 
 * in a toast.
 */
public class FinancialCategoryDialog extends BaseSimpleDialog {

    @Override
    protected String check() {
        try {
            final UrlCheckService service = new UrlCheckService(mContext);
            String url = mTextView.getText().toString();
            try {
                FinanceCategory result = service.checkBankUrl(url);
                if (result != null) {
                    return mContext.getString(R.string.str_financial_category_dialog_result) + result.toString();
                } else {
                    return mContext.getString(R.string.str_financial_category_dialog_result_null);
                }
            } catch (Exception e) {
                return mContext.getString(R.string.str_financial_category_dialog_exception) + e.toString();
            }
        } catch (SdkLicenseViolationException e) {
            return mContext.getString(R.string.str_financial_category_dialog_exception) + e.toString();
        } catch (Throwable e) {
            return mContext.getString(R.string.str_financial_category_dialog_exception) + e.toString();
        }
    }

    @Override
    protected int getHintTextId() {
        return R.string.bank_url_check_textview;
    }

    @Override
    protected ArrayAdapter<?> getAdapter() {
        return null;
    }

    @Override
    protected int getTitleId() {
        return R.string.main_check_bank_url_button;
    }
}
