package com.kavsdkexample.ui.data_protection;

import com.kavsdkexample.samples.SecureSmsComponentSample;
import com.kavsdkexample.shared.AppStatus;
import com.kavsdkexample.shared.activities.DialogFragmentActivity;
import com.kavsdkexample.R;
import com.kavsdkexample.samples.SecureSmsComponentSample.ISecureSmsView;
import com.kavsdkexample.app.SdkSampleApplication;
import com.kavsdkexample.Utils;
import com.kavsdkexample.shared.dialogs.DialogsSupplier;
import com.kavsdkexample.shared.BaseFragment;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.telephony.SmsManager;
import android.text.TextUtils;

import java.nio.charset.Charset;

public class DataProtectionFragment extends BaseFragment implements View.OnClickListener,
      ISecureSmsView {
    private static final int REQUEST_CODE_SENT_SMS = 32001;
    
    /* GUI controls */
    private TextView                  mPhonesView;
    private TextView                  mSmsView;
    private View                      mToggleSmsFilterCommandView;
    private TextView                  mToggleSmsFilterCommandCaptionView;
    private View                      mProcessCommandView;
    
    /* GUI controls for send data-sms*/
    private EditText mPhoneNumber;
    private EditText mPort;
    private EditText mSmsText;
    
    private SecureSmsComponentSample mSecureSmsComponent;
    
    /* A handler for sending messages about GUI events */
    private final Handler             mHandler = new Handler();

    @Override
    public View initUi(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(DataProtectionFragment.class.getSimpleName(), "DataProtectionFragment::initUi()");
        final View view = inflater.inflate(R.layout.data_protection_fragment, container, false);
        mProcessCommandView = view.findViewById(R.id.processInbox);
        
        Utils.initCommandView(view, R.id.open_regular_activity_button, R.string.str_secure_input_open_regular_activity_button, this);
        Utils.initCommandView(view, R.id.open_config_changes_activity_button, R.string.str_secure_input_open_config_changes_activity_button, this); 
        Utils.initCommandView(view, R.id.test_database_button, R.string.str_secure_storage_test_database_button, this);
        Utils.initCommandView(view, R.id.test_secure_file_button, R.string.str_secure_storage_test_secure_file_button, this);
                
        mPhonesView   = (TextView) view.findViewById(R.id.phones);
        mSmsView      = (TextView) view.findViewById(R.id.smsView);
                
        mToggleSmsFilterCommandView = view.findViewById(R.id.toogleSmsFilter);
        mToggleSmsFilterCommandView.setOnClickListener(this);   
        mToggleSmsFilterCommandCaptionView = (TextView) mToggleSmsFilterCommandView.findViewById(R.id.commandCaption);
        mToggleSmsFilterCommandCaptionView.setText(R.string.str_securesms_start_filter);
                
        
        Utils.initCommandView(view, R.id.processInbox, R.string.str_securesms_process_inbox, this);
        Utils.initCommandView(view, R.id.addPhone, R.string.str_securesms_add_filter, this);
        Utils.initCommandView(view, R.id.clearAll, R.string.str_securesms_clear_all, this);
                
        initDataSmsControls(view);

        return view;
    }

    @Override
    public void initComponent(AppStatus appStatus, String additionalInfo) {
        mSecureSmsComponent = ((SdkSampleApplication)getActivity().getApplication()).getSecureSmsComponent();

        // Subscribing for SecureSms events
        mSecureSmsComponent.setGuiUpdater(this);

        // Refreshing GUI
        updatePhonesView();
        updateSmsView();
        updateToggleButtonState(mSecureSmsComponent.isSecureSmsEnabled());
    }

    private void initDataSmsControls(View root) {
        mPhoneNumber = (EditText) root.findViewById(R.id.etPhoneNumber);
        mPort = (EditText) root.findViewById(R.id.etPort);
        mSmsText = (EditText) root.findViewById(R.id.etSmsText);
        root.findViewById(R.id.btnSendSms).setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View v) {
                onSendDataSmsClick();
            }
        });
        
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mSecureSmsComponent.removeGuiUpdater();
    }
    
    @Override
    public void updatePhonesView() {
        mPhonesView.setText(mSecureSmsComponent.getPhonesAsText());
    }
    
    @Override
    public void updateSmsView() {
        mHandler.post(new Runnable() {          
            @Override
            public void run() {
                mSmsView.setText(mSecureSmsComponent.getSmsAsText());
            }
        });     
    }
    
    @Override
    public void updateToggleButtonState(boolean newState) {
        mToggleSmsFilterCommandCaptionView.setText(newState ? R.string.str_securesms_stop_filter
                : R.string.str_securesms_start_filter);
    }
    
    private void showAddPhoneDialog() {
        ((DialogFragmentActivity)getActivity()).showFragmentDialog(DialogsSupplier.DIALOG_ADD_PHONE_NUMBER);
    }

    @Override
    public void onClick(View v) {
        final Activity act = getActivity();
        switch(v.getId()) {
        case R.id.open_regular_activity_button:
            startActivity(new Intent(act, TestSecureInputBaseActivity.RegularActivity.class));
            break;
        case R.id.open_config_changes_activity_button:
            startActivity(new Intent(act, TestSecureInputBaseActivity.ConfigChangesActivity.class));
            break;
        case R.id.test_database_button:
            startActivity(new Intent(act, TestDatabaseActivity.class));
            break;
        case R.id.test_secure_file_button:
            startActivity(new Intent(act, TestSecureFileActivity.class));
            break;
        case R.id.toogleSmsFilter:
            mSecureSmsComponent.toogleSmsFilter();
            break;
        case R.id.addPhone:
            showAddPhoneDialog();
            break;
        case R.id.clearAll:
            mSecureSmsComponent.clearAll();
            break;
        case R.id.processInbox:
            mSecureSmsComponent.processInbox();
            v.setEnabled(false);
            break;              
        default:
            break;
        }       
    }
    
    @Override
    public void setProcessButtonEnabled(final boolean enable) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mProcessCommandView.setEnabled(enable);
            }
        });
    }
    
    @Override
    public void showToast(int resId) {
        Activity act = getActivity();
        if (act != null) {
            Toast.makeText(act, resId, Toast.LENGTH_LONG).show();
        }
    }
    
    protected void onSendDataSmsClick() {

        final String number = mPhoneNumber.getText().toString();
        final byte[] data;
        data = mSmsText.getText().toString().getBytes(Charset.defaultCharset());
        if (!TextUtils.isEmpty(number) && data.length > 0) {
            String strPort = mPort.getText().toString();
            if (!TextUtils.isEmpty(strPort)) {
                final short port = Short.parseShort(strPort);
                SmsManager smsManager = SmsManager.getDefault();
                PendingIntent sendSmsPi = getActivity().createPendingResult(REQUEST_CODE_SENT_SMS, new Intent(), PendingIntent.FLAG_UPDATE_CURRENT);
                smsManager.sendDataMessage(number, null, port, data, sendSmsPi, null);
                            
                showToast(R.string.str_securesms_msg_sent);
            } else {
                showToast(R.string.str_securesms_msg_not_specified_port);
            }
        } else {
            showToast(R.string.str_securesms_msg_not_specified_phone_data);
        }
    }
    
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_SENT_SMS) {
            switch (resultCode) {
            case Activity.RESULT_OK:
                showToast(R.string.str_securesms_msg_sent_successfully);
                break;
            case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
            case SmsManager.RESULT_ERROR_RADIO_OFF:
            case SmsManager.RESULT_ERROR_NULL_PDU:
                showToast(R.string.str_securesms_msg_sent_error);
                break;
            default:
                break;
            }
        }
    }
}
