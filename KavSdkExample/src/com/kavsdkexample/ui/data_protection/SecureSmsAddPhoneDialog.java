package com.kavsdkexample.ui.data_protection;

import com.kavsdk.securesms.SecureSmsFilterItem;
import com.kavsdkexample.R;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

/**
 * This is a dialog for adding a new phone into the secure SMS filtering list
 */
public class SecureSmsAddPhoneDialog extends DialogFragment {
    private AddItemObserver mObserver;

    public SecureSmsAddPhoneDialog setObserver(AddItemObserver observer) {
        mObserver = observer;
        return this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final View view = inflater.inflate(R.layout.securesms_add_phone_dialog, container, false);

        final Button addButton = (Button) view.findViewById(R.id.addButton);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mObserver != null) {
                    final String phone = ((EditText) view.findViewById(R.id.phoneText)).getText().toString();
                    if (TextUtils.isEmpty(phone)) {
                        dismiss();
                        return;
                    }
                    final SecureSmsFilterItem item = new SecureSmsFilterItem(phone);
                    // Updating the list of phone numbers
                    mObserver.onAddItem(item);
                }
                dismiss();
            }
        });

        Button cancelButton = (Button) view.findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return view;
    }
    
    /**
     * An interface for notification about new phone numbers adding
     */
    public interface AddItemObserver {
        void onAddItem(final SecureSmsFilterItem item);
    }
}
