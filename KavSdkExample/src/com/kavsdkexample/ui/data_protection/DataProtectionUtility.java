package com.kavsdkexample.ui.data_protection;

import android.app.KeyguardManager;
import android.content.Context;
import android.os.Build;

import com.kavsdk.securestorage.database.SQLiteException;
import com.kavsdk.securestorage.file.CryptoFileException;
import com.kavsdk.securestorage.fingerprint.FingerprintOperationException;
import com.kavsdk.securestorage.fingerprint.FingerprintOperationHelper;
import com.kavsdkexample.R;

public final class DataProtectionUtility {


    private DataProtectionUtility() {
    }

    public static String getFingerprintOperationErrorMessage(Exception exception, Context context) {
        int messageId = R.string.fingerprint_error_unknown;
        if (exception instanceof FingerprintOperationException) {
            switch (((FingerprintOperationException) exception).getFingerprintOperationError()) {
                case PermanentKeyInvalidated:
                    messageId = R.string.fingerprint_error_key_permanent_invalidated;
                    break;
                case PasswordFileError:
                    messageId = R.string.fingerprint_error_password_operations_error;
                    break;
                case NoEnrolledFingerprint:
                    messageId = R.string.fingerprint_error_no_enrolled_fingerprints;
                    break;
                case FingerprintInputError:
                    messageId = R.string.fingerprint_error_invalid_input;
                    break;
                case EncryptDecryptError:
                    messageId = R.string.fingerprint_error_encrypt_decrypt;
                    break;
                default:
                    break;
            }

        } else if (exception instanceof CryptoFileException || exception instanceof SQLiteException) {
            messageId = R.string.fingerprint_error_invalid_password;
        }
        return context.getString(messageId);
    }


    /**
     * Check fingerprint hardware and software for some errors. In case of some errors return message.
     * @param context
     * @return message in case of some error with fingerprint, null - otherwise.
     */
    public static String getFingerprintErrorMessage(Context context) {
        String message = null;
        if (Build.VERSION.SDK_INT < 23) {
            message = "Fingerprint input are not supported at API < 23";
        } else { //API >= 23
            KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
            if (!FingerprintOperationHelper.isHardwareDetected(context)) {
                message = "Fingerprint hardware was not found on this device";
            } else if (!keyguardManager.isKeyguardSecure()) {
                // Show a message that the user hasn't set up a fingerprint or lock screen.
                message = "Secure lock screen hasn't set up.\n"
                        + "Go to 'Settings -> Security -> Fingerprint' to set up a fingerprint";
            } else if (!FingerprintOperationHelper.hasEnrolledFingerprints(context)) {
                message = "Go to 'Settings -> Security -> Fingerprint' and register at least one fingerprint";
            }
        }
        return message;
    }
}
