package com.kavsdkexample.ui.update;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.kavsdk.antivirus.Antivirus;
import com.kavsdk.license.SdkLicenseViolationException;
import com.kavsdk.updater.UpdateEventListener;
import com.kavsdk.updater.Updater;
import com.kavsdk.updater.UpdaterConstants;
import com.kavsdkexample.R;
import com.kavsdkexample.Utils;
import com.kavsdkexample.app.SdkSampleApplication;
import com.kavsdkexample.shared.AppStatus;
import com.kavsdkexample.shared.BaseFragment;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

/**
 * This is a fragment for managing antivirus parameters and handling antivirus
 * events
 */
public class UpdateFragment extends BaseFragment implements UpdateEventListener,
        OnClickListener {
    private static final String TAG = UpdateFragment.class.getSimpleName();
    private Antivirus mAntivirusComponent;
    private volatile boolean mCancelUpdate;

    /*
     * Component for update
     */
    private static final int COMPONENT_ALL = 0;
    private static final int COMPONENT_ANTIVIRUS = 1;
    private static final int COMPONENT_FINANCIAL_CATEGORIZER = 2;
    private static final int COMPONENT_ROOT_DETECTOR = 3;

    private final Updater mUpdater = Updater.getInstance();

    private List<String> mComponents;

    private ArrayAdapter<String> mAdapter;

    /*
     * UI Controls
     */
    private Spinner mComponentsView;
    private RadioButton mRandomServerRadioButton;
    private RadioButton mDefaultServerRadioButton;
    private RadioButton mSpecifiedServerRadioButton;
    private EditText mServerEditText;
    private Button mButton;
    private TextView mDetailsTextView;

    @Override
    public View initUi(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.update_fragment,
                container, false);

        mComponents = Arrays.asList(new String[] {
                getString(R.string.updates_component_all),
                getString(R.string.updates_component_antivirus),
                getString(R.string.updates_component_financial_categorizer),
                getString(R.string.updates_component_root_detector),
        });

        mAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, mComponents);
        mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mComponentsView = (Spinner) view.findViewById(R.id.components);
        mComponentsView.setAdapter(mAdapter);

        mRandomServerRadioButton = (RadioButton) view
                .findViewById(R.id.randomServer);

        mDefaultServerRadioButton = (RadioButton) view
                .findViewById(R.id.defaultServer);
        mDefaultServerRadioButton.setText(Utils.BASES_UPDATE_URL);

        mSpecifiedServerRadioButton = (RadioButton) view.findViewById(R.id.specifiedServer);
        mServerEditText = (EditText) view.findViewById(R.id.updateServer);
        mServerEditText.setEnabled(mSpecifiedServerRadioButton.isChecked());
        mSpecifiedServerRadioButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mServerEditText.setEnabled(isChecked);
            }
        });

        mButton = (Button) view.findViewById(R.id.startUpdate);
        mButton.setOnClickListener(this);

        mDetailsTextView = (TextView) view.findViewById(R.id.updateDetails);

        return view;
    }

    @Override
    public void initComponent(AppStatus appStatus, String additionalInfo) {
        final SdkSampleApplication app = ((SdkSampleApplication)getActivity().getApplication());
        mAntivirusComponent = app.getAntivirus();
    }

    private void runOnUiThread(Runnable runnable) {
        Utils.runOnUiThread(this, runnable);
    }
    
    private void startUpdate() {
        if (mUpdater.isUpdateInProgress()) {
            String msg = getString(R.string.updates_canceling_update);
            Log.d(TAG, msg);
            printDetails(msg);
            mCancelUpdate = true;
            mButton.setEnabled(false);
            return;
        }
        final boolean randomServer = mRandomServerRadioButton.isChecked();
        final boolean specifiedServer = mSpecifiedServerRadioButton.isChecked();
        final String url = (specifiedServer) ? mServerEditText.getText().toString() : Utils.BASES_UPDATE_URL;
        try {
            int port = new URL(url).getPort();
            if (port != -1 && (port <= 0x0000 || port > 0xFFFF)) {
                throw new MalformedURLException("Wrong port = " + port);
            }
        } catch (MalformedURLException e1) {
            mButton.setEnabled(true);
            Utils.showToast(getActivity(), R.string.updates_bad_url);
            return;
        }
        final int component = mComponentsView.getSelectedItemPosition();
        final String componentName = (String) mComponentsView.getSelectedItem();
        final UpdateEventListener listener = this;
        mDetailsTextView.setText("");
        printDetails(String.format(getString(R.string.updates_selected_component), componentName, component));
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    switch (component) {
                        case COMPONENT_ROOT_DETECTOR:
                            if (randomServer) {
                                mUpdater.updateRootDetector(listener);
                            } else {
                                mUpdater.updateRootDetector(url, listener);
                            }
                            break;
                        case COMPONENT_FINANCIAL_CATEGORIZER:
                            if (randomServer) {
                                mUpdater.updateFinancialBases(listener);
                            } else {
                                mUpdater.updateFinancialBases(url, listener);
                            }
                            break;
                        case COMPONENT_ANTIVIRUS:
                            if (randomServer) {
                                mUpdater.updateAntivirusBases(listener);
                            } else {
                                mUpdater.updateAntivirusBases(url, listener);
                            }
                            break;
                        case COMPONENT_ALL:
                            if (randomServer) {
                                mUpdater.updateAllBases(listener);
                            } else {
                                mUpdater.updateAllBases(url, listener);
                            }
                            break;
                        default:
                            onUpdateFailed(R.string.wrong_component);
                            break;
                    }
                } catch (SdkLicenseViolationException e) {
                    onUpdateFailed(R.string.str_error_license_expired);
                    return;
                }
            }
        }).start();
    }

    private void printDetails(String message) {
        mDetailsTextView.append(message + "\n");
    }
    
    private void onUpdateFailed(final int messageResId) {
        onUpdateFailed(getString(messageResId));
    }

    private void onUpdateFailed(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                printDetails(message);
            }
        });
        mCancelUpdate = true;
    }

    private void onUpdateStarted() {
        mCancelUpdate = false;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mButton.setText(R.string.updates_cancel_update);
                mButton.setEnabled(true);
                printDetails(getString(R.string.updates_update_started));
            }
        });
    }
    
    private void onServerChanged() {
        final URL url = mUpdater.getUpdateServer();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                printDetails(String.format(getString(R.string.updates_server_changed), url));
            }
        });
    }
    
    private void onServerSelected() {
        final URL url = mUpdater.getUpdateServer();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                printDetails(String.format(getString(R.string.updates_server_selected), url));
            }
        });
    }
    
    private void onBasesDownloaded() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                printDetails(getString(R.string.updates_bases_downloaded));
            }
        });
    }
    
    private void onBasesApplied() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                printDetails(getString(R.string.updates_bases_applied));
                int component = mComponentsView.getSelectedItemPosition();
                if  (component == COMPONENT_ANTIVIRUS || component == COMPONENT_ALL) {
                    // Printing the information about a new database in Logcat
                    Context context = getActivity();
                    if (context != null) {
                        Utils.printAvDbDetails(context, TAG,
                                mAntivirusComponent.getVirusDbInfo());
                    }
                }
            }
        });
    }
    
    private void onUpdateFinished(final int result) {
        mCancelUpdate = false;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mButton.setText(R.string.updates_start_update);
                mButton.setEnabled(true);
                switch (result) {
                case UpdaterConstants.UPDATE_RESULT_DBUPDATE_SUCCESS:
                    printDetails(getString(R.string.updates_update_finished_succesfully));
                    break;
                case UpdaterConstants.UPDATE_RESULT_DBUPDATE_NO_NEW_BASES:
                    printDetails(getString(R.string.updates_update_finished_no_new_bases));
                    break;
                case UpdaterConstants.UPDATE_RESULT_DBUPDATE_FAILED_NO_CONNECTION:
                    printDetails(getString(R.string.updates_update_finished_failed_no_connection));
                    break;
                case UpdaterConstants.UPDATE_RESULT_DBUPDATE_FAILED_NO_DISK_SPACE:
                    printDetails(getString(R.string.updates_update_finished_failed_no_disk_space));
                    break;
                case UpdaterConstants.UPDATE_RESULT_DBUPDATE_FAILED:
                    printDetails(getString(R.string.updates_update_finished_failed));
                    break;
                case UpdaterConstants.UPDATE_RESULT_DBUPDATE_CANCELED:
                    printDetails(getString(R.string.updates_update_finished_canceled));
                    break;
                case UpdaterConstants.UPDATE_RESULT_DBUPDATE_CANCELED_DATE_INCORRECT:
                    printDetails(getString(R.string.updates_update_finished_canceled_date_incorrect));
                    break;
                case UpdaterConstants.UPDATE_RESULT_DBUPDATE_BASES_CORRUPTED:
                    printDetails(getString(R.string.updates_update_finished_bases_corrupted));
                    break;
                default:
                    printDetails(String.format(getString(R.string.updates_update_finished_with_result_code), result));
                    break;
                }
                
            }
        });
    }

    /*
     * Handles updating events and prints the information into Logcat
     */
    @Override
    public boolean onUpdateEvent(int event, int result) {
        boolean cancelUpdate = mCancelUpdate;

        String evt, res;
        res = Utils.convertUpdateResultIdToString(result);
        evt = Utils.convertUpdateEventIdToString(event);
        if (event == UpdaterConstants.UPDATE_EVENT_IN_PROGRESS) {
            // You can update your progress bar here
            Log.d(TAG, "onUpdateEvent(" + evt + ")");
        } else {
            Log.d(TAG, "onUpdateEvent(" + evt + ", " + res + ")");
        }
        
        switch (event) {
        case UpdaterConstants.UPDATE_EVENT_TASK_STARTED:
            onUpdateStarted();
            break;
        case UpdaterConstants.UPDATE_EVENT_SERVER_CHANGED:
            onServerChanged();
            break;  
        case UpdaterConstants.UPDATE_EVENT_SERVER_SELECTED:
            onServerSelected();
            break;
        case UpdaterConstants.UPDATE_EVENT_BASES_DOWNLOADED:
            onBasesDownloaded();
            break;
        case UpdaterConstants.UPDATE_EVENT_BASES_APPLIED:
            onBasesApplied();
            break;
        case UpdaterConstants.UPDATE_EVENT_TASK_FINISHED:
            onUpdateFinished(result);
            break;
        default:
            break;
        }
        //return false to cancel update
        return cancelUpdate;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.startUpdate:
            v.setEnabled(false);
            startUpdate();
            break;
        default:
            break;
        }
    }
}
