package com.kavsdkexample.ui.risk_detection;

import com.kavsdkexample.R;
import com.kavsdkexample.shared.AppStatus;
import com.kavsdkexample.Utils;
import com.kavsdkexample.shared.activities.DialogFragmentActivity;
import com.kavsdkexample.shared.dialogs.DialogsSupplier;
import com.kavsdkexample.shared.BaseFragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;


public class RiskDetectionFragment extends BaseFragment implements OnClickListener {
    
    @Override
    public View initUi(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        
        final View view = inflater.inflate(R.layout.risk_detection_fragment, container, false);
        
        Utils.initCommandView(view, R.id.check_root_button, R.string.str_dsc_check_root, this);
        Utils.initCommandView(view, R.id.check_firmware_button, R.string.str_dsc_check_firmware, this);
        Utils.initCommandView(view, R.id.check_usb_debug_button, R.string.str_check_usb_debug_button, this);
        Utils.initCommandView(view, R.id.device_lock_button, R.string.str_check_device_lock_pattern_button, this);
        Utils.initCommandView(view, R.id.check_wifi_button, R.string.str_dsc_check_wifi_safety, this);
        Utils.initCommandView(view, R.id.check_untrusted_app_button, R.string.str_check_non_market_apps_button, this);
        Utils.initCommandView(view, R.id.open_fingerprint_button, R.string.str_open_fingerprint_button, this);
        
        return view;
    }

    @Override
    public void initComponent(AppStatus appStatus, String additionalInfo) {

    }

    @Override
    public void onClick(View v) {
        DialogFragmentActivity activity = (DialogFragmentActivity) getActivity();
        switch (v.getId()) {
        case R.id.check_root_button:
            activity.showFragmentDialog(DialogsSupplier.DIALOG_ROOT_CHECK);
            break;
        case R.id.check_firmware_button:
            activity.showFragmentDialog(DialogsSupplier.DIALOG_CHECK_FIRMWARE);
            break;
        case R.id.check_usb_debug_button:
            activity.showFragmentDialog(DialogsSupplier.DIALOG_USB_DEBUG_CHECK);
            break;
        case R.id.device_lock_button:
            activity.showFragmentDialog(DialogsSupplier.DIALOG_DEVICE_PASSWORD_QUALITY_CHECK);
            break;
        case R.id.open_fingerprint_button:
            startActivity(new Intent(activity, FingerprintActivity.class));
            break;
        case R.id.check_wifi_button:
            activity.showFragmentDialog(DialogsSupplier.DIALOG_WIFI_CHECK);
            break;
        case R.id.check_untrusted_app_button:
            activity.showFragmentDialog(DialogsSupplier.DIALOG_NON_MARKET_APPS_CHECK);
            break;
        default:
            break;
        }
        
    }
}
