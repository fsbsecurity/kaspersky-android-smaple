package com.kavsdkexample.ui.risk_detection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.kavsdk.fingerprint.FingerprintNotPersistentProperties;
import com.kavsdk.fingerprint.FingerprintPersistentProperties;
import com.kavsdk.fingerprint.LocationProviders;
import com.kavsdk.fingerprint.SmFingerprint;
import com.kavsdk.fingerprint.LocationReceiver;
import com.kavsdk.license.SdkLicenseViolationException;
import com.kavsdkexample.R;
import com.kavsdkexample.Utils;
import com.kavsdkexample.shared.AppStatus;
import com.kavsdkexample.shared.activities.BaseActivity;

import android.location.Location;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;
import android.widget.Toast;

/**
 * This is a sample activity for showing a list of device properties.
 * Besides, it allows the user to find out their location.
 * The information about the device includes persistent properties (e.g. device model and IMEI) 
 * and properties which can be changed, for example, after OS updates or settings changes
 * (e.g. locale and OS version).
 */
public class FingerprintActivity extends BaseActivity implements OnClickListener {
    /* Labels for storing properties in pairs {@see android.util.Pair} */
    private static final String NAME = "NAME";
    private static final String VALUE = "VALUE";
    
    /*
     * GUI controls
     */
    private ProgressBar mLocationProgress;
    private TextView mLocationText;
    private Button mUpdateLocButton;
    private Button mCancelButton;
    
    /* A SDK component responsible for providing the information about the device */
    private SmFingerprint mFingerprint;
    
    /* A flag indicating whether a location was requested and not received yet */
    private boolean mWaitingForLocation;

    @Override
    public void initUi(Bundle savedInstanceState) {
        setContentView(R.layout.fingerprint_activity);

        try {
            mFingerprint = new SmFingerprint(this);
        } catch (SdkLicenseViolationException e) {
            Toast.makeText(this, getString(R.string.str_license_expired_error), Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        // GUI initialization
        mUpdateLocButton  = (Button)      findViewById(R.id.updateLocationButton);
        mCancelButton     = (Button)      findViewById(R.id.cancelButton);
        mLocationText     = (TextView)    findViewById(R.id.locationText);
        mLocationProgress = (ProgressBar) findViewById(R.id.locationProgress);

        mUpdateLocButton.setOnClickListener(this);
        mCancelButton.setOnClickListener(this);

        ExpandableListView list = (ExpandableListView) findViewById(R.id.properties_list);
        fillList(list);
    }

    @Override
    public void initComponent(AppStatus appStatus, String additionalInfo) {

    }

    @Override
    public void onStop() {
        super.onStop();
        
        if (mWaitingForLocation) {
            // Assume that location request result is no longer needed
            cancelFingerprintRequest();
        }
    }
    
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.updateLocationButton) {
            
            mWaitingForLocation = true;

            mUpdateLocButton.setEnabled(false);
            mCancelButton.setEnabled(true);
            mLocationProgress.setVisibility(View.VISIBLE);
            
            mFingerprint.requestLocation(new LocationReceiver() {
                
                /*
                 * Note: These methods are called from a worker thread, update GUI properly
                 */
                @Override
                public void onSuccess(Location location) {
                    setLocationInfo(formatLocation(location));
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mUpdateLocButton.setEnabled(true);
                            mCancelButton.setEnabled(false);
                        }
                    });
                }
                
                @Override
                public void onError(int errorID) {
                    setLocationInfo(Utils.convertErrorToString(FingerprintActivity.this, errorID));
                }
                
            }, LocationProviders.All);
            
        } else if (v.getId() == R.id.cancelButton) {
            cancelFingerprintRequest();
        }
    }
    
    private void cancelFingerprintRequest() {
        mFingerprint.cancelLocationRequest();
        
        mWaitingForLocation = false;
        mLocationProgress.setVisibility(View.INVISIBLE);
        mUpdateLocButton.setEnabled(true);
        mCancelButton.setEnabled(false);
    }

    private String formatLocation(Location location) {
        return String.format(getString(R.string.str_fingerprint_location_format), 
                             location.getProvider(),
                             location.getLatitude(),
                             location.getLongitude(),
                             location.getAccuracy());
    }
    
    private void setLocationInfo(final String info) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mLocationText.setText(info);
                mWaitingForLocation = false;
                mLocationProgress.setVisibility(View.INVISIBLE);
            }
        });
    }
    
    /**
     * The method acquires the information about the device and shows it
     */
    private void fillList(ExpandableListView list) {

        List<Map<String, String>> groupData = new ArrayList<Map<String, String>>();

        Map<String, String> persistentGroup = new HashMap<String, String>();
        persistentGroup.put(NAME, getString(R.string.str_fingerprint_list_group_persistent));
        groupData.add(persistentGroup);
        
        Map<String, String> notPersistentGroup = new HashMap<String, String>();
        notPersistentGroup.put(NAME, getString(R.string.str_fingerprint_list_group_not_persistent));
        groupData.add(notPersistentGroup);
        
        List<List<Map<String, String>>> childData = new ArrayList<List<Map<String, String>>>();
        List<Map<String, String>> firstGroupChilds = new ArrayList<Map<String, String>>();
        List<Map<String, String>> secondGroupChilds = new ArrayList<Map<String, String>>();
        Map<String, String> curChildMap = new HashMap<String, String>();
        
        for (Pair<String, String> pair : getPersistentList(mFingerprint.getPersistentProperties())) {
            
            curChildMap = new HashMap<String, String>();
            curChildMap.put(NAME, pair.first);
            curChildMap.put(VALUE, pair.second);
            firstGroupChilds.add(curChildMap);
        }
        
        childData.add(firstGroupChilds);
        
        for (Pair<String, String> pair : getNotPersistentList(mFingerprint.getNotPersistentProperties())) {
            curChildMap = new HashMap<String, String>();
            curChildMap.put(NAME, pair.first);
            curChildMap.put(VALUE, pair.second);
            secondGroupChilds.add(curChildMap);
        }
        
        childData.add(secondGroupChilds);
         
       ExpandableListAdapter adapter = new SimpleExpandableListAdapter(
            this,
            groupData,
            android.R.layout.simple_expandable_list_item_1,
            new String[] { NAME },
            new int[] { android.R.id.text1 },
            childData,
            R.layout.simple_expandable_list_item_2,
            new String[] { NAME, VALUE },
            new int[] { android.R.id.text1, android.R.id.text2 }
        );
       
        list.setAdapter(adapter);
    }
    
    private List<Pair<String, String>> getPersistentList(FingerprintPersistentProperties p) {
        
        List<Pair<String, String>> list = new ArrayList<Pair<String, String>>();
        
        list.add(new Pair<String, String>(getString(R.string.str_fingerprint_list_imei), p.IMEI));
        list.add(new Pair<String, String>(getString(R.string.str_fingerprint_list_mac), p.wifiMAC));
        list.add(new Pair<String, String>(getString(R.string.str_fingerprint_list_model), p.model));
        list.add(new Pair<String, String>(getString(R.string.str_fingerprint_list_screen_dp), 
                                          Integer.toString(p.screenSizeDp)));
        list.add(new Pair<String, String>(getString(R.string.str_fingerprint_list_screen_px),
                                          Integer.toString(p.screenSizePx)));
        list.add(new Pair<String, String>(getString(R.string.str_fingerprint_list_hash), 
                                          p.persistentPropertiesHash));
        
        return list;
    }
    private List<Pair<String, String>> getNotPersistentList(FingerprintNotPersistentProperties p) {
        
        List<Pair<String, String>> list = new ArrayList<Pair<String, String>>();
        
        list.add(new Pair<String, String>(getString(R.string.str_fingerprint_list_locale), p.locale));
        list.add(new Pair<String, String>(getString(R.string.str_fingerprint_list_os_version), 
                                          Integer.toString(p.osVersion)));
        list.add(new Pair<String, String>(getString(R.string.str_fingerprint_list_ip), p.wifiIpAddress));
        list.add(new Pair<String, String>(getString(R.string.str_fingerprint_list_sim_serial),
                                          p.simSerialNumber));
        list.add(new Pair<String, String>(getString(R.string.str_fingerprint_list_android_id), p.androidID));
        list.add(new Pair<String, String>(getString(R.string.str_fingerprint_list_cell_cid),
                p.phoneCellId.isValid() ? p.phoneCellId.asCGI()
                        : getString(R.string.str_fingerprint_list_cell_cid_unknown)));
        
        return list;
    }

}
