package com.kavsdkexample.ui.self_defense;

import com.kavsdk.KavSdk;
import com.kavsdkexample.R;
import com.kavsdkexample.shared.dialogs.BaseSimpleDialog;

import android.widget.ArrayAdapter;

public class EnterClientUserIdDialog extends BaseSimpleDialog {

    @Override
    protected String check() {
        final String userID = mTextView.getText().toString();
        final String strOk = mContext.getString(R.string.str_enter_client_user_id_ok);
        final String strError = mContext.getString(R.string.str_enter_client_user_id_error);

        try {
            KavSdk.getLicense().sendClientUserID(userID);
            return strOk;
        } catch (Exception e) {
            return strError + e.getClass().getSimpleName() + " " + e.getMessage();
        }
    }

    @Override
    protected int getHintTextId() {
        return R.string.str_enter_client_user_id_enter;
    }

    @Override
    protected ArrayAdapter<?> getAdapter() {
        return null;
    }

    @Override
    protected int getTitleId() {
        return R.string.str_enter_client_user_id;
    }

}
