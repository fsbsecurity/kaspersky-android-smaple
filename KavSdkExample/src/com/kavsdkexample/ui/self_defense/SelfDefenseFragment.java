package com.kavsdkexample.ui.self_defense;

import com.kavsdk.KavSdk;
import com.kavsdk.notificationsaccess.NotificationAccessListener;
import com.kavsdk.sdkstatus.SdkStatus;
import com.kavsdkexample.Crasher;
import com.kavsdkexample.R;
import com.kavsdkexample.shared.AppStatus;
import com.kavsdkexample.Utils;
import com.kavsdkexample.shared.activities.DialogFragmentActivity;
import com.kavsdkexample.shared.dialogs.DialogsSupplier;
import com.kavsdkexample.shared.BaseFragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;

import java.lang.ref.WeakReference;

public class SelfDefenseFragment extends BaseFragment implements OnClickListener, NotificationAccessListener {

    private static final int NOTIFICATION_ACCESS_REQUEST = 1;
    private CheckBox mNotificationAccessCheckBox;
    private static final boolean NA_SUPPORTED = Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2;

    private View mRootView;

    private void update() {
        if (KavSdk.isInitialized() && NA_SUPPORTED) {
            mNotificationAccessCheckBox.setChecked(KavSdk.getNotificationAccess().isEnabled());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        update();
    }

    @Override
    public View initUi(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.self_defense_fragment, container, false);

        Utils.initCommandView(mRootView, R.id.check_app_signature_button, R.string.check_app_signature_button, this);
        Utils.initCommandView(mRootView, R.id.check_sdk_button, R.string.str_dsc_check_sdkstatus, this);

        Utils.initCommandView(mRootView, R.id.java_crash_button, R.string.str_dsc_check_crashapp, this);
        Utils.initCommandView(mRootView, R.id.native_crash_button, R.string.str_dsc_check_nativecrashapp, this);
        Utils.initCommandView(mRootView, R.id.enter_client_id_button, R.string.str_enter_client_user_id, this);

        mNotificationAccessCheckBox = (CheckBox) mRootView.findViewById(R.id.selfdefence_notification_access_enabled);

        if (NA_SUPPORTED) {
            mNotificationAccessCheckBox.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (KavSdk.isInitialized()) {
                        startActivityForResult(KavSdk.getNotificationAccess().getGrantingIntent(), NOTIFICATION_ACCESS_REQUEST);
                    }
                }
            });
        } else {
            mNotificationAccessCheckBox.setEnabled(false);
        }

        return mRootView;
    }

    @Override
    public void initComponent(AppStatus appStatus, String additionalInfo) {
        if (NA_SUPPORTED) {
            KavSdk.getNotificationAccess().setListener(this);
            update();
        }
        Utils.initCheckBox(mRootView, R.id.autorestart_enabled_checkbox, true, KavSdk.isSdkAutoRestartEnabled(), this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        KavSdk.getNotificationAccess().setListener(null);
    }

    @Override
    public void onClick(View v) {
        final DialogFragmentActivity act = (DialogFragmentActivity) getActivity();
        
        switch (v.getId()) {
        case R.id.check_app_signature_button:
            startActivity(new Intent(act, CheckSignatureActivity.class));
            break;
        case R.id.check_sdk_button:
            new CheckSdkStatusTask(act)
                .execute();
            break;
        case R.id.java_crash_button:
            Crasher.invokeJavaCrash();
            break;
        case R.id.native_crash_button:
            Crasher.invokeNativeCrash();
            break;
        case R.id.enter_client_id_button:
            act.showFragmentDialog(DialogsSupplier.DIALOG_ENTER_CLIENT_USER_ID);
            break;
        case R.id.autorestart_enabled_checkbox:
            KavSdk.setSdkAutoRestartEnabled(((CheckBox) v).isChecked());
            break;
        default:
            break;
        }   
    }

    @Override
    public void onNotificationAccessChanged(boolean isEnabled) {
        update();
    }

    private static class CheckSdkStatusTask extends AsyncTask<Void, Void, SdkStatus> {
        private final WeakReference<Context> mContext;
        private final ProgressDialog mSdkStatusProgressDialog;

        public CheckSdkStatusTask(Context context) {
            mContext = new WeakReference<Context>(context);
            mSdkStatusProgressDialog = ProgressDialog.show(context, "", context.getString(R.string.app_loading));
        }

        @Override
        protected SdkStatus doInBackground(Void... params) {
            return KavSdk.requestSdkStatus();
        }

        @Override
        protected void onPostExecute(SdkStatus sdkStatus) {
            super.onPostExecute(sdkStatus);
            final Context context = mContext.get();

            if (context != null) {
                StringBuilder message = new StringBuilder();

                message.append(context.getString(R.string.str_dsc_sdkstatus_dlg_status));
                message.append(" ");
                message.append(sdkStatus.getStatus().toString());
                message.append("\n");

                message.append(context.getString(R.string.str_dsc_sdkstatus_dlg_url));
                message.append(" ");
                message.append(sdkStatus.getUrl());
                message.append("\n");

                message.append(context.getString(R.string.str_dsc_sdkstatus_dlg_version));
                message.append(" ");
                message.append(sdkStatus.getVersion());
                message.append("\n");

                new AlertDialog.Builder(context)
                        .setTitle(R.string.str_dsc_check_sdkstatus)
                        .setMessage(message)
                        .create()
                        .show();

                if (mSdkStatusProgressDialog.isShowing()) {
                    mSdkStatusProgressDialog.dismiss();
                }
            }
        }
    }
}
