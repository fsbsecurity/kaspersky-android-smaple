package com.kavsdkexample.shared.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;

import com.kavsdkexample.R;
import com.kavsdkexample.app.SdkSampleApplication;
import com.kavsdkexample.shared.ComponentInitializer;
import com.kavsdkexample.shared.InitializedComponent;

public abstract class BaseActivity extends FragmentActivity implements InitializedComponent {
    private ProgressDialog mProgressDialog;
    private ComponentInitializer mInitializer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SdkSampleApplication app = (SdkSampleApplication) getApplication();
        initUi(savedInstanceState);
        mInitializer = new ComponentInitializer();
        mInitializer.init(app, this);
    }

    public abstract void initUi(Bundle savedInstanceState);

    public void showProgressDialog() {
        mProgressDialog = new ProgressDialog(this) {
            public boolean onKeyDown(int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    return true;
                }
                return false;
            }
        };
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setMessage(getString(R.string.str_main_activity_loading_application));
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.cancel();
        }
    }
}
