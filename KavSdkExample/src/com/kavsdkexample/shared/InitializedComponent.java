package com.kavsdkexample.shared;

public interface InitializedComponent {
    void initComponent(AppStatus appStatus, String additionalInfo);
    void showProgressDialog();
    void hideProgressDialog();
}
