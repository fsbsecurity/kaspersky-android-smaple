package com.kavsdkexample.shared;

import com.kavsdkexample.app.ApplicationStateListener;
import com.kavsdkexample.app.SdkSampleApplication;

public class ComponentInitializer implements ApplicationStateListener {
    private InitializedComponent mInitializer;

    public void init(SdkSampleApplication app, InitializedComponent initializedComponent) {
        mInitializer = initializedComponent;
        if (!app.checkAppInited(this)) {
            mInitializer.showProgressDialog();
        } else {
            mInitializer.initComponent(AppStatus.InitNotRequired, "");
            mInitializer = null;
        }
    }

    @Override
    public void onApplicationReady() {
        endInit(AppStatus.Inited, "");
    }

    @Override
    public void onApplicationInitFailed(String error) {
        endInit(AppStatus.InitFailed, error);
    }

    private void endInit(AppStatus status, String additionalInfo) {
        mInitializer.hideProgressDialog();
        mInitializer.initComponent(status, additionalInfo);
        mInitializer = null;
    }
}
