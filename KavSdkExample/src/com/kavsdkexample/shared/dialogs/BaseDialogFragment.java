package com.kavsdkexample.shared.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

public abstract class BaseDialogFragment extends DialogFragment {
    private static final String TAG = BaseDialogFragment.class.getSimpleName();
    private int mRequestId;
    
    protected static void show(FragmentActivity activity, Class<? extends DialogFragment> classFragment,
            Bundle args, int requestId) {
        
        final String flagmentTag = String.format("%s_%s", TAG, classFragment.getSimpleName());
        
        // remove prev if exists
        FragmentManager fm = activity.getSupportFragmentManager();

        FragmentTransaction ft = fm.beginTransaction();
        Fragment prev = fm.findFragmentByTag(flagmentTag);
        if (prev != null) {
            ft.remove(prev);
        }

        DialogFragment fragment = null;
        try {
            fragment = classFragment.newInstance();
            if (fragment instanceof BaseDialogFragment) {
                ((BaseDialogFragment)fragment).mRequestId = requestId;
            }
        } catch (java.lang.InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        if (fragment != null) {
            fragment.setArguments(args);
            fragment.show(ft, flagmentTag);
        }

    }

    public int getRequestId() {
        return mRequestId;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }
    
    @Override
    public void onDestroyView() {
        // Work around bug: http://code.google.com/p/android/issues/detail?id=17423
        // for support lib use:     getDialog().setOnDismissListener(null);
        // for real fragments:      getDialog().setDismissMessage(null);
        Dialog dlg = getDialog();
        if (dlg != null && getRetainInstance()) {
            dlg.setDismissMessage(null);
        }
        super.onDestroyView();
    }
    
    protected void setDialogResult(Object data) {
        if (getActivity() instanceof OnDialogFragmentResultListener) {
            ((OnDialogFragmentResultListener) getActivity()).onDialogFragmentResult(this, data);
        }
    }
}
