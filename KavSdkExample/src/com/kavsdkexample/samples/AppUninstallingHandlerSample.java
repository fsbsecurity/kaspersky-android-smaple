package com.kavsdkexample.samples;

import android.content.Context;
import android.graphics.PixelFormat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.TextView;

import com.kavsdk.antivirus.AntivirusInstance;
import com.kavsdk.antivirus.AppUninstallingHandler;
import com.kavsdk.antivirus.ThreatInfo;
import com.kavsdkexample.R;
import com.kavsdkexample.shared.UninstallDialogListener;

public class AppUninstallingHandlerSample implements AppUninstallingHandler {
    private final Context mContext;
    private final WindowManager mWindowManager;
    private final  View mInfoView;
    private final  LayoutParams mLayoutParams;
    private boolean mBlockDisabled;
    private final TextView mInfo;

    private UninstallDialogListener mListener;

    public void setListener(UninstallDialogListener listener) {
        mListener = listener;
    }

    public AppUninstallingHandlerSample(Context context) {
        mContext = context;
        mWindowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mInfoView = inflater.inflate(R.layout.uninstall_app_info, null);
        mLayoutParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT, 
                WindowManager.LayoutParams.WRAP_CONTENT, 
                WindowManager.LayoutParams.TYPE_SYSTEM_ALERT, 
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                    | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH 
                    | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, PixelFormat.TRANSLUCENT);
        mLayoutParams.gravity = Gravity.TOP;
        Button cancelButton = (Button) mInfoView.findViewById(R.id.cancelUninstall);
        mInfo = (TextView)  mInfoView.findViewById(R.id.info);
        cancelButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mBlockDisabled = true;
            }
        });
    }

    @Override
    public void showUnblockDialog() {
        long period = AntivirusInstance.getInstance().getUninstallAppProtectionTime() / 1000;
        mInfo.setText(String.format(mContext.getText(R.string.str_app_monitor_remove_dialog_text).toString(), period));
        mBlockDisabled = false;
        mWindowManager.addView(mInfoView, mLayoutParams);

        if (mListener != null) {
            mListener.onShow();
        }
    }

    @Override
    public void hideUnblockDialog() {
        mWindowManager.removeView(mInfoView);

        if (mListener != null) {
            mListener.onHide();
        }
    }
    
    @Override
    public boolean isBlockDisabled() {
        return mBlockDisabled;
    }

    @Override
    public void onUninstallFinished(ThreatInfo threatInfo, boolean success) {
        mListener.onUninstall(threatInfo, success);
    }
}
