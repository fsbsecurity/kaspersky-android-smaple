package com.kavsdkexample.samples;

import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import android.content.Context;

import com.kavsdk.license.SdkLicenseViolationException;
import com.kavsdk.securesms.SecureSmsCallback;
import com.kavsdk.securesms.SecureSmsFilterItem;
import com.kavsdk.securesms.SecureSmsHandleResult;
import com.kavsdk.securesms.SecureSmsManager;
import com.kavsdk.securesms.SmsInfo;
import com.kavsdkexample.R;
import com.kavsdkexample.storage.Settings;
import com.kavsdkexample.ui.data_protection.SecureSmsAddPhoneDialog;
import com.kavsdkexample.storage.DataStorage;

/**
 * This class is a sample implementation of {@link SecureSMSCallback}.
 * The class listens for filtered SMSes, notifies GUI ({@link ISecureSmsView})
 * for updates and provides the information about filtered phone
 * numbers and messages to GUI.
 * 
 * @see SecureSmsFragment, ISecureSmsView
 */
public class SecureSmsComponentSample implements SecureSmsCallback,
                                            SecureSmsAddPhoneDialog.AddItemObserver {
    /* We do not store hard references to not force the activity to live */
    private volatile WeakReference<ISecureSmsView> mSecureSmsEventsListener = new WeakReference<ISecureSmsView>(null);
    
    private final Context mContext;
    private SecureSmsManager mManager;
    
    /* A list of filtered SMS messages */
    private List<SmsInfo>  mSecSmsList;
    
    /* A flag indicated whether secure SMS function is enabled. 
     * This flag is also stored in Settings {@link Settings.SECURE_SMS_FILTERING_ENABLED} */
    private boolean mWorking;
    
    public SecureSmsComponentSample(Context context) {
        mContext = context;
        final DataStorage secSmsCategoryStorage = new DataStorage(context, DataStorage.SECSMS_SETTINGS_STORAGE);
        try {
            mManager = new SecureSmsManager(secSmsCategoryStorage, this, context, true);
        } catch (SdkLicenseViolationException e) {
            final ISecureSmsView view = mSecureSmsEventsListener.get(); 
            if (view != null) {
                view.showToast(R.string.str_securesms_license_issue);
            }
            e.printStackTrace();
        }
        mSecSmsList = new Vector<SmsInfo>();
        mWorking = Settings.isSecureSmsFilteringEnabled(mContext, false);
    }
    
    public void setGuiUpdater(ISecureSmsView updater) {
        mSecureSmsEventsListener = new WeakReference<ISecureSmsView>(updater);
    }
    
    public void removeGuiUpdater() {
        mSecureSmsEventsListener.clear();
    }
    
    public boolean isSecureSmsEnabled() {
        return mWorking;
    }
    
    /**
     * Can be called from different threads
     */
    @Override
    public SecureSmsHandleResult onSecureSms(final SmsInfo smsInfo) {
        mSecSmsList.add(smsInfo);
        
        final ISecureSmsView view = mSecureSmsEventsListener.get(); 
        if (view != null) {
            view.updateSmsView();               
        }
        
        // Let's block all SMSes from the list
        return SecureSmsHandleResult.Block;
    }

    public void onAddItem(SecureSmsFilterItem item) {
        if (mManager != null) {
            mManager.addFilterItem(item);
            mManager.saveChanges();
            
            final ISecureSmsView view = mSecureSmsEventsListener.get(); 
            if (view != null) {
                view.updatePhonesView();
            }
        }
    }
    
    public String getPhonesAsText() {
        StringBuilder sb = new StringBuilder(mContext.getString(R.string.str_securesms_sec_phones));
        if (mManager != null) {
            final int count  = mManager.getSecureSmsFilterItemsCount();
            for (int i = 0; i < count; ++i) {
                sb.append(mManager.getSecureSmsFilterItem(i).getPhoneNumber()).append("\n");
            }           
        }
        return sb.toString();
    }
    
    public String getSmsAsText() {
        StringBuilder sb = new StringBuilder(mContext.getString(R.string.str_securesms_sec_sms));
        synchronized (mSecSmsList) {
            for (SmsInfo sms: mSecSmsList) {
                sb.append(
                        sms.isIncoming() ? mContext.getString(R.string.str_securesms_incoming) 
                                         : mContext.getString(R.string.str_securesms_inbox)
                        )
                        .append(" ")
                        .append(new Date(sms.getSmsDateTime()).toString())
                        .append(" ")
                        .append(sms.getSmsPhoneNumber())
                        .append(" ")
                        .append(sms.getSmsBody());
                
                byte[] data = sms.getData();
                
                if(data != null){
                    //Incoming data sms. In this example, we expect that the text
                    String strData = "";
                    try {
                        strData = new String(data, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    sb.append(" data: [").append(strData).append("]");
                }
                sb.append("\n");
            }
        }
        return sb.toString();
    }
    
    /**
     * This method removes all phones from secure SMS settings 
     */
    public void clearAll() {
        mSecSmsList.clear();
        if (mManager!= null) {
            mManager.clearFilter();
            mManager.saveChanges();
        }
        final ISecureSmsView view = mSecureSmsEventsListener.get(); 
        if (view != null) {
            view.updatePhonesView();
            view.updateSmsView();
        }
    }
    
    public void processInbox() {
        if (mManager == null) {
            return;
        }
        new Thread(new Runnable() {         
            @Override
            public void run() {
                if (mManager == null) {
                    return;
                }
                // Inbox processing can be rather time consuming. 
                // Therefore, it is invoked from a separate thread
                mManager.processInbox();
                final ISecureSmsView view = mSecureSmsEventsListener.get(); 
                if (view != null) {
                    view.setProcessButtonEnabled(true);
                }
            }
        }).start();
    }
    
    public void toogleSmsFilter() {
        if (mManager == null) {
            return;
        }
        if (mWorking) {
            mManager.stopFiltering();
            mWorking = false;
        } else {
            mManager.startFiltering();
            mWorking = true;
        }
        final ISecureSmsView view = mSecureSmsEventsListener.get(); 
        if (view != null) {
            view.updateToggleButtonState(mWorking);
        }
        Settings.setSecureSmsFilteringEnabaled(mContext, mWorking);
    }
    
    /**
     * This is a contract between {@link SecureSmsComponentSample} and {@link SecureSmsFragment}
     * and is used to notify GUI.
     */
    public interface ISecureSmsView {
        void updateSmsView();
        void updatePhonesView();
        void showToast(int resId);
        void setProcessButtonEnabled(boolean enable);
        void updateToggleButtonState(boolean newState);
    }
}
