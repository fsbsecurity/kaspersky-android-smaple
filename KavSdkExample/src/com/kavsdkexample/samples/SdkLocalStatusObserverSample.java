package com.kavsdkexample.samples;

import android.content.Context;
import android.os.Handler;
import android.widget.Toast;

import com.kavsdk.KavSdk;
import com.kavsdk.sdkstatus.ComponentStatus;
import com.kavsdk.sdkstatus.SdkLocalStatus;
import com.kavsdk.sdkstatus.SdkLocalStatusObserver;
import com.kavsdkexample.R;
import com.kavsdkexample.ui.network_protection.ChangeProxyPortListener;

import java.lang.ref.WeakReference;


/**
 * This is the sample of local SDK status change handling.
 * When one of the component changes its status
 * {@link SdkLocalStatusObserverSample#onLocalStatusChanged} method is called.
 * In this method you should get local SDK staus by calling {@link com.kavsdk.KavSdk#getLocalSdkStatus()}
 * and check the status of each component by calling corresponding method.
 * If the status of component is {@link com.kavsdk.sdkstatus.ComponentStatus#OK} than it works normally.
 * If the status of component is {@link com.kavsdk.sdkstatus.ComponentStatus#Off} than it switched off.
 * For more information see {@link com.kavsdk.sdkstatus.SdkLocalStatus}.
 */
public class SdkLocalStatusObserverSample implements SdkLocalStatusObserver {
    private final Context mContext;
    private final Handler mHandler;
    private WeakReference<ChangeProxyPortListener> mWebFilter;

    public SdkLocalStatusObserverSample(Context context, Handler handler) {
        mContext = context;
        mHandler = handler;
    }

    public void setWebFilterActivityReference(ChangeProxyPortListener activity) {
        mWebFilter = new WeakReference<ChangeProxyPortListener>(activity);
    }

    @Override
    public void onLocalStatusChanged() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                SdkLocalStatus sdkLocalStatus = KavSdk.getLocalSdkStatus();
                ComponentStatus webfilterStatus = sdkLocalStatus.getWebfilterStatus();
                ComponentStatus taskReputationStatus = sdkLocalStatus.getTaskReputationStatus();

                if (mWebFilter != null && webfilterStatus == ComponentStatus.SocketListeningError) {
                    ChangeProxyPortListener activity = mWebFilter.get();
                    if (activity != null) {
                        activity.changeProxyPort();
                    }
                }

                if (webfilterStatus != ComponentStatus.OK && webfilterStatus != ComponentStatus.Off) {
                    Toast.makeText(mContext, mContext.getString(R.string.web_filter_not_working), Toast.LENGTH_LONG).show();
                }
                if (taskReputationStatus != ComponentStatus.OK && taskReputationStatus != ComponentStatus.Off) {
                    Toast.makeText(mContext, mContext.getString(R.string.task_reputation_not_working), Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
