package com.kavsdkexample.samples;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.kavsdk.antivirus.SuspiciousThreatType;
import com.kavsdk.antivirus.ThreatInfo;
import com.kavsdk.antivirus.ThreatType;
import com.kavsdk.antivirus.foldermonitor.FolderMonitor;
import com.kavsdk.antivirus.foldermonitor.FolderMonitorBuilder;
import com.kavsdk.antivirus.foldermonitor.FolderMonitorListener;
import com.kavsdk.antivirus.foldermonitor.FolderMonitorSuspiciousListener;
import com.kavsdk.license.SdkLicenseViolationException;
import com.kavsdkexample.DuplicateKeyException;
import com.kavsdkexample.storage.Settings;

public class FolderMonitorSample implements FolderMonitorListener, FolderMonitorSuspiciousListener {
    private static final boolean DEBUG = true;
    private static final String TAG = FolderMonitorSample.class.getSimpleName();
    private static final String PREF_MONITORING_FOLDERS = "monitoring-folders";
    private final Context mContext;
    private final Handler mHandler;
    private final HashMap<String, FolderMonitor> mMonitors;
    private final Set<String> mFolders;
    
    public FolderMonitorSample(Context context) throws SdkLicenseViolationException {
        mContext = context;
        mHandler = new Handler(Looper.getMainLooper());
        mMonitors = new HashMap<String, FolderMonitor>();
        mFolders = Settings.getStringSet(mContext, PREF_MONITORING_FOLDERS, new HashSet<String>(0));
    }
    
    public void loadMonitors() throws SdkLicenseViolationException {
        unloadMonitors();
        for (String path : mFolders) {
            File dir = new File(path);
            if (dir.exists() && dir.isDirectory()) {
                try {
                    loadMonitor(dir);
                } catch (DuplicateKeyException e) {
                    if (DEBUG) {
                        Log.d(TAG, String.format("Directory '%s' is already monitored", path));
                    }
                }
            }
        }
    }
    
    private void saveMonitoringFolders() {
        Settings.putStringSet(mContext, PREF_MONITORING_FOLDERS, mFolders);
    }
    
    public Set<String> getFolders() {
        return mMonitors.keySet();
    }
    
    private void loadMonitor(File dir) throws DuplicateKeyException, SdkLicenseViolationException {
        final String path = dir.getAbsolutePath();
        if (mMonitors.containsKey(path)) {
            throw new DuplicateKeyException();
        }
        FolderMonitor monitor = new FolderMonitorBuilder(path)
                    .setFolderMonitorListener(this)
                    .setFolderMonitorSuspiciousListener(this)
                    .setScanUdsAllow(isUdsEnabled())
                    .setScanArchived(isUnpackArchives())
                    .setSkipRiskware(isSkipRiskware())
                    .setCureInfectedFiles(isCureInfected())
                .create();
        monitor.start();
        mMonitors.put(path, monitor);
    }
    
    private void updateMonitor(FolderMonitorApplier applier) {
        for (FolderMonitor folderMonitor : mMonitors.values()) {
            applier.onUpdate(folderMonitor);
        }
    }
    
    public void addFolderMonitor(File dir) throws DuplicateKeyException, SdkLicenseViolationException {
        loadMonitor(dir);
        mFolders.add(dir.getAbsolutePath());
        saveMonitoringFolders();
    }
    
    public void deleteFolderMonitor(String path) {
        FolderMonitor monitor = mMonitors.remove(path);
        if (monitor != null) {
            monitor.stop();
        }
        mFolders.remove(path);
        saveMonitoringFolders();
    }
    
    public void unloadMonitors() {
        for (FolderMonitor monitor : mMonitors.values()) {
            monitor.stop();
        }
        mMonitors.clear();
    }
    
    public void clearAllFolderMonitors() {
        unloadMonitors();
        mFolders.clear();
        saveMonitoringFolders();
    }
    
    public boolean isUdsEnabled() {
        return Settings.isFolderMonitorUdsEnabled(mContext, true);
    }
    
    public void setUdsEnabled(final boolean value) throws SdkLicenseViolationException {
        Settings.setFolderMonitorUdsEnabled(mContext, value);
        updateMonitor(new FolderMonitorApplier() {
            
            @Override
            public void onUpdate(FolderMonitor folderMonitor) {
                folderMonitor.setScanUdsAllow(value);
            }
        });
    }
    
    public boolean isUnpackArchives() {
        return Settings.isFolderMonitorUnpackArchives(mContext, true);
    }
    
    public void setUnpackArchives(final boolean value) throws SdkLicenseViolationException {
        Settings.setFolderMonitorUnpackArchives(mContext, value);
        updateMonitor(new FolderMonitorApplier() {
            
            @Override
            public void onUpdate(FolderMonitor folderMonitor) {
                folderMonitor.setScanArchived(value);
            }
        });
    }
    
    public boolean isSkipRiskware() {
        return Settings.isFolderMonitorSkipRiskware(mContext, false);
    }
    
    public void setSkipRiskware(final boolean value) throws SdkLicenseViolationException {
        Settings.setFolderMonitorSkipRiskware(mContext, value);
        updateMonitor(new FolderMonitorApplier() {
            
            @Override
            public void onUpdate(FolderMonitor folderMonitor) {
                folderMonitor.setSkipRiskware(value);
            }
        });
    }
    
    public boolean isCureInfected() {
        return Settings.isFolderMonitorCureInfected(mContext, true);
    }
    
    public void setCureInfected(final boolean value) throws SdkLicenseViolationException {
        Settings.setFolderMonitorCureInfected(mContext, value);
        updateMonitor(new FolderMonitorApplier() {
            
            @Override
            public void onUpdate(FolderMonitor folderMonitor) {
                folderMonitor.setCureInfectedFiles(value);
            }
        });
    }

    @Override
    public void onSuspiciousDetected(ThreatInfo threatInfo, SuspiciousThreatType threatType) {
        final String msg = "onSuspiciousDetected, virusName = " + threatInfo.getVirusName()
                + ", threat type = " + threatType.toString();
        if (DEBUG) {
            Log.d(TAG, msg);
        }
        showToast(msg);
    }

    @Override
    public boolean onVirusDetected(ThreatInfo threatInfo, ThreatType threatType) {
        final String msg = "onVirusDetected, virusName = " + threatInfo.getVirusName();
        if (DEBUG) {
            Log.d(TAG, msg);
        }
        showToast(msg);
        return true;
    }
    
    private void showToast(final String msg) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onMonitorStop(FolderMonitor monitor) {
        mMonitors.remove(monitor.getFolderPath());
    }
    
    /************************************* Inner classes *****************************************/
    private interface FolderMonitorApplier {
        void onUpdate(FolderMonitor folderMonitor);
    }
}
