package com.kavsdkexample.storage;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.text.TextUtils;

/**
 * This is a class for quick access to the settings of this sample application. 
 *       They are stored as a set of preferences 
 *  See {@link SharedPreferences}
 */
public final class Settings {
    private static volatile SharedPreferences sSharedPreferences;
    
    /** A preferences filename where sample application settings are stored */
    private static final String PREFERENCES = "SDK_EXAMPLE_PREFERENCES";
    
    /*
     * Actions applied to not-cured files
     */
    public static final int DELETE_THREAT_ACTION        = 0;
    public static final int QUARANTINE_THREAT_ACTION    = 1;
    public static final int SKIP_THREAT_ACTION      = 2;

    /* Settings keys */
    private static final String MONITOR_SCAN_MODE_PREF               = "monitorScanMode";
    private static final String MONITOR_ACTION                       = "monitorAction";
    private static final String ODS_SCAN_MODE_PREF                   = "odsScanMode";
    private static final String ODS_CLEAN_MODE_PREF                  = "odsCleanMode";
    private static final String ODS_MULTISCAN_MODE_ENABLED           = "odsMultiscanModeEnabled";
    private static final String ODS_MULTISCAN_MODE_ADD_APPS_ENABLED  = "odsMultiscanModeAddAppsEnabled";
    private static final String ODS_MULTISCAN_MODE_SCAN_THREADS      = "odsMultiscanScanThreads";
    private static final String ODS_MULTISCAN_MODE_EXPLORE_THREADS   = "odsMultiscanExploreThreads";
    private static final String MONITOR_ENABLED_PREF                 = "monitorEnabled";
    private static final String APK_INSTALLATION_ENABLED_PREF        = "installationCheckEnabled";
    private static final String ACTION_IF_NOT_CURED_PREF             = "actionIfNotCured";
    private static final String SCAN_ACTION_PREF                     = "monitorScanAction";
    private static final String WEB_FILTER_ENABLED                   = "webfilterEnabled";
    private static final String WEB_FILTER_APN_PROXY_ENABLED         = "apnProxyEnabled";
    private static final String WEB_FILTER_WIFI_PROXY_ENABLED        = "wifiProxyEnabled";
    private static final String ANTISPAM_ENABLED                     = "antispamEnabled";
    private static final String ANTISPAM_MODE                        = "antispamMode";
    private static final String SECURE_SMS_FILTERING_ENABLED         = "secureSmsFilteringEnabled";
    private static final String LOAD_ONLY_TRUSTED_URLS_ENABLED       = "loadOnlyTrustedUrlsEnabled";
    private static final String SD_CARD_PATH_FOR_SCAN                = "sdCardPathForScan";
    private static final String SIM_WATCH_ENABLED                    = "simWatchEnabled";
    private static final String APP_MONITOR_UDS_ENABLED              = "appMonitorUdsEnabled";
    private static final String APP_MONITOR_DEEP_SCAN_ENABLED        = "appMonitorDeepScanEnabled";
    private static final String APP_MONITOR_SCAN_MISSED_ENABLED      = "appMonitorScanMissedEnabled";
    private static final String FOLDER_MONITOR_UDS_ENABLED           = "folderMonitorUdsEnabled";
    private static final String FOLDER_MONITOR_UNPACK_ARCHIVES       = "folderMonitorUnpackArchives";
    private static final String FOLDER_MONITOR_SKIP_RISKWARE         = "folderMonitorSkipRiskware";
    private static final String FOLDER_MONITOR_CURE_INFECTED         = "folderMonitorCureInfected";
    private static final String APPCONTROL_ENABLED                   = "appControlEnabled";
    private static final String APPCONTROL_USE_WINDOWMANAGER         = "appControlUseWindowManager";
    
    
    /*
     * A thread-safe method. Always returns the same {@link SharedPreferences} object
     * which stores sample application settings
     *  
     * @param context application context
     * @return a {@link SharedPreferences} file with {@link #PREFERENCES} filename
     */

    private Settings(){}

    private static SharedPreferences getSharedPreferences(Context context) {
        if (sSharedPreferences == null) {
            synchronized (Settings.class) {
                if (sSharedPreferences == null) {
                    sSharedPreferences = context.getSharedPreferences(PREFERENCES, Activity.MODE_PRIVATE);
                }
            }
        }
        return sSharedPreferences;
    }
    
    public static boolean isAppControlEnabled(Context context, boolean defValue) {
        return getSharedPreferences(context).getBoolean(
                APPCONTROL_ENABLED, defValue);
    }   
    
    public static void setAppControlEnabled(Context context, boolean value) {
        writeBooleanPref(context, APPCONTROL_ENABLED, value);
    }
    
    public static boolean useAppControlWindowManager(Context context, boolean defValue) {
        return getSharedPreferences(context).getBoolean(
                APPCONTROL_USE_WINDOWMANAGER, defValue);
    }   
    
    public static void setUsingWindowManager(Context context, boolean value) {
        writeBooleanPref(context, APPCONTROL_USE_WINDOWMANAGER, value);
    }   
        
    public static boolean isFolderMonitorCureInfected(Context context, boolean defValue) {
        return getSharedPreferences(context).getBoolean(
                FOLDER_MONITOR_CURE_INFECTED, defValue);
    }
    
    public static void setFolderMonitorCureInfected(Context context, boolean value) {
        writeBooleanPref(context, FOLDER_MONITOR_CURE_INFECTED, value);
    }
    
    public static boolean isFolderMonitorSkipRiskware(Context context, boolean defValue) {
        return getSharedPreferences(context).getBoolean(
                FOLDER_MONITOR_SKIP_RISKWARE, defValue);
    }
    
    public static void setFolderMonitorSkipRiskware(Context context, boolean value) {
        writeBooleanPref(context, FOLDER_MONITOR_SKIP_RISKWARE, value);
    }
    
    public static boolean isFolderMonitorUnpackArchives(Context context, boolean defValue) {
        return getSharedPreferences(context).getBoolean(
                FOLDER_MONITOR_UNPACK_ARCHIVES, defValue);
    }
    
    public static void setFolderMonitorUnpackArchives(Context context, boolean value) {
        writeBooleanPref(context, FOLDER_MONITOR_UNPACK_ARCHIVES, value);
    }
    
    public static boolean isFolderMonitorUdsEnabled(Context context, boolean defValue) {
        return getSharedPreferences(context).getBoolean(
                FOLDER_MONITOR_UDS_ENABLED, defValue);
    }
    
    public static void setFolderMonitorUdsEnabled(Context context, boolean value) {
        writeBooleanPref(context, FOLDER_MONITOR_UDS_ENABLED, value);
    }

    public static boolean isAppMonitorUdsEnabled(Context context, boolean defValue) {
        return getSharedPreferences(context).getBoolean(
                APP_MONITOR_UDS_ENABLED, defValue);
    }
    
    public static void setAppMonitorUdsEnabled(Context context, boolean value) {
        writeBooleanPref(context, APP_MONITOR_UDS_ENABLED, value);
    }
    
    public static boolean isAppMonitorDeepScanEnabled(Context context, boolean defValue) {
        return getSharedPreferences(context).getBoolean(
                APP_MONITOR_DEEP_SCAN_ENABLED, defValue);
    }
    
    public static void setAppMonitorDeepScanEnabled(Context context, boolean value) {
        writeBooleanPref(context, APP_MONITOR_DEEP_SCAN_ENABLED, value);
    }
    
    public static void setAppMonitorScanMissedEnabled(Context context, boolean value) {
        writeBooleanPref(context, APP_MONITOR_SCAN_MISSED_ENABLED, value);
    }
    
    public static boolean isAppMonitorScanMissedEnabled(Context context, boolean defValue) {
        return getSharedPreferences(context).getBoolean(
                APP_MONITOR_SCAN_MISSED_ENABLED, defValue);
    }

    public static boolean isMonitorEnabled(Context context, boolean defValue) {
        return getSharedPreferences(context).getBoolean(
                MONITOR_ENABLED_PREF, defValue);
    }
    
    public static void setMonitorEnabled(Context context, boolean value) {
        writeBooleanPref(context, MONITOR_ENABLED_PREF, value);
    }
    
    public static boolean isSimWatchEnabled(Context context, boolean defValue) {
        return getSharedPreferences(context).getBoolean(SIM_WATCH_ENABLED, defValue);
    }
    
    public static void setSimWatchEnabled(Context context, boolean value) {
        writeBooleanPref(context, SIM_WATCH_ENABLED, value);
    }
    
    public static int getMonitorScanMode(Context context, int defValue) {
        return getSharedPreferences(context).getInt(MONITOR_SCAN_MODE_PREF, defValue);
    }
    
    public static void setMonitorScanMode(Context context, int value) {
        writeIntPref(context, MONITOR_SCAN_MODE_PREF, value);
    }
    
    public static int getMonitorAction(Context context) {
        return getSharedPreferences(context).getInt(MONITOR_ACTION, SKIP_THREAT_ACTION);
    }

    public static void setMonitorAction(Context context, int value) {
        writeIntPref(context, MONITOR_ACTION, value);
    }

    public static boolean isApkInstallationEnabled(Context context, boolean defValue) {
        return getSharedPreferences(context).getBoolean(
                APK_INSTALLATION_ENABLED_PREF, defValue);
    }
    
    public static void setApkInstallationEnabled(Context context, boolean value) {
        writeBooleanPref(context, APK_INSTALLATION_ENABLED_PREF, value);
    }
    
    public static int getActionIfNotCured(Context context, int defValue) {
        return getSharedPreferences(context).getInt(ACTION_IF_NOT_CURED_PREF, defValue);
    }
    
    public static void setActionIfNotCured(Context context, int value) {
        writeIntPref(context, ACTION_IF_NOT_CURED_PREF, value);
    }
    
    public static int getScanAction(Context context, int defValue) {
        return getSharedPreferences(context).getInt(SCAN_ACTION_PREF, defValue);
    }
    
    public static void setScanAction(Context context, int value) {
        writeIntPref(context, SCAN_ACTION_PREF, value);
    }
    
    public static boolean getWebFilterEnabled(Context context, boolean defValue) {
        return getSharedPreferences(context).getBoolean(WEB_FILTER_ENABLED, defValue);
    }
    
    public static void setWebFilterEnabled(Context context, boolean value) {
        writeBooleanPref(context, WEB_FILTER_ENABLED, value);
    }
    
    public static boolean getApnProxyEnabled(Context context, boolean defValue) {
        return getSharedPreferences(context).getBoolean(WEB_FILTER_APN_PROXY_ENABLED, defValue);
    }
    
    public static void setApnProxyEnabled(Context context, boolean value) {
        writeBooleanPref(context, WEB_FILTER_APN_PROXY_ENABLED, value);
    }
    
    public static boolean getWifiProxyEnabled(Context context, boolean defValue) {
        return getSharedPreferences(context).getBoolean(WEB_FILTER_WIFI_PROXY_ENABLED, defValue);
    }
    
    public static void setWifiProxyEnabled(Context context, boolean value) {
        writeBooleanPref(context, WEB_FILTER_WIFI_PROXY_ENABLED, value);
    }
    
    public static int getOdsScanMode(Context context, int defValue) {
        return getSharedPreferences(context).getInt(ODS_SCAN_MODE_PREF, defValue);
    }
    
    public static void setOdsScanMode(Context context, int value) {
        writeIntPref(context, ODS_SCAN_MODE_PREF, value);
    }
    
    public static int getOdsCleanMode(Context context, int defValue) {
        return getSharedPreferences(context).getInt(ODS_CLEAN_MODE_PREF, defValue);
    }
    
    public static void setOdsCleanMode(Context context, int value) {
        writeIntPref(context, ODS_CLEAN_MODE_PREF, value);
    }
    
    public static boolean isAntiSpamEnabled(Context context, boolean defValue) {
        return getSharedPreferences(context).getBoolean(ANTISPAM_ENABLED, defValue);
    }
    
    public static void setAntiSpamEnabled(Context context, boolean value) {
        writeBooleanPref(context, ANTISPAM_ENABLED, value);
    }
    
    public static int getAntiSpamMode(Context context, int defValue) {
        return getSharedPreferences(context).getInt(ANTISPAM_MODE, defValue);
    }
    
    public static void setAntiSpamMode(Context context, int value) {
        writeIntPref(context, ANTISPAM_MODE, value);
    }
    
    public static boolean isSecureSmsFilteringEnabled(Context context, boolean defValue) {
        return getSharedPreferences(context).getBoolean(SECURE_SMS_FILTERING_ENABLED, defValue);
    }
    
    public static void setSecureSmsFilteringEnabaled(Context context, boolean value) {
        writeBooleanPref(context, SECURE_SMS_FILTERING_ENABLED, value);
    }
    
    public static boolean isLoadOnlyTrustedUrlsEnabled(Context context, boolean defValue) {
        return getSharedPreferences(context).getBoolean(LOAD_ONLY_TRUSTED_URLS_ENABLED, false);
    }
    
    public static void setLoadOnlyTrustedUrlsEnabled(Context context, boolean value) {
        writeBooleanPref(context, LOAD_ONLY_TRUSTED_URLS_ENABLED, value);
    }
    
    public static String getSdCardPathForScan(Context context, String defValue) {
        return getSharedPreferences(context).getString(SD_CARD_PATH_FOR_SCAN, defValue);
    }
    
    public static void setSdCardPathForScan(Context context, String value) {
        writeStringPref(context, SD_CARD_PATH_FOR_SCAN, value);
    }

    public static boolean isMultithreadedModeEnabled(Context context, boolean defValue) {
        return getSharedPreferences(context).getBoolean(ODS_MULTISCAN_MODE_ENABLED, defValue);
    }

    public static void setMultithreadedModeEnabled(Context context, boolean value) {
        writeBooleanPref(context, ODS_MULTISCAN_MODE_ENABLED, value);
    }

    public static boolean isMultithreadedModeAddApps(Context context, boolean defValue) {
        return getSharedPreferences(context).getBoolean(ODS_MULTISCAN_MODE_ADD_APPS_ENABLED, defValue);
    }

    public static void setMultithreadedModeAddApps(Context context, boolean value) {
        writeBooleanPref(context, ODS_MULTISCAN_MODE_ADD_APPS_ENABLED, value);
    }

    public static int getExploringThreadsCount(Context context, int defValue) {
        return getSharedPreferences(context).getInt(ODS_MULTISCAN_MODE_EXPLORE_THREADS, defValue);
    }

    public static void setExploringThreadsCount(Context context, int value) {
        writeIntPref(context, ODS_MULTISCAN_MODE_EXPLORE_THREADS, value);
    }

    public static int getScanningThreadsCount(Context context, int defValue) {
        return getSharedPreferences(context).getInt(ODS_MULTISCAN_MODE_SCAN_THREADS, defValue);
    }

    public static void setScanningThreadsCount(Context context, int value) {
        writeIntPref(context, ODS_MULTISCAN_MODE_SCAN_THREADS, value);
    }

    private static void writeStringPref(Context context, String pref, String value) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(pref, value);
        editor.commit();
    }
    
    /*
     * Methods for saving settings represented by numbers
     */
    private static void writeIntPref(Context context, String pref, int value) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt(pref, value);
        editor.commit();
    }

    /*
     * Methods for saving settings represented by boolean values
     */
    private static void writeBooleanPref(Context context, String pref, boolean value) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(pref, value);
        editor.commit();
    }
    

    /*
     * Set a set of String values in the preferences editor,
     */
    @SuppressLint("NewApi")
    public static void putStringSet(Context context, String key, Set<String> value) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            editor.putString(key, TextUtils.join(",", value));
        } else {
            editor.putStringSet(key, value);
        }
        editor.commit();
    }
    
    /*
     * Retrieve a set of String values from the preferences. 
     */
    @SuppressLint("NewApi")
    public static Set<String> getStringSet(Context context, String key, Set<String> defValues) {
        SharedPreferences pref = getSharedPreferences(context);
        Set<String> value = null;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            String s = pref.getString(key, "");
            if (!TextUtils.isEmpty(s)) {
                value = new HashSet<String>(Arrays.asList(s.split(",")));
            } else {
                value = defValues;
            }
        } else {
            value = pref.getStringSet(key, defValues);
        }
        return new HashSet<String>(value);
    }

}
