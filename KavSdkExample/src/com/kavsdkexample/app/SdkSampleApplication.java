package com.kavsdkexample.app;

import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Vibrator;
import android.text.format.DateUtils;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.GregorianCalendar;
import java.util.Vector;

import com.kavsdk.KavSdk;
import com.kavsdk.antispam.BothListsResult;
import com.kavsdk.antispam.CallFilterControl;
import com.kavsdk.antispam.CallFilterControlFactory;
import com.kavsdk.antispam.CallFilterControlFactoryImpl;
import com.kavsdk.antispam.CallFilterEvent;
import com.kavsdk.antispam.CallFilterEventOrigin;
import com.kavsdk.antispam.CallFilterItem;
import com.kavsdk.antispam.CustomFilter;
import com.kavsdk.antispam.FilterModes;
import com.kavsdk.antivirus.Antivirus;
import com.kavsdk.antivirus.AntivirusInstance;
import com.kavsdk.license.SdkLicense;
import com.kavsdk.license.SdkLicenseDateTimeException;
import com.kavsdk.license.SdkLicenseException;
import com.kavsdk.license.SdkLicenseNetworkException;
import com.kavsdk.license.SdkLicenseViolationException;
import com.kavsdk.shared.iface.KavError;
import com.kavsdk.shared.iface.ServiceStateStorage;
import com.kavsdk.webfilter.WebFilterControl;
import com.kavsdk.webfilter.WebFilterControlFactoryImpl;
import com.kavsdkexample.R;
import com.kavsdkexample.Utils;
import com.kavsdkexample.samples.AppUninstallingHandlerSample;
import com.kavsdkexample.samples.ProxyAuthRequestListenerSample;
import com.kavsdkexample.samples.SdkLocalStatusObserverSample;
import com.kavsdkexample.samples.SecureSmsComponentSample;
import com.kavsdkexample.samples.UrlFilterHandlerSample;
import com.kavsdkexample.storage.DataStorage;
import com.kavsdkexample.storage.Settings;
import com.kavsdkexample.ui.device_protection.AppControlManager;
import com.kavsdkexample.ui.device_protection.DetectedApplicationsActivity;
import com.kavsdkexample.ui.device_protection.KillLockerActivity;
import com.kavsdkexample.ui.device_protection.LockerSuppressionStep;

/**
 * This is a class for initialization, storing and sharing of
 *          SDK components between different parts of the sample application.
 */
public class SdkSampleApplication extends Application
                                  implements ServiceConnection, SdkService.InitializationListener {
    private static final String TAG = SdkSampleApplication.class.getSimpleName();

    private static final int VIBRO_PERIOD = 500;
    private SdkService mSdkService;
    
    /* Web filter parameters */
    public static final String LOCAL_HOST = "127.0.0.1";
    public static final int PORT_NUMBER = 3128;
    private int mNewPortNumber = -1;
    
    private static final int INIT_ERROR_CODE = -1;
    
    /* Let's filter web browsers now. You can change it and try different flags combinations */
    private static final int WEB_FILTER_FLAGS = 0x0;
    
    /* KavSDK components*/
    private Antivirus mAntivirusComponent;
    private volatile WebFilterControl mWebFilterControl;
    private SecureSmsComponentSample mSecureSmsControl;
    private CallFilterControl mCallFilterControl;
    private AppControlManager mAppControlManager;
    private final AntiSpamProxyFilter mAntiSpamProxyFilter = new AntiSpamProxyFilter();
    private DataStorage mWfCategoryStorage, mWfExclusionStorage, mWfSettingsStorage;
    private AppUninstallingHandlerSample mAppUninstallingHandlerSample;
    private ShakeDetector mShakeDetector;

    private final Object mInitLock = new Object();
    private LockerSuppressionStep mStep = LockerSuppressionStep.Scanning;

    public void setLockerSuppressionStep(LockerSuppressionStep step) {
        mStep = step;
    }
    /* 
     * A flag indicating whether the application was initialized
     * Note: if the flag true, the SDK service can be not started yet  
     */
    private volatile boolean mIsInited;
    private volatile boolean mInitFailed;
    private boolean mNeedNewCode;
    
    private final Vector<ApplicationStateListener> mAppReadinessListeners = new Vector<>();

    private Handler mHandler;
    private SdkLocalStatusObserverSample mSdkLocalStatusObserverSample;
    
    public AppUninstallingHandlerSample getAppUninstallingHandlerSample() {
        return mAppUninstallingHandlerSample;
    }
    
    private final BlockedNumbersController mBlockedNumbersController = new BlockedNumbersControllerImpl();
    
    public AppControlManager getAppControlManager() {
        return mAppControlManager;
    }
    
    public BlockedNumbersController getBlockedNumbersController() {
        return mBlockedNumbersController;
    }
    
    public static SdkSampleApplication from(Context context) {
        
        if (context instanceof SdkSampleApplication) {
            return (SdkSampleApplication) context;
            
        } else {
            return (SdkSampleApplication) context.getApplicationContext();
        }
    }
    
    @Override
    public void onCreate() {
        mHandler = new Handler(Looper.getMainLooper());
        mSdkLocalStatusObserverSample = new SdkLocalStatusObserverSample(this, mHandler);
        new Thread(new Runnable() {
            @Override
            public void run() {                
                initApplication();
            }
        }).start();
        /*@Internal_token@*/
        super.onCreate();

        // ShakeDetector initialization
        mShakeDetector = new ShakeDetector(getApplicationContext());
        mShakeDetector.setOnShakeListener(new ShakeDetector.OnShakeListener() {
            @Override
            public void onShake(int count) {
                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                v.vibrate(VIBRO_PERIOD);
                Toast.makeText(getApplicationContext(), "Device was shaken", Toast.LENGTH_SHORT).show();
                Intent intent = null;
                switch (mStep) {
                    case Scanning:
                        intent = new Intent(getApplicationContext(), KillLockerActivity.class);
                        break;
                    case Killing:
                        intent = DetectedApplicationsActivity.newStartIntent(getApplicationContext());
                        intent.putExtra(DetectedApplicationsActivity.WITH_SUPPRESSION_EXTRA, true);
                        break;
                    default:
                        throw new IllegalStateException("It shouldn't be this way");
                }
                LockerSuppressor.getInstance().startActivity(getApplicationContext(), intent);
            }
        });
        mShakeDetector.on();
    }
    
    public boolean isNeedNewCode() {
        return mNeedNewCode;
    }

    private void onInitError(String message) {
        mInitFailed = false;
        onInitFailed(message);    
    }

    public void changePortNumber(int newPort) {
        mNewPortNumber = newPort;
    }

    public void reinitWebFilterControl() throws SdkLicenseViolationException {
        if (mNewPortNumber == -1) {
            reinitWebFilterControl(LOCAL_HOST, PORT_NUMBER);
        } else {
            reinitWebFilterControl(LOCAL_HOST, mNewPortNumber);
        }
    }

    public void reinitWebFilterControl(String proxyHost, int proxyPort) throws SdkLicenseViolationException {
        reinitWebFilterControl(proxyHost, proxyPort, proxyHost, proxyPort);
    }

    public void reinitWebFilterControl(String wifiProxyHost, int wifiProxyPort, String apnProxyHost, int apnProxyPort) throws SdkLicenseViolationException {
        boolean isWifiProxyEnabled = Settings.getWifiProxyEnabled(getApplicationContext(), false);
        synchronized (SdkSampleApplication.class) {
        if (mWebFilterControl != null && mWebFilterControl.isEnabled()) {
            mWebFilterControl.enable(false);
        }                
        
        int flags = WEB_FILTER_FLAGS;
        
        if (!isWifiProxyEnabled) {
            flags |= WebFilterControl.DISABLE_PROXY;
        } else {
            flags &= ~WebFilterControl.DISABLE_PROXY;
        }
                
            mWebFilterControl = new WebFilterControlFactoryImpl().create(mWfCategoryStorage, mWfExclusionStorage,
                    mWfSettingsStorage, new UrlFilterHandlerSample(getApplicationContext()),
                    getApplicationContext(), flags, wifiProxyHost, wifiProxyPort, apnProxyHost, apnProxyPort);
            
            if (Settings.getWebFilterEnabled(getApplicationContext(), false)) {
                mWebFilterControl.enable(true);
            }
        }
    }
    
    /**
     * This method initializes KAV SDK components. In case of errors, 
     * the method shows a toast with corresponding error messages
     * 
     * @return KavError.KAV_OK {@link KavError#KAV_OK} if the initialization has been successfully completed or
     *          an error code {@link KavError} otherwise 
     */
    private int initApplication() {
        synchronized (mInitLock) {
            if (mInitFailed) {
                onInitFailed("Bad license");                 
                return INIT_ERROR_CODE;            
            } else if (mIsInited) {
                return KavError.KAV_OK;
            }
            
            Log.d(TAG, "======================= BEGIN =======================");
           
            final File basesPath = getDir("bases", Context.MODE_PRIVATE);

            DataStorage generalStorage  = new DataStorage(this, DataStorage.GENERAL_SETTINGS_STORAGE);
            
            // If you do not want to store native libraries in the application data directory
            // SDK provides the ability to specify another path (otherwise, you can simply omit pathToLibraries parameter). 
            // Note: storing the libraries outside the application data directory is not secure as
            // the libraries can be replaced. In this case, the libraries correctness checking is required.
            // Besides, the specified path must be to device specific libraries, i.e. you should care about device architecture. 
            String path = null;
            try {
                PackageManager m = getPackageManager();
                String packageName = getPackageName();
                PackageInfo packageInfo;
                packageInfo = m.getPackageInfo(packageName, 0);
                path = packageInfo.applicationInfo.nativeLibraryDir;
            } catch (NameNotFoundException e) {
                e.printStackTrace();
            }
            
            try {
                KavSdk.setProxyAuthListener(new ProxyAuthRequestListenerSample(getApplicationContext()));
                KavSdk.getLocalSdkStatus().setObserver(mSdkLocalStatusObserverSample);
                KavSdk.initSafe(this, basesPath, generalStorage, R.raw.avbases, path);

                final SdkLicense license = KavSdk.getLicense();
                if (!license.isValid()) {
                    if (!license.isClientUserIDRequired()) {
                        license.activate(null);
                    }
                }                
            } catch (SdkLicenseNetworkException e) {
                mNeedNewCode = false;
                onInitError(String.format("Cannot init SDK: network problems\nDetails: %s", e.getMessage()));
                return INIT_ERROR_CODE;
            } catch (SdkLicenseDateTimeException e) {
                mNeedNewCode = false;
                onInitError(String.format("Cannot init SDK: date on the device is incorrect\nDetails: %s", e.getMessage()));
                return INIT_ERROR_CODE;
            } catch (SdkLicenseException e) {
                mNeedNewCode = true;
                onInitError(String.format("License is not valid\nDetails: %s", e.getMessage()));
                return INIT_ERROR_CODE;
            } catch (IOException e) {
                onInitError(e.getMessage());
                return INIT_ERROR_CODE;
            }
            SdkLicense license = KavSdk.getLicense();
            if (license.isValid()) {
                final GregorianCalendar calendar = new GregorianCalendar();
                calendar.setTimeInMillis(license.getLicenseKeyExpireDate() * 1000);
                Log.d(TAG, "License valid till " +             
                        DateUtils.formatDateTime(this, 
                                calendar.getTimeInMillis(),
                                DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_YEAR));
            } else {
                Log.d(TAG, "License is not valid");
                mInitFailed = false;
                mNeedNewCode = true;
                onInitFailed("Bad license");                 
                return INIT_ERROR_CODE;            
            }

            mAntivirusComponent = AntivirusInstance.getInstance();
            
            File scanTmpDir = getDir("scan_tmp", Context.MODE_PRIVATE);
            File monitorTmpDir = getDir("monitor_tmp", Context.MODE_PRIVATE);
            Log.d(TAG, "AV scanner directory for temp files " + scanTmpDir);
            Log.d(TAG, "AV FS monitor directory for temp files " + monitorTmpDir);
            
            try {
                mAntivirusComponent.initAntivirus(getApplicationContext(),
                        scanTmpDir.getAbsolutePath(), monitorTmpDir.getAbsolutePath());

                mAppUninstallingHandlerSample = new AppUninstallingHandlerSample(this);
                mAntivirusComponent.setAppUninstallingHandler(mAppUninstallingHandlerSample);
            } catch (SdkLicenseViolationException e) {
                mInitFailed = true;
                e.printStackTrace();
                onInitFailed("Bad license");                 
                return INIT_ERROR_CODE;            
            }
            
            Utils.printAvDbDetails(getApplicationContext(), TAG, mAntivirusComponent.getVirusDbInfo());
            
            mSecureSmsControl = new SecureSmsComponentSample(getApplicationContext());
            
            mWfCategoryStorage  = new DataStorage(this, DataStorage.WF_CATEGORY_STORAGE);
            mWfExclusionStorage = new DataStorage(this, DataStorage.WF_EXCLUSION_STORAGE);
            mWfSettingsStorage  = new DataStorage(this, DataStorage.WF_SETTINGS_STORAGE);
            
            try {

                reinitWebFilterControl();
                                
                CallFilterControlFactory factory = new CallFilterControlFactoryImpl();
                ServiceStateStorage storage = new DataStorage(this, DataStorage.ANTISPAM_SETTINGS_STORAGE);
                mCallFilterControl = factory.create(storage, mAntiSpamProxyFilter, this);
                
            } catch (SdkLicenseViolationException e) {
                mInitFailed = true;
                Log.d(TAG, "license violation");
                onInitFailed("Bad license");
                return INIT_ERROR_CODE;
            }
            Intent serviceIntent = new Intent(this, SdkService.class);
            startService(serviceIntent);
            boolean result = bindService(serviceIntent, this, Context.BIND_AUTO_CREATE);
            if (!result) {
                mInitFailed = true;
                onInitFailed("can't bind SDK Service");    
                return INIT_ERROR_CODE;
            } else {
                mIsInited = true;
            }
            
            ServiceStateStorage appControlStorage = new DataStorage(this, DataStorage.APPCONTROL_SETTINGS_STORAGE);
            mAppControlManager = new AppControlManager(appControlStorage, getApplicationContext());
            mAppControlManager.enableWindowManager(Settings.useAppControlWindowManager(getApplicationContext(), false));
            mAppControlManager.enableAppControl(Settings.isAppControlEnabled(getApplicationContext(), false));
            
        }
        if (mSdkService != null) {
            mSdkService.checkInit(this);
        }

        Log.d(TAG, "Initialization completed.");
        Log.d(TAG, "Installation id: " + KavSdk.getInstallationId());
        Log.d(TAG, "Hardware id: " + KavSdk.getHashOfHardwareId());

        return KavError.KAV_OK;
    }
    
    public boolean isInitialized() {
        return mIsInited && mSdkService != null && mSdkService.isReady();
    }
    
    /**
     * This method checks whether the application has been initialized, initializes the application if needed 
     * and notifies a listener about the operation finish
     *  
     * @param listener {@link ApplicationStateListener} to be invoked when initialization will complete
     * @return whether the application has been initialized or not
     */
    public boolean checkAppInited(final ApplicationStateListener listener) {
        if (!mIsInited) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    mAppReadinessListeners.add(listener);
                    initApplication();
                }
            }).start();
        }
        return mIsInited;
    }
    
    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        SdkService.Binder binder = (SdkService.Binder) service;
        mSdkService = binder.getService();
        if (mIsInited) {
            mSdkService.checkInit(this);
        }
    }
    
    @Override
    public void onServiceDisconnected(ComponentName name) {
        mSdkService = null;
    }
    
    @Override
    public void onInitComplete() {
        if (mIsInited) {
            mHandler.post(new Runnable(){
                @Override
                public void run() {
                    if (Settings.isAntiSpamEnabled(SdkSampleApplication.this, false)) {
                        /*This should be called on UI thread*/
                        int mode = Settings.getAntiSpamMode(SdkSampleApplication.this, FilterModes.BOTH_LISTS_FILTER);
                        mCallFilterControl.setFilteringMode(mode);
                        mCallFilterControl.startFiltering();
                    }
                    
                    for (ApplicationStateListener listener : mAppReadinessListeners) {
                        listener.onApplicationReady();
                    }
                    mAppReadinessListeners.clear();
                }
            });
        }
    }
    
    @Override
    public void onInitFailed(final String errorMessage) {
        Log.e(TAG, errorMessage);
        for (final ApplicationStateListener listener : mAppReadinessListeners) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    listener.onApplicationInitFailed(errorMessage);
                }
            });
        }
        mAppReadinessListeners.clear();
    }
    
    public SecureSmsComponentSample getSecureSmsComponent() {
        return mSecureSmsControl;
    }
    
    public Antivirus getAntivirus() {
        return mAntivirusComponent;
    }
    
    public CallFilterControl getCallFilterControl() {
        return mCallFilterControl;
    }
    
    public SdkService getService() {
        return mSdkService;
    }
    
    public WebFilterControl getWebFilterControl() {
        return mWebFilterControl;
    }
    
    public void setAntiSpamFilter(CustomFilter filter) {
        mAntiSpamProxyFilter.setFilter(filter);
    }

    public SdkLocalStatusObserverSample getSdkLocalStatusObserver() {
        return mSdkLocalStatusObserverSample;
    }

    private void runOnUiThread(Runnable action) {
        mHandler.post(action);
    }
    
    private static class AntiSpamProxyFilter implements CustomFilter {
        
        CustomFilter mUserFilter;
        
        public void setFilter(CustomFilter userFilter) {
            mUserFilter = userFilter;
        }
        
        @Override
        public void onBlackList(CallFilterItem item, CallFilterEvent event, CallFilterEventOrigin origin) {
            if (mUserFilter != null) {
                mUserFilter.onBlackList(item, event, origin);
            }
        }

        @Override
        public void onWhiteList(CallFilterItem item, CallFilterEvent event, CallFilterEventOrigin origin) {
            if (mUserFilter != null) {
                mUserFilter.onWhiteList(item, event, origin);
            }
        }

        @Override
        public int onBothLists(CallFilterEvent event, boolean initialEvent, CallFilterEventOrigin origin) {
            if (mUserFilter != null) {
                return mUserFilter.onBothLists(event, initialEvent, origin);
            }
            return BothListsResult.IGNORE;
        }

        @Override
        public void onEventBlocked(CallFilterEvent event, CallFilterEventOrigin origin) {
            if (mUserFilter != null) {
                mUserFilter.onEventBlocked(event, origin);
            }
        }
    }
}