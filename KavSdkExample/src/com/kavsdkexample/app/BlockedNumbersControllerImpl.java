package com.kavsdkexample.app;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BlockedNumbersControllerImpl implements BlockedNumbersController {
    private final ArrayList<String> mElements = new ArrayList<>();
    
    @Override
    public List<String> getElements() {
        return Collections.unmodifiableList(mElements);
    }

    @Override
    public void add(String element) {
        mElements.add(element);     
    }
}
