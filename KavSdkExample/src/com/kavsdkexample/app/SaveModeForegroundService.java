package com.kavsdkexample.app;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import com.kavsdkexample.R;
import com.kavsdkexample.ui.SdkMainActivity;

public class SaveModeForegroundService extends Service {

    private static final int NOTIFICATION_ID = 7;
    
    private static boolean sStarted;
    
    public static void start(Context context) {
        if (!sStarted) {
            context.startService(new Intent(context, SaveModeForegroundService.class));
            sStarted = true;
        }
    }
    
    public static void stop(Context context) {
        if (sStarted) {
            context.stopService(new Intent(context, SaveModeForegroundService.class));
            sStarted = false;
        }
    }
    
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startForeground(NOTIFICATION_ID, buildNotification());
        return START_STICKY;
    }
    
    private Notification buildNotification() {
        Intent notificationIntent = new Intent(this, SdkMainActivity.class);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        return new android.support.v4.app.NotificationCompat.Builder(this)
            .setSmallIcon(R.drawable.icon)
            .setTicker(getText(R.string.notification_ticker_text))
            .setContentTitle(getText(R.string.power_save_mode_service_notification_title))
            .setContentText(getText(R.string.notification_message))
            .setContentIntent(pendingIntent).build();
    }
    
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
