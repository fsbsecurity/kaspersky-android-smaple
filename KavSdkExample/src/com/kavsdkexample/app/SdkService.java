package com.kavsdkexample.app;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

import com.kavsdk.antivirus.Antivirus;
import com.kavsdk.antivirus.AntivirusInstance;
import com.kavsdk.antivirus.MonitorConstants;
import com.kavsdk.antivirus.MonitorEventListener;
import com.kavsdk.antivirus.MonitorSuspiciousEventListener;
import com.kavsdk.antivirus.MonitoringListener;
import com.kavsdk.antivirus.QuarantineException;
import com.kavsdk.antivirus.SuspiciousThreatType;
import com.kavsdk.antivirus.ThreatInfo;
import com.kavsdk.antivirus.ThreatType;
import com.kavsdk.antivirus.appmonitor.AppInstallationMonitor;
import com.kavsdk.antivirus.appmonitor.AppInstallationMonitorListener;
import com.kavsdk.antivirus.appmonitor.AppInstallationMonitorSuspiciousListener;
import com.kavsdk.license.SdkLicenseViolationException;
import com.kavsdk.simwatch.SimWatch;
import com.kavsdk.simwatch.SimWatchFactory;
import com.kavsdk.simwatch.SimWatchObserver;
import com.kavsdk.simwatch.SimWatchValidator;
import com.kavsdk.simwatch.android.SimWatchFactoryImpl;
import com.kavsdkexample.R;
import com.kavsdkexample.Utils;
import com.kavsdkexample.samples.AntivirusSample;
import com.kavsdkexample.samples.FolderMonitorSample;
import com.kavsdkexample.storage.DataStorage;
import com.kavsdkexample.storage.Settings;
import com.kavsdkexample.ui.SdkMainActivity;
import com.kavsdkexample.ui.device_protection.DetectedApplicationsActivity;

import java.lang.ref.WeakReference;

/**
 * This is a service which initializes antivirus.
 *      In addition, the services is used for decreasing the possibility to be closed 
 *      if the main activity ({@link com.kavsdkexample.ui.SdkMainActivity}) of the sample moves back
 */
public class SdkService extends Service implements 
                    AppInstallationMonitorListener,
                    AppInstallationMonitorSuspiciousListener,
                    SimWatchObserver,
                    MonitoringListener {
    private static final String TAG = SdkService.class.getSimpleName();
    private static final boolean LOGGING = true;

    private static final int ONGOING_NOTIFICATION_ID = 1;

    private static final int FLAG_RTP_MONITOR_ENABLED = 0x0001;
    private static final int FLAG_APP_MONITOR_ENABLED = 0x0002;
    private static final int FLAG_FLD_MONITOR_ENABLED = 0x0004;
    
    private static final String INTENT_EXTRA_KEY = "AppMonitor";

    private InitializationListener mListener;

    private AntivirusSample mAntivirusSample;

    private final Handler mHandler = new Handler();

    /** A flag which indicates whether the service has been initialized */
    private volatile boolean mIsReady;
    
    private AppInstallationMonitor mAppInstallationMonitor;
    private SimWatch mSimWatch;
    private FolderMonitorSample mFolderMonitor;
    private final ServiceMonitorEventListener mMonitorEventListener = new ServiceMonitorEventListener();
    private final ServiceMonitorSuspiciousEventListener mMonitorSuspiciousEventListener = new ServiceMonitorSuspiciousEventListener();

    private int mMonitorEnabledFlags;
    private boolean mIsForeground;

    @Override
    public void onCreate() {
        super.onCreate();
        if (((SdkSampleApplication)getApplication()).isInitialized()) {
            init();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        /*
         * SDK handles restarting by itself.
         */
        return START_NOT_STICKY;
    }

    private void startForeground() {
        Intent notificationIntent = new Intent(this, SdkMainActivity.class);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        Notification notification = new android.support.v4.app.NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.icon)
                .setTicker(getText(R.string.notification_ticker_text))
                .setContentTitle(getText(R.string.notification_title))
                .setContentText(getText(R.string.notification_message))
                .setContentIntent(pendingIntent).build();

        startForeground(ONGOING_NOTIFICATION_ID, notification);
        mIsForeground = true;
    }

    private void stopForeground() {
        stopForeground(true);
        mIsForeground = false;
    }

    private void addMonitorFlags(final int flags) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mMonitorEnabledFlags |= flags;
                updateForeground();
            }
        });
    }

    private void removeMonitorFlags(final int flags) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mMonitorEnabledFlags &= ~flags;
                updateForeground();
            }
        });
    }

    private void updateForeground() {
        boolean foreground = mMonitorEnabledFlags != 0;
        if (mIsForeground != foreground) {
            if (foreground) {
                startForeground();
            } else {
                stopForeground();
            }
        }
    }

    public boolean isReady() {
        return mIsReady;
    }

    public synchronized void checkInit(InitializationListener listener) {
        mListener = listener;
        if (mIsReady) {
            listener.onInitComplete();
        } else {
            init();
        }
    }

    /**
     * This method initializes essential components
     * Note: must be invoked after antivirus component has been initialized
     */
    private void init() {
        initAVParameters();
        try {
            initSimWatch();
            mFolderMonitor = new FolderMonitorSample(this);
            mFolderMonitor.loadMonitors();
        } catch (SdkLicenseViolationException e) {
            e.printStackTrace();
        }
        mIsReady = true;
        if (mListener != null) {
            mListener.onInitComplete();
        }
    }

    public AntivirusSample getAntivirusSample() {
        return mAntivirusSample;
    }

    public FolderMonitorSample getSampleFolderMonitor() {
        return mFolderMonitor;
    }

    public void initSimWatch() throws SdkLicenseViolationException {
        if (Settings.isSimWatchEnabled(this, false)) {
            SimWatchFactory swf = new SimWatchFactoryImpl(new DataStorage(this, DataStorage.SIM_WATCH_SETTINGS_STORAGE), this);
            mSimWatch = swf.createSimWatchForSafeEnvironment(this);
            mSimWatch.start();
        }
    }

    /**
     * This method sets up or updates the antivirus parameters
     */
    public void initAVParameters() {
        //init file monitor
        //The method always returns the same kav component object
        Antivirus kavComponent = AntivirusInstance.getInstance();
        kavComponent.setMonitoringListener(this);
        mAntivirusSample = new AntivirusSample(this, kavComponent);

        kavComponent.setMonitorListener(mMonitorEventListener);
        kavComponent.setMonitorSuspiciousListener(mMonitorSuspiciousEventListener);

        boolean monitorEnabled = Settings.isMonitorEnabled(this, kavComponent.isMonitorActive());
        if (monitorEnabled != kavComponent.isMonitorActive()) {
            enableRtpMonitor(null, monitorEnabled);
        }

        int monitorScanMode = Settings.getMonitorScanMode(this, kavComponent.getMonitorScanMode());
        if (monitorScanMode != kavComponent.getMonitorScanMode()) {
            kavComponent.setMonitorScanMode(monitorScanMode);
        }

        kavComponent.setMonitorCleanMode(MonitorConstants.LOG_ONLY);

        // init APK installation monitor
        mAppInstallationMonitor = new AppInstallationMonitor(getApplicationContext());
        boolean apkInstallationCheckEnabled = Settings.isApkInstallationEnabled(this, false);
        if (apkInstallationCheckEnabled) {
            updateApkMonitorState(true);
        }

    }

    /**
     * Turn on/turn off the apk monitor.
     *
     * @param enabled a new state of the the apk monitor. If enabled is true and monitor is
     * running, it will be restarted.
     */
    public void updateApkMonitorState(boolean enabled) {

        if (enabled) {
            if (LOGGING) {
                Log.d(TAG, "Enable application install monitor");
            }

            mAppInstallationMonitor.disable();

            boolean enableUds = Settings.isAppMonitorUdsEnabled(this, true);
            boolean deepScan = Settings.isAppMonitorDeepScanEnabled(this, false);
            boolean scanMissedApps = Settings.isAppMonitorScanMissedEnabled(this, false);

            if (!scanMissedApps) {
                mAppInstallationMonitor.resetMissedPackagesList();
            }

            mAppInstallationMonitor.setScanUdsAllow(enableUds);
            mAppInstallationMonitor.setDeepScan(deepScan);

            mAppInstallationMonitor.enable(this, this);

        } else {
            if (LOGGING) {
                Log.d(TAG, "Disable application install monitor");
            }
            mAppInstallationMonitor.disable();
        }
    }

    public AppInstallationMonitor getAppInstallationMonitor() {
        if (isReady()) {
            return mAppInstallationMonitor;
        }
        throw new IllegalStateException("SdkService not ready!");
    }

    /**
     * Turn on/off real time protection file monitor
     *
     * @param v if the method was invoked from GUI (e.g. by pressing a button),
     *          disable the control while the monitor state is changing
     * @param enable a new state of the monitor
     */
    public void enableRtpMonitor(final View v, final boolean enable) {
        final WeakReference<View> viewRef = new WeakReference<>(v);
        if (v != null) {
            v.setEnabled(false);
        }
        new Thread() {
            public void run() {
                final View view = viewRef.get();
                try {
                    AntivirusInstance.getInstance().setMonitorStateSync(enable);
                    processSettingMonitorStateResult(false, view);
                    if (LOGGING) {
                        Log.d(TAG, "Change RTP monitor state changed ");
                    }
                } catch (SdkLicenseViolationException e) {
                    processSettingMonitorStateResult(true, view);
                }
            }
        }.start();
    }

    public void setSimWatchEnabled(boolean enabled) throws SdkLicenseViolationException {

        boolean currState = Settings.isSimWatchEnabled(this, false);
        if (enabled == currState) {
            return;
        }

        Settings.setSimWatchEnabled(this, enabled);
        if (enabled) {
            initSimWatch();
        } else {
            if (mSimWatch != null) {
                mSimWatch.stop();
                mSimWatch = null;
            }
        }
    }

    private void processSettingMonitorStateResult(final boolean licenseProblem, final View view) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (licenseProblem) {
                    Utils.showToast(getApplicationContext(), R.string.str_license_expired_error);
                }
                if (view != null) {
                    view.setEnabled(true);
                    if (licenseProblem && view instanceof CheckBox) {
                        ((CheckBox) view).setChecked(false);
                    }
                }
            }
        });
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new Binder(this);
    }

    /**
     * The final cleanup of the service
     * Note: Do not forget to unregister receivers here if needed
     */
    @Override
    public void onDestroy() {
        if (mIsReady) {
            mSimWatch.stop();
            mAppInstallationMonitor.disable();
            mFolderMonitor.unloadMonitors();
        }
        super.onDestroy();
    }

    @Override
    public boolean onVirusDetected(ThreatInfo threat, ThreatType threatType) {

        if (LOGGING) {
            Log.d(TAG, "AppInstallationMonitor: detected virus '" + threat.getVirusName() + "' in '"
                    + threat.getPackageName() + "'");
        }

        mAntivirusSample.addToAppDetectedListFromAppMonitor(threat);

        Intent intent = new Intent(getApplicationContext(), DetectedApplicationsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(INTENT_EXTRA_KEY, true);
        startActivity(intent);

        //scan of this package should stop
        return true;
    }

    @Override
    public void onSuspiciousDetected(ThreatInfo threat, SuspiciousThreatType threatType) {
        if (LOGGING) {
            Log.d(TAG, "AppInstallationMonitor: detected suspicious '" + threat.getVirusName() + "' in '"
                    + threat.getPackageName() + "'");
        }
    }

    @Override
    public void validateSimChange(SimWatchValidator validator) {
        if (LOGGING) {
            Log.e(TAG, "SIM card changed!");
        }
        validator.validateSimChange();
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(SdkService.this, getString(R.string.str_antivirus_sim_changed), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onMonitorStart(int monitorId) {
        switch (monitorId) {
            case RTP_MONITOR_ID:
                addMonitorFlags(FLAG_RTP_MONITOR_ENABLED);
                break;
            case APPLICATION_MONITOR_ID:
                addMonitorFlags(FLAG_APP_MONITOR_ENABLED);
                break;
            case FOLDER_MONITOR_ID:
                addMonitorFlags(FLAG_FLD_MONITOR_ENABLED);
                break;
            default:
                break;
        }
    }

    @Override
    public void onMonitorStop(int monitorId) {
        switch (monitorId) {
            case RTP_MONITOR_ID:
                removeMonitorFlags(FLAG_RTP_MONITOR_ENABLED);
                break;
            case APPLICATION_MONITOR_ID:
                removeMonitorFlags(FLAG_APP_MONITOR_ENABLED);
                break;
            case FOLDER_MONITOR_ID:
                removeMonitorFlags(FLAG_FLD_MONITOR_ENABLED);
                break;
            default:
                break;
        }
    }

    /********************************Inner classes************************************************/
    public class Binder extends android.os.Binder implements ServiceProvider {
        private final WeakReference<SdkService> mService;
        
        Binder(SdkService self) {
            mService = new WeakReference<>(self);
        }
        
        public SdkService getService() {
            return mService.get();
        }
    }
    
    /**
     * This is an interface to notify about an initialization result
     */
    public interface InitializationListener {
        void onInitComplete();
        void onInitFailed(String errorMessage);
    }
    
    public interface ScannerListener {
        void onEvent(int eventId, int result, ThreatInfo threat, ThreatType threatType);
        void onSuspiciousEvent(ThreatInfo threat, SuspiciousThreatType threatType);
    }

    private class ServiceMonitorEventListener implements MonitorEventListener {

        @Override
        public void onMonitorEvent(ThreatInfo threatInfo, ThreatType threatType) {
            final String threatPath = threatInfo.getFileFullPath();
            final String objectName = threatInfo.getObjectName();
            
            Log.d(TAG, "Monitor event: object = " + objectName + ", path = " + threatPath + ", virus = " + threatInfo.getVirusName());
            if (threatType != null && threatType != ThreatType.None) {
                final int action = Settings.getMonitorAction(SdkService.this);
                final Antivirus antivirus = AntivirusInstance.getInstance();
                switch (action) {
                    case Settings.DELETE_THREAT_ACTION:
                        if (antivirus.removeThreat(threatInfo)) {
                            Log.d(TAG, "Monitor event: path = " + threatPath + " was removed");
                        } else {
                            Log.d(TAG, "Monitor event: path = " + threatPath + " was not removed");
                        }
                        break;
                    case Settings.QUARANTINE_THREAT_ACTION:
                        try {
                            antivirus.addToQuarantine(threatInfo);
                            boolean removed = antivirus.removeThreat(threatInfo);
                            if (removed) {
                                Log.d(TAG, "Monitor event: path = " + threatPath + " moved to quarantine");
                            } else {
                                Log.d(TAG, "Monitor event: path = " + threatPath + " copied to quarantine, couldn't delete");
                            }
                        } catch (QuarantineException e) {
                            Log.d(TAG, "Monitor event: path = " + threatPath + " quarantine exception: " + e.getMessage());
                        }
                        break;
                    case Settings.SKIP_THREAT_ACTION:
                        Log.d(TAG, "Monitor event: path = " + threatPath + " was skipped");
                        break;
                    default:
                        break;
                }
            }
        }
        
    }
    
    private class ServiceMonitorSuspiciousEventListener implements MonitorSuspiciousEventListener {
        
        @Override
        public void onMonitorEvent(ThreatInfo threatInfo,
                SuspiciousThreatType threatType) {
            
            
            final String threatPath = threatInfo.getFileFullPath();
            final String objectName = threatInfo.getObjectName();
            
            Log.d(TAG, "Monitor suspicious event: object = " + objectName + ", path = " + threatPath + ", virus = " + threatInfo.getVirusName());           
        }
    }

    public interface ServiceProvider {
        SdkService getService();
    }

}