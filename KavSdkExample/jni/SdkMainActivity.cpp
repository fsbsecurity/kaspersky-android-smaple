#include <jni.h>
#include <cstdio>

#ifdef __cplusplus
extern "C" {
#endif

static int* sPtr = 0;

JNIEXPORT void JNICALL Java_com_kavsdkexample_Crasher_invokeNativeCrash(JNIEnv *, jclass)
{
	printf("test %d", (*sPtr));

	/*
	int x = *((int*)0);
	int* x = new int;
	delete x;
	delete x;
	*/
}

#ifdef __cplusplus
}
#endif
