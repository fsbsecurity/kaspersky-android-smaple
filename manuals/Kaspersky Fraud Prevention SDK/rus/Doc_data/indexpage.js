﻿var IndexPage = [
{ "class": "indexheading", "name": "A1040", "text": "А" },
{ "class": "index1", "text": "Анти-Спам" },
{ "class": "indexlink", "href": "10372460.htm", "text": "CallFilterControl (com.kavsdk.antispam)", nested: true  },
{ "class": "indexlink", "href": "10372223.htm", "text": "О компоненте Защита в интернете и локальных сетях", nested: true  },
{ "class": "indexlink", "href": "84724.htm", "text": "АО «Лаборатория Касперского»" },

{ "class": "indexheading", "name": "A1041", "text": "Б" },
{ "class": "index1", "text": "Безопасное соединение" },
{ "class": "indexlink", "href": "10371613.htm", "text": "DnsChecker (com.kavsdk.dnschecker)", nested: true  },
{ "class": "indexlink", "href": "10371713.htm", "text": "SecureHttpClient (com.kavsdk.secureconnection)", nested: true  },
{ "class": "indexlink", "href": "10371714.htm", "text": "SecureURL (com.kavsdk.secureconnection)", nested: true  },
{ "class": "indexlink", "href": "128341.htm", "text": "Обеспечение безопасного соединения по протоколу HTTP", nested: true  },
{ "class": "index1", "text": "Безопасный ввод данных" },
{ "class": "indexlink", "href": "10372557.htm", "text": "SafeEditText (com.kavsdk.secureinput.widget)", nested: true  },
{ "class": "indexlink", "href": "128345.htm", "text": "Защита ввода данных от кейлоггеров", nested: true  },

{ "class": "indexheading", "name": "A1042", "text": "В" },
{ "class": "index1", "text": "Веб-Антивирус" },
{ "class": "indexlink", "href": "10373462.htm", "text": "scanUrl (с параметрами HTTP-клиента)", nested: true  },
{ "class": "indexlink", "href": "10372394.htm", "text": "scanUrl", nested: true  },
{ "class": "indexlink", "href": "10372223.htm", "text": "О компоненте Защита в интернете и локальных сетях", nested: true  },
{ "class": "indexlink", "href": "10373915.htm", "text": "Предоставляемые данные: Защита в интернете и локальной сети", nested: true  },
{ "class": "index1", "text": "Веб-Фильтр" },
{ "class": "indexlink", "href": "10371756.htm", "text": "WebFilterControl (com.kavsdk.webfilter)", nested: true  },
{ "class": "indexlink", "href": "128344.htm", "text": "Предотвращение фишинговых атак с помощью Веб-Фильтра", nested: true  },

{ "class": "indexheading", "name": "A1047", "text": "З" },
{ "class": "index1", "text": "Защищенное хранилище" },
{ "class": "indexlink", "href": "10372335.htm", "text": "com.kavsdk.securestorage.database", nested: true  },
{ "class": "indexlink", "href": "10372038.htm", "text": "CryptoFileInputStream (com.kavsdk.securestorage.file)", nested: true  },
{ "class": "indexlink", "href": "10372047.htm", "text": "CryptoFileOutputStream (com.kavsdk.securestorage.file)", nested: true  },
{ "class": "indexlink", "href": "128347.htm", "text": "Шифрование конфиденциальных данных в базе данных", nested: true  },
{ "class": "indexlink", "href": "128348.htm", "text": "Шифрование конфиденциальных данных в файле", nested: true  },

{ "class": "indexheading", "name": "A1054", "text": "О" },
{ "class": "index1", "text": "Обновление баз компонентов" },
{ "class": "indexlink", "href": "10371717.htm", "text": "Updater (com.kavsdk.updater)", nested: true  },
{ "class": "indexlink", "href": "10371791.htm", "text": "UpdaterConstants (com.kavsdk.updater)", nested: true  },
{ "class": "indexlink", "href": "10372283.htm", "text": "Настройка обновления баз", nested: true  },
{ "class": "indexlink", "href": "129280.htm", "text": "Обнаружение вредоносных приложений с помощью Проверки по требованию", nested: true  },

{ "class": "indexheading", "name": "A1055", "text": "П" },
{ "class": "index1", "text": "Проверка по требованию" },
{ "class": "indexlink", "href": "10374534.htm", "text": "MultithreadedScanner (com.kavsdk.antivirus.multithread)", nested: true  },
{ "class": "indexlink", "href": "10371708.htm", "text": "Scanner (com.kavsdk.antivirus)", nested: true  },
{ "class": "indexlink", "href": "129280.htm", "text": "Обнаружение вредоносных приложений с помощью Проверки по требованию", nested: true  },

{ "class": "indexheading", "name": "A1060", "text": "Ф" },
{ "class": "index1", "text": "Файловый антивирусный монитор." },
{ "class": "indexlink", "href": "10371722.htm", "text": "Antivirus (com.kavsdk.antivirus)", nested: true  },
{ "class": "indexlink", "href": "10372431.htm", "text": "AppInstallationMonitor", nested: true  },
{ "class": "indexlink", "href": "10373407.htm", "text": "FolderMonitor (com.kavsdk.antivirus.foldermonitor)", nested: true  },
{ "class": "indexlink", "href": "129285.htm", "text": "Предотвращение установки вредоносных приложений с помощью Файлового антивирусного монитора", nested: true  }
];
