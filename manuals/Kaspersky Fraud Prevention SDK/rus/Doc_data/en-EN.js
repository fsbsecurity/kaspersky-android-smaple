﻿var Localization = {

    "HeaderTitle": "Kaspersky Online Help",
    "HeaderLogoLink": "/",

    "LanguageLabel": "Language:",
    "HomeButton": "Home",
    "ContentsTab": "Contents",
    "IndexTab": "Index",

    "PrintButtonText": "Print",
    "SupportButtonText": "Support",
    "SupportButtonLink": "http://support.kaspersky.com/",
    "FeedbackButtonText": "Feedback",
    "FeedbackButtonLink": "http://support.kaspersky.com/docloc",

    "SearchLabel": "Search",
    "FoundTextBegin": "",
    "FoundTextEnd": "matches found",
    "NothingFoundText": "Nothing found",
    "SearchPlaceholder": "Search our helps",

    "FooterLogoLink": "http://www.kaspersky.com",
    "FooterLogoTitle": "",

    "FooterCopyrightText": "© 2016 AO Kaspersky Lab. All Rights Reserved.",

    // Old format. Remove next 6 lines after migration to AIConverter 1.1
    "FooterOnlineHelpText": "Kaspersky Online Help",
    "FooterOnlineHelpLink": "https://help.kaspersky.com",
    "FooterPrivacyPolicyText": "Privacy Policy",
    "FooterPrivacyPolicyLink": "http://www.kaspersky.com/privacy",
    "FooterLegalText": "Legal",
    "FooterLegalLink": "http://www.kaspersky.com/about/contactinfo/legal",

    "FooterLinks": [
        {
            "label": "Kaspersky Online Help",
            "url": "https://help.kaspersky.com"
        },
        {
            "label": "Privacy Policy",
            "url": "http://www.kaspersky.com/privacy"
        },
        {
            "label": "Legal",
            "url": "http://www.kaspersky.com/about/contactinfo/legal"
        }
    ],

    "TwitterLink": "http://twitter.com/Kaspersky",
    "FacebookLink": "http://facebook.com/Kaspersky",
    "YouTubeLink": "http://youtube.com/Kaspersky",
    "GoogleLink": "https://plus.google.com/104178448349952735431?prsrc=3",

    "MainPageSubheaderHome": "Help for Kaspersky Lab products",
    "MainPageSubheaderB2C": "For home",
    "MainPageSubheaderB2B": "For business",
    "MainPageBackHomeLink": "Back home",
    "MainPageMoreProducts": "More products",

    "isRTL": "false",

    "meta": {
        "title": "Kaspersky Lab | Antivirus Protection &amp; Internet Security Software",
        "description": "Antivirus and Internet Security software for home or business. The world's fastest antivirus updates. Kaspersky Lab. Free virus scan and antivirus trial downloads.",
        "keywords": "antivirus, Kaspersky Lab"
    },

    "products_b2c": [
/* Kaspersky Internet Security for Android */
    {
            "icon": "kisa",
            "brand": "Kaspersky",
            "name": "Internet Security for Android",
            "url": "KISA",
            "mainPageOrder": "",
            "versions": [
                {
                    "label": "MR11",
                    "url": "MR11/en-EN"
                }
            ]
        },
/* Kaspersky Safe Kids Mobile*/
    {
            "icon": "safe-kids",
            "brand": "Kaspersky",
            "name": "Safe Kids Android/iOS",
            "url": "KSKMob",
            "mainPageOrder": "5",
            "versions": [
                {
                    "label": " ",
                    "url": "1.0/en-EN"
                }
            ]
        },
/* Kaspersky Safe Kids Mac*/
    {
            "icon": "safe-kids",
            "brand": "Kaspersky",
            "name": "Safe Kids for Mac",
            "url": "KSKMac",
            "mainPageOrder": " ",
            "versions": [
                {
                    "label": " ",
                    "url": "1.0MR1/en.lroj/pgs"
                }
            ]
        },
/* Kaspersky QR Scanner Android */
        {
            "icon": "qr-scanner",
            "brand": "Kaspersky",
            "name": "QR Scanner Android",
            "url": "QRSAndroid",
            "mainPageOrder": " ",
            "versions": [
                {
                    "label": " ",
                    "url": "1.1/en-US"
                }
            ]
        },
/* Kaspersky QR Scanner iOS */
        {
            "icon": "qr-scanner",
            "brand": "Kaspersky",
            "name": "QR Scanner iOS",
            "url": "QRSiOS",
            "mainPageOrder": " ",
            "versions": [
                {
                    "label": " ",
                    "url": "1.1/en-US"
                }
            ]
        },
/* Kaspersky Internet Security for Mac */
        {
            "icon": "kis-mac",
            "brand": "Kaspersky",
            "name": "Internet Security for Mac",
            "url": "KIS4Mac",
            "mainPageOrder": "6",
            "versions": [
                {
                    "label": "16",
                    "url": "16.0/en.lproj/pgs"
                }
            ]
        },
/* Kaspersky Security Scan */
        {
            "icon": "kss",
            "brand": "Kaspersky",
            "name": "Security Scan",
            "url": "KSS",
            "mainPageOrder": "2",
            "versions": [
                {
                    "label": " ",
                    "url": "4.0/en-EN"
                }
            ]
        },
/* Kaspersky Software Updater */
        {
            "icon": "ksu",
            "brand": "Kaspersky",
            "name": "Software Updater",
            "url": "KSU",
            "mainPageOrder": "",
            "versions": [
                {
                    "label": " ",
                    "url": "2.0/en-EN"
                }
            ]
        },
/* My Kaspersky */
        {
            "icon": "kts-md",
            "brand": "",
            "name": "My Kaspersky",
            "url": "KPC",
            "mainPageOrder": "1",
            "versions": [
                {
                    "label": " ",
                    "url": "1.0/en-US"
                }
            ]
        },
/* Kaspersky Free */
        {
            "icon": "free-tools",
            "brand": "Kaspersky",
            "name": "Free",
            "url": " ",
            "mainPageOrder": " ",
            "versions": [
                {
                    "label": " ",
                    "url": " "
                },
                {
                    "label": " ",
                    "url": " "
                }
            ]
        },
/* Kaspersky Anti-Virus */
        {
            "icon": "kav",
            "brand": "Kaspersky",
            "name": "Anti-Virus",
            "url": "KAV",
            "mainPageOrder": " ",
            "versions": [
                {
                    "label": "2017 Beta",
                    "url": "2017/en-EN"
                },
                {
                    "label": "2016",
                    "url": "2016/en-EN"
                }
            ]
        },
/* Kaspersky Internet Security */
        {
            "icon": "kis-md",
            "brand": "Kaspersky",
            "name": "Internet Security",
            "url": "KIS",
            "mainPageOrder": " ",
            "versions": [
                {
                    "label": "2017 Beta",
                    "url": "2017/en-EN"
                },
                {
                    "label": "2016",
                    "url": "2016/en-EN"
                }
            ]
        },
/* Kaspersky Total Security */
        {
            "icon": "kts-md",
            "brand": "Kaspersky",
            "name": "Total Security",
            "url": "KTS",
            "mainPageOrder": " ",
            "versions": [
                {
                    "label": "2017 Beta",
                    "url": "2017/en-EN"
                },
                {
                    "label": "2016",
                    "url": "2016/en-EN"
                }
            ]
        },
/* Kaspersky Secure Connection (VPN) */
        {
            "icon": "ksos",
            "brand": "Kaspersky",
            "name": "Secure Connection",
            "url": "VPNWin",
            "mainPageOrder": " ",
            "versions": [
                {
                    "label": " ",
                    "url": "1.0/en-EN"
                }
            ]
        },
/* Kaspersky Fraud Prevention for Endpoints (for Microsoft Windows) */
        {
            "icon": "kfp",
            "brand": "Kaspersky",
            "name": "Fraud Prevention for Endpoints (for Microsoft Windows)",
            "url": "KFPWin",
            "mainPageOrder": " ",
            "versions": [
                {
                    "label": "6.0",
                    "url": "6.0/en-EN"
                },
                {
                    "label": "5.0",
                    "url": "5.0/en-EN"
                }
            ]
        }
    ],

    "products_b2b": [
/* Kaspersky Endpoint Security Cloud */
        {
            "icon": "kes",
            "brand": "Kaspersky",
            "name": "Endpoint Security Cloud (Beta)",
            "url": "Cloud",
            "mainPageOrder": " ",
            "versions": [
                {
                    "label": " ",
                    "url": "1.0/en-EN"
                }
            ]
        },
/* Kaspersky Endpoint Security for Windows */
        {
            "icon": "kes",
            "brand": "Kaspersky",
            "name": "Endpoint Security for Windows",
            "url": "KESWin",
            "mainPageOrder": " ",
            "versions": [
                {
                    "label": "10",
                    "url": "10SP2/en-EN"
                }
            ]
        },
/* Kaspersky Security for Microsoft Exchange Servers */
        {
            "icon": "ks-mail-server",
            "brand": "Kaspersky",
            "name": "Security for Microsoft Exchange Servers",
            "url": "KS4Exchange",
            "mainPageOrder": "3",
            "versions": [
                {
                    "label": "9.0 MR3",
                    "url": "9.3/en-US/"
                },
                {
                    "label": "9.0 MR2",
                    "url": "9.2/en-EN/"
                }
            ]
        },
/* Kaspersky Security for SharePoint Server */
        {
            "icon": "ks-collab",
            "brand": "Kaspersky",
            "name": "Security for SharePoint Server",
            "url": "KS4Sharepoint",
            "mainPageOrder": "4",
            "versions": [
                {
                    "label": "9.0 MR3",
                    "url": "9.3/en-US/"
                },
                {
                    "label": "9.0 MR2",
                    "url": "9.2/en-EN/"
                }
            ]
        },
/* Kaspersky Security Mail Gateway */
        {
            "icon": "ks-mail-server",
            "brand": "Kaspersky",
            "name": "Secure Mail Gateway",
            "url": "KSMG",
            "mainPageOrder": " ",
            "versions": [
                {
                    "label": " ",
                    "url": "1.0/en-EN/"
                }
            ]
        },
/* Kaspersky Small Office Security */
        {
            "icon": "ksos",
            "brand": "Kaspersky",
            "name": "Small Office Security",
            "url": "KSOS",
            "mainPageOrder": " ",
            "versions": [
                {
                    "label": "5 Beta",
                    "url": "5.0/en-EN"
                }
            ]
        },
/* Kaspersky Small Office Security Management Console */
        {
            "icon": "ksos",
            "brand": "Kaspersky",
            "name": "Small Office Security Management Console",
            "url": "KSOSMC",
            "mainPageOrder": "7",
            "versions": [
                {
                    "label": " ",
                    "url": "1.0/en-US/"
                }
            ]
        },
/* Kaspersky Security for Virtualization Light Agent */
        {
            "icon": "ks-virtual",
            "brand": "Kaspersky",
            "name": "Security for Virtualization Light Agent",
            "url": "KSVLA",
            "mainPageOrder": " ",
            "versions": [
                {
                    "label": "4.0",
                    "url": "4.0/en-EN/"
                }
            ]
        },
/* Kaspersky Anti-Virus for Lotus Domino */
        {
            "icon": "ks-mail-server",
            "brand": "Kaspersky",
            "name": "Anti-Virus for Lotus Domino",
            "url": "Lotus",
            "mainPageOrder": " ",
            "versions": [
                {
                    "label": "8.0",
                    "url": "8.0/en-EN/"
                }
            ]
        },
/* Kaspersky Feed Service for Splunk */
        {
            "icon": "pro-services",
            "brand": "Kaspersky",
            "name": "Feed Service for Splunk",
            "url": "KFS",
            "mainPageOrder": " ",
            "versions": [
                {
                    "label": "",
                    "url": "1.0/en-EN/"
                }
            ]
        }

    ]

};
