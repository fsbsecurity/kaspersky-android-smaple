
Prerequesties:
- Android SDK 2.3 including "SDK Platform Android 2.3, API 9", "Android SDK Tools, revision 9", "Android SDK Platform-tools".
It is recomended that you use Eclipse with Android Development Toolkit installed (https://dl-ssl.google.com/android/eclipse/)

Creating new project:
New project should be created using File->New->"New Android Project".
Build Target should be Android 2.3 - API level 9 or greater.
Possible values of "min sdk version" are from 9 to 17.

Adding SDK java package to your project:
- "In Package Explorer" right mouse click on project->Properties;
- Select Java Build Path menu item;
- Select Libraries tab;
- Select "Add JARs..." and select KavSdk.jar from SDK.
Note: you should use only documented classes (see SDK documentation).

Adding Javadoc to your project:
- Expand "Referenced libraries" in "Package Explorer";
- Right mouse click on "KavSdk.jar";
- Select "Properties";
- Select "Javadoc location" and specify path to attached "doc" folder from SDK package.

Adding native library from SDK to your project:
- copy libs folder from sdk archive to you project folder
- in Eclipse select right click on your project in Project Explorer view and press Refresh

In order to operate properly, SDK requires following content in the android application manifest:
        <service android:name="com.kavsdk.SdkService" />
		  
		<activity
            android:name="com.kavsdk.shared.cellmon.FakeActivity"
            android:theme="@android:style/Theme.Dialog" >
        </activity>

        <receiver android:name="com.kavsdk.shared.cellmon.SMSReceiver" >
			<intent-filter android:priority="2147483647" >
                <action android:name="android.provider.Telephony.SMS_RECEIVED" />
                <action	android:name="android.provider.Telephony.GSM_SMS_RECEIVED" />
            </intent-filter>
             <intent-filter android:priority="2147483647" >
				<action android:name="android.intent.action.DATA_SMS_RECEIVED" />
				<data android:scheme="sms" android:host="localhost" />
			</intent-filter> 	
        </receiver>

		 <receiver
            android:name="com.kavsdk.StartReceiver"
            android:exported="true" >
            <intent-filter>
                <action
                    android:name="com.avsdk.READY"
                    android:exported="true" >
				<action android:name="com.avsdk.LAUNCH" android:enabled="true" android:exported="true" />
                </action>
            </intent-filter>
            <intent-filter>
                <action android:name="android.intent.action.BOOT_COMPLETED" >
                </action>
                <category android:name="android.intent.category.DEFAULT" >
                </category>
            </intent-filter>        
            <intent-filter>
                <action android:name="android.intent.action.QUICKBOOT_POWERON" >
                </action>
            </intent-filter>
            <intent-filter>
                <action android:name="android.intent.action.PACKAGE_REPLACED" >
                </action>
            </intent-filter>
            <intent-filter >
            	<action android:name="android.intent.action.MY_PACKAGE_REPLACED" >
                </action>
            </intent-filter>
        </receiver>
		
		<receiver android:name="com.kavsdk.AppInstallationReceiver">
            <intent-filter>
                <action android:name="android.intent.action.PACKAGE_ADDED"/>
                <data android:scheme="package" />
            </intent-filter>
        </receiver>
        
        <receiver android:name="com.kavsdk.AlarmReceiver"/>
		
And the following permissions
 	<uses-permission android:name="android.permission.WAKE_LOCK" > <!-- used for receiving location updates -->
    </uses-permission>
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" > <!-- used for receiving location updates -->
    </uses-permission>
    <uses-permission android:name="android.permission.INTERNET" > <!-- used for database updates -->
    </uses-permission>
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" > <!-- used for working with SD card -->
    </uses-permission>
    <uses-permission android:name="android.permission.READ_SMS" >
    </uses-permission>
    <uses-permission android:name="android.permission.WRITE_SMS" >
    </uses-permission>
    <uses-permission android:name="android.permission.RECEIVE_SMS" >
    </uses-permission>
    <uses-permission android:name="android.permission.READ_CONTACTS" >
    </uses-permission>
    <uses-permission android:name="android.permission.CALL_PHONE" >
    </uses-permission>
    <uses-permission android:name="android.permission.KILL_BACKGROUND_PROCESSES" >
    </uses-permission>
    <uses-permission android:name="android.permission.READ_PHONE_STATE">
    </uses-permission>
    <uses-permission android:name="android.permission.READ_LOGS"> <!-- used by web filter -->
    </uses-permission>
    <uses-permission android:name="com.android.browser.permission.READ_HISTORY_BOOKMARKS"> <!-- used by web filter -->
    </uses-permission>
    <uses-permission android:name="com.android.browser.permission.WRITE_HISTORY_BOOKMARKS"> <!-- used by web filter -->
    </uses-permission>
    <uses-permission android:name="android.permission.WRITE_APN_SETTINGS" > <!-- used by web filter -->
    </uses-permission>
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" > <!-- used by web filter -->
    </uses-permission>
    <uses-permission android:name="android.permission.ACCESS_WIFI_STATE" > <!-- used by web filter -->
    </uses-permission>
    <uses-permission android:name="android.permission.CHANGE_WIFI_STATE" > <!-- used by web filter -->
    </uses-permission>
    <uses-permission android:name="android.permission.WRITE_SETTINGS" >
    </uses-permission>
    <uses-permission android:name="android.permission.GET_TASKS"> <!-- used by task reputation manager -->
   	</uses-permission> 
	<uses-permission android:name="android.permission.VIBRATE" > <!-- used by SecureKeyboard -->
    </uses-permission>

File system AV monitor operates in the background thread of native code, so you must ensure application is not closed by system, e.g. you can start a service or restart application as soon as possible after it has been killed.

File called "avbases.avc" should be placed in res\raw folder of your project.
